<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting', function (Blueprint $table) {
            $table->increments('id');
            $table->string('meeting_name', 100);
            $table->string('agenda', 100);
            $table->date('date');
            $table->time('start_time');
            $table->time('end_time');
            $table->unsignedInteger('board_id')->nullable();
            $table->string('meeting_place', 200);
            $table->text('information')->nullable();
            $table->date('acceptance_end_date');
            $table->time('acceptance_end_time');
            $table->unsignedInteger('meeting_organization_id')->nullable();
            $table->string('meeting_organization_name', 200)->nullable();
            $table->unsignedInteger('voter_id')->nullable();
            $table->boolean('is_propose')->default(0);
            $table->boolean('is_migrate')->default(0);
            $table->boolean('is_enabled')->default(1);
            $table->boolean('is_deleted')->default(0);
            $table->unsignedInteger('created_by')->default(0);
            $table->unsignedInteger('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('meeting_organization_id')->references('id')->on('organizations')->onDelete('set null');
            $table->foreign('board_id')->references('id')->on('boards')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting');
    }
}
