<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProponentTopicFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proponent_topic_file', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('proponent_topic_id')->index();
            $table->unsignedInteger('file_id')->index();
            $table->boolean('is_migrate')->default(0);
            $table->boolean('is_enabled')->default(1);
            $table->unsignedInteger('created_by')->default(0);
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();

             //FOREIGN KEY CONSTRAINTS
             $table->foreign('proponent_topic_id')->references('id')->on('proponent_topic')->onDelete('cascade');
             $table->foreign('file_id')->references('id')->on('file')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proponent_topic_file');
    }
}
