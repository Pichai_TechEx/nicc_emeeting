<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendanceMeetingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendance_meeting', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('meeting_id')->index();
            $table->unsignedInteger('attendance_id')->nullable();
            $table->unsignedInteger('title_id')->nullable();
            $table->unsignedInteger('organization_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('position_in_meeting_id')->nullable();
            $table->unsignedInteger('status_id')->nullable();
            $table->string('title_name', 100);
            $table->string('organization_name', 100);
            $table->string('position_in_meeting_name', 100);
            $table->string('first_name', 100)->nullable();
            $table->string('last_name', 100)->nullable();
            $table->string('position', 100)->nullable();
            $table->string('other', 100)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('phone', 20)->nullable();
            $table->boolean('is_migrate')->default(0);
            $table->boolean('is_enabled')->default(1);
            $table->unsignedInteger('created_by')->default(0);
            $table->unsignedInteger('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();

             //FOREIGN KEY CONSTRAINTS
             $table->foreign('meeting_id')->references('id')->on('meeting')->onDelete('cascade');
             $table->foreign('attendance_id')->references('id')->on('attendance')->onDelete('set null');
             $table->foreign('position_in_meeting_id')->references('id')->on('position_in_meeting')->onDelete('set null');
             $table->foreign('status_id')->references('id')->on('statuses')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendance_meeting');
    }
}
