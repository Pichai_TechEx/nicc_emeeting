<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingPermissionsIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_permissions_issues', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('meeting_permissions_id')->index();
            $table->unsignedInteger('meeting_issues_id')->index();
            $table->unsignedInteger('user_id')->index();
            $table->unsignedInteger('organization_id')->index();
            $table->boolean('is_migrate')->default(0);
            $table->boolean('is_enabled')->default(1);
            $table->unsignedInteger('created_by')->default(0);
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('meeting_permissions_id')->references('id')->on('meeting_permissions')->onDelete('cascade');
            $table->foreign('meeting_issues_id')->references('id')->on('meeting_issues')->onDelete('no action');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_permissions_issues');
    }
}
