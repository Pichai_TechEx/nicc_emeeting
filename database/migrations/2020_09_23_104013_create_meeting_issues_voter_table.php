<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingIssuesVoterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_issues_voter', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('meeting_issues_id')->index();
            $table->integer('score')->default(0);
            $table->integer('is_voter')->length(3); //1= เห็นด้วย  2= ไม่เห็นด้วย 3= ไม่ลงความคิดเห็น
            $table->unsignedInteger('created_by')->default(0);
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('meeting_issues_id')->references('id')->on('meeting_issues')->onDelete('cascade');
        });
    }
  
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_issues_voter');
    }
}
