<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProposeTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propose_topics', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('meeting_id')->index();
            $table->date('end_date')->nullable();
            $table->time('end_time')->nullable();
            $table->boolean('is_migrate')->default(0);
            $table->boolean('is_enabled')->default(1);
            $table->unsignedInteger('created_by')->default(0);
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('meeting_id')->references('id')->on('meeting')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propose_topics');
    }
}
