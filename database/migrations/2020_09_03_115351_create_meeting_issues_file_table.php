<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingIssuesFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_issues_file', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('meeting_issues_id')->index();
            $table->unsignedInteger('file_id')->index();
            $table->boolean('is_migrate')->default(0);
            $table->boolean('is_enabled')->default(1);
            $table->unsignedInteger('created_by')->default(0);
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();

             //FOREIGN KEY CONSTRAINTS
             $table->foreign('meeting_issues_id')->references('id')->on('meeting_issues')->onDelete('cascade');
             $table->foreign('file_id')->references('id')->on('file')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_issues_file');
    }
}
