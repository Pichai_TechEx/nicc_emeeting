<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoardsAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boards_attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('board_id');
            $table->unsignedInteger('title_id')->nullable();
            $table->unsignedInteger('organization_id')->nullable();
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('position_in_meeting_id')->nullable();
            $table->string('title_name', 100);
            $table->string('organization_name', 100);
            $table->string('position_in_meeting_name', 100);
            $table->string('first_name', 100)->nullable();
            $table->string('last_name', 100)->nullable();
            $table->string('position', 100)->nullable();
            $table->string('other', 100)->nullable();
            $table->string('email', 100)->nullable();
            $table->string('phone', 20)->nullable();
            $table->boolean('is_migrate')->default(0);
            $table->boolean('is_enabled')->default(1);
            $table->unsignedInteger('created_by')->default(0);
            $table->unsignedInteger('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('organization_id')->references('id')->on('organizations')->onDelete('set null');
            $table->foreign('title_id')->references('id')->on('titles')->onDelete('set null');
            $table->foreign('position_in_meeting_id')->references('id')->on('position_in_meeting')->onDelete('set null');
            $table->foreign('board_id')->references('id')->on('boards')->onDelete('cascade');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boards_attendances');
    }
}
