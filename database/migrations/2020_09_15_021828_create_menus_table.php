<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 20);
            $table->string('name', 100);
            $table->string('description', 100);
            $table->string('controller', 100)->nullable();
            $table->string('icon', 100)->nullable();
            $table->unsignedInteger('group')->length(2)->nullable()->index();
            $table->unsignedInteger('position')->length(2)->nullable()->index();
            $table->unsignedInteger('parent')->length(2)->nullable()->index();
            $table->unsignedInteger('level')->length(2)->nullable()->index();
            $table->string('lang', 10);
            $table->boolean('is_tab_menu')->default(0);
            $table->boolean('is_menu')->default(0);
            $table->boolean('is_dropdown_menu')->default(0);
            $table->boolean('is_migrate')->default(0);
            $table->boolean('is_enabled')->default(1);
            $table->unsignedInteger('created_by')->default(0);
            $table->unsignedInteger('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
