<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingIssuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meeting_issues', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('meeting_id')->index();
            $table->string('title', 200);
            $table->text('detail')->nullable();
            $table->text('meeting_record')->nullable();
            $table->integer('level')->default(0);
            $table->unsignedInteger('parent')->nullable();
            $table->integer('vote_by')->length(3)->default(3); //1= เลขาฯ  2= ผู้เข้าร่วมการประชุม 3= ไม่มีการลงมติฯ
            $table->boolean('is_topic')->default(0);
            $table->boolean('is_migrate')->default(0);
            $table->boolean('is_enabled')->default(1);
            $table->unsignedInteger('created_by')->default(0);
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();

            //FOREIGN KEY CONSTRAINTS
            $table->foreign('meeting_id')->references('id')->on('meeting')->onDelete('cascade');
        });
    }
   
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meeting_issues');
    }
}
