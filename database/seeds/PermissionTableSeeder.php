<?php

use Illuminate\Database\Seeder;
use TechEx\Http\Controllers\Core;
use TechEx\Permission;
use Illuminate\Support\Facades\{DB};
use Carbon\Carbon;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            [
                'name'      => 'เพิ่ม',
                'slug'      => 'create',
                'sort_no'   => 1,
            ],
            [
                'name'      => 'แก้ไข',
                'slug'      => 'edit',
                'sort_no'   => 2,
            ],
            [
                'name'      => 'ลบ',
                'slug'      => 'delete',
                'sort_no'   => 3,
            ],
            [
                'name'      => 'ค้นหา',
                'slug'      => 'search',
                'sort_no'   => 4,
            ],
            [
                'name'      => 'ดูรายละเอียด',
                'slug'      => 'show',
                'sort_no'   => 5,
            ],
            [
                'name'      => 'ส่งออก',
                'slug'      => 'export',
                'sort_no'   => 6,
            ],
            [
                'name'      => 'นำเข้า',
                'slug'      => 'import',
                'sort_no'   => 7,
            ],
            [
                'name'      => 'พิมพ์',
                'slug'      => 'print',
                'sort_no'   => 8,
            ],
        ];

        DB::beginTransaction();
            
        try {

            if(count($list) > 0) {
                foreach ($list as $key => $row) {
                    $permission = new Permission();
                    $permission->name          = $row['name'];
                    $permission->slug          = $row['slug'];
                    $permission->sort_no       = $row['sort_no'];
                    $permission->created_at    = Carbon::now();
                    $permission->save();
                }
            }

        } catch(ValidationException $e) {
            
            DB::rollback();

            return $e->getErrors();

        } catch (\Exception $e) {

            DB::rollback();
            throw $e;
        }

        DB::commit();
    }
}
