<?php

use Illuminate\Database\Seeder;
use TechEx\Http\Controllers\Core;
use TechEx\Role;
use Illuminate\Support\Facades\{DB};
use Carbon\Carbon;


class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            [
                'name' => 'ผู้ดูแลระบบ',
                'note' => 'ผู้ดูแล ควบคุมโครงการที่เกี่ยวข้องกับระบบ',
            ],
      
            [
                'name' => 'เลขานุการ',
                'note' => 'ผู้ทำหน้าที่ออกจดหมายเชิญประชุม จัดระเบียบวาระการประชุม',
            ],
            [
                'name' => 'ทั่วไป',
                'note' => 'ผู้ใช้งานทั่วไป',
            ],
        ];

        DB::beginTransaction();
            
        try {
            
            if(count($list) > 0) {
                foreach ($list as $key => $row) {
                    $role = new Role();
                    $role->name         = $row['name'];
                    $role->note         = $row['note'];
                    $role->is_enabled   = 1;
                    $role->created_by   = 0;
                    $role->created_at   = Carbon::now();
                    $role->save();
                }
            }

        } catch(ValidationException $e) {
            
            DB::rollback();

            return $e->getErrors();

        } catch (\Exception $e) {

            DB::rollback();
            throw $e;
        }

        DB::commit();
    }
}
