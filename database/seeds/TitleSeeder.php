<?php

use Illuminate\Database\Seeder;
use TechEx\Http\Controllers\Core;
use TechEx\Title;
use Illuminate\Support\Facades\{DB};
use Carbon\Carbon;


class TitleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            [
                'name' => 'นางสาว',
                'description' => 'คำนำหน้าชื่อผู้หญิงที่ยังไม่มีสามี.',
                'lang' => 'th',
                'sort_no' => 1,
            ],
            [
                'name' => 'นาง',
                'description' => 'คำนำหน้าชื่อผู้หญิงที่มีสามีแล้ว',
                'lang' => 'th',
                'sort_no' => 2,
            ],
            [
                'name' => 'นาย',
                'description' => 'คำนำหน้าชื่อผู้ชาย',
                'lang' => 'th',
                'sort_no' => 3,
            ],
            [
                'name' => 'เด็กหญิง',
                'description' => 'คำนำหน้าชื่อเด็กหญิงที่มีอายุไม่ถึง ๑๕ ปี.',
                'lang' => 'th',
                'sort_no' => 4,
            ],
            [
                'name' => 'เด็กชาย',
                'description' => 'คำนำหน้าชื่อเด็กชายที่มีอายุไม่ถึง ๑๕ ปี.',
                'lang' => 'th',
                'sort_no' => 5,
            ],
            [
                'name' => 'ศาสตราจารย์',
                'description' => 'ตำแหน่งศาสตราจารย์ ถือเป็นตำแหน่งที่มีเกียรติที่แสดงถึงความเป็นผู้มีความรู้สูง และมีผลงานด้านการศึกษาของบุคคลนั้น',
                'lang' => 'th',
                'sort_no' => 12,
            ],
            [
                'name' => 'ผู้ช่วยศาสตราจารย์',
                'description' => 'ตำแหน่งผู้ช่วยศาสตราจารย์เป็นตำแหน่งต่อจากตำแหน่ง อาจารย์ ก่อนจะเป็น รองศาสตราจารย์',
                'lang' => 'th',
                'sort_no' => 13,
            ],
            [
                'name' => 'รองศาสตราจารย์',
                'description' => 'เป็นตำแหน่งทางวิชาการ ต่อมาจากตำแหน่ง ผู้ช่วยศาสตราจารย์ ก่อนจะเป็น ศาสตราจารย์',
                'lang' => 'th',
                'sort_no' => 14,
            ],
        ];

        DB::beginTransaction();
            
        try {
            
            if(count($list) > 0) {
                foreach ($list as $key => $row) {
                    $title = new Title();
                    $title->code          = Core::uniqidReal();
                    $title->name          = $row['name'];
                    $title->description   = $row['description'];
                    $title->lang          = $row['lang'];
                    $title->sort_no       = $row['sort_no'];
                    $title->is_enabled    = 1;
                    $title->created_by    = 0;
                    $title->created_at    = Carbon::now();
                    $title->save();
                }
            }

        } catch(ValidationException $e) {
            
            DB::rollback();

            return $e->getErrors();

        } catch (\Exception $e) {

            DB::rollback();
            throw $e;
        }

        DB::commit();
    }
}
