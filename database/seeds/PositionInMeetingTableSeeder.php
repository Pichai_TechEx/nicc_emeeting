<?php

use Illuminate\Database\Seeder;
use TechEx\Http\Controllers\Core;
use TechEx\PositionInMeeting;
use Illuminate\Support\Facades\{DB};
use Carbon\Carbon;

class PositionInMeetingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            [
                'name' => 'ผู้จัดประชุม',
                'description' => 'บุคคลหรือองค์กรที่ริเริ่มให้มีการประชุม เป็นผู้กำหนดเรื่องประชุม กำหนดตัวผู้เข้าประชุมและเตรียมการบันทึกผลการประชุม',
                'lang' => 'th',
                'sort_no' => 1,
            ],
            [
                'name' => 'ผู้มีสิทธิ์เข้าประชุม',
                'description' => 'ผู้ที่ได้รับเชิญหรือได้รับแต่งตั้งให้เข้าประชุม มีสิทธิ์อภิปรายเสนอเรื่องให้ที่ประชุมพิจารณาและมีสิทธิ์ออกเสียงลงมติได้',
                'lang' => 'th',
                'sort_no' => 2,
            ],
            [
                'name' => 'ผู้เข้าประชุม',
                'description' => 'ผู้มีสิทธิเข้าประชุมที่ปรากฎตัวในที่ประชุมและมีส่วนร่วมแสดงความคิดเห็นในการประชุม แต่ไม่รวมถึงผู้เข้าสังเกตการณ์',
                'lang' => 'th',
                'sort_no' => 3,
            ],
            [
                'name' => 'องค์ประชุม',
                'description' => 'จำนวนผู้เข้าประชุมที่กำหนดไว้ในข้อบังคับว่า ต้องมีอย่างน้อยกี่คนจึงจะเปิดการประชุมและดำเนินการประชุมได้ คำว่าครบองค์ประชุมหมายถึง จำนวนผู้เข้าประชุมครบตามที่ระบุไว้ในระเบียบข้อบังคับโดยทั่วไป ซึ่งมีจำนวนไม่น้อยกว่ากึ่งหนึ่งของจำนวนสมาชิก หากการประชุมมีไม่ครบองค์ประชุม มติที่ได้ถือเป็นโมฆะ (โมฆะ แปลว่า สูญเปล่า)',
                'lang' => 'th',
                'sort_no' => 4,
            ],
            [
                'name' => 'ประธาน',
                'description' => 'ผู้ทำหน้าที่ควบคุมการประชุมทั้งหมด',
                'lang' => 'th',
                'sort_no' => 5,
            ],
            [
                'name' => 'รองประธาน',
                'description' => 'ผู้ทำหน้าที่แทนประธาน ถ้าประธานไม่อยู่',
                'lang' => 'th',
                'sort_no' => 6,
            ],
            [
                'name' => 'เลขานุการ',
                'description' => 'ผู้ทำหน้าที่ออกจดหมายเชิญประชุม จัดระเบียบวาระการประชุมบันทึกรายงานการประชุมและอำนวยความสะดวกต่างๆ นอกจากนี้ต้องจัดทำรายงานการประชุมครั้งที่แล้ว เตรียมเอกสาร อุปการณ์และสถานที่ เตรียมข้อคิดเห็นที่เป็นประโยชน์ อ่านรายงานการประชุมครั้งที่แล้ว อ่านระเบียบวาระการประชุม ส่งเอกสารแจ้งมติที่ประชุมให้กรรมการทราบ รวมทั้งช่วยประสานงานในการติดตามผลการปฎิบัติงานตามมติที่ประชุม',
                'lang' => 'th',
                'sort_no' => 7,
            ],
            [
                'name' => 'ผู้ช่วยเลขานุการ',
                'description' => 'ผู้ที่ทำหน้าที่ช่วยเลขานุการ',
                'lang' => 'th',
                'sort_no' => 8,
            ],
            [
                'name' => 'กรรมการ',
                'description' => 'ผู้มีหน้าที่พิจารณาเรื่องในวาระการประชุม และตั้งข้อเสนอหรือญัตติให้ที่ประชุมพิจารณา ควรเข้าประชุมให้ตรงเวลาและแสดงความคิดเห็นในเรื่องการประชุม',
                'lang' => 'th',
                'sort_no' => 9,
            ],
            [
                'name' => 'กรรมการเลขานุการ',
                'description' => 'ผู้ทำหน้าที่จดบันทึกการประชุมและเตรียมการประชุมนอกเหนือไปจากการทำหน้าที่เช่นเดียวกับกรรมการคนอื่นๆ',
                'lang' => 'th',
                'sort_no' => 10,
            ],
        ];

        DB::beginTransaction();
            
        try {
            
            if(count($list) > 0) {
                foreach ($list as $key => $row) {
                    $position_in_meeting = new PositionInMeeting();
                    $position_in_meeting->code          = Core::uniqidReal();
                    $position_in_meeting->name          = $row['name'];
                    $position_in_meeting->description   = $row['description'];
                    $position_in_meeting->lang          = $row['lang'];
                    $position_in_meeting->sort_no       = $row['sort_no'];
                    $position_in_meeting->is_enabled    = 1;
                    $position_in_meeting->created_by    = 0;
                    $position_in_meeting->created_at    = Carbon::now();
                    $position_in_meeting->save();
                }
            }

        } catch(ValidationException $e) {
            
            DB::rollback();

            return $e->getErrors();

        } catch (\Exception $e) {

            DB::rollback();
            throw $e;
        }

        DB::commit();
    }
}
