<?php

use Illuminate\Database\Seeder;
use TechEx\System;
use Illuminate\Support\Facades\{DB};
use Carbon\Carbon;

class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            [
                'variable' => 'system_name_en',
                'value' => 'system_name_en',
            ],
            [
                'variable' => 'system_name_th',
                'value' => 'system_name_en',
            ],
            [
                'variable' => 'system_short_name_en',
                'value' => 'system_short_name_en',
            ],
            [
                'variable' => 'system_short_name_th',
                'value' => 'system_short_name_th',
            ],
            [
                'variable' => 'smtp_host',
                'value' => 'smtp.gmail.com',
            ],
            [
                'variable' => 'smtp_port',
                'value' => '587',
            ],
            [
                'variable' => 'mail_accout',
                'value' => 'janethai44ams@gmail.com',
            ],
            [
                'variable' => 'mail_password',
                'value' => 'janethai44',
            ],
            [
                'variable' => 'mail_is_enabled',
                'value' => 1,
            ],
            [
                'variable' => 'file_id',
            ],
        ];

        DB::beginTransaction();
            
        try {
            
            if(count($list) > 0) {
                foreach ($list as $key => $row) {
                    $system = new System();
                    $system->variable      = $row['variable'];
                    $system->value         = !empty($row['value']) ? $row['value'] : '';
                    $system->is_enabled    = 1;
                    $system->created_by    = 0;
                    $system->created_at    = Carbon::now();
                    $system->save();
                }
            }
         
        } catch(ValidationException $e) {
            
            DB::rollback();

            return $e->getErrors();

        } catch (\Exception $e) {

            DB::rollback();
            throw $e;
        }

        DB::commit();
    }
}
