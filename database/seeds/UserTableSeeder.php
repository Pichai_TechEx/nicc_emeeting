<?php

use Illuminate\Database\Seeder;
use TechEx\Http\Controllers\Core;
use TechEx\{User, Permission, Role, Menu, RolesMenusPermissions};
use Illuminate\Support\Facades\{DB};
use Carbon\Carbon;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::beginTransaction();
            
        try {

            $permissions = Permission::all();
            $menus       = Menu::all();
    
            $role = Role::find(1);
     
            $roles_menus_permissions_arr = [];
            if($menus->count() > 0) {
                foreach ($menus as $key => $menu) {
                    $menu->permissions()->attach($permissions);
                   
                    if($permissions->count() > 0) {
                        foreach ($permissions as $permission) {
                            $roles_menus_permissions = new RolesMenusPermissions();
                            $roles_menus_permissions->menu_id       = $menu->id;
                            $roles_menus_permissions->permission_id = $permission->id;
                            $roles_menus_permissions_arr[] = $roles_menus_permissions;
                        }
                    }
                }
            }
       
            if(count($roles_menus_permissions_arr) > 0) {
                $role->roles_menus_permissions()->saveMany($roles_menus_permissions_arr);
            }
    
            $user = new User();
            $user->username     = 'ADMIN';
            $user->email        = 'janethai44@gmail.com';
            $user->first_name   = 'Admin';
            $user->last_name    = 'System';
            $user->position     = 'ผู้จัดการระบบ';
            $user->password     = bcrypt('password');
            $user->is_login     = 1;
            $user->created_at   = Carbon::now();
            $user->save();
            $user->roles()->attach($role);

        } catch(ValidationException $e) {
            
            DB::rollback();

            return $e->getErrors();

        } catch (\Exception $e) {

            DB::rollback();
            throw $e;
        }

        DB::commit();
    }
}
