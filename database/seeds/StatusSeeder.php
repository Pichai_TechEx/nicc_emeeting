<?php

use Illuminate\Database\Seeder;
use TechEx\Http\Controllers\Core;
use TechEx\Status;
use Illuminate\Support\Facades\{DB};
use Carbon\Carbon;


class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            [
                'name' => 'รอประชุม',
                'description' => 'รอประชุม',
                'sort_no' => 1,
            ],
            [
                'name' => 'ระหว่างการประชุม',
                'description' => 'ระหว่างการประชุม',
                'sort_no' => 2,
            ],
            [
                'name' => 'ปิดการประชุมได้',
                'description' => 'ปิดการประชุมได้',
                'sort_no' => 3,
            ]
        ];

        DB::beginTransaction();
            
        try {
            
            if(count($list) > 0) {
                foreach ($list as $key => $row) {
                    $status = new Status();
                    $status->code          = Core::uniqidReal();
                    $status->name          = $row['name'];
                    $status->description   = $row['description'];
                    $status->lang          = 'th';
                    $status->sort_no       = $row['sort_no'];
                    $status->is_enabled    = 1;
                    $status->created_by    = 0;
                    $status->created_at    = Carbon::now();
                    $status->save();
                }
            }
         
        } catch(ValidationException $e) {
            
            DB::rollback();

            return $e->getErrors();

        } catch (\Exception $e) {

            DB::rollback();
            throw $e;
        }

        DB::commit();
    }
}
