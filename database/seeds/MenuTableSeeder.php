<?php

use Illuminate\Database\Seeder;
use TechEx\Http\Controllers\Core;
use TechEx\Menu;
use Illuminate\Support\Facades\{DB};
use Carbon\Carbon;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            [
                'name'                  => 'ตั้งค่าตำแหน่งในที่ประชุม',
                'description'           => 'ตั้งค่าตำแหน่งในที่ประชุม',
                'controller'            => 'PositionInMeetingController',
                'icon'                  => '',
                'group'                 => '',
                'position'              => '',
                'parent'                => '',
                'level'                 => '',
                'is_tab_menu'           => 0,
                'is_menu'               => 1,
                'is_dropdown_menu'      => 0,
            ],
            [
                'name'                  => 'ตั้งค่าการประชุม',
                'description'           => 'ตั้งค่าการประชุม',
                'controller'            => 'MeetingController',
                'icon'                  => '',
                'group'                 => '',
                'position'              => '',
                'parent'                => '',
                'level'                 => '',
                'is_tab_menu'           => 0,
                'is_menu'               => 1,
                'is_dropdown_menu'      => 0,
            ],
            [
                'name'                  => 'ตั้งค่าระบบ',
                'description'           => 'ตั้งค่าระบบ',
                'controller'            => 'SystemController',
                'icon'                  => '',
                'group'                 => '',
                'position'              => '',
                'parent'                => '',
                'level'                 => '',
                'is_tab_menu'           => 0,
                'is_menu'               => 1,
                'is_dropdown_menu'      => 0,
            ],
            [
                'name'                  => 'ตั้งค่าหน่วยงาน',
                'description'           => 'ตั้งค่าหน่วยงาน',
                'controller'            => 'OrganizationsController',
                'icon'                  => '',
                'group'                 => '',
                'position'              => '',
                'parent'                => '',
                'level'                 => '',
                'is_tab_menu'           => 0,
                'is_menu'               => 1,
                'is_dropdown_menu'      => 0,
            ],
            [
                'name'                  => 'ตั้งค่าประเภทผู้ใช้งาน',
                'description'           => 'ตั้งค่าประเภทผู้ใช้งาน',
                'controller'            => 'UserTypeController',
                'icon'                  => '',
                'group'                 => '',
                'position'              => '',
                'parent'                => '',
                'level'                 => '',
                'is_tab_menu'           => 0,
                'is_menu'               => 1,
                'is_dropdown_menu'      => 0,
            ],
            [
                'name'                  => 'รายงานสถิติการจัดประชุม',
                'description'           => 'รายงานสถิติการจัดประชุม',
                'controller'            => 'MeetingStatisticsReportController',
                'icon'                  => '',
                'group'                 => '',
                'position'              => '',
                'parent'                => '',
                'level'                 => '',
                'is_tab_menu'           => 0,
                'is_menu'               => 1,
                'is_dropdown_menu'      => 0,
            ],
            [
                'name'                  => 'ตั้งค่าเทมเพลตวาระการประชุม',
                'description'           => 'ตั้งค่าเทมเพลตวาระการประชุม',
                'controller'            => 'TemplateController',
                'icon'                  => '',
                'group'                 => '',
                'position'              => '',
                'parent'                => '',
                'level'                 => '',
                'is_tab_menu'           => 0,
                'is_menu'               => 1,
                'is_dropdown_menu'      => 0,
            ],
            [
                'name'                  => 'เสนอหัวข้อการประชุม',
                'description'           => 'เสนอหัวข้อการประชุม',
                'controller'            => 'ProposeTopicsController',
                'icon'                  => '',
                'group'                 => '',
                'position'              => '',
                'parent'                => '',
                'level'                 => '',
                'is_tab_menu'           => 0,
                'is_menu'               => 1,
                'is_dropdown_menu'      => 0,
            ],
            [
                'name'                  => 'เผยแพร่หัวข้อการประชุม',
                'description'           => 'เผยแพร่หัวข้อการประชุม',
                'controller'            => 'MeetingPermissionsController',
                'icon'                  => '',
                'group'                 => '',
                'position'              => '',
                'parent'                => '',
                'level'                 => '',
                'is_tab_menu'           => 0,
                'is_menu'               => 1,
                'is_dropdown_menu'      => 0,
            ],
        ];

        DB::beginTransaction();
            
        try {

            if(count($list) > 0) {
                foreach ($list as $key => $row) {
                    $menu = new Menu();
                    $menu->code             = Core::uniqidReal();
                    $menu->name             = $row['name'];
                    $menu->description      = $row['description'];
                    $menu->controller       = $row['controller'];
                    $menu->icon             = $row['icon'];
                    $menu->group            = $row['group'];
                    $menu->position         = $row['position'];
                    $menu->parent           = $row['parent'];
                    $menu->level            = $row['level'];
                    $menu->lang             = 'th';
                    $menu->is_tab_menu      = $row['is_tab_menu'];
                    $menu->is_menu          = $row['is_menu'];
                    $menu->is_dropdown_menu = $row['is_dropdown_menu'];
                    $menu->created_at       = Carbon::now();
                    $menu->save();
                }
            }

        } catch(ValidationException $e) {
            
            DB::rollback();

            return $e->getErrors();

        } catch (\Exception $e) {

            DB::rollback();
            throw $e;
        }

        DB::commit();
    }
}
