<?php

namespace TechEx\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use TechEx\{User, Meeting, MeetingIssues};
use Carbon\Carbon;

class MeetingInvitation extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Meeting $meeting)
    {
        $this->user = $user;
        $this->meeting = $meeting;
        $this->text = '';
      
        $txt       = '';
        $daysTH = ['Sunday' => 'วันอาทิตย์', 'Monday' => 'วันจันทร์', 'Tuesday' => 'วันอังคาร', 'Wednesday' => 'วันพุธ', 'Thursday' => 'วันพฤหัสบดี', 'Friday' => 'วันศุกร์', 'Saturday' => 'วันเสาร์'];
        $monthTH = [null,'มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $monthTH_brev = [null,'ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];

        $dateCarbon = Carbon::createFromFormat('Y-m-d', $meeting->date, 'Asia/Bangkok');
        $date = $daysTH[$dateCarbon->format('l')];
        $date .= "ที่ {$dateCarbon->format('d')} ";
        $date .= "เดือน{$monthTH[$dateCarbon->format('n')]} ";
        $date .= "พ.ศ.{$dateCarbon->modify('+543 year')->format('Y ')} ";

        $start_time = explode(':', $meeting->start_time);
        $start_time = $start_time[0].'.'.$start_time[1];

        $end_time = explode(':', $meeting->end_time);
        $end_time = $end_time[0].'.'.$end_time[1];

        $time = "{$start_time}-{$end_time} น.";

        $date .= "เวลา {$time}";

        $txt = "";
        $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
            $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                    $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                        $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                            $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px"><div>'.$meeting->meeting_name.' ครั้งที่ '.$meeting->agenda.'</div></blockquote>';
                        $txt .= '</blockquote>';
                    $txt .= '</blockquote>';
                $txt .= '</blockquote>';
            $txt .= '</blockquote>';
        $txt .= '</blockquote>';

        $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
            $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px"><div>'.$date.'</div></blockquote>';
            $txt .= '</blockquote>';
        $txt .= '</blockquote>';

        $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
            $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px"><div> ณ '.$meeting->meeting_place.'</div></blockquote>';
            $txt .= '</blockquote>';
        $txt .= '</blockquote>';

        if($meeting->meetingissues()->where('level', 0)->count() > 0) {
            foreach ($meeting->meetingissues()->where('level', 0)->get() as $key_level_1 => $meeting_issues) {
                $level_1 = 'วาระที่ '. ($key_level_1 + 1).' ';
                $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">'.$level_1.$meeting_issues->title.'</blockquote>';

                if($meeting_issues->detail) {
                    $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                        $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">'.$meeting_issues->detail.'</blockquote>';
                    $txt .= '</blockquote>';
                }
                 
                if($meeting_issues->parent()->count() > 0) {
                    foreach ($meeting_issues->parent()->get() as $key_level_2 => $issues) {
                    
                        if(!$issues->is_topic) {
                            $level_2 = $level_1.'.'.($key_level_2 + 1).' ';
                        } else {
                            $level_2 = "ข้อที่ ".($key_level_2 + 1).' ';
                        }

                        $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                            $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">'.$level_2.$issues->title.'</blockquote>';
                        $txt .= '</blockquote>';

                        if($issues->detail) {
                            $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                                $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                                    $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">'.$issues->detail.'</blockquote>';
                                $txt .= '</blockquote>';
                            $txt .= '</blockquote>';
                        }
                    
                        if($issues->parent()->count() > 0) {
                            foreach ($issues->parent()->get() as $key_level_3 => $sub) {

                                if(!$sub->is_topic) {
                                    $level_3 = $level_2.'.'.($key_level_3 + 1).' ';
                                } else {
                                    $level_3 = "ข้อที่ ".($key_level_3 + 1).' ';
                                }

                                $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                                    $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                                        $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">'.$level_3.$sub->title.'</blockquote>';
                                    $txt .= '</blockquote>';
                                $txt .= '</blockquote>';

                                if($sub->detail) {
                                    $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                                        $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                                            $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                                                $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">'.$sub->detail.'</blockquote>';
                                            $txt .= '</blockquote>';
                                        $txt .= '</blockquote>';
                                    $txt .= '</blockquote>';
                                }
                            }
                        }
                    }
                }
            }

            $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">หน่วยงานจัดการประชุม test<br>ตอบรับการเข้าประชุมผ่าน URL :&nbsp;</blockquote>';
            $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px"><br></blockquote>';
            $txt .= '</blockquote>';
            $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px">';
                $txt .= '<blockquote style="margin:0 0 0 40px;border:none;padding:0px"></blockquote>';
            $txt .= '</blockquote>';
        }
    //    dd($txt);
       $this->text = $txt;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user = $this->user;
        $text = $this->text;
        return $this->view('mail.meetingInvitation', compact('user', 'text'));
    }
}
