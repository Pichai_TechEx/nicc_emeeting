<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;

class MeetingIssuesFile extends Model
{
    protected $table = 'meeting_issues_file';

    public function file() {
        return $this->hasOne(File::class, 'id', 'file_id');
    }
}
