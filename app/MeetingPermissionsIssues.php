<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;

class MeetingPermissionsIssues extends Model
{
    protected $table = 'meeting_permissions_issues';
}
