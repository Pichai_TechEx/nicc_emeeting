<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\{DB};

class Status extends Model
{
    use SoftDeletes;
    
    public static function selection() {

        $model = Status::select([
            'statuses.id',
            'statuses.name',
        ])
        ->where('statuses.is_enabled', 1);
       
        return $model;
    }
}
