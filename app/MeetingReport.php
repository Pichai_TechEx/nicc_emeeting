<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;

class MeetingReport extends Model
{
    protected $table = 'meeting_report';
}
