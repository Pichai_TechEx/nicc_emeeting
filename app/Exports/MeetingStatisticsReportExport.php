<?php

namespace TechEx\Exports;

use TechEx\Meeting;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;

class MeetingStatisticsReportExport implements FromCollection
{

    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection($text)
    {
        return Meeting::all();
    }
}
