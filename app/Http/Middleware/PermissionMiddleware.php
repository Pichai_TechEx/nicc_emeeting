<?php

namespace TechEx\Http\Middleware;

use Closure;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission = null)
    {
        dd($permission);
        if($permission !== null && !$request->user()->can(trim($permission))) {
            abort(404);
        }
        
        return $next($request);
    }
}
