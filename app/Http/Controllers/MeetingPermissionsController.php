<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Crypt, Storage, Response};
use TechEx\{MeetingPermissions, MeetingPermissionsIssues, Meeting};
use Collective\Html\FormFacade as Form;
use TechEx\Http\Controllers\Core;
use Carbon\Carbon;
use Validator;
use Auth;

class MeetingPermissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];

        return view('pages.meetingpermissions.index',[
            'result' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $result = [];

        return view('pages.meetingpermissions.create',[
            'result' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();
            
            try {

                if(Crypt::decryptString($request->meeting_id)) {

                    $meeting = Meeting::find(Crypt::decryptString($request->meeting_id));

                    $meeting_permissions = new MeetingPermissions();
                    $meeting_permissions->created_by        = $user->id;
                    $meeting_permissions->created_at        = Carbon::now();

                    $meeting_permissions_arr = [];
                    if($request->has('meeting_issues') && count($request->input('meeting_issues', [])) > 0) {
                        foreach ($request->input('meeting_issues') as $key => $item) {
                            if($request->has("meeting_select_issues.{$key}.organization") && count($request->input("meeting_select_issues.{$key}.organization", [])) > 0) {
                                foreach ($request->input("meeting_select_issues.{$key}.organization") as $key_rows => $rows) {
                                    $meeting_permissions_issues = new MeetingPermissionsIssues();
                                    $meeting_permissions_issues->meeting_issues_id = Crypt::decryptString($item['id']);
                                    $meeting_permissions_issues->user_id           = Crypt::decryptString($request->input("meeting_select_issues.{$key}.name_option.{$key_rows}"));
                                    $meeting_permissions_issues->organization_id   = Crypt::decryptString($rows);
                                    $meeting_permissions_issues->created_by        = $user->id;
                                    $meeting_permissions_issues->created_at        = Carbon::now();
                                    $meeting_permissions_issues_arr[] = $meeting_permissions_issues;
                                }
                            }
                        }
                    }
                    
                    if($meeting->meetingpermissions()->save($meeting_permissions)){
                        if(count($meeting_permissions_issues_arr) > 0) {
                            $meeting_permissions->meetingpermissionsissues()->saveMany($meeting_permissions_issues_arr);
                        }

                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'บันทึกสำเร็จ';
                        $results->redirect_url   = route('pages.meetingpermissions.index');
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'บันทึกไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = [];

        if(Crypt::decryptString($id)) {

            $meeting_permissions = MeetingPermissions::find(Crypt::decryptString($id));
            $meeting_permissions_arr = [];
            if($meeting_permissions->meetingpermissionsissues()->count() > 0) {
                foreach ($meeting_permissions->meetingpermissionsissues()->get() as $key => $item) {
                    $meeting_permissions_arr[$item->meeting_issues_id]['meeting_issues_id'] = Crypt::encryptString($item->meeting_issues_id);
                    $meeting_permissions_arr[$item->meeting_issues_id]['permissions'][] = (object)[
                        'organization_id'   =>  Crypt::encryptString($item->organization_id),
                        'user_id'           =>  Crypt::encryptString($item->user_id),
                    ];
                }
            }
            
            if($meeting_permissions) {
                $result['data'] = (object)[
                    'id'                    => Crypt::encryptString($meeting_permissions->id),
                    'meeting_id'            => Crypt::encryptString($meeting_permissions->meeting_id),
                    'meeting_permissions'   => array_values($meeting_permissions_arr),
                ];
            }
            unset($meeting_permissions);
        }
    
        return view('pages.meetingpermissions.edit',[
            'result' => $result
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();
            
            try {
               
                if(Crypt::decryptString($request->meeting_id)) {

                    $meeting_permissions = MeetingPermissions::find(Crypt::decryptString($id));
                    $meeting_permissions->meeting_id        = Crypt::decryptString($request->meeting_id);
                    $meeting_permissions->updated_by        = $user->id;
                    $meeting_permissions->updated_at        = Carbon::now();

                    $meeting_permissions_arr = [];
                    if($request->has('meeting_issues') && count($request->input('meeting_issues', [])) > 0) {
                        foreach ($request->input('meeting_issues') as $key => $item) {
                            if($request->has("meeting_select_issues.{$key}.organization") && count($request->input("meeting_select_issues.{$key}.organization", [])) > 0) {
                                foreach ($request->input("meeting_select_issues.{$key}.organization") as $key_rows => $rows) {
                                    $meeting_permissions_issues = new MeetingPermissionsIssues();
                                    $meeting_permissions_issues->meeting_issues_id = Crypt::decryptString($item['id']);
                                    $meeting_permissions_issues->user_id           = Crypt::decryptString($request->input("meeting_select_issues.{$key}.name_option.{$key_rows}"));
                                    $meeting_permissions_issues->organization_id   = Crypt::decryptString($rows);
                                    $meeting_permissions_issues->created_by        = $user->id;
                                    $meeting_permissions_issues->created_at        = Carbon::now();
                                    $meeting_permissions_issues_arr[] = $meeting_permissions_issues;
                                }
                            }
                        }
                    }
                 
                    if($meeting_permissions->save()){
                        if(count($meeting_permissions_issues_arr) > 0) {
                            if($meeting_permissions->meetingpermissionsissues()->count() > 0) {
                                $meeting_permissions->meetingpermissionsissues()->delete();
                            }

                            $meeting_permissions->meetingpermissionsissues()->saveMany($meeting_permissions_issues_arr);
                        }

                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'บันทึกสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'บันทึกไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function selects(Request $request) {
     
        $selected_id = !empty($request->selected_id) ? Crypt::decryptString($request->selected_id) : null;
      
        $meeting = Meeting::find(Crypt::decryptString($request->meeting_id));
  
        $option = [];
        $meeting_issues = $meeting->meetingissues()->whereNull('parent')->get();
     
        if($meeting_issues->sortBy->level->count() > 0) {
            foreach ($meeting_issues->sortBy->level->all() as $key_list => $list) {
                $no = $key_list + 1;
                
                if($list->is_topic) {
                    $selected = false;

                    if($selected_id == $list->id) {
                        $selected = true;
                    }

                    $option[] = (object)[
                        'id'        => Crypt::encryptString($list->id),
                        'text'      => "วาระที่ {$no} : ".htmlspecialchars($list->title),
                        'selected'  => $selected
                    ];
                }

                if($list->parent()->get()->count() > 0) {
                    foreach ($list->parent()->get()->all() as $key_sub => $sub) {

                        $no_sub = $key_sub + 1;
                        $txt_sub = "วาระที่ {$no} > วาระที่ {$no}.{$no_sub} : ";

                        if($sub->is_topic){
                            $txt_sub = "วาระที่ {$no} > ข้อที่ {$no_sub} : ";
                        }

                        if($sub->is_topic) {
                            $selected = false;

                            if($selected_id == $sub->id) {
                                $selected = true;
                            }

                            $option[] = (object)[
                                'id'        => Crypt::encryptString($sub->id),
                                'text'      => $txt_sub.htmlspecialchars($sub->title),
                                'selected'  => $selected
                            ];
                        }

                        if($sub->parent()->get()->count() > 0) {
                            foreach ($sub->parent()->get()->all() as $key_last => $last) {

                                $no_last = $key_last + 1;
                                $txt_last = "วาระที่ {$no} > วาระที่ {$no}.{$no_sub} > ข้อที่ {$no_last} : ";

                                if($last->is_topic) {
                                    $selected = false;

                                    if($selected_id == $last->id) {
                                        $selected = true;
                                    }

                                    $option[] = (object)[
                                        'id'        => Crypt::encryptString($last->id),
                                        'text'      => $txt_last.htmlspecialchars($last->title),
                                        'selected'  => $selected
                                    ];
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return Response::json($option);
    }
    
    public function rules()
    {
        //
        $rules = [

        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [

        ];
        return $messages;
    }

    public function datatables(Request $request) {

        $user = Auth::user();

        $columns = [
            0 =>'meeting_permissions.name',
            1 =>'meeting_permissions.description',
            2 =>'meeting_permissions.updated_at',
            3 =>'updated_name',
        ];

        $datas = MeetingPermissions::getList();
    
        $totalData = $datas->get()->count();
        $totalFiltered = $totalData;

        $start = $request->input( 'start' );
        $length = $request->input( 'length' );

        if ($request->has('search')) {
            if ($request->input ( 'search.value' ) != '') {
                
                $searchTerm = $request->input('search.value');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where( 'position_in_meeting.name', 'Like', '%' . $searchTerm . '%' );
                    $query->orWhere( 'position_in_meeting.description', 'Like', '%' . $searchTerm . '%' );
                });
            }
        }

        if ($request->has('order')) {
            if ($request->input('order.0.column') != '') {

                $orderColumn = $request->input('order.0.column');
                $orderDirection = $request->input('order.0.dir');

                $data_count = $datas;
                $totalFiltered = $data_count->get()->count();

                $datas = $datas->orderBy($columns[intval( $orderColumn )], $orderDirection )->skip($start)->take($length)->get();
            }
        }
        else {
            $datas = $datas->orderBy('meeting.meeting_name', 'ASC' )->skip($start)->take($length)->get();
        }

        $data = [];
        foreach ( $datas as $key => $col ) {
            $nestedData = [];
  
            $nestedData[0] = !empty($col->meeting_name) ? $col->meeting_name : '-';
            $nestedData[1] = !empty($col->agenda) ? $col->agenda : '-';
            $nestedData[2] = !empty($col->date) ? $col->date : '-';
            $nestedData[3] = !empty($col->end_date) ? $col->end_date : '-';
            $nestedData[4] = !empty($col->meeting_organization_name) ? $col->meeting_organization_name : '-';

            $nestedData[5] = '<div class="btn-group" role="group" aria-label="Basic example">';
                $nestedData[5] .= '<a class="btn btn-info" data-toggle="tooltip" data-placement="top" title="เสนอหัวข้อ" href="'.(route('pages.meetingpermissions.show',['id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ])).'"><i class="fas fa-search-plus"></i></a>';
                $nestedData[5] .= '<a class="btn btn-warning" data-toggle="tooltip" data-placement="top" title="แก้ไข" href="'.(route('pages.meetingpermissions.edit',['id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ])).'"><i class="far fa-edit"></i></a>';
                $nestedData[5] .= '<a class="btn btn-danger btn-delete" data-toggle="tooltip" data-placement="top" title="ลบ" >';
                    $nestedData[5] .= Form::open(['route' => ['pages.meetingpermissions.destroy', 'id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ], 'method' => 'DELETE', 'enctype' => 'multipart/form-data', 'id' => 'from_'.$key]);
                    $nestedData[5] .= '<i class="far fa-trash-alt"></i>';
                    $nestedData[5] .= Form::close();
                $nestedData[5] .= '</a>';
            $nestedData[5] .= '</div>';
    
            $data [] = $nestedData;
        }

        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }
}
