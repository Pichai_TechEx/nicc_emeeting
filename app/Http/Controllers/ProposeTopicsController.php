<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Crypt, Storage, Response};
use TechEx\{PositionInMeeting, Organization, ProposeTopic, ProposeTopicProponent, ProponentTopic, ProponentTopicFile};
use Collective\Html\FormFacade as Form;
use Collective\Html\HtmlFacade as Html;
use TechEx\Http\Controllers\Core;
use Carbon\Carbon;
use Validator;
use Auth;

class ProposeTopicsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];

        return view('pages.proposetopics.index',[
            'result' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $result = [];

        $organization_arr = [];
        $organization = Organization::selection();
        if($organization->count() > 0) {
            foreach ($organization->get() as $organization) {
                $organization_arr[$organization->id] = $organization->name;
            }
        }
        unset($organization);

        $result = [
            'organization' => $organization_arr
        ];

        return view('pages.proposetopics.create',[
            'result' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();
            
            try {
            
                $propose_topic_proponent_arr = [];
                if($request->has('organization') && count($request->input('organization', [])) > 0) {
                    foreach ($request->input('organization') as $key => $item) {
                        $propose_topic_proponent = new ProposeTopicProponent();
                        $propose_topic_proponent->user_id           = !empty($request->input("name_option.{$key}")) ? Crypt::decryptString($request->input("name_option.{$key}")) : '';
                        $propose_topic_proponent->organization_id   = Crypt::decryptString($item);
                        $propose_topic_proponent->created_by        = $user->id;
                        $propose_topic_proponent->created_at        = Carbon::now();
                        $propose_topic_proponent_arr[] = $propose_topic_proponent;
                    
                    }
                }
            
                $propose_topic = new ProposeTopic();
                $propose_topic->meeting_id  = !empty($request->meeting_id) ? Crypt::decryptString($request->input('meeting_id')) : '';
                $propose_topic->end_date    = $request->input('end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('end_date'))->format('Y-m-d') : Carbon::now();
                $propose_topic->end_time    = $request->input('end_time') ? Carbon::createFromFormat('H:i', $request->input('end_time'))->format('H:i:s.u') : Carbon::now()->toTimeString();
                $propose_topic->is_enabled  = $request->input('is_enabled', 0);
                $propose_topic->created_by  = $user->id;
                $propose_topic->created_at  = Carbon::now();
                
                if($propose_topic->save()){

                    if(count($propose_topic_proponent_arr) > 0) {
                        $propose_topic->proposetopicproponent()->saveMany($propose_topic_proponent_arr);
                    }

                    $results->status         = true;
                    $results->icon           = 'success';
                    $results->message        = 'บันทึกสำเร็จ';
                    $results->redirect_url   = route('pages.proposetopics.index');
                } else {
                    $results->status        = false;
                    $results->icon          = 'error';
                    $results->message       = 'บันทึกไม่สำเร็จ';
                    $results->redirect_url  = '';
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = [];

        $daysTH = ['Sunday' => 'วันอาทิตย์', 'Monday' => 'วันจันทร์', 'Tuesday' => 'วันอังคาร', 'Wednesday' => 'วันพุธ', 'Thursday' => 'วันพฤหัสบดี', 'Friday' => 'วันศุกร์', 'Saturday' => 'วันเสาร์'];
        $monthTH = [null,'มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $monthTH_brev = [null,'ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];

        if(Crypt::decryptString($id)) {

            $propose_topic = ProposeTopic::find(Crypt::decryptString($id));
           
            if($propose_topic->meeting) {
                    
                $dateCarbon = Carbon::createFromFormat('Y-m-d', $propose_topic->meeting->date, 'Asia/Bangkok');
                $date = $daysTH[$dateCarbon->format('l')];
                $date .= "ที่ {$dateCarbon->format('d')} ";
                $date .= "เดือน{$monthTH[$dateCarbon->format('n')]} ";
                $date .= "พ.ศ.{$dateCarbon->modify('+543 year')->format('Y')} ";

                $start_time = explode(':', $propose_topic->meeting->start_time);
                $start_time = $start_time[0].'.'.$start_time[1];
    
                $end_time = explode(':', $propose_topic->meeting->end_time);
                $end_time = $end_time[0].'.'.$end_time[1];
    
                $time = "{$start_time}-{$end_time} น.";
    
                $date .= "เวลา {$time}";

                $result['data'] = (object)[
                    'id'            => Crypt::encryptString($propose_topic->id),
                    'meeting_id'    => isset($propose_topic->meeting_id) ? Crypt::encryptString($propose_topic->meeting_id) : '',
                    'meeting_name'  => isset($propose_topic->meeting->meeting_name) ? $propose_topic->meeting->meeting_name : '',
                    "date"          => $date
                ];
            }
            unset($propose_topic);
        }

        return view('pages.proposetopics.show',[
            'result' => $result
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function propose($id)
    {
        $result = [];

        $user = Auth::user();
        $daysTH = ['Sunday' => 'วันอาทิตย์', 'Monday' => 'วันจันทร์', 'Tuesday' => 'วันอังคาร', 'Wednesday' => 'วันพุธ', 'Thursday' => 'วันพฤหัสบดี', 'Friday' => 'วันศุกร์', 'Saturday' => 'วันเสาร์'];
        $monthTH = [null,'มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $monthTH_brev = [null,'ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];

        if(Crypt::decryptString($id)) {

            $propose_topic = ProposeTopic::find(Crypt::decryptString($id));
            if($propose_topic_proponent = $propose_topic->proposetopicproponent()->where('user_id', $user->id)->first()) {
                
                $topic_arr = [];
                if($propose_topic_proponent->proponenttopic()->count() > 0) {
                    foreach ($propose_topic_proponent->proponenttopic as $key => $topic) {

                        $file_1 = [];
                        if($topic->files()->get()->count() > 0) {
                            foreach ($topic->files()->get() as $item) {
                                $files = $item->file()->first();
                                if($files) {
                                    $files->urlencode_path = Crypt::encryptString($files->path);
                                    $files->path = asset(Storage::disk('local')->url($files->path));
                                    $file_1[] = (object)[
                                        'name' => $files->originalName,
                                        'size' => $files->size,
                                        'type' => $files->mimeType,
                                        'url'  => $files->path
                                    ];
                                }
                            };
                        }
                      
                        $topic_arr[] = (object)[
                            'title'     => htmlspecialchars($topic->topic),
                            'detail'    => htmlspecialchars($topic->detail),
                            'files'     => $file_1,
                    
                        ];
                    }
                }
               
                if($propose_topic->meeting) {
                    
                    $dateCarbon = Carbon::createFromFormat('Y-m-d', $propose_topic->meeting->date, 'Asia/Bangkok');
                    $date = $daysTH[$dateCarbon->format('l')];
                    $date .= "ที่ {$dateCarbon->format('d')} ";
                    $date .= "เดือน{$monthTH[$dateCarbon->format('n')]} ";
                    $date .= "พ.ศ.{$dateCarbon->modify('+543 year')->format('Y')} ";
    
                    $start_time = explode(':', $propose_topic->meeting->start_time);
                    $start_time = $start_time[0].'.'.$start_time[1];
        
                    $end_time = explode(':', $propose_topic->meeting->end_time);
                    $end_time = $end_time[0].'.'.$end_time[1];
        
                    $time = "{$start_time}-{$end_time} น.";
        
                    $date .= "เวลา {$time}";

                    $result['data'] = (object)[
                        'id'                => Crypt::encryptString($propose_topic->id),
                        'meeting_name'      => isset($propose_topic->meeting->meeting_name) ? $propose_topic->meeting->meeting_name : '',
                        "date"              => $date,
                        'proponent_topic'   => json_encode($topic_arr),
                    ];
                }

            }
            unset($propose_topic);
        }

        return view('pages.proposetopics.propose',[
            'result' => $result
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = [];

        if(Crypt::decryptString($id)) {

            $propose_topic = ProposeTopic::find(Crypt::decryptString($id));

            if(!empty($propose_topic)) {
                $result['data'] = (object)[
                    'id'                        => Crypt::encryptString($propose_topic->id),
                    'meeting_id'                => isset($propose_topic->meeting_id) ? Crypt::encryptString($propose_topic->meeting_id) : '',
                    "end_date"                  => isset($propose_topic->end_date) ? Carbon::createFromFormat('Y-m-d', $propose_topic->end_date, 'Asia/Bangkok')->format('d/m/Y') : '',
                    'end_time'                  => isset($propose_topic->end_time) ? $propose_topic->end_time : '',
                    'propose_topic_proponent'   => $propose_topic->proposetopicproponent()->count() > 0 ? $propose_topic->proposetopicproponent()->get() : [],
                ];
            }
            unset($propose_topic);
        }

        return view('pages.proposetopics.edit',[
            'result' => $result
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();
            
            try {
            
                $propose_topic_proponent_arr = [];
                if($request->has('organization') && count($request->input('organization', [])) > 0) {
                    foreach ($request->input('organization') as $key => $item) {
                        $propose_topic_proponent = new ProposeTopicProponent();
                        $propose_topic_proponent->user_id           = !empty($request->input("name_option.{$key}")) ? Crypt::decryptString($request->input("name_option.{$key}")) : '';
                        $propose_topic_proponent->organization_id   = Crypt::decryptString($item);
                        $propose_topic_proponent->created_by        = $user->id;
                        $propose_topic_proponent->created_at        = Carbon::now();
                        $propose_topic_proponent_arr[] = $propose_topic_proponent;
                    
                    }
                }
            
                $propose_topic = ProposeTopic::find(Crypt::decryptString($id));
                $propose_topic->meeting_id  = !empty($request->meeting_id) ? Crypt::decryptString($request->input('meeting_id')) : '';
                $propose_topic->end_date    = $request->input('end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('end_date'))->format('Y-m-d') : Carbon::now();
                $propose_topic->end_time    = $request->input('end_time') ? Carbon::createFromFormat('H:i', $request->input('end_time'))->format('H:i:s.u') : Carbon::now()->toTimeString();
                $propose_topic->updated_by  = $user->id;
                $propose_topic->updated_at  = Carbon::now();
                
                if($propose_topic->save()){

                    if(count($propose_topic_proponent_arr) > 0) {

                        if($propose_topic->proposetopicproponent()->count() > 0) {
                            $propose_topic->proposetopicproponent()->delete();
                        }

                        $propose_topic->proposetopicproponent()->saveMany($propose_topic_proponent_arr);
                    }

                    $results->status         = true;
                    $results->icon           = 'success';
                    $results->message        = 'บันทึกสำเร็จ';
                    $results->redirect_url   = '';
                } else {
                    $results->status        = false;
                    $results->icon          = 'error';
                    $results->message       = 'บันทึกไม่สำเร็จ';
                    $results->redirect_url  = '';
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request, $id)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();
            
            try {
                
                $proponent_topic_arr = [];
                if($request->has('meeting_issues') && count($request->input('meeting_issues', [])) > 0) {
                    foreach ($request->input('meeting_issues') as $key => $item) {
                 
                        $file_1 = [];
                        if(!empty($request->meeting_issues[$key]['file']) && count($request->meeting_issues[$key]['file']) > 0) {
                            foreach ($request->meeting_issues[$key]['file'] as $file) {
                       
                                $file_result = Core::saveFile($file);
                                $path_unlink[]  = $file_result['destinationPath'];
                                $file_new_arr[] = $file_result['file_new_id'];

                                $proponent_topic_file = new ProponentTopicFile();
                                $proponent_topic_file->file_id       = $file_result['file_new_id'];
                                $proponent_topic_file->created_by    = $user->id;
                                $proponent_topic_file->created_at    = Carbon::now();

                                $file_1[] = $proponent_topic_file;
                            }
                        }

                        $proponent_topic = new ProponentTopic();
                        $proponent_topic->topic         = $request->input("meeting_issues.{$key}.value", '');
                        $proponent_topic->detail        = $request->input("meeting_issues.{$key}.detail", '');
                        $proponent_topic->created_by    = $user->id;
                        $proponent_topic->created_at    = Carbon::now();
                        $proponent_topic_arr[] = [
                            'main' => $proponent_topic,
                            'file' => $file_1,
                        ];
                    }
                }

                $propose_topic = ProposeTopic::find(Crypt::decryptString($id));

                if($propose_topic_proponent = $propose_topic->proposetopicproponent()->where('user_id', $user->id)->first()) {

                    if($propose_topic_proponent->proponenttopic()->count() > 0) {
                        $propose_topic_proponent->proponenttopic()->delete();
                    }
                    
                    $propose_topic_proponent->updated_by  = $user->id;
                    $propose_topic_proponent->updated_at  = Carbon::now();

                    if($propose_topic_proponent->save()) {
                        if(count($proponent_topic_arr) > 0) {
                            foreach ($proponent_topic_arr as $key => $proponent_topic) {

                                $propose_topic_proponent->proponenttopic()->save($proponent_topic['main']);

                                if(count($proponent_topic['file']) > 0) {

                                    if($proponent_topic['main']->files()->count() > 0) {
                                        $proponent_topic['main']->files()->delete();
                                    }

                                    $proponent_topic['main']->files()->saveMany($proponent_topic['file']);
                                }
                            }
                        }

                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'บันทึกสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'บันทึกไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                } else {
                    $results->status        = false;
                    $results->icon          = 'error';
                    $results->message       = 'บันทึกไม่สำเร็จ';
                    $results->redirect_url  = '';
                }
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';

        if(Crypt::decryptString($id)) {


            DB::beginTransaction();

            try {

                $propose_topic = ProposeTopic::find(Crypt::decryptString($id));

                if($propose_topic) {
                    if($propose_topic->delete()){
                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'ลบรายการสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'ลบรายการไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                }

            } catch(ValidationException $e) {
                
                DB::rollback();

                return Response::json($e->getErrors());

            } catch (\Exception $e) {

                DB::rollback();

                throw $e;
            }

            DB::commit();
       
            return Response::json($results);
        }
    }
    
    public function rules()
    {
        //
        $rules = [

        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [

        ];
        return $messages;
    }

    public function datatables(Request $request) {

        $user = Auth::user();

        $columns = [
            0 =>'propose_topics.name',
            1 =>'propose_topics.description',
            2 =>'propose_topics.updated_at',
            3 =>'updated_name',
        ];
  
        $datas = ProposeTopic::getList();
      
        $totalData = $datas->get()->count();
        $totalFiltered = $totalData;

        $start = $request->input( 'start' );
        $length = $request->input( 'length' );

        if ($request->has('search')) {
            if ($request->input ( 'search.value' ) != '') {
                
                $searchTerm = $request->input('search.value');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where( 'propose_topics.name', 'Like', '%' . $searchTerm . '%' );
                    $query->orWhere( 'propose_topics.description', 'Like', '%' . $searchTerm . '%' );
                });
            }
        }

        if($user->roles()->first()->id == 3) {
            $datas = $datas->where( 'propose_topic_proponent.user_id', $user->id );
        }
    
        if ($request->has('order')) {
            if ($request->input('order.0.column') != '') {

                $orderColumn = $request->input('order.0.column');
                $orderDirection = $request->input('order.0.dir');

                $data_count = $datas;
                $totalFiltered = $data_count->get()->count();

                $datas = $datas->orderBy($columns[intval( $orderColumn )], $orderDirection )->skip($start)->take($length)->get();
            }
        }
        else {
            $datas = $datas->orderBy('propose_topics.updated_at', 'ASC' )->skip($start)->take($length)->get();
        }

        $data = [];
        foreach ( $datas as $key => $col ) {
            $nestedData = [];
       
            // dd(Carbon::createFromFormat('Y-m-d H:i:s', str_replace('.0000000', '', date('Y-m-d ').$col->end_time), 'Asia/Bangkok')->format('H.i น.'));
            $time = !empty($col->end_time) ? Carbon::createFromFormat('Y-m-d H:i:s', str_replace('.0000000', '', date('Y-m-d ').$col->end_time), 'Asia/Bangkok')->format('H.i น.') : '';
            $end_date = !empty($col->end_date) ? Carbon::createFromFormat('Y-m-d', $col->end_date, 'Asia/Bangkok')->modify('+543 year')->format('d/m/Y') : '';

            $nestedData[0] = !empty($col->meeting_name) ? $col->meeting_name : '-';
            $nestedData[1] = !empty($col->agenda) ? $col->agenda : '-';
            $nestedData[2] = !empty($col->date) ? Carbon::createFromFormat('Y-m-d', $col->date, 'Asia/Bangkok')->modify('+543 year')->format('d/m/Y')  : '-';
            $nestedData[3] = !empty($end_date) ? $end_date . (!empty($time) ? " {$time}" : '')  : '-';
            $nestedData[4] = !empty($col->meeting_organization_name) ? $col->meeting_organization_name : '-';

            $nestedData[5] = '<div class="dropdown">';
                $nestedData[5] .= '<button class="btn btn-secondary option dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    $nestedData[5] .= 'ดำเนินการ';
                $nestedData[5] .= '</button>';
                $nestedData[5] .= '<div class="dropdown-menu">';
                
                    if($user->roles()->first()->id == 2) {
                        $nestedData[5] .= '<a class="dropdown-item text-dark" href="'.(route('pages.proposetopics.show',['id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ])).'"><i class="fas fa-address-card"></i> เสนอหัวข้อ</a>';
                    }

                    if($user->roles()->first()->id == 3) {
                        $nestedData[5] .= '<a class="dropdown-item text-dark" href="'.(route('pages.proposetopics.propose',['id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ])).'"><i class="fas fa-address-card"></i> เสนอหัวข้อ</a>';
                    }

                    if($user->roles()->first()->id == 2) {
                        $nestedData[5] .= '<a class="dropdown-item text-dark" href="'.(route('pages.proposetopics.edit',['id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ])).'"><i class="far fa-edit"></i> แก้ไข</a>';
                        $nestedData[5] .= '<a class="dropdown-item text-danger btn-delete">';
                            $nestedData[5] .= Form::open(['route' => ['pages.proposetopics.destroy', 'id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ], 'style' => 'display : none;', 'method' => 'DELETE', 'enctype' => 'multipart/form-data', 'id' => 'from_'.$key]);
                            $nestedData[5] .= Form::close();
                            $nestedData[5] .= '<i class="far fa-trash-alt"></i> ลบ';
                        $nestedData[5] .= '</a>';
                    }

                $nestedData[5] .= '</div>';
            $nestedData[5] .= '</div>';
    
            $data [] = $nestedData;
        }

        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }

    public function meetingdatatables(Request $request) {

        $user = Auth::user();
     
        $datas = ProposeTopic::getTopicList()->where('propose_topics.meeting_id', Crypt::decryptString($request->meeting_id));
    
        $totalData = $datas->get()->count();
        $totalFiltered = $totalData;
  
        $data = [];
        foreach ( $datas->get() as $key => $col ) {
            $nestedData = [];
            $file = [];
            
            $file_txt = "";
            if($propose_topic_proponent = $col->proposetopicproponent()->where('user_id', $col->user_id)->first()) {
                if($propose_topic_proponent->proponenttopic()->count() > 0) {
                    foreach ($propose_topic_proponent->proponenttopic as $key => $topic) {
                    
                        if($topic->files()->get()->count() > 0) {
                            foreach ($topic->files()->get() as $item) {
                                $files = $item->file()->first();
                                if($files) {
                                    $files->urlencode_path = Crypt::encryptString($files->path);
                                    $files->path = asset(Storage::disk('local')->url($files->path));
                                    $file[] = (object)[
                                        'id'    => $files->id,
                                        'name'  => $files->originalName,
                                        'size'  => $files->size,
                                        'type'  => $files->mimeType,
                                        'url'   => $files->path
                                    ];

                                    $file_txt .= $files->originalName."<br>";
                                }
                            };
                        }
                    }
                }
            }
         
            if($request->has('is_show') && $request->input('is_show', false)) {
                $nestedData[0] = !empty($col->topic) ? $col->topic : '-';
                $nestedData[1] = !empty($col->detail) ? $col->detail : '-';
                $nestedData[2] = $file_txt."<span class='d-none'>".json_encode($file)."</span>";
                $nestedData[3] = !empty($col->proposer_name) ? $col->proposer_name : '-';
            } else {
                $nestedData[0] = isset($col->id) ? Html::decode(Form::checkbox('propose_topics[]', Crypt::encryptString($col->id), false, [])) : '-';
                $nestedData[1] = !empty($col->topic) ? $col->topic : '-';
                $nestedData[2] = !empty($col->detail) ? $col->detail : '-';
                $nestedData[3] = $file_txt."<span class='d-none'>".json_encode($file)."</span>";
                $nestedData[4] = !empty($col->proposer_name) ? $col->proposer_name : '-';
            }

            $data [] = $nestedData;
        }

        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }
}
