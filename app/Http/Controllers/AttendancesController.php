<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Crypt, Storage, Response};
use TechEx\{Attendance, Title, Organization, User, Role};
use Collective\Html\FormFacade as Form;
use TechEx\Http\Controllers\Core;
use Carbon\Carbon;
use Validator;
use Auth;

class AttendancesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];

        return view('pages.attendances.index',[
            'result' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $result = [];

        $titles_arr = [];
        $titles = Title::selection();
        if($titles->count() > 0) {
            foreach ($titles->get() as $title) {
                $titles_arr[$title->id] = $title->name;
            }
        }
        unset($titles);

        $organizations_arr = [];
        $organizations = Organization::selection();
        if($organizations->count() > 0) {
            foreach ($organizations->get() as $org) {
                $organizations_arr[$org->id] = $org->name;
            }
        }
        unset($organizations);

        $roles_arr = [];
        $roles = Role::selection();
        if($roles->count() > 0) {
            foreach ($roles->get() as $org) {
                $roles_arr[$org->id] = $org->name;
            }
        }
        unset($roles);

        $result = [
            'role'                  => $roles_arr,
            'title'                 => $titles_arr,
            'org'                   => $organizations_arr,
        ];

        return view('pages.attendances.create',[
            'result' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $users = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();

            try {
         
                $attendance = new Attendance();
                $attendance->organization_id             = $request->input('org', '');
                $attendance->title_id                    = $request->input('title', '');
                $attendance->first_name                  = $request->input('first_name', '');
                $attendance->last_name                   = $request->input('last_name', '');
                $attendance->position                    = $request->input('position', '');
                $attendance->phone                       = $request->input('phone', '');
                $attendance->other                       = $request->input('other', '');
                $attendance->email                       = $request->input('email', '');
                $attendance->is_external                 = $request->input('is_external', 0);
                $attendance->is_enabled                  = $request->input('is_enabled', 0);
                $attendance->created_by                  = $users->id;
                $attendance->created_at                  = Carbon::now();

                $username = explode('@', $request->input('email', ''));
                $username = reset($username);
              
                $user = new User();
                $user->username         = $username;
                $user->title_id         = $request->input('title', '');
                $user->organization_id  = $request->input('org', '');
                $user->email            = $request->input('email', '');
                $user->first_name       = $request->input('first_name', '');
                $user->last_name        = $request->input('last_name', '');
                $user->position         = $request->input('position', '');
                $user->phone            = $request->input('phone', '');
                $user->password         = bcrypt(Core::uniqidReal());
                $user->created_by       = $users->id;
                $user->created_at       = Carbon::now();
                $user->save();
               
                if($attendance->save()){
                    $results->status         = true;
                    $results->icon           = 'success';
                    $results->message        = 'บันทึกสำเร็จ';
                    $results->redirect_url   = route('pages.attendances.index');
                } else {
                    $results->status        = false;
                    $results->icon          = 'error';
                    $results->message       = 'บันทึกไม่สำเร็จ';
                    $results->redirect_url  = '';
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = [];

        if(Crypt::decryptString($id)) {

            $attendance = Attendance::find(Crypt::decryptString($id));
            if(!empty($attendance)) {

                $titles_arr = [];
                $titles = Title::selection();
                if($titles->count() > 0) {
                    foreach ($titles->get() as $title) {
                        $titles_arr[$title->id] = $title->name;
                    }
                }
                unset($titles);
        
                $organizations_arr = [];
                $organizations = Organization::selection();
                if($organizations->count() > 0) {
                    foreach ($organizations->get() as $org) {
                        $organizations_arr[$org->id] = $org->name;
                    }
                }
                unset($organizations);
        
                $roles_arr = [];
                $roles = Role::selection();
                if($roles->count() > 0) {
                    foreach ($roles->get() as $org) {
                        $roles_arr[$org->id] = $org->name;
                    }
                }
                unset($roles);
          
                $result = [
                    'data' => (object)[
                        'id'                    => Crypt::encryptString($attendance->id),
                        'org'                   => isset($attendance->organization_id) ? $attendance->organization_id : '',
                        'title'                 => isset($attendance->title_id) ? $attendance->title_id : '',
                        'position_in_meeting'   => isset($attendance->position_in_meeting_id) ? $attendance->position_in_meeting_id : '',
                        'first_name'            => isset($attendance->first_name) ? $attendance->first_name : '',
                        'last_name'             => isset($attendance->last_name) ? $attendance->last_name : '',
                        'position'              => isset($attendance->position) ? $attendance->position : '',
                        'phone'                 => isset($attendance->phone) ? $attendance->phone : '',
                        'other'                 => isset($attendance->other) ? $attendance->other : '',
                        'email'                 => isset($attendance->email) ? $attendance->email : '',
                        'is_external'           => isset($attendance->is_external) ? $attendance->is_external : '',
                        'is_enabled'            => isset($attendance->is_enabled) ? $attendance->is_enabled : '',
                    ],
                    'role'                  => $roles_arr,
                    'title'                 => $titles_arr,
                    'org'                   => $organizations_arr,
                ];
            }
            unset($title);
        }

        return view('pages.attendances.edit',[
            'result' => $result
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        if(Crypt::decryptString($id)) {
            $validator = Validator::make($request->all(), $this->rules(), $this->messages());
            if($validator->fails()){
                return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
            } else {
        
                DB::beginTransaction();

                try {
            
                    $attendance = Attendance::find(Crypt::decryptString($id));
                    $attendance->organization_id            = $request->input('org', '');
                    $attendance->title_id                   = $request->input('title', '');
                    $attendance->first_name                 = $request->input('first_name', '');
                    $attendance->last_name                  = $request->input('last_name', '');
                    $attendance->position                   = $request->input('position', '');
                    $attendance->phone                      = $request->input('phone', '');
                    $attendance->other                      = $request->input('other', '');
                    $attendance->email                      = $request->input('email', '');
                    $attendance->is_external                = $request->input('is_external', 0);
                    $attendance->is_enabled                 = $request->input('is_enabled', 0);
                    $attendance->updated_by                 = $user->id;
                    $attendance->updated_at                 = Carbon::now();
                   
                    if($attendance->save()){
                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'บันทึกสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'บันทึกไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                    
                } catch(ValidationException $e) {
                    
                    DB::rollback();
    
                    return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);
    
                } catch (\Exception $e) {
    
                    DB::rollback();
                    throw $e;
                }
    
                DB::commit();
       
                return response()->json($results);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';

        if(Crypt::decryptString($id)) {


            DB::beginTransaction();

            try {

                $title = Title::find(Crypt::decryptString($id));

                if($title) {
                    if($title->delete()){
                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'ลบรายการสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'ลบรายการไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                }

            } catch(ValidationException $e) {
                
                DB::rollback();

                return Response::json($e->getErrors());

            } catch (\Exception $e) {

                DB::rollback();

                throw $e;
            }

            DB::commit();
       
            return Response::json($results);
        }
    }

    public function rules()
    {
        //
        $rules = [

        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [

        ];
        return $messages;
    }

    public function datatables(Request $request) {

        $user = Auth::user();

        $columns = [
            0 =>DB::raw("CONCAT(attendances.first_name , CHAR(32), attendances.last_name)"),
            1 =>'organization_name',
            2 =>'is_external_name',
            3 =>'attendances.updated_at',
            4 =>'updated_name',
        ];

        $datas = Attendance::getList();
    
        $totalData = $datas->get()->count();
        $totalFiltered = $totalData;

        $start = $request->input( 'start' );
        $length = $request->input( 'length' );

        if ($request->has('search')) {
            if ($request->input ( 'search.value' ) != '') {
                
                $searchTerm = $request->input('search.value');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where(DB::raw("CONCAT(attendances.first_name , CHAR(32), attendances.last_name)"), 'Like', '%' . $searchTerm . '%' );
                    $query->orWhere( 'organizations.name', 'Like', '%' . $searchTerm . '%' );
                });
            }
        }

        if ($request->has('order')) {
            if ($request->input('order.0.column') != '') {

                $orderColumn = $request->input('order.0.column');
                $orderDirection = $request->input('order.0.dir');

                $data_count = $datas;
                $totalFiltered = $data_count->get()->count();

                $datas = $datas->orderBy($columns[intval( $orderColumn )], $orderDirection )->skip($start)->take($length)->get();
            }
        }
        else {
            $datas = $datas->orderBy(DB::raw("CONCAT(attendances.first_name , CHAR(32), attendances.last_name)"), 'ASC' )->skip($start)->take($length)->get();
        }

        $data = [];
        foreach ( $datas as $key => $col ) {
            $nestedData = [];
         
            $nestedData[0] = !empty($col->name) ? $col->name : '-';
            $nestedData[1] = !empty($col->organization_name) ? $col->organization_name : '-';
            $nestedData[2] = !empty($col->is_external_name) ? $col->is_external_name : '-';
            $nestedData[3] = !empty($col->updated_at) ? $col->updated_at->modify('+543 year')->format('d/m/Y H.i น.') : '-';
            $nestedData[4] = !empty($col->updated_name) ? $col->updated_name : '-';

            $nestedData[5] = '<div class="dropdown">';
                $nestedData[5] .= '<button class="btn btn-secondary option dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    $nestedData[5] .= 'ดำเนินการ';
                $nestedData[5] .= '</button>';
                $nestedData[5] .= '<div class="dropdown-menu">';
                    $nestedData[5] .= '<a class="dropdown-item text-dark" href="'.(route('pages.attendances.edit',['id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ])).'"><i class="far fa-edit"></i> แก้ไข</a>';
                    $nestedData[5] .= '<a class="dropdown-item text-danger btn-delete">';
                        $nestedData[5] .= Form::open(['route' => ['pages.attendances.destroy', 'id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ], 'style' => 'display : none;', 'method' => 'DELETE', 'enctype' => 'multipart/form-data', 'id' => 'from_'.$key]);
                        $nestedData[5] .= Form::close();
                        $nestedData[5] .= '<i class="far fa-trash-alt"></i> ลบ';
                    $nestedData[5] .= '</a>';
                $nestedData[5] .= '</div>';
            $nestedData[5] .= '</div>';
    
            $data [] = $nestedData;
        }

        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }

    public function selectsapi(Request $request) {
        
        $selected_id = !empty($request->selected_id) ? Crypt::decryptString($request->selected_id) : null;

        $datas = Attendance::getList();
   
        if ($request->has('search')) {
            $searchTerm = $request->input('search');
            $datas = $datas->where(function($query) use ($searchTerm) {
                $query->where(DB::raw("CONCAT(attendances.first_name , CHAR(32), attendances.last_name)"), 'Like', '%' . $searchTerm . '%' );
            });
        }
    
        $datas = $datas->orderBy("name", 'ASC' )->take(10)->get();
      
        $option = [];
        if($datas->count() > 0) {
            foreach ($datas as $item) {
                $selected = false;

                if($selected_id == $item->id) {
                    $selected = true;
                }
              
                $option[] = (object)[
                    'id'                        => Crypt::encryptString($item->id),
                    'text'                      => $item->name,
                    'position'                  => $item->position,
                    'tel'                       => $item->phone,
                    'email'                     => $item->email,
                    'other'                     => $item->other,
                    'org'                       => $item->organization_name,
                    'position_in_meeting_id'    => Crypt::encryptString($item->position_in_meeting_id),
                    'selected'                  => $selected
                ];
            }
        }

        return Response::json($option);
    }
}
