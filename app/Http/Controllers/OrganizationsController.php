<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Crypt, Storage, Response};
use TechEx\{Organization};
use Collective\Html\FormFacade as Form;
use TechEx\Http\Controllers\Core;
use Carbon\Carbon;
use Validator;
use Auth;


class OrganizationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];

        return view('pages.organizations.index',[
            'result' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $result = [];

        return view('pages.organizations.create',[
            'result' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();
            
            try {
             
                $organization = new Organization();
                $organization->code          = Core::uniqidReal();
                $organization->name          = $request->input('name', '');
                $organization->initials      = $request->input('initials', '');
                $organization->note          = $request->input('note', '');
                $organization->lang          = $request->input('lang', '');
                $organization->sort_no       = $request->input('sort_no', 0);
                $organization->is_enabled    = $request->input('is_enabled', 0);
                $organization->created_by    = $user->id;
                $organization->created_at    = Carbon::now();
  
                if($organization->save()){
                    $results->status         = true;
                    $results->icon           = 'success';
                    $results->message        = 'บันทึกสำเร็จ';
                    $results->redirect_url   = route('pages.organizations.index');
                } else {
                    $results->status        = false;
                    $results->icon          = 'error';
                    $results->message       = 'บันทึกไม่สำเร็จ';
                    $results->redirect_url  = '';
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = [];

        if(Crypt::decryptString($id)) {

            $organization = Organization::find(Crypt::decryptString($id));

            if(!empty($organization)) {

                $result['data'] = (object)[
                    'id'            => Crypt::encryptString($organization->id),
                    'name'          => isset($organization->name) ? $organization->name : '',
                    'initials'      => isset($organization->initials) ? $organization->initials : '',
                    'note'          => isset($organization->note) ? $organization->note : '',
                    'sort_no'       => isset($organization->sort_no) ? $organization->sort_no : '',
                    'is_enabled'    => isset($organization->is_enabled) ? $organization->is_enabled : '',
                ];
            }
            unset($organization);
        }

        return view('pages.organizations.edit',[
            'result' => $result
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        if(Crypt::decryptString($id)) {
            $validator = Validator::make($request->all(), $this->rules(), $this->messages());
            if($validator->fails()){
                return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
            } else {
        
                DB::beginTransaction();

                try {

                    $organization = Organization::find(Crypt::decryptString($id));
                    $organization->name         = $request->input('name', '');
                    $organization->initials     = $request->input('initials', '');
                    $organization->note         = $request->input('note', '');
                    $organization->lang         = $request->input('lang', '');
                    $organization->sort_no      = $request->input('sort_no', 0);
                    $organization->is_enabled   = $request->input('is_enabled', 0);
                    $organization->updated_by   = $user->id;
                    $organization->updated_at   = Carbon::now();
                   
                    if($organization->save()){
                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'บันทึกสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'บันทึกไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                    
                } catch(ValidationException $e) {
                    
                    DB::rollback();
    
                    return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);
    
                } catch (\Exception $e) {
    
                    DB::rollback();
                    throw $e;
                }
    
                DB::commit();
       
                return response()->json($results);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';

        if(Crypt::decryptString($id)) {


            DB::beginTransaction();

            try {

                $organization = Organization::find(Crypt::decryptString($id));

                if($organization) {
                    if($organization->delete()){
                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'ลบรายการสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'ลบรายการไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                }

            } catch(ValidationException $e) {
                
                DB::rollback();

                return Response::json($e->getErrors());

            } catch (\Exception $e) {

                DB::rollback();

                throw $e;
            }

            DB::commit();
       
            return Response::json($results);
        }
    }

    public function rules()
    {
        //
        $rules = [

        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [

        ];
        return $messages;
    }

    public function datatables(Request $request) {

        $user = Auth::user();

        $columns = [
            0 =>'organizations.name',
            1 =>'organizations.initials',
            2 =>'organizations.is_enabled',
            3 =>'organizations.updated_at',
            4 =>'updated_name',
        ];

        $datas = Organization::getList();
    
        $totalData = $datas->get()->count();
        $totalFiltered = $totalData;

        $start = $request->input( 'start' );
        $length = $request->input( 'length' );

        if ($request->has('search')) {
            if ($request->input ( 'search.value' ) != '') {
                
                $searchTerm = $request->input('search.value');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where( 'organizations.name', 'Like', '%' . $searchTerm . '%' );
                    $query->orWhere( 'organizations.initials', 'Like', '%' . $searchTerm . '%' );
                });
            }
        }

        if ($request->has('order')) {
            if ($request->input('order.0.column') != '') {

                $orderColumn = $request->input('order.0.column');
                $orderDirection = $request->input('order.0.dir');

                $data_count = $datas;
                $totalFiltered = $data_count->get()->count();

                $datas = $datas->orderBy($columns[intval( $orderColumn )], $orderDirection )->skip($start)->take($length)->get();
            }
        }
        else {
            $datas = $datas->orderBy('sort_no', 'ASC' )->skip($start)->take($length)->get();
        }

        $data = [];
        foreach ( $datas as $key => $col ) {
            $nestedData = [];
         
            $nestedData[0] = !empty($col->name) ? $col->name : '-';
            $nestedData[1] = !empty($col->initials) ? $col->initials : '-';
            $nestedData[2] = !empty($col->is_enabled) ? 'เปิด' : 'ปิด';
            $nestedData[3] = !empty($col->updated_at) ? $col->updated_at->modify('+543 year')->format('d/m/Y H.i น.') : '-';
            $nestedData[4] = !empty($col->updated_name) ? $col->updated_name : '-';
    
            $nestedData[5] = '<div class="dropdown">';
                $nestedData[5] .= '<button class="btn btn-secondary option dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    $nestedData[5] .= 'ดำเนินการ';
                $nestedData[5] .= '</button>';
                $nestedData[5] .= '<div class="dropdown-menu">';
                    $nestedData[5] .= '<a class="dropdown-item text-dark" href="'.(route('pages.organizations.edit',['id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ])).'"><i class="far fa-edit"></i> แก้ไข</a>';
                    $nestedData[5] .= '<a class="dropdown-item text-danger btn-delete">';
                        $nestedData[5] .= Form::open(['route' => ['pages.organizations.destroy', 'id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ], 'style' => 'display : none;', 'method' => 'DELETE', 'enctype' => 'multipart/form-data', 'id' => 'from_'.$key]);
                        $nestedData[5] .= Form::close();
                        $nestedData[5] .= '<i class="far fa-trash-alt"></i> ลบ';
                    $nestedData[5] .= '</a>';
                $nestedData[5] .= '</div>';
            $nestedData[5] .= '</div>';

            $data [] = $nestedData;
        }

        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }

    
    public function selects(Request $request) {
     
        $selected_id = !empty($request->selected_id) ? Crypt::decryptString($request->selected_id) : null;
    
        $option = [];
        if(Organization::count() > 0) {
            foreach (Organization::all() as $item) {
                $selected = false;

                if($selected_id == $item->id) {
                    $selected = true;
                }

                $option[] = (object)[
                    'id'        => Crypt::encryptString($item->id),
                    'text'      => $item->name,
                    'selected'  => $selected
                ];
            }
        }

        return Response::json($option);
    }

    public function selectsapi(Request $request) {
     
        $selected_id = !empty($request->selected_id) ? Crypt::decryptString($request->selected_id) : null;
        
        $datas = Organization::getList();
   
        if ($request->has('search')) {
            $searchTerm = $request->input('search');
            $datas = $datas->where(function($query) use ($searchTerm) {
                $query->where('name', 'Like', '%' . $searchTerm . '%' );
            });
        }
     
        $datas = $datas->orderBy("name", 'ASC' )->take(10)->get();
      
        $option = [];
        if($datas->count() > 0) {
            foreach ($datas as $item) {
                $selected = false;

                if($selected_id == $item->id) {
                    $selected = true;
                }
              
                $option[] = (object)[
                    'id'        => Crypt::encryptString($item->id),
                    'text'      => $item->name,
                    'selected'  => $selected
                ];
            }
        }

        return Response::json($option);
    }
}
