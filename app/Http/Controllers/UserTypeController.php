<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Crypt, Storage, Response};
use TechEx\{User, Menu, Permission, Role, RolesMenusPermissions};
use Collective\Html\FormFacade as Form;
use TechEx\Http\Controllers\Core;
use Carbon\Carbon;
use Validator;
use Auth;

class UserTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];

        return view('pages.usertype.index',[
            'result' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $result = [];

        $result = [
            'permission'    => Permission::all(),
            'menu'          => Menu::all()
        ];
        
        return view('pages.usertype.create',[
            'result' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();
            
            try {

                $role = new Role();
                $role->name          = $request->input('name', '');
                $role->note          = $request->input('note', '');
                $role->created_by    = $user->id;
                $role->created_at    = Carbon::now();
               
                if(count($request->input('menu', [])) > 0) {
                    foreach ($request->input('menu') as $key => $rmp) {
                        if(count($request->input("menu.{$key}.permissions", [])) > 0) {
                            foreach ($request->input("menu.{$key}.permissions") as $i => $p) {
                                $roles_menus_permissions = new RolesMenusPermissions();
                                $roles_menus_permissions->menu_id       = $request->input("menu.{$key}.id", null);
                                $roles_menus_permissions->permission_id = $p;
                                $roles_menus_permissions->is_enabled    = $request->input("menu.{$key}.is_enabled", 1);
                                $roles_menus_permissions_arr[] = $roles_menus_permissions;
                            }
                        }
                    }
                }

                if($role->save()) {

                    if(count($roles_menus_permissions_arr) > 0) {
                        $role->roles_menus_permissions()->saveMany($roles_menus_permissions_arr);
                    }

                    $results->status         = true;
                    $results->icon           = 'success';
                    $results->message        = 'บันทึกสำเร็จ';
                    $results->redirect_url   = route('pages.usertype.index');
                } else {
                    $results->status        = false;
                    $results->icon          = 'error';
                    $results->message       = 'บันทึกไม่สำเร็จ';
                    $results->redirect_url  = '';
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = [];

        if(Crypt::decryptString($id)) {

            $role = Role::find(Crypt::decryptString($id));

            if(!empty($role)) {

                $menus_permissions_arr = [];
                if($role->roles_menus_permissions()->count() > 0) {
                    foreach ($role->roles_menus_permissions()->get() as $key => $rmp) {
                        $menus_permissions_arr[$rmp->menu_id][$rmp->permission_id]['checked'] = true;
                        $menus_permissions_arr[$rmp->menu_id][$rmp->permission_id]['is_enabled'] = $rmp->is_enabled;
                    }
                }
            
                $result = [
                    'data' =>  (object)[
                        'id'            => Crypt::encryptString($role->id),
                        'name'          => isset($role->name) ? $role->name : '',
                        'note'          => isset($role->note) ? $role->note : '',
                    ],
                    'permission'        => Permission::all(),
                    'menu'              => Menu::all(),
                    'menus_permissions' => $menus_permissions_arr
                ];
            }
            unset($position_in_meeting);
        }

        return view('pages.usertype.edit',[
            'result' => $result
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        if(Crypt::decryptString($id)) {
            $validator = Validator::make($request->all(), $this->rules(), $this->messages());
            if($validator->fails()){
                return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
            } else {
        
                DB::beginTransaction();

                try {
            
                    $role = Role::find(Crypt::decryptString($id));
                    $role->name          = $request->input('name', '');
                    $role->note          = $request->input('note', '');
                    $role->updated_by    = $user->id;
                    $role->updated_at    = Carbon::now();
                
                    if(count($request->input('menu', [])) > 0) {
                        foreach ($request->input('menu') as $key => $rmp) {
                            if(count($request->input("menu.{$key}.permissions", [])) > 0) {
                                foreach ($request->input("menu.{$key}.permissions") as $i => $p) {
                                    $roles_menus_permissions = new RolesMenusPermissions();
                                    $roles_menus_permissions->menu_id       = $request->input("menu.{$key}.id", null);
                                    $roles_menus_permissions->permission_id = $p;
                                    $roles_menus_permissions->is_enabled    = $request->input("menu.{$key}.is_enabled", 1);
                                    $roles_menus_permissions_arr[] = $roles_menus_permissions;
                                }
                            }
                        }
                    }

                    if($role->save()){

                        if(count($roles_menus_permissions_arr) > 0) {

                            if($role->roles_menus_permissions()->count() > 0) {
                                $role->roles_menus_permissions()->delete();
                            }

                            $role->roles_menus_permissions()->saveMany($roles_menus_permissions_arr);
                        }

                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'บันทึกสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'บันทึกไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                    
                } catch(ValidationException $e) {
                    
                    DB::rollback();
    
                    return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);
    
                } catch (\Exception $e) {
    
                    DB::rollback();
                    throw $e;
                }
    
                DB::commit();
       
                return response()->json($results);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';

        if(Crypt::decryptString($id)) {

            DB::beginTransaction();

            try {

                $role = Role::find(Crypt::decryptString($id));

                if($role) {
                    if($role->delete()){
                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'ลบรายการสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'ลบรายการไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                }
                
            } catch(ValidationException $e) {
                
                DB::rollback();

                return Response::json($e->getErrors());

            } catch (\Exception $e) {

                DB::rollback();

                throw $e;
            }

            DB::commit();
       
            return Response::json($results);
        }
    }
    
    public function rules()
    {
        //
        $rules = [

        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [

        ];
        return $messages;
    }

    public function datatables(Request $request) {

        $user = Auth::user();

        $columns = [
            0 =>'roles.name',
            1 =>'roles.updated_at',
            2 =>'updated_name',
        ];

        $datas = Role::getList();
    
        $totalData = $datas->get()->count();
        $totalFiltered = $totalData;

        $start = $request->input( 'start' );
        $length = $request->input( 'length' );

        if ($request->has('search')) {
            if ($request->input ( 'search.value' ) != '') {
                
                $searchTerm = $request->input('search.value');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where( 'roles.name', 'Like', '%' . $searchTerm . '%' );
                });
            }
        }

        if ($request->has('order')) {
            if ($request->input('order.0.column') != '') {

                $orderColumn = $request->input('order.0.column');
                $orderDirection = $request->input('order.0.dir');

                $data_count = $datas;
                $totalFiltered = $data_count->get()->count();

                $datas = $datas->orderBy($columns[intval( $orderColumn )], $orderDirection )->skip($start)->take($length)->get();
            }
        }
        else {
            $datas = $datas->orderBy('name', 'ASC' )->skip($start)->take($length)->get();
        }

        $data = [];
        foreach ( $datas as $key => $col ) {
            $nestedData = [];
         
            $nestedData[0] = !empty($col->name) ? $col->name : '-';
            $nestedData[1] = !empty($col->updated_at) ? $col->updated_at->modify('+543 year')->format('d/m/Y H.i น.') : '-';
            $nestedData[2] = !empty($col->updated_name) ? $col->updated_name : '-';

            $nestedData[3] = '<div class="dropdown">';
                $nestedData[3] .= '<button class="btn btn-secondary option dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    $nestedData[3] .= 'ดำเนินการ';
                $nestedData[3] .= '</button>';
                $nestedData[3] .= '<div class="dropdown-menu">';
                    $nestedData[3] .= '<a class="dropdown-item text-dark" href="'.(route('pages.usertype.edit',['id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ])).'"><i class="far fa-edit"></i> แก้ไข</a>';
                    $nestedData[3] .= '<a class="dropdown-item text-danger btn-delete">';
                        $nestedData[3] .= Form::open(['route' => ['pages.usertype.destroy', 'id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ], 'style' => 'display : none;', 'method' => 'DELETE', 'enctype' => 'multipart/form-data', 'id' => 'from_'.$key]);
                        $nestedData[3] .= Form::close();
                        $nestedData[3] .= '<i class="far fa-trash-alt"></i> ลบ';
                    $nestedData[3] .= '</a>';
                $nestedData[3] .= '</div>';
            $nestedData[3] .= '</div>';
    
            $data [] = $nestedData;
        }

        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }
}
