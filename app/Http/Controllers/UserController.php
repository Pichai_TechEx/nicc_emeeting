<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Crypt, Storage, Response};
use TechEx\{User, Role, Title, Organization, Attendance};
use Collective\Html\FormFacade as Form;
use TechEx\Http\Controllers\Core;
use Carbon\Carbon;
use Validator;
use Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];

        return view('pages.user.index',[
            'result' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $result = [];

        $titles_arr = [];
        $titles = Title::selection();
        if($titles->count() > 0) {
            foreach ($titles->get() as $title) {
                $titles_arr[$title->id] = $title->name;
            }
        }
        unset($titles);

        $organizations_arr = [];
        $organizations = Organization::selection();
        if($organizations->count() > 0) {
            foreach ($organizations->get() as $org) {
                $organizations_arr[$org->id] = $org->name;
            }
        }
        unset($organizations);

        $roles_arr = [];
        $roles = Role::selection();
        if($roles->count() > 0) {
            foreach ($roles->get() as $org) {
                $roles_arr[$org->id] = $org->name;
            }
        }
        unset($roles);

        $result = [
            'role'  => $roles_arr,
            'title' => $titles_arr,
            'org'   => $organizations_arr
        ];
    
        return view('pages.user.create',[
            'result' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();
         
            try {
              
                $users = new User();
                $users->username        = $request->input('username', '');
                $users->email           = $request->input('email', '');
                $users->title_id        = $request->input('title_id', '');
                $users->first_name      = $request->input('first_name', '');
                $users->last_name       = $request->input('last_name', '');
                $users->organization_id = $request->input('organization_id', '');
                $users->position        = $request->input('position', '');
                $users->phone           = $request->input('phone', '');
                $users->password        = bcrypt($request->input('password', ''));
                $users->is_enabled      = $request->input('is_enabled', 0);
                $users->is_login        = $request->input('is_login', 0);
                $users->created_by      = $user->id;
                $users->created_at      = Carbon::now();
          
                if($users->save()){

                    if($request->has('role')) {
                        $users->roles()->sync([$request->input('role', '')]);
                    }
                    
                    $results->status         = true;
                    $results->icon           = 'success';
                    $results->message        = 'บันทึกสำเร็จ';
                    $results->redirect_url   = route('pages.user.index');
                } else {
                    $results->status        = false;
                    $results->icon          = 'error';
                    $results->message       = 'บันทึกไม่สำเร็จ';
                    $results->redirect_url  = '';
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = [];

        if(Crypt::decryptString($id)) {

            $user = User::find(Crypt::decryptString($id));

            if(!empty($user)) {

                $titles_arr = [];
                $titles = Title::selection();
                if($titles->count() > 0) {
                    foreach ($titles->get() as $title) {
                        $titles_arr[$title->id] = $title->name;
                    }
                }
                unset($titles);
        
                $organizations_arr = [];
                $organizations = Organization::selection();
                if($organizations->count() > 0) {
                    foreach ($organizations->get() as $org) {
                        $organizations_arr[$org->id] = $org->name;
                    }
                }
                unset($organizations);
        
                $roles_arr = [];
                $roles = Role::selection();
                if($roles->count() > 0) {
                    foreach ($roles->get() as $org) {
                        $roles_arr[$org->id] = $org->name;
                    }
                }
                unset($roles);
            
                $result = [
                    'data' => (object)[
                        'id'                => Crypt::encryptString($user->id),
                        'username'          => isset($user->username) ? $user->username : '',
                        'email'             => isset($user->email) ? $user->email : '',
                        'title_id'          => isset($user->title_id) ? $user->title_id : '',
                        'first_name'        => isset($user->first_name) ? $user->first_name : '',
                        'last_name'         => isset($user->last_name) ? $user->last_name : '',
                        'organization_id'   => isset($user->organization_id) ? $user->organization_id : '',
                        'position'          => isset($user->position) ? $user->position : '',
                        'phone'             => isset($user->phone) ? $user->phone : '',
                        'role'              => !empty($user->roles()->first()) ? $user->roles()->first()->id : '',
                        'is_enabled'        => isset($user->is_enabled) ? $user->is_enabled : '',
                        'is_login'          => isset($user->is_login) ? $user->is_login : '',
                    ],
                    'role'  => $roles_arr,
                    'title' => $titles_arr,
                    'org'   => $organizations_arr
                ];
            }
         
            unset($position_in_meeting);
        }

        return view('pages.user.edit',[
            'result' => $result
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        if(Crypt::decryptString($id)) {
            $validator = Validator::make($request->all(), $this->rules(), $this->messages());
            if($validator->fails()){
                return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
            } else {
        
                DB::beginTransaction();

                try {
            
                    $users = User::find(Crypt::decryptString($id));
                    $users->username        = $request->input('username', '');
                    $users->email           = $request->input('email', '');
                    $users->title_id        = $request->input('title_id', '');
                    $users->first_name      = $request->input('first_name', '');
                    $users->last_name       = $request->input('last_name', '');
                    $users->organization_id = $request->input('organization_id', '');
                    $users->position        = $request->input('position', '');
                    $users->phone           = $request->input('phone', '');

                    if($request->has('password') && $request->has('confirmed') || $request->input('password') == $request->input('confirmed')) {
                        $users->password = bcrypt($request->input('password', ''));
                    }
               
                    $users->is_enabled      = $request->input('is_enabled', 0);
                    $users->is_login        = $request->input('is_login', 0);
                    $users->updated_by      = $user->id;
                    $users->updated_at      = Carbon::now();
                   
                    if($users->save()){

                        if($request->has('role')) {
                            $users->roles()->sync([$request->input('role', '')]);
                        } else {
                            $users->roles()->detach();
                        }

                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'บันทึกสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'บันทึกไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                    
                } catch(ValidationException $e) {
                    
                    DB::rollback();
    
                    return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);
    
                } catch (\Exception $e) {
    
                    DB::rollback();
                    throw $e;
                }
    
                DB::commit();
       
                return response()->json($results);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';

        if(Crypt::decryptString($id)) {

            DB::beginTransaction();

            try {

                $user = User::find(Crypt::decryptString($id));

                if($user) {
                    if($user->delete()){
                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'ลบรายการสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'ลบรายการไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                }

            } catch(ValidationException $e) {
                
                DB::rollback();

                return Response::json($e->getErrors());

            } catch (\Exception $e) {

                DB::rollback();

                throw $e;
            }

            DB::commit();
       
            return Response::json($results);
        }
    }

    public function rules()
    {
        //
        $rules = [
            // 'password' => 'confirmed|min:8'
        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [
            // 'password.confirmed' => 'test'
        ];
        return $messages;
    }

    public function datatables(Request $request) {

        $user = Auth::user();

        $columns = [
            0 =>DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name)"),
            1 =>DB::raw("(SELECT T2.name FROM organizations T2 WHERE T2.id = users.organization_id)"),
            2 =>DB::raw("(SELECT T4.name FROM roles T4 WHERE T4.id = (SELECT T3.role_id FROM users_roles T3 WHERE T3.user_id = users.id))"),
            3 =>'users.updated_at',
            4 =>DB::raw("(SELECT CONCAT(T.first_name, CHAR(32), T.last_name) FROM users T WHERE T.id = users.updated_by)"),
        ];
  
        $datas = User::getList();
      
        $totalData = $datas->get()->count();
        $totalFiltered = $totalData;

        $start = $request->input( 'start' );
        $length = $request->input( 'length' );

        if ($request->has('search')) {
            if ($request->input ( 'search.value' ) != '') {
                
                $searchTerm = $request->input('search.value');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where( DB::raw("(SELECT CONCAT(T.first_name, CHAR(32), T.last_name) FROM users T WHERE T.id = users.updated_by)"), 'Like', '%' . $searchTerm . '%' );
                    $query->orWhere( DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name)"), 'Like', '%' . $searchTerm . '%' );
                    $query->orWhere( DB::raw("(SELECT T2.name FROM organizations T2 WHERE T2.id = users.organization_id)"), 'Like', '%' . $searchTerm . '%' );
                    $query->orWhere( DB::raw("(SELECT T4.name FROM roles T4 WHERE T4.id = (SELECT T3.role_id FROM users_roles T3 WHERE T3.user_id = users.id))"), 'Like', '%' . $searchTerm . '%' );
                });
            }
        }

        if ($request->has('order')) {
            if ($request->input('order.0.column') != '') {

                $orderColumn = $request->input('order.0.column');
                $orderDirection = $request->input('order.0.dir');

                $data_count = $datas;
                $totalFiltered = $data_count->get()->count();

                $datas = $datas->orderBy($columns[intval( $orderColumn )], $orderDirection )->skip($start)->take($length)->get();
            }
        }
        else {
            $datas = $datas->orderBy('updated_name', 'ASC' )->skip($start)->take($length)->get();
        }

        $data = [];
        foreach ( $datas as $key => $col ) {
            $nestedData = [];
           
            $nestedData[0] = !empty($col->name) ? $col->name : '-';
            $nestedData[1] = !empty($col->organizations_name) ? $col->organizations_name : '-';
            $nestedData[2] = !empty($col->roles_name) ? $col->roles_name : '-';
            $nestedData[3] = !empty($col->updated_at) ? $col->updated_at->modify('+543 year')->format('d/m/Y H.i น.') : '-';
            $nestedData[4] = !empty($col->updated_name) ? $col->updated_name : '-';
        
            $nestedData[5] = '<div class="dropdown">';
                $nestedData[5] .= '<button class="btn btn-secondary option dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    $nestedData[5] .= 'ดำเนินการ';
                $nestedData[5] .= '</button>';
                $nestedData[5] .= '<div class="dropdown-menu">';
                    $nestedData[5] .= '<a class="dropdown-item text-dark" href="'.(route('pages.user.edit',['id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ])).'"><i class="far fa-edit"></i> แก้ไข</a>';
                    $nestedData[5] .= '<a class="dropdown-item text-danger btn-delete">';
                        $nestedData[5] .= Form::open(['route' => ['pages.user.destroy', 'id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ], 'style' => 'display : none;', 'method' => 'DELETE', 'enctype' => 'multipart/form-data', 'id' => 'from_'.$key]);
                        $nestedData[5] .= Form::close();
                        $nestedData[5] .= '<i class="far fa-trash-alt"></i> ลบ';
                    $nestedData[5] .= '</a>';
                $nestedData[5] .= '</div>';
            $nestedData[5] .= '</div>';
    
            $data [] = $nestedData;
        }

        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }

    public function selectsapi(Request $request) {
      
        $selected_id    = !empty($request->selected_id) ? Crypt::decryptString($request->selected_id) : null;
        $attendance_id  = !empty($request->attendance_id) ? Crypt::decryptString($request->attendance_id) : null;
        $board_id       = !empty($request->board_id) ? Crypt::decryptString($request->board_id) : null;
        $is_edit        = !empty($request->is_edit) ? $request->is_edit : null;
     
        if($board_id && $is_edit) {
            $datas = User::getListOrBoard($attendance_id);
        } else {
            if(!empty($request->is_participant)) {
                $datas = Attendance::getList();

                if ($request->has('search')) {
                    $searchTerm = $request->input('search');
                    $datas = $datas->where(function($query) use ($searchTerm) {
                        $query->where(DB::raw("CONCAT(attendances.first_name, CHAR(32), attendances.last_name)"), 'Like', '%' . $searchTerm . '%' );
                    });
                }

                $datas = $datas->orderBy(DB::raw("CONCAT(attendances.first_name, CHAR(32), attendances.last_name)"), 'ASC' )->take(10)->get();
            } else {
                $datas = User::getList();

                if ($request->has('search')) {
                    $searchTerm = $request->input('search');
                    $datas = $datas->where(function($query) use ($searchTerm) {
                        $query->where(DB::raw("CONCAT(users.first_name , CHAR(32), users.last_name)"), 'Like', '%' . $searchTerm . '%' );
                    });
                }

                $datas = $datas->orderBy(DB::raw("CONCAT(users.first_name , CHAR(32), users.last_name)"), 'ASC' )->take(10)->get();
            }
           
        }
      
  
    
        // $datas = $datas->orderBy(DB::raw("CONCAT(users.first_name , CHAR(32), users.last_name)"), 'ASC' )->take(10)->get();
      
        $option = [];
        if($datas->count() > 0) {
            foreach ($datas as $item) {
                $selected = false;
             
                if($selected_id == $item->id) {
                    $selected = true;
                }
             
                $option[] = (object)[
                    'id'                        => Crypt::encryptString($item->id),
                    'text'                      => !empty($item->old_name) ? $item->old_name : $item->name,
                    'title_id'                  => !empty($item->old_title_id) ? Crypt::encryptString($item->old_title_id) : (!empty($item->title_id) ? Crypt::encryptString($item->title_id) : ''),
                    'title_name'                => !empty($item->old_title_name) ? $item->old_title_name : $item->title_name,
                    'first_name'                => !empty($item->old_first_name) ? $item->old_first_name : $item->first_name,
                    'last_name'                 => !empty($item->old_last_name) ? $item->old_last_name : $item->last_name,
                    'position'                  => !empty($item->old_position) ? $item->old_position : $item->position,
                    'tel'                       => !empty($item->old_phone) ? $item->old_phone : $item->phone,
                    'email'                     => !empty($item->old_email) ? $item->old_email : $item->email,
                    'other'                     => !empty($item->old_other) ? $item->old_other : '',
                    'organization_id'           => !empty($item->old_organization_id) ? Crypt::encryptString($item->old_organization_id) : (!empty($item->organization_id) ? Crypt::encryptString($item->organization_id) : ''),
                    'org'                       => !empty($item->old_organizations_name) ? $item->old_organizations_name : $item->organizations_name,
                    'position_in_meeting_id'    => Crypt::encryptString($item->position_in_meeting_id),
                    'position_in_meeting_name'  => !empty($item->old_position_in_meeting_name) ? $item->old_position_in_meeting_name : '',
                    'selected'                  => $selected
                ];
            }
        }
 
        return Response::json($option);
    }
}
