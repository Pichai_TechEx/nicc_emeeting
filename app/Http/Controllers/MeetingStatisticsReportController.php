<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Crypt, Storage, Response};
use TechEx\{Meeting};
use Collective\Html\FormFacade as Form;
use TechEx\Http\Controllers\Core;
use Carbon\Carbon;
use Validator;
use Auth;
use Maatwebsite\Excel\Excel;
use TechEx\Exports\MeetingStatisticsReportExport;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\SimpleType\JcTable;
use PhpOffice\PhpWord\Style\Cell;

class MeetingStatisticsReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];

        return view('pages.meetingstatisticsreport.index',[
            'result' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function export(Request $request)
    {
        $header_th = ['วันที่ประชุม', 'เวลาเริ่ม - เวลาสิ้นสุด', 'ชื่อเรื่องการจัดประชุม', 'สถานะที่ประชุม', 'หน่วยงานที่จัดการประชุม'];
        $step_name = [1 => 'รอ', 2 => 'รอ', 3 => 'ระหว่างการประชุม', 4 => 'ระหว่างการประชุม', 5 => 'ปิดการประชุม'];
        $meeting = Meeting::getList();

        $phpWord = new PhpWord();

        $section = $phpWord->addSection();

        $header = array('size' => 16, 'bold' => true);
        $font = array('size' => 16, 'bold' => false);

        $section->addText('รายงานสถิติการจัดประชุม', $header);

        $fancyTableStyleName = 'รายงานสถิติการจัดประชุม';
        $fancyTableStyle = ['borderSize' => 1, 'borderColor' => '212121', 'cellMargin' => 80, 'alignment' => JcTable::CENTER, 'cellSpacing' => 1];
        $fancyTableFirstRowStyle = ['borderBottomColor' => '212121', 'bgColor' => '95b3d7'];
        $fancyTableCellStyle = ['valign' => 'center'];
        $fancyTableCellBtlrStyle = ['valign' => 'center', 'textDirection' => Cell::TEXT_DIR_BTLR];
        $fancyTableFontStyle = ['bold' => true];
        $phpWord->addTableStyle($fancyTableStyleName, $fancyTableStyle, $fancyTableFirstRowStyle);
        $table = $section->addTable($fancyTableStyleName);

        $table->addRow();
        foreach ($header_th as $th) {
            $table->addCell(1750)->addText($th, $header);
        }
   
        foreach ($meeting->get() as $col) {

            $start_time = explode(':', $col->start_time);
            $start_time = $start_time[0].'.'.$start_time[1];

            $end_time = explode(':', $col->end_time);
            $end_time = $end_time[0].'.'.$end_time[1];

            $time = "{$start_time}-{$end_time} น.";

            $table->addRow();
            $table->addCell(1750)->addText((!empty($col->date) ? Carbon::createFromFormat('Y-m-d', $col->date, 'Asia/Bangkok')->modify('+543 year')->format('d/m/Y') : '-'), $font);
            $table->addCell(1750)->addText((!empty($col->start_time) ? "{$time}" : '-'), $font);
            $table->addCell(1750)->addText((!empty($col->meeting_name) ? $col->meeting_name : '-'), $font);
            $table->addCell(1750)->addText($step_name[$col->step], $font);
            $table->addCell(1750)->addText(!empty($col->meeting_organization_name) ? $col->meeting_organization_name : '-', $font);
        }


        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');

        try {
            $objWriter->save(storage_path('helloWorld.docx'));
        } catch (Exception $e) {

        }

        return response()->download(storage_path('helloWorld.docx'));

        // return (new MeetingStatisticsReportExport('ok'))->download('invoices.xlsx', Excel::XLSX);
        // return (new MeetingStatisticsReportExport('ok'))->download('invoices.xls', Excel::XLS);
    }
    
    public function datatables(Request $request) {

        $user = Auth::user();

        $step_name = [1 => 'รอ', 2 => 'รอ', 3 => 'ระหว่างการประชุม', 4 => 'ระหว่างการประชุม', 5 => 'ปิดการประชุม'];

        $columns = [
            0 =>'date',
            1 =>'start_time',
            2 =>'meeting_name',
            3 => DB::raw("(select MAX(t.step) FROM meeting_step t WHERE t.meeting_id = meeting.id)"),
            4 =>'meeting_organization_name',
        ];

        $datas = Meeting::getList();
     
        $totalData = $datas->get()->count();
        $totalFiltered = $totalData;

        $start = $request->input( 'start' );
        $length = $request->input( 'length' );
      
        if ($request->has('search')) {
            if ($request->input ( 'search.meeting_name' ) != '') {
                
                $searchTerm = $request->input('search.meeting_name');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where( 'meeting_name', 'Like', '%' . $searchTerm . '%' );
                });
                unset($searchTerm);
                
                $searchTerm = $request->input('search.meeting_place');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where( 'meeting_place', 'Like', '%' . $searchTerm . '%' );
                });
                unset($searchTerm);
          
                $searchTerm = $request->input('search.meeting_organization_name');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where( 'meeting_organization_name', 'Like', '%' . $searchTerm . '%' );
                });
                unset($searchTerm);

                $searchTerm = $request->input('search.meeting_organizer_name');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where(DB::raw("(select CONCAT(u.first_name, CHAR(32), u.last_name) FROM users u WHERE u.id = meeting.created_by)"), 'Like', '%' . $searchTerm . '%' );
                });
                unset($searchTerm);
            }
        }

        if ($request->has('order')) {
            if ($request->input('order.0.column') != '') {

                $orderColumn = $request->input('order.0.column');
                $orderDirection = $request->input('order.0.dir');

                $data_count = $datas;
                $totalFiltered = $data_count->get()->count();

                $datas = $datas->orderBy($columns[intval( $orderColumn )], $orderDirection )->skip($start)->take($length)->get();
            }
        }
        else {
            $datas = $datas->orderBy('created_at', 'ASC' )->skip($start)->take($length)->get();
        }

        $data = [];
        foreach ( $datas as $key => $col ) {
            $nestedData = [];
            
            $start_time = explode(':', $col->start_time);
            $start_time = $start_time[0].'.'.$start_time[1];

            $end_time = explode(':', $col->end_time);
            $end_time = $end_time[0].'.'.$end_time[1];

            $time = "{$start_time}-{$end_time} น.";

            $nestedData[0] = !empty($col->date) ? Carbon::createFromFormat('Y-m-d', $col->date, 'Asia/Bangkok')->modify('+543 year')->format('d/m/Y') : '-';
            $nestedData[1] = !empty($col->start_time) ? "{$time}" : '-';
            $nestedData[2] = !empty($col->meeting_name) ? $col->meeting_name : '-';
            $nestedData[3] = $step_name[$col->step];
            $nestedData[4] = !empty($col->meeting_organization_name) ? $col->meeting_organization_name : '-';

    
            $data [] = $nestedData;
        }

        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }
}
