<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Crypt, Storage, Response};
use TechEx\{PositionInMeeting};
use Collective\Html\FormFacade as Form;
use TechEx\Http\Controllers\Core;
use Carbon\Carbon;
use Validator;
use Auth;

class PositionInMeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];

        return view('pages.positioninmeeting.index',[
            'result' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $result = [];

        return view('pages.positioninmeeting.create',[
            'result' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();
            
            try {
             
                $position_in_meeting = new PositionInMeeting();
                $position_in_meeting->code          = Core::uniqidReal();
                $position_in_meeting->name          = $request->input('name', '');
                $position_in_meeting->description   = $request->input('description', '');
                $position_in_meeting->lang          = $request->input('lang', '');
                $position_in_meeting->sort_no       = $request->input('sort_no', 0);
                $position_in_meeting->is_enabled    = $request->input('is_enabled', 0);
                $position_in_meeting->created_by    = $user->id;
                $position_in_meeting->created_at    = Carbon::now();
               
                if($position_in_meeting->save()){
                    $results->status         = true;
                    $results->icon           = 'success';
                    $results->message        = 'บันทึกสำเร็จ';
                    $results->redirect_url   = route('pages.positioninmeeting.index');
                } else {
                    $results->status        = false;
                    $results->icon          = 'error';
                    $results->message       = 'บันทึกไม่สำเร็จ';
                    $results->redirect_url  = '';
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = [];

        if(Crypt::decryptString($id)) {

            $position_in_meeting = PositionInMeeting::find(Crypt::decryptString($id));

            if(!empty($position_in_meeting)) {

                $result['data'] = (object)[
                    'id'            => Crypt::encryptString($position_in_meeting->id),
                    'name'          => isset($position_in_meeting->name) ? $position_in_meeting->name : '',
                    'description'   => isset($position_in_meeting->description) ? $position_in_meeting->description : '',
                    'sort_no'       => isset($position_in_meeting->sort_no) ? $position_in_meeting->sort_no : '',
                    'is_enabled'    => isset($position_in_meeting->is_enabled) ? $position_in_meeting->is_enabled : '',
                ];
            }
            unset($position_in_meeting);
        }

        return view('pages.positioninmeeting.edit',[
            'result' => $result
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        if(Crypt::decryptString($id)) {
            $validator = Validator::make($request->all(), $this->rules(), $this->messages());
            if($validator->fails()){
                return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
            } else {
        
                DB::beginTransaction();

                try {
            
                    $position_in_meeting = PositionInMeeting::find(Crypt::decryptString($id));
                    $position_in_meeting->name          = $request->input('name', '');
                    $position_in_meeting->description   = $request->input('description', '');
                    $position_in_meeting->lang          = $request->input('lang', '');
                    $position_in_meeting->sort_no       = $request->input('sort_no', 0);
                    $position_in_meeting->is_enabled    = $request->input('is_enabled', 0);
                    $position_in_meeting->updated_by    = $user->id;
                    $position_in_meeting->updated_at    = Carbon::now();
                   
                    if($position_in_meeting->save()){
                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'บันทึกสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'บันทึกไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                    
                } catch(ValidationException $e) {
                    
                    DB::rollback();
    
                    return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);
    
                } catch (\Exception $e) {
    
                    DB::rollback();
                    throw $e;
                }
    
                DB::commit();
       
                return response()->json($results);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';

        if(Crypt::decryptString($id)) {


            DB::beginTransaction();

            try {

                $position_in_meeting = PositionInMeeting::find(Crypt::decryptString($id));

                if($position_in_meeting) {
                    if($position_in_meeting->delete()){
                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'ลบรายการสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'ลบรายการไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                }

            } catch(ValidationException $e) {
                
                DB::rollback();

                return Response::json($e->getErrors());

            } catch (\Exception $e) {

                DB::rollback();

                throw $e;
            }

            DB::commit();
       
            return Response::json($results);
        }
    }

    public function rules()
    {
        //
        $rules = [

        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [

        ];
        return $messages;
    }

    public function selects(Request $request) {
     
        $selected_id = !empty($request->selected_id) ? Crypt::decryptString($request->selected_id) : null;
    
        $option = [];
        if(PositionInMeeting::count() > 0) {
            foreach (PositionInMeeting::all() as $item) {
                $selected = false;

                if($selected_id == $item->id) {
                    $selected = true;
                }

                $option[] = (object)[
                    'id'        => Crypt::encryptString($item->id),
                    'text'      => $item->name,
                    'selected'  => $selected
                ];
            }
        }

        return Response::json($option);
    }

    public function datatables(Request $request) {

        $user = Auth::user();

        $columns = [
            0 =>'position_in_meeting.name',
            1 =>'position_in_meeting.description',
            2 =>'position_in_meeting.updated_at',
            3 =>'updated_name',
        ];

        $datas = PositionInMeeting::getList();
    
        $totalData = $datas->get()->count();
        $totalFiltered = $totalData;

        $start = $request->input( 'start' );
        $length = $request->input( 'length' );

        if ($request->has('search')) {
            if ($request->input ( 'search.value' ) != '') {
                
                $searchTerm = $request->input('search.value');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where( 'position_in_meeting.name', 'Like', '%' . $searchTerm . '%' );
                    $query->orWhere( 'position_in_meeting.description', 'Like', '%' . $searchTerm . '%' );
                });
            }
        }

        if ($request->has('order')) {
            if ($request->input('order.0.column') != '') {

                $orderColumn = $request->input('order.0.column');
                $orderDirection = $request->input('order.0.dir');

                $data_count = $datas;
                $totalFiltered = $data_count->get()->count();

                $datas = $datas->orderBy($columns[intval( $orderColumn )], $orderDirection )->skip($start)->take($length)->get();
            }
        }
        else {
            $datas = $datas->orderBy('sort_no', 'ASC' )->skip($start)->take($length)->get();
        }

        $data = [];
        foreach ( $datas as $key => $col ) {
            $nestedData = [];
         
            $nestedData[0] = !empty($col->name) ? $col->name : '-';
            $nestedData[1] = !empty($col->description) ? $col->description : '-';
            $nestedData[2] = !empty($col->updated_at) ? $col->updated_at->modify('+543 year')->format('d/m/Y H.i น.') : '-';
            $nestedData[3] = !empty($col->updated_name) ? $col->updated_name : '-';

            $nestedData[4] = '<div class="btn-group" role="group" aria-label="Basic example">';
                $nestedData[4] .= '<a class="btn btn-warning" href="'.(route('pages.positioninmeeting.edit',['id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ])).'"><i class="far fa-edit"></i></a>';
                $nestedData[4] .= '<a class="btn btn-danger btn-delete">';
                    $nestedData[4] .= Form::open(['route' => ['pages.positioninmeeting.destroy', 'id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ], 'method' => 'DELETE', 'enctype' => 'multipart/form-data', 'id' => 'from_'.$key]);
                    $nestedData[4] .= '<i class="far fa-trash-alt"></i>';
                    $nestedData[4] .= Form::close();
                $nestedData[4] .= '</a>';
            $nestedData[4] .= '</div>';
    
            $data [] = $nestedData;
        }

        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }
}
