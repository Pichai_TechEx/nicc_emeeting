<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Crypt, Storage, Response};
use TechEx\{Menu, Permission};
use Collective\Html\FormFacade as Form;
use TechEx\Http\Controllers\Core;
use Carbon\Carbon;
use Validator;
use Auth;

class MenusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];

        return view('pages.menus.index',[
            'result' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $result = [];

        $result = [
            'permission'    => Permission::all(),
        ];

        return view('pages.menus.create',[
            'result' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();
            
            try {
             
                $menu = new Menu();
                $menu->code         = Core::uniqidReal();
                $menu->name         = $request->input('name', '');
                $menu->description  = $request->input('description', '');
                $menu->controller   = $request->input('controller', '');
                $menu->icon         = $request->input('icon', '');
                $menu->group        = $request->input('group', '');
                $menu->position     = $request->input('position', '');
                $menu->parent       = $request->input('parent', '');
                $menu->level        = $request->input('level', '');

                if($request->has('what_is_it')) {
                    switch ($request->input('what_is_it')) {
                        case 1:
                            $menu->is_tab_menu      = 1;
                            $menu->is_menu          = 0;
                            $menu->is_dropdown_menu = 0;
                            break;
                        case 2:
                            $menu->is_tab_menu      = 0;
                            $menu->is_menu          = 1;
                            $menu->is_dropdown_menu = 0;
                            break;
                        case 3:
                            $menu->is_tab_menu      = 0;
                            $menu->is_menu          = 0;
                            $menu->is_dropdown_menu = 1;
                            break;
                    }
                }

                $menu->lang         = $request->input('lang', 'th');
                $menu->is_enabled   = $request->input('is_enabled', 0);
                $menu->created_by   = $user->id;
                $menu->created_at   = Carbon::now();

                if($menu->save()){
                    $results->status         = true;
                    $results->icon           = 'success';
                    $results->message        = 'บันทึกสำเร็จ';
                    $results->redirect_url   = route('pages.menus.index');
                } else {
                    $results->status        = false;
                    $results->icon          = 'error';
                    $results->message       = 'บันทึกไม่สำเร็จ';
                    $results->redirect_url  = '';
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = [];

        if(Crypt::decryptString($id)) {

            $menu = Menu::find(Crypt::decryptString($id));

            if(!empty($menu)) {

                $permissions_arr = [];
                if($menu->permissions()->count() > 0) {
                    foreach ($menu->permissions()->get() as $key => $p) {
                        $permissions_arr[$p->id]['checked'] = true;
                    }
                }

                $result = [
                    'data' => (object)[
                        'id'            => Crypt::encryptString($menu->id),
                        'name'          => isset($menu->name) ? $menu->name : '',
                        'description'   => isset($menu->description) ? $menu->description : '',
                        'controller'    => isset($menu->controller) ? $menu->controller : '',
                        'icon'          => isset($menu->icon) ? $menu->icon : '',
                        'group'         => isset($menu->group) ? $menu->group : '',
                        'position'      => isset($menu->position) ? $menu->position : '',
                        'parent'        => isset($menu->parent) ? $menu->parent : '',
                        'level'         => isset($menu->level) ? $menu->level : '',
                        'what_is_it'    => !empty($menu->is_tab_menu) ? 1 : ( !empty($menu->is_menu) ? 2 : ( !empty($menu->is_dropdown_menu) ? 3 : 0 ) ),
                        'is_enabled'    => isset($menu->is_enabled) ? $menu->is_enabled : '',
                    ],
                    'permission'            => Permission::all(),
                    'permission_checked'    => $permissions_arr,
                ];
            }

            unset($menu);
        }

        return view('pages.menus.edit',[
            'result' => $result
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';

        if(Crypt::decryptString($id)) {
            $validator = Validator::make($request->all(), $this->rules(), $this->messages());
            if($validator->fails()){
                return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
            } else {
        
                DB::beginTransaction();

                try {
            
                    $menu = Menu::find(Crypt::decryptString($id));
                    $menu->name         = $request->input('name', '');
                    $menu->description  = $request->input('description', '');
                    $menu->controller   = $request->input('controller', '');
                    $menu->icon         = $request->input('icon', '');
                    $menu->group        = $request->input('group', '');
                    $menu->position     = $request->input('position', '');
                    $menu->parent       = $request->input('parent', '');
                    $menu->level        = $request->input('level', '');
    
                    if($request->has('what_is_it')) {
                        switch ($request->input('what_is_it')) {
                            case 1:
                                $menu->is_tab_menu      = 1;
                                $menu->is_menu          = 0;
                                $menu->is_dropdown_menu = 0;
                                break;
                            case 2:
                                $menu->is_tab_menu      = 0;
                                $menu->is_menu          = 1;
                                $menu->is_dropdown_menu = 0;
                                break;
                            case 3:
                                $menu->is_tab_menu      = 0;
                                $menu->is_menu          = 0;
                                $menu->is_dropdown_menu = 1;
                                break;
                        }
                    }
    
                    $menu->lang         = $request->input('lang', 'th');
                    $menu->is_enabled   = $request->input('is_enabled', 0);
                    $menu->updated_by    = $user->id;
                    $menu->updated_at    = Carbon::now();
                   
                    if($menu->save()){

                        if($request->has('permissions')) {
                            $menu->permissions()->sync(array_values($request->input('permissions')));
                        } else {
                            $menu->permissions()->detach();
                        }

                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'บันทึกสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'บันทึกไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                    
                } catch(ValidationException $e) {
                    
                    DB::rollback();
    
                    return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);
    
                } catch (\Exception $e) {
    
                    DB::rollback();
                    throw $e;
                }
    
                DB::commit();
       
                return response()->json($results);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';

        if(Crypt::decryptString($id)) {


            DB::beginTransaction();

            try {

                $menu = Menu::find(Crypt::decryptString($id));

                if($menu) {
                    if($menu->delete()){
                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'ลบรายการสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'ลบรายการไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                }

            } catch(ValidationException $e) {
                
                DB::rollback();

                return Response::json($e->getErrors());

            } catch (\Exception $e) {

                DB::rollback();

                throw $e;
            }

            DB::commit();
       
            return Response::json($results);
        }
    }
    
    public function rules()
    {
        //
        $rules = [

        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [

        ];
        return $messages;
    }
    
    public function datatables(Request $request) {

        $user = Auth::user();

        $columns = [
            0 =>'menus.name',
            1 =>'menus.description',
            2 =>'menus.updated_at',
            3 =>'updated_name',
        ];

        $datas = Menu::getList();
     
        $totalData = $datas->get()->count();
        $totalFiltered = $totalData;

        $start = $request->input( 'start' );
        $length = $request->input( 'length' );

        if ($request->has('search')) {
            if ($request->input ( 'search.value' ) != '') {
                
                $searchTerm = $request->input('search.value');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where( 'menus.name', 'Like', '%' . $searchTerm . '%' );
                    $query->orWhere( 'menus.description', 'Like', '%' . $searchTerm . '%' );
                });
            }
        }

        if ($request->has('order')) {
            if ($request->input('order.0.column') != '') {

                $orderColumn = $request->input('order.0.column');
                $orderDirection = $request->input('order.0.dir');

                $data_count = $datas;
                $totalFiltered = $data_count->get()->count();

                $datas = $datas->orderBy($columns[intval( $orderColumn )], $orderDirection )->skip($start)->take($length)->get();
            }
        }
        else {
            $datas = $datas->orderBy('name', 'ASC' )->skip($start)->take($length)->get();
        }

        $data = [];
        foreach ( $datas as $key => $col ) {
            $nestedData = [];
         
            $nestedData[0] = !empty($col->name) ? $col->name : '-';
            $nestedData[1] = !empty($col->description) ? $col->description : '-';
            $nestedData[2] = !empty($col->updated_at) ? $col->updated_at->modify('+543 year')->format('d/m/Y H.i น.') : '-';
            $nestedData[3] = !empty($col->updated_name) ? $col->updated_name : '-';

            $nestedData[4] = '<div class="btn-group" role="group" aria-label="Basic example">';
                $nestedData[4] .= '<a class="btn btn-warning" href="'.(route('pages.menus.edit',['id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ])).'"><i class="far fa-edit"></i></a>';
                $nestedData[4] .= '<a class="btn btn-danger btn-delete">';
                    $nestedData[4] .= Form::open(['route' => ['pages.menus.destroy', 'id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ], 'method' => 'DELETE', 'enctype' => 'multipart/form-data', 'id' => 'from_'.$key]);
                    $nestedData[4] .= '<i class="far fa-trash-alt"></i>';
                    $nestedData[4] .= Form::close();
                $nestedData[4] .= '</a>';
            $nestedData[4] .= '</div>';
    
            $data [] = $nestedData;
        }

        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }
}
