<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
    
        $user = Auth::user();
 
        $events = [];

        $backgroundColor = ['C' => '##bdbbbd', 'W' => '#1E88E5', 'R' => '#F44336', 'AP' => '#4CAF50', 'AC' => '#BA68C8'];
        $borderColor = ['C' => '##bdbbbd', 'W' => '#1565C0', 'R' => '#D32F2F', 'AP' => '#388E3C', 'AC' => '#9C27B0'];

        $calendar = \Calendar::setOptions([ 
                'selectable' => true,
                'header' => [
                    'left' => 'prev,next today',
                    'center' => 'title',
                    'right' => 'month',
                ],
                'defaultDate' => date('Y-m-d\TH:i:s'),
                'eventLimit' => true,
            
            ])->setCallbacks([
                'eventClick' => "function( event, jsEvent, view ) {   if (event.url) { window.open(event.url, '_parent'); return false; } }",
                'eventRender' => "function(info, element) {
                    var html = '';
                    var people_to_meet = '';

                    if(info.people_to_meet) {
                        people_to_meet = JSON.parse(info.people_to_meet);
                        var tm = '';
                        $.each(people_to_meet, function(i, e) {
                            tm += `<i class='fa fa-user'></i> ` + e + `<br>`;
                        });

                        people_to_meet = tm;
                    }
                    html += `<div class='fc-content' data-status='` + info.visit_status + `' data-user='` + info.created_by + `'>`;
                    html += `<div class='fc-title'>` + people_to_meet + `<i class='fa fa-building-o'></i> `+ info.title +`<br> <i class='fa fa-dot-circle-o'></i> `+ info.objective +`<br><i class='fa fa-clock-o'></i> `+ info.time +` - `+ info.to_time +`</div>`;
                    html += `</div>`;

                    element[0].innerHTML = html;
                }",
                'viewRender' => "async function(view, element) {
                    var _id = $(element).parent().parent().attr('id');
                    var moment = $('#' + _id).fullCalendar('getDate');
                    var is_status = [];
                    var users_code = [];
        
                    await $.each($('input[name^=option_is_status]:checked'), function (i,e) {
                        is_status.push($(this).val());
                    });
        
                    await $.each($('input[name^=option_user]:checked'), function (i,e) {
                        users_code.push($(this).val());
                    });
                  
                    //if(getDatepickerMonths()) {
                     //   var d = new Date(moment.format('YYYY-MM-DD'));
                     //   getDatepickerMonths().datepicker('setDate', d);
                 //   }

                    //await load_calendar($.fullCalendar.moment(moment.format('YYYY-MM-DD')).format('x'), is_status, users_code);
                }",
               
            ]);
            
      
        $optionVisitStatus = ['C' => 'Cancel', 'D' => 'Draft', 'W' => 'Waiting for Approve', 'AP' => 'Approved', 'R' => 'Rejected', 'AC' => 'Actual'];
            
        // $data_user = User::getList();
        // $optionUser = $data_user->get();

        return view('pages.home.index', [
            'calendar' => $calendar,
            // 'optionUser' => $optionUser,
            'optionVisitStatus' => $optionVisitStatus,
            'backgroundColor' => ['C' => '#bdbbbd', 'D' => '#FFC107','W' => '#1E88E5', 'R' => '#F44336', 'AP' => '#4CAF50', 'AC' => '#BA68C8'],
            'borderColor' => ['C' => '#bdbbbd', 'D' => '#FFA000','W' => '#1565C0', 'R' => '#D32F2F', 'AP' => '#388E3C', 'AC' => '#9C27B0'],
        
            // 'backgroundColor' => $backgroundColor,
            // 'borderColor' => $borderColor,        
        ]);
        // return view('home');
    }
}
