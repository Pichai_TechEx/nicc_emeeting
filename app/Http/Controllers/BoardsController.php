<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Crypt, Storage, Response};
use TechEx\{Board, BoardsAttendances, User, BoardsFile, File};
use Collective\Html\FormFacade as Form;
use Collective\Html\HtmlFacade as Html;
use TechEx\Http\Controllers\Core;
use Carbon\Carbon;
use Validator;
use Auth;

class BoardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];

        return view('pages.boards.index',[
            'result' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $result = [];

        $result = [
            'boardsattendances' => json_encode([])
        ];

        return view('pages.boards.create',[
            'result' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $users = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';
        $path_unlink  = [];
        $boards_file_arr  = [];

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();
            
            try {
                
                if($request->has('fileToUpload')) {
                    foreach ($request->fileToUpload as $key => $file) {
                      
                        $file_result = Core::saveFile($file);
                        $path_unlink[]  = $file_result['destinationPath'];

                        $boards_file = new BoardsFile();
                        $boards_file->file_id       = $file_result['file_new_id'];
                        $boards_file->created_by    = $users->id;
                        $boards_file->created_at    = Carbon::now();

                        $boards_file_arr[] = $boards_file;
                    }
                }

                $board = new Board();
                $board->name            = $request->input('name', '');
                $board->detail          = $request->input('detail', '');
                $board->start_date      = $request->input('start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('start_date'))->format('Y-m-d') : Carbon::now();
                $board->end_date        = $request->input('end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('end_date'))->format('Y-m-d') : Carbon::now();
                $board->is_enabled      = $request->input('is_enabled', 0);
                $board->created_by      = $users->id;
                $board->created_at      = Carbon::now();
             
                $boards_attendances_arr = [];

                if($request->has('attendance.news') && count($request->input('attendance.news', [])) > 0) {
                    foreach ($request->input('attendance.news.organization') as $key => $item) {
                        
                        $username = explode('@', $request->input("attendance.news.email.{$key}", ''));
                        $username = reset($username);
                    
                        $user = new User();
                        $user->username         = $username;
                        $user->title_id         = !empty($request->input("attendance.news.title.{$key}")) ? Crypt::decryptString($request->input("attendance.news.title.{$key}")) : '';
                        $user->organization_id  = !empty($request->input("attendance.news.organization.{$key}")) ? Crypt::decryptString($request->input("attendance.news.organization.{$key}")) : '';
                        $user->email            = $request->input("attendance.news.email.{$key}", '');
                        $user->first_name       = $request->input("attendance.news.first_name.{$key}", '');
                        $user->last_name        = $request->input("attendance.news.last_name.{$key}", '');
                        $user->position         = $request->input("attendance.news.position.{$key}", '');
                        $user->phone            = $request->input("attendance.news.phone.{$key}", '');
                        $user->password         = bcrypt(Core::uniqidReal());
                        $user->created_by       = $users->id;
                        $user->created_at       = Carbon::now();
                        
                        if($user->save()) {

                            $boards_attendances = new BoardsAttendances();
                            $boards_attendances->user_id                    = $user->id;
                            $boards_attendances->title_id                   = !empty($request->input("attendance.news.title.{$key}")) ? Crypt::decryptString($request->input("attendance.news.title.{$key}")) : '';
                            $boards_attendances->organization_id            = !empty($request->input("attendance.news.organization.{$key}")) ? Crypt::decryptString($request->input("attendance.news.organization.{$key}")) : '';
                            $boards_attendances->position_in_meeting_id     = !empty($request->input("attendance.news.position_in_meeting.{$key}")) ? Crypt::decryptString($request->input("attendance.news.position_in_meeting.{$key}")) : '';
                            $boards_attendances->title_name                 = $request->input("attendance.news.title_name.{$key}", '');
                            $boards_attendances->organization_name          = $request->input("attendance.news.organization_name.{$key}", '');
                            $boards_attendances->position_in_meeting_name   = $request->input("attendance.news.position_in_meeting_name.{$key}", '');
                            $boards_attendances->first_name                 = $request->input("attendance.news.first_name.{$key}", '');
                            $boards_attendances->last_name                  = $request->input("attendance.news.last_name.{$key}", '');
                            $boards_attendances->position                   = $request->input("attendance.news.position_name.{$key}", '');
                            $boards_attendances->other                      = $request->input("attendance.news.other.{$key}", '');
                            $boards_attendances->email                      = $request->input("attendance.news.email.{$key}", '');
                            $boards_attendances->phone                      = $request->input("attendance.news.phone.{$key}", '');
                            $boards_attendances->created_by                 = $users->id;
                            $boards_attendances->created_at                 = Carbon::now();
                            $boards_attendances_arr[] =                     $boards_attendances;
                        }
                    }
                }
                
                if($request->has('attendance.old') && count($request->input('attendance.old', [])) > 0) {
                    foreach ($request->input('attendance.old.organization') as $key => $item) {
                  
                        $boards_attendances = new BoardsAttendances();
                        $boards_attendances->user_id                    = !empty($request->input("attendance.old.name_option.{$key}")) ? Crypt::decryptString($request->input("attendance.old.name_option.{$key}")) : '';
                        $boards_attendances->title_id                   = !empty($request->input("attendance.old.title.{$key}")) ? Crypt::decryptString($request->input("attendance.old.title.{$key}")) : '';
                        $boards_attendances->organization_id            = !empty($request->input("attendance.old.organization.{$key}")) ? Crypt::decryptString($request->input("attendance.old.organization.{$key}")) : '';
                        $boards_attendances->position_in_meeting_id     = !empty($request->input("attendance.old.position_in_meeting.{$key}")) ? Crypt::decryptString($request->input("attendance.old.position_in_meeting.{$key}")) : '';
                        $boards_attendances->title_name                 = $request->input("attendance.old.title_name.{$key}", '');
                        $boards_attendances->organization_name          = $request->input("attendance.old.organization_name.{$key}", '');
                        $boards_attendances->position_in_meeting_name   = $request->input("attendance.old.position_in_meeting_name.{$key}", '');
                        $boards_attendances->first_name                 = $request->input("attendance.old.first_name.{$key}", '');
                        $boards_attendances->last_name                  = $request->input("attendance.old.last_name.{$key}", '');
                        $boards_attendances->position                   = $request->input("attendance.old.position_name.{$key}", '');
                        $boards_attendances->other                      = $request->input("attendance.old.other.{$key}", '');
                        $boards_attendances->email                      = $request->input("attendance.old.email.{$key}", '');
                        $boards_attendances->phone                      = $request->input("attendance.old.phone.{$key}", '');
                        $boards_attendances->created_by                 = $users->id;
                        $boards_attendances->created_at                 = Carbon::now();
                        $boards_attendances_arr[] =                     $boards_attendances;
                    }
                }
       
                if($board->save()){
                    
                    if(count($boards_file_arr) > 0) {
                        $board->files()->saveMany($boards_file_arr);
                    }
                    
                    if(count($boards_attendances_arr) > 0) {
                        $board->boardsattendances()->saveMany($boards_attendances_arr);
                    }

                    $results->status         = true;
                    $results->icon           = 'success';
                    $results->message        = 'บันทึกสำเร็จ';
                    $results->redirect_url   = route('pages.boards.index');
                } else {
                    $results->status        = false;
                    $results->icon          = 'error';
                    $results->message       = 'บันทึกไม่สำเร็จ';
                    $results->redirect_url  = '';
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                if(count($path_unlink) > 0) {
                    Storage::delete($path_unlink);
                }

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();

                if(count($path_unlink) > 0) {
                    Storage::delete($path_unlink);
                }

                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = [];
        $file_upload = [];

        if(Crypt::decryptString($id)) {

            $board = Board::find(Crypt::decryptString($id));

            if(!empty($board)) {

                $boardsattendances_arr = [];
                if($board->boardsattendances()->count() > 0) {
                    foreach ($board->boardsattendances as $key => $attendances) {
                      
                        $boardsattendances_arr[] = (object)[
                            'id'                        => Crypt::encryptString($attendances->id),
                            'user_id'                   => Crypt::encryptString($attendances->user_id),
                            'position_in_meeting_id'    => Crypt::encryptString($attendances->position_in_meeting_id),
                            'title_name'                => !empty($attendances->title_name) ? $attendances->title_name : '',
                            'organization_name'         => !empty($attendances->organization_name) ? $attendances->organization_name : '',
                            'position_in_meeting_name'  => !empty($attendances->position_in_meeting_name) ? $attendances->position_in_meeting_name : '', 
                            'first_name'                => !empty($attendances->first_name) ? $attendances->first_name : '',
                            'last_name'                 => !empty($attendances->last_name) ? $attendances->last_name : '',
                            'position'                  => !empty($attendances->position) ? $attendances->position : '',
                            'email'                     => !empty($attendances->email) ? $attendances->email : '',
                            'phone'                     => !empty($attendances->phone) ? $attendances->phone : '',
                            'other'                     => !empty($attendances->other) ? $attendances->other : ''
                        ];
                    }
                }

                if($board->files()->get()->count() > 0) {
                    foreach ($board->files()->get() as $item) {
                        $files = $item->file()->first();
                      
                        if($files) {
                            $files->urlencode_path = Crypt::encryptString($files->path);
                            $files->path = asset(Storage::disk('local')->url($files->path));
                            $file_upload[] = (object)[
                                'id'   => $files->id,
                                'name' => $files->originalName,
                                'size' => $files->size,
                                'type' => $files->mimeType,
                                'url'  => $files->path
                            ];
                        }
                    };
                }
              
                $result = [
                    'data' => (object)[
                        'id'            => Crypt::encryptString($board->id),
                        'name'          => isset($board->name) ? $board->name : '',
                        'detail'        => isset($board->detail) ? $board->detail : '',
                        'start_date'    => isset($board->start_date) ? Carbon::createFromFormat('Y-m-d', $board->start_date, 'Asia/Bangkok')->format('d/m/Y') : '',
                        'end_date'      => isset($board->end_date) ? Carbon::createFromFormat('Y-m-d', $board->end_date, 'Asia/Bangkok')->format('d/m/Y') : '',
                        'is_enabled'    => isset($board->is_enabled) ? $board->is_enabled : '',
                    ],
                    'boardsattendances' => json_encode($boardsattendances_arr),
                    'files'             => json_encode($file_upload)
                ];
            }
            unset($board);
        }

        return view('pages.boards.edit',[
            'result' => $result
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';
        $path_unlink  = [];
        $boards_file_arr  = [];
    
        if(Crypt::decryptString($id)) {
            $validator = Validator::make($request->all(), $this->rules(), $this->messages());
            if($validator->fails()){
                return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
            } else {
        
                DB::beginTransaction();

                try {
                
                    if($request->has('fileToUpload')) {
                        foreach ($request->fileToUpload as $key => $file) {
                          
                            $file_result = Core::saveFile($file);
                            $path_unlink[]  = $file_result['destinationPath'];
    
                            $boards_file = new BoardsFile();
                            $boards_file->file_id       = $file_result['file_new_id'];
                            $boards_file->created_by    = $users->id;
                            $boards_file->created_at    = Carbon::now();
    
                            $boards_file_arr[] = $boards_file;
                        }
                    }
    
                    if($request->has('file_id') && count($request->file_id) > 0) {
                        foreach ($request->file_id as $file_id) {
                          
                            $file = File::find($file_id);
    
                            $boards_file = new BoardsFile();
                            $boards_file->file_id       = $file->id;
                            $boards_file->created_by    = $users->id;
                            $boards_file->created_at    = Carbon::now();
    
                            $boards_file_arr[] = $boards_file;
                        }
                    }

                    $board = Board::find(Crypt::decryptString($id));
                    $board->name          = $request->input('name', '');
                    $board->detail        = $request->input('detail', '');
                    $board->start_date    = $request->input('start_date') ? Carbon::createFromFormat('d/m/Y', $request->input('start_date'))->format('Y-m-d') : Carbon::now();
                    $board->end_date      = $request->input('end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('end_date'))->format('Y-m-d') : Carbon::now();
                    $board->is_enabled    = $request->input('is_enabled', 0);
                    $board->updated_by    = $users->id;
                    $board->updated_at    = Carbon::now();

                    $boards_attendances_arr = [];

                    if($request->has('attendance.news') && count($request->input('attendance.news', [])) > 0) {
                        foreach ($request->input('attendance.news.organization') as $key => $item) {
                            
                            $username = explode('@', $request->input("attendance.news.email.{$key}", ''));
                            $username = reset($username);
                        
                            $user = new User();
                            $user->username         = $username;
                            $user->title_id         = !empty($request->input("attendance.news.title.{$key}")) ? Crypt::decryptString($request->input("attendance.news.title.{$key}")) : '';
                            $user->organization_id  = !empty($request->input("attendance.news.organization.{$key}")) ? Crypt::decryptString($request->input("attendance.news.organization.{$key}")) : '';
                            $user->email            = $request->input("attendance.news.email.{$key}", '');
                            $user->first_name       = $request->input("attendance.news.first_name.{$key}", '');
                            $user->last_name        = $request->input("attendance.news.last_name.{$key}", '');
                            $user->position         = $request->input("attendance.news.position.{$key}", '');
                            $user->phone            = $request->input("attendance.news.phone.{$key}", '');
                            $user->password         = bcrypt(Core::uniqidReal());
                            $user->created_by       = $users->id;
                            $user->created_at       = Carbon::now();
                            
                            if($user->save()) {
    
                                $boards_attendances = new BoardsAttendances();
                                $boards_attendances->user_id                    = $user->id;
                                $boards_attendances->title_id                   = !empty($request->input("attendance.news.title.{$key}")) ? Crypt::decryptString($request->input("attendance.news.title.{$key}")) : '';
                                $boards_attendances->organization_id            = !empty($request->input("attendance.news.organization.{$key}")) ? Crypt::decryptString($request->input("attendance.news.organization.{$key}")) : '';
                                $boards_attendances->position_in_meeting_id     = !empty($request->input("attendance.news.position_in_meeting.{$key}")) ? Crypt::decryptString($request->input("attendance.news.position_in_meeting.{$key}")) : '';
                                $boards_attendances->title_name                 = $request->input("attendance.news.title_name.{$key}", '');
                                $boards_attendances->organization_name          = $request->input("attendance.news.organization_name.{$key}", '');
                                $boards_attendances->position_in_meeting_name   = $request->input("attendance.news.position_in_meeting_name.{$key}", '');
                                $boards_attendances->first_name                 = $request->input("attendance.news.first_name.{$key}", '');
                                $boards_attendances->last_name                  = $request->input("attendance.news.last_name.{$key}", '');
                                $boards_attendances->position                   = $request->input("attendance.news.position_name.{$key}", '');
                                $boards_attendances->other                      = $request->input("attendance.news.other.{$key}", '');
                                $boards_attendances->email                      = $request->input("attendance.news.email.{$key}", '');
                                $boards_attendances->phone                      = $request->input("attendance.news.phone.{$key}", '');
                                $boards_attendances->created_by                 = $users->id;
                                $boards_attendances->created_at                 = Carbon::now();
                                $boards_attendances_arr[] =                     $boards_attendances;
                            }
                        }
                    }

                    if($request->has('attendance.old') && count($request->input('attendance.old', [])) > 0) {
                        foreach ($request->input('attendance.old.organization') as $key => $item) {
                      
                            $boards_attendances = new BoardsAttendances();
                            $boards_attendances->user_id                    = !empty($request->input("attendance.old.name_option.{$key}")) ? Crypt::decryptString($request->input("attendance.old.name_option.{$key}")) : '';
                            $boards_attendances->title_id                   = !empty($request->input("attendance.old.title.{$key}")) ? Crypt::decryptString($request->input("attendance.old.title.{$key}")) : '';
                            $boards_attendances->organization_id            = !empty($request->input("attendance.old.organization.{$key}")) ? Crypt::decryptString($request->input("attendance.old.organization.{$key}")) : '';
                            $boards_attendances->position_in_meeting_id     = !empty($request->input("attendance.old.position_in_meeting.{$key}")) ? Crypt::decryptString($request->input("attendance.old.position_in_meeting.{$key}")) : '';
                            $boards_attendances->title_name                 = $request->input("attendance.old.title_name.{$key}", '');
                            $boards_attendances->organization_name          = $request->input("attendance.old.organization_name.{$key}", '');
                            $boards_attendances->position_in_meeting_name   = $request->input("attendance.old.position_in_meeting_name.{$key}", '');
                            $boards_attendances->first_name                 = $request->input("attendance.old.first_name.{$key}", '');
                            $boards_attendances->last_name                  = $request->input("attendance.old.last_name.{$key}", '');
                            $boards_attendances->position                   = $request->input("attendance.old.position_name.{$key}", '');
                            $boards_attendances->other                      = $request->input("attendance.old.other.{$key}", '');
                            $boards_attendances->email                      = $request->input("attendance.old.email.{$key}", '');
                            $boards_attendances->phone                      = $request->input("attendance.old.phone.{$key}", '');
                            $boards_attendances->created_by                 = $users->id;
                            $boards_attendances->created_at                 = Carbon::now();
                            $boards_attendances_arr[] =                     $boards_attendances;
                        }
                    }
                  
                    if($board->save()){
                        
                        if(count($boards_file_arr) > 0) {
                            if($board->files()->count() > 0) {
                                $board->files()->delete();
                            }

                            $board->files()->saveMany($boards_file_arr);
                        }

                        if(count($boards_attendances_arr) > 0) {
                            if($board->boardsattendances()->count() > 0) {
                                $board->boardsattendances()->delete();
                            }

                            $board->boardsattendances()->saveMany($boards_attendances_arr);
                        }

                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'บันทึกสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'บันทึกไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                    
                } catch(ValidationException $e) {
                    
                    DB::rollback();

                    if(count($path_unlink) > 0) {
                        Storage::delete($path_unlink);
                    }
    
                    return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);
    
                } catch (\Exception $e) {
    
                    DB::rollback();

                    if(count($path_unlink) > 0) {
                        Storage::delete($path_unlink);
                    }

                    throw $e;
                }
    
                DB::commit();
       
                return response()->json($results);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';

        if(Crypt::decryptString($id)) {


            DB::beginTransaction();

            try {

                $board = Board::find(Crypt::decryptString($id));

                if($board) {
                    if($board->delete()){
                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'ลบรายการสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'ลบรายการไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                }

            } catch(ValidationException $e) {
                
                DB::rollback();

                return Response::json($e->getErrors());

            } catch (\Exception $e) {

                DB::rollback();

                throw $e;
            }

            DB::commit();
       
            return Response::json($results);
        }
    }

    public function rules()
    {
        //
        $rules = [

        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [

        ];
        return $messages;
    }

    public function datatables(Request $request) {

        $user = Auth::user();

        $columns = [
            0 =>'boards.name',
            1 =>DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name)"),
            2 =>'boards.updated_at',
            3 =>DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name)"),
        ];

        $datas = Board::getList();
     
        $totalData = $datas->get()->count();
        $totalFiltered = $totalData;

        $start = $request->input( 'start' );
        $length = $request->input( 'length' );

        if ($request->has('search')) {
            if ($request->input ( 'search.value' ) != '') {
                
                $searchTerm = $request->input('search.value');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where( 'boards.name', 'Like', '%' . $searchTerm . '%' );
                });
            }
        }

        if ($request->has('order')) {
            if ($request->input('order.0.column') != '') {

                $orderColumn = $request->input('order.0.column');
                $orderDirection = $request->input('order.0.dir');

                $data_count = $datas;
                $totalFiltered = $data_count->get()->count();

                $datas = $datas->orderBy($columns[intval( $orderColumn )], $orderDirection )->skip($start)->take($length)->get();
            }
        }
        else {
            $datas = $datas->orderBy(DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name)"), 'ASC' )->skip($start)->take($length)->get();
        }

        $data = [];
        foreach ( $datas as $key => $col ) {
            $nestedData = [];
       
            $nestedData[0] = !empty($col->name) ? $col->name : '-';
            $nestedData[1] = !empty($col->board_count) ? $col->board_count : 0;
            $nestedData[2] = !empty($col->updated_at) ? $col->updated_at->modify('+543 year')->format('d/m/Y H.i น.') : '-';
            $nestedData[3] = !empty($col->updated_name) ? $col->updated_name : '-';

            $nestedData[4] = '<div class="dropdown">';
                $nestedData[4] .= '<button class="btn btn-secondary option dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    $nestedData[4] .= 'ดำเนินการ';
                $nestedData[4] .= '</button>';
                $nestedData[4] .= '<div class="dropdown-menu">';
                    $nestedData[4] .= '<a class="dropdown-item text-dark" href="'.(route('pages.boards.edit',['id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ])).'"><i class="far fa-edit"></i> แก้ไข</a>';
                    $nestedData[4] .= '<a class="dropdown-item text-danger btn-delete">';
                        $nestedData[4] .= Form::open(['route' => ['pages.boards.destroy', 'id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ], 'style' => 'display : none;', 'method' => 'DELETE', 'enctype' => 'multipart/form-data', 'id' => 'from_'.$key]);
                        $nestedData[4] .= Form::close();
                        $nestedData[4] .= '<i class="far fa-trash-alt"></i> ลบ';
                    $nestedData[4] .= '</a>';
                $nestedData[4] .= '</div>';
            $nestedData[4] .= '</div>';
    
            $data [] = $nestedData;
        }

        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }

    public function datatablesselect(Request $request) {

        $data = [];
        $totalData = 0;
        $totalFiltered = 0;
        if($request->has('board_id') && !empty($request->input('board_id'))) {

            $datas = Board::find($request->input('board_id'))->boardsattendances();
    
            $totalData = $datas->get()->count();
            $totalFiltered = $totalData;
         
            $rowNo = 1;
            foreach ( $datas->get() as $key => $col ) {
              
                $nestedData = [];
                $nestedData[0] = $rowNo++;
                $nestedData[1] = "{$col->organization_name}" . Html::decode(Form::hidden("board[board_id][]", Crypt::encryptString($request->input('board_id')))) . Html::decode(Form::hidden("board[organization_id][]", Crypt::encryptString($col->organization_id))) . Html::decode(Form::hidden("board[organization_name][]", $col->organization_name));
                $nestedData[2] = "{$col->position}" . Html::decode(Form::hidden("board[position_name][]", $col->position));
                $nestedData[3] = "{$col->first_name} {$col->last_name}" . Html::decode(Form::hidden("board[user_id][]", Crypt::encryptString($col->user_id))) . Html::decode(Form::hidden("board[title_id][]", Crypt::encryptString($col->title_id))) . Html::decode(Form::hidden("board[title_name][]", $col->title_name)) . Html::decode(Form::hidden("board[first_name][]", $col->first_name)) . Html::decode(Form::hidden("board[last_name][]", $col->last_name));
                $nestedData[4] = "{$col->position_in_meeting_name}" . Html::decode(Form::hidden("board[position_in_meeting_id][]", Crypt::encryptString($col->position_in_meeting_id))) . Html::decode(Form::hidden("board[position_in_meeting_name][]", $col->position_in_meeting_name));
                $nestedData[5] = "{$col->email}" . Html::decode(Form::hidden("board[email][]", $col->email));
                $nestedData[6] = "{$col->phone}" . Html::decode(Form::hidden("board[phone][]", $col->phone));
                $nestedData[7] = Html::decode(Form::text("board[other][]", $col->other, ['class' => 'form-control']));
                $nestedData[8] = Html::decode(Form::select("board[status][]", [], null, ['class' => 'form-control', 'placeholder' => 'กรุณาเลือก']));
                $nestedData[9] = '<div class="dropdown">';
                    $nestedData[9] .= '<button class="btn btn-secondary option dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                        $nestedData[9] .= 'ดำเนินการ';
                    $nestedData[9] .= '</button>';
                    $nestedData[9] .= '<div class="dropdown-menu">';
                        $nestedData[9] .= '<a class="dropdown-item text-dark" onclick="sendMail(this, '."'".'CURRENT'."'".')"><i class="far fa-envelope"></i> ส่งอีเมล</a>';
                    $nestedData[9] .= '</div>';
                $nestedData[9] .= '</div>';

                $data [] = $nestedData;
            }
        }
   
        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }
}
