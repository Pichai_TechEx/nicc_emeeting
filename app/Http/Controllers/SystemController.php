<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Crypt, Storage, Response, Mail};
use TechEx\{System, File};
use Collective\Html\FormFacade as Form;
use TechEx\Http\Controllers\Core;
use Carbon\Carbon;
use TechEx\Mail\MeetingInvitation;
use Validator;
use Auth, Config;
use Intervention\Image\ImageManagerStatic as Image;

class SystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];
      
        if(System::count() > 0) {
            foreach (System::all() as $key => $variable) {
                $result['data'][$variable->variable] = $variable->value;
            }
            
            $result['data'] = (object)$result['data'];
            if($result['data']->file_id) {
                if($file = File::find($result['data']->file_id)) {
                    $result['data']->path = asset(Storage::disk('local')->url($file->path));
                }
            }
        }

        return view('pages.system.index', [
            'result' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';
        $file_id = null;

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {
    
            DB::beginTransaction();

            try {
        
                $variable = [
                    [
                        'system_name_th',
                        'system_name_en',
                        'system_short_name_th',
                        'system_short_name_en',
                        'file_id'
                    ],
                    [
                        'smtp_host',
                        'smtp_port',
                        'mail_accout',
                        'mail_password',
                        'mail_is_enabled'
                    ]
                ];

                if($request->file('fileToUpload')) {
                
                    $fileName = "emeeting.png";
                    $path = $request->file('fileToUpload')->storeAs('public/uploads', $fileName);
                    
                    Image::configure(array('driver' => 'imagick'));
                    $img    = Image::make(Storage::path($path));
                    $width  = $img->width();
                    $height = $img->height();
            
                    // thumbnail
                    $height_bg = 0;
                    $width_bg = 0;
                    $width_thumbnail  = $width;
                    $height_thumbnail = $height;
                    $MAX_WIDTH_thumbnail = 275;
                    $MAX_HEIGHT_thumbnail = 275;
                    $img->backup();
            
                    if($width>$height) {
            
                        if ($width > $MAX_WIDTH_thumbnail) {
                            $height_thumbnail *= $MAX_WIDTH_thumbnail / $width;
                            $width_thumbnail = $MAX_WIDTH_thumbnail;
                        }
                
                        $img->encode("png", 100)->resize($width_thumbnail, $height_thumbnail)->resizeCanvas(null, $MAX_HEIGHT_thumbnail, 'center', false, '000000')->save(Storage::disk('public')->path("uploads/emeeting-thumbnail.png"), 100)->reset();
                    } else {
            
                        if ($height > $MAX_HEIGHT_thumbnail) {
                            $width_thumbnail *= $MAX_HEIGHT_thumbnail / $height;
                            $height_thumbnail = $MAX_HEIGHT_thumbnail;
                        }
                
                        $img->encode("png", 100)->resize($width_thumbnail, $height_thumbnail)->resizeCanvas($MAX_WIDTH_thumbnail, null, 'center', false, '000000')->save(Storage::disk('public')->path("uploads/emeeting-thumbnail.png"), 100)->reset();
                    }

                    $img->encode('png', 100)->resize(180, 180)->save(Storage::disk('public')->path("uploads/emeeting-icon-180x180.png"), 100)->reset();
                    $img->encode('png', 100)->resize(192, 192)->save(Storage::disk('public')->path("uploads/emeeting-icon-192x192.png"), 100)->reset();
                    $img->encode('ico', 100)->resize(32, 32)->save(Storage::disk('public')->path("uploads/emeeting-icon.ico"), 100)->reset();
                                    
                    error_reporting(E_ALL);

                    ini_set("display_errors", 1);
                    ini_set('memory_limit', '256M');

                    $hashName = "emeeting-thumbnail.png";
                    $destinationPath = 'uploads/'.$hashName;

                    $file_new = new File();
                    $file_new->originalName = $hashName;
                    $file_new->hashName     = $hashName;
                    $file_new->size         = Storage::disk('public')->size("uploads/emeeting-thumbnail.png");
                    $file_new->mimeType     = "image/png";
                    $file_new->extension    = 'png';
                    $file_new->path         = $destinationPath;
                    $file_new->created_at   = Carbon::now();

                    if($file_new->save()) {
                        $file_id = $file_new->id;
                        if(copy(Storage::disk('public')->path("uploads/emeeting-icon-180x180.png"), str_replace("storage\app", "public\images", Storage::path("logos/emeeting-icon-180x180.png")))){
                            if(copy(Storage::disk('public')->path("uploads/emeeting-icon-192x192.png"), str_replace("storage\app", "public\images", Storage::path("logos/emeeting-icon-192x192.png")))){
                                if(copy(Storage::disk('public')->path("uploads/emeeting-icon.ico"), str_replace("storage\app", "public\images", Storage::path("logos/emeeting-icon.ico")))){
                                    Storage::delete(["emeeting.png", "uploads/emeeting-icon-180x180.png", "uploads/emeeting-icon-192x192.png", "uploads/emeeting-icon.ico"]);
                                }
                            }
                        }
                    }
                }

                $variable_new = [];
                if($request->has('cerrent_tab')) {
                    switch (intval($request->cerrent_tab)) {
                        case 1:
                            $variable_new = $variable[0];
                            $system = System::whereIn('variable', $variable_new);
                            $variable_new = array_flip($variable_new);
                            break;
                        case 2:
                            $variable_new = $variable[1];
                            $system = System::whereIn('variable', $variable_new);
                            $variable_new = array_flip($variable_new);
                            break;
                    }
                }

                $start = 1;
                if($system->count() > 0) {
                    $count = $system->get()->count();
                    foreach ($system->get()->all() as $key => $item) {
                        
                        if($item->variable == 'file_id') {
                            $item->value        = $file_id;
                        } else {
                            $item->value        = $request->input($item->variable, '');
                        }

                        $item->updated_by   = $user->id;
                        $item->updated_at   = Carbon::now();
                        if($count == $start) {
                            if($item->save()){
                                $results->status         = true;
                                $results->icon           = 'success';
                                $results->message        = 'บันทึกสำเร็จ';
                                $results->redirect_url   = '';
                            } else {
                                $results->status        = false;
                                $results->icon          = 'error';
                                $results->message       = 'บันทึกไม่สำเร็จ';
                                $results->redirect_url  = '';
                            }
                        } else {
                            $item->save();
                        }
                        $start++;
                    }
                }
                
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
   
            return response()->json($results);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

        
    public function rules()
    {
        //
        $rules = [

        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [

        ];
        return $messages;
    }

    // public function testmail()
    // {
    //     $user = Auth::user();
    //     $variable = [
    //         'system_name_en',
    //         'smtp_host',
    //         'smtp_port',
    //         'mail_accout',
    //         'mail_password',
    //         'mail_is_enabled'
    //     ];

    //     $variable = array_flip($variable);
    //     $system_mail = System::whereIn('variable', ['system_name_en', 'smtp_host', 'smtp_port', 'mail_accout', 'mail_password', 'mail_is_enabled']);

    //     if($system_mail->count() > 0) {
    //         foreach ($system_mail->get() as $key => $value) {
    //             $variable[$value->variable] = $value->value;
    //         }
    //     }

    //     $variable = (object)$variable;
        
    //     Config::set('mail.driver', 'smtp');
    //     Config::set('mail.host', $variable->smtp_host);
    //     Config::set('mail.port', $variable->smtp_port);
    //     Config::set('mail.username', $variable->mail_accout);
    //     Config::set('mail.password', $variable->mail_password);
    //     Config::set('mail.encryption', 'tls');
    //     Config::set('mail.from.name', $variable->system_name_en);
      
    //     Mail::to($user->email)->send(new MeetingInvitation($user));

    //     if(count(Mail::failures()) > 0) {

    //         $text = "มีความล้มเหลวอย่างน้อยหนึ่งรายการ พวกเขาเป็น : <br />";
         
    //         foreach(Mail::failures() as $email_address) {
    //             $text .= " - $email_address <br />";
    //          }
    //          return $text;
    //      } else {
    //          return "ไม่มีข้อผิดพลาด, ส่งทั้งหมดสำเร็จ!";
    //      }
    // }

}
