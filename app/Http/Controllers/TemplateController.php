<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Crypt, Storage, Response};
use TechEx\{Template, TemplateIssues, File};
use Collective\Html\FormFacade as Form;
use TechEx\Http\Controllers\Core;
use Carbon\Carbon;
use Validator;
use Auth;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $result = [];

        return view('pages.template.index',[
            'result' => $result
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $result = [];

        return view('pages.template.create',[
            'result' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $users = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';
        $path_unlink = [];

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();
            
            try {

                $file_id = null;
                if($request->has('fileToUpload')) {
                    foreach ($request->fileToUpload as $key => $file) {
                        $file_result    = Core::saveFile($file);
                        $path_unlink[]  = $file_result['destinationPath'];
                        $file_id        = $file_result['file_new_id'];
                    }
                }

                $template = new Template();
                $template->name             = $request->input('name', '');
                $template->note             = $request->input('note', '');
                $template->text_watermark   = $request->input('text_watermark', '');
                $template->file_id          = $file_id;
                $template->is_enabled       = $request->input('is_enabled', 0);
                $template->created_by       = $users->id;
                $template->created_at       = Carbon::now();

                $template_issues_arr = [];
                if($request->has('template_issues')) {
                    if(count($request->input('template_issues')) > 0) {
                        foreach ($lists = $request->input('template_issues') as $key => $list) {
                 
                            $template_issues                 = new TemplateIssues();
                            $template_issues->title          = $request->input("template_issues.{$key}.value", '');
                            $template_issues->is_topic       = 0;
                            $template_issues->level          = 0;
                            $template_issues->created_by     = $users->id;
                            $template_issues->created_at     = Carbon::now();

                            $template_issues_arr[] = $template_issues;
                        }
                    }
                }
               
                if($template->save()){
                    if(count($template_issues_arr) > 0) {
                        $template->templateissues()->saveMany($template_issues_arr);
                    }

                    $results->status         = true;
                    $results->icon           = 'success';
                    $results->message        = 'บันทึกสำเร็จ';
                    $results->redirect_url   = route('pages.template.index');
                } else {
                    $results->status        = false;
                    $results->icon          = 'error';
                    $results->message       = 'บันทึกไม่สำเร็จ';
                    $results->redirect_url  = '';
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                if(count($path_unlink) > 0) {
                    Storage::delete($path_unlink);
                }

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();

                if(count($path_unlink) > 0) {
                    Storage::delete($path_unlink);
                }

                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = [];
        $file_upload = [];

        $user = Auth::user();

        if(Crypt::decryptString($id)) {
            $template = Template::find(Crypt::decryptString($id));
       
            $template_issues_arr = [];
            $template_issues = $template->templateissues()->whereNull('parent')->get();
         
            if($template_issues->sortBy->level->count() > 0) {
                foreach ($template_issues->sortBy->level->all() as $list) {
                    $main = (object)[
                        'template_issues_id'=> Crypt::encryptString($list->id),
                        'title'             => htmlspecialchars($list->title),
                    ];

                    $template_issues_arr[] = $main;
                }
            }
            
            if($template->file_id) {
                $files = File::find($template->file_id);
                if($files) {
                    $files->urlencode_path = Crypt::encryptString($files->path);
                    $files->path = asset(Storage::disk('local')->url($files->path));
                    $file_upload[] = (object)[
                        'id'   => $files->id,
                        'name' => $files->originalName,
                        'size' => $files->size,
                        'type' => $files->mimeType,
                        'url'  => $files->path
                    ];
                }
            }

            $result = [
                'data' => (object)[
                    "id"            => Crypt::encryptString($template->id),
                    "name"          => $template->name,
                    "note"          => $template->note,
                    "text_watermark"=> $template->text_watermark,
                    'is_enabled'    => isset($template->is_enabled) ? $template->is_enabled : '',
                  
                ],
                "template_issues"   => json_encode($template_issues_arr),
                "files"             => json_encode($file_upload)
            ];
        }

   
        return view('pages.template.edit',[
            'result' => $result
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = Auth::user();

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->redirect_url   = '';
        $path_unlink = [];

        if(Crypt::decryptString($id)) {
            $validator = Validator::make($request->all(), $this->rules(), $this->messages());
            if($validator->fails()){
                return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
            } else {
        
                DB::beginTransaction();

                try {
            
                    $file_id = null;
                    if($request->has('file_id') && count($request->file_id) > 0) {
                        foreach ($request->file_id as $file_id) {
                            $file = File::find($file_id);
                            $file_id = $file->id;
                        }
                    }

                    if($request->has('fileToUpload')) {
                        foreach ($request->fileToUpload as $key => $file) {
                            $file_result    = Core::saveFile($file);
                            $path_unlink[]  = $file_result['destinationPath'];
                            $file_id        = $file_result['file_new_id'];
                        }
                    }
    
                    $template = Template::find(Crypt::decryptString($id));
                    $template->name             = $request->input('name', '');
                    $template->note             = $request->input('note', '');
                    $template->text_watermark   = $request->input('text_watermark', '');
                    $template->file_id          = $file_id;
                    $template->is_enabled       = $request->input('is_enabled', 0);
                    $template->updated_by       = $users->id;
                    $template->updated_at       = Carbon::now();
    
                    $template_issues_arr = [];
                    if($request->has('template_issues')) {
                        if(count($request->input('template_issues')) > 0) {
                            foreach ($lists = $request->input('template_issues') as $key => $list) {
                     
                                $template_issues                 = new TemplateIssues();
                                $template_issues->title          = $request->input("template_issues.{$key}.value", '');
                                $template_issues->is_topic       = 0;
                                $template_issues->level          = 0;
                                $template_issues->created_by     = $users->id;
                                $template_issues->created_at     = Carbon::now();
    
                                $template_issues_arr[] = $template_issues;
                            }
                        }
                    }
                   
                    if($template->save()){
                        
                        if(count($template_issues_arr) > 0) {

                            if($template->templateissues()->count() > 0) {
                                $template->templateissues()->delete();
                            }

                            $template->templateissues()->saveMany($template_issues_arr);
                        }

                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'บันทึกสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'บันทึกไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                    
                } catch(ValidationException $e) {
                    
                    DB::rollback();

                    if(count($path_unlink) > 0) {
                        Storage::delete($path_unlink);
                    }
    
                    return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);
    
                } catch (\Exception $e) {
    
                    DB::rollback();

                    if(count($path_unlink) > 0) {
                        Storage::delete($path_unlink);
                    }

                    throw $e;
                }
    
                DB::commit();
       
                return response()->json($results);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';

        if(Crypt::decryptString($id)) {


            DB::beginTransaction();

            try {

                $template = Template::find(Crypt::decryptString($id));

                if($template) {
                    if($template->delete()){
                        $results->status         = true;
                        $results->icon           = 'success';
                        $results->message        = 'ลบรายการสำเร็จ';
                        $results->redirect_url   = '';
                    } else {
                        $results->status        = false;
                        $results->icon          = 'error';
                        $results->message       = 'ลบรายการไม่สำเร็จ';
                        $results->redirect_url  = '';
                    }
                }

            } catch(ValidationException $e) {
                
                DB::rollback();

                return Response::json($e->getErrors());

            } catch (\Exception $e) {

                DB::rollback();

                throw $e;
            }

            DB::commit();
       
            return Response::json($results);
        }
    }

    public function rules()
    {
        //
        $rules = [

        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [

        ];
        return $messages;
    }

    public function datatables(Request $request) {

        $user = Auth::user();

        $columns = [
            0 =>'templates.name',
            1 =>'templates.is_enabled',
            2 =>'templates.updated_at',
            3 =>'updated_name',
        ];

        $datas = Template::getList();
    
        $totalData = $datas->get()->count();
        $totalFiltered = $totalData;

        $start = $request->input( 'start' );
        $length = $request->input( 'length' );

        if ($request->has('search')) {
            if ($request->input ( 'search.value' ) != '') {
                
                $searchTerm = $request->input('search.value');
                $datas = $datas->where(function($query) use ($searchTerm) {
                    $query->where( 'templates.name', 'Like', '%' . $searchTerm . '%' );
                });
            }
        }

        if ($request->has('order')) {
            if ($request->input('order.0.column') != '') {

                $orderColumn = $request->input('order.0.column');
                $orderDirection = $request->input('order.0.dir');

                $data_count = $datas;
                $totalFiltered = $data_count->get()->count();

                $datas = $datas->orderBy($columns[intval( $orderColumn )], $orderDirection )->skip($start)->take($length)->get();
            }
        } else {
            $datas = $datas->orderBy('templates.updated_at', 'ASC' )->skip($start)->take($length)->get();
        }

        $data = [];
        foreach ( $datas as $key => $col ) {
            $nestedData = [];
         
            $nestedData[0] = !empty($col->name) ? $col->name : '-';
            $nestedData[1] = !empty($col->is_enabled) ? 'เปิด' : 'ปิด';
            $nestedData[2] = !empty($col->updated_at) ? $col->updated_at->modify('+543 year')->format('d/m/Y H.i น.') : '-';
            $nestedData[3] = !empty($col->updated_name) ? $col->updated_name : '-';

            $nestedData[4] = '<div class="dropdown">';
                $nestedData[4] .= '<button class="btn btn-secondary option dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    $nestedData[4] .= 'ดำเนินการ';
                $nestedData[4] .= '</button>';
                $nestedData[4] .= '<div class="dropdown-menu">';
                    $nestedData[4] .= '<a class="dropdown-item text-dark" href="'.(route('pages.template.edit',['id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ])).'"><i class="far fa-edit"></i> แก้ไข</a>';
                    $nestedData[4] .= '<a class="dropdown-item text-danger btn-delete">';
                        $nestedData[4] .= Form::open(['route' => ['pages.template.destroy', 'id' => ( !empty($col->id) ? Crypt::encryptString($col->id) : '' ) ], 'style' => 'display : none;', 'method' => 'DELETE', 'enctype' => 'multipart/form-data', 'id' => 'from_'.$key]);
                        $nestedData[4] .= Form::close();
                        $nestedData[4] .= '<i class="far fa-trash-alt"></i> ลบ';
                    $nestedData[4] .= '</a>';
                $nestedData[4] .= '</div>';
            $nestedData[4] .= '</div>';
    
            $data [] = $nestedData;
        }

        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }
    
    public function calljson(Request $request) {
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->data      = [];
        $results->message   = 'ไม่ได้กำหนด';
        
        $template_arr = [];
        if($request->has('template_id')) {
            $template = Template::find($request->input('template_id', ''));

            if($template && $template->templateissues()->count() > 0) {
                foreach ($template->templateissues()->get() as $item) {
                    $template_arr[] = (object)[
                        'title'     => $item->title,
                        'is_topic'  => 0,
                        'level'     => 0
                    ];
                }

                $results->status         = true;
                $results->icon           = 'success';
                $results->message        = 'พบ template';
                $results->data = $template_arr;
            }
        }
        
        return Response::json($results);
    }
}
