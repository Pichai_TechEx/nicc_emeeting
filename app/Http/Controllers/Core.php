<?php

namespace TechEx\Http\Controllers;

use Illuminate\Support\Facades\{DB, Crypt, Response, Storage};
use Intervention\Image\ImageManagerStatic as Image;
use TechEx\NumberSorting;
use TechEx\{File};
use TechEx\Http\Controllers\{GifFrameExtractor, GifCreator};
use PHPImageWorkshop\ImageWorkshop;
use Auth;
use Carbon\Carbon;


class Core extends Controller
{
    private static $OptionIsEnabled = ['Y' => 'Enable', 'N' => 'Disable'];

    private static $OptionIsType = ['S' => 'Sales', 'R' => 'Rent'];
    
    private static $OptionIsTypeSupplier = ['L' => 'Local', 'F' => 'Foreign'];

    private static $TitleTypeOrderName = ['S' => 'ใบสั่งขาย', 'R' => 'ใบสั่งเช่า'];

    private static $TitleTypeDeliveryNoteName = ['S' => 'ใบส่งสินค้า', 'R' => 'ใบส่งสินค้า'];

    private static $TitleTypeQuotationName = ['S' => 'ใบเสนอราคาขาย/Quotation for Sale', 'R' => 'ใบเสนอราคาเช่า/Quotation for Rent'];

    private static $TitleTypeReservationName = ['S' => 'ใบจองสินค้าขาย', 'R' => 'ใบจองสินค้าเช่า'];

    private static $OptionVisitStatus = ['C' => 'Cancel', 'W' => 'Waiting for Approve', 'R' => 'Rejected', 'AP' => 'Approved', 'AC' => 'Actual', 'D' => 'Draft', 'DRE' => 'Draft Reimbursement', 'WAC' => 'Waiting for Accounting approve', 'APAC' => 'Accounting approved', 'ARE' => 'Accounting rejected', 'P' => 'Paid'];
    
    private static $OptionMaterialType = ['1' => 'For Sale', '2' => 'For Service'];
    private static $OptionManhourType = ['1' => 'For Manhour', '2' => 'For Service'];
    private static $OptionIoCostSheet = ['1' => 'Open', '2' => 'Saved', '3' => 'Waiting for Approve', '4' => 'Approve', '5' =>  'Rejected'];
    private static $OptionQuotationType = ['1' => 'Project', '2' => 'Day To Day', '3' => 'Service'];
    private static $OptionPoStatus = ['1' => 'Wait for receive', '2' => 'Cancel', '3' => 'Success'];

    public static function uniqidReal($lenght = 13) {
        // uniqid gives 13 chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($lenght / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($lenght / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        return strtoupper(substr(bin2hex($bytes), 0, $lenght));
    }

    public static function running_code($table = null, $customer_code = null) {

        $numbersorting = NumberSorting::where('table_name', $table)->first();

        $year4digit = date('Y');
        $old_year   = date('y', strtotime($numbersorting->updated_at));
        $old_month  = date('m', strtotime($numbersorting->updated_at));
        $current_year   = date('y');
        $current_month  = date('m');
        $t = [
            'old_year' => $old_year,
            'old_month' => $old_month,
            'current_year' => $current_year,
            'current_month' => $current_month
        ];
  
        switch ($table) {
            case 'users':
            case 'user_code':
                break;
            case 'project':
                break;
            default:
                if("{$old_year}{$old_month}" != "{$current_year}{$current_month}") {
                    $numbersorting->current_running_number = 0;
                }
                break;
        }

        $new_code = self::index_news_id($numbersorting->current_running_number, $numbersorting->digit);
        $numbersorting->current_running_number += 1;
        $numbersorting->updated_at = date('Y-m-d h:i:s');
        $numbersorting->save();
        
        switch ($table) {
            case 'users':
            case 'user_code':
                return "{$numbersorting->lettering_code}{$new_code}";
                break;
            case 'project':
                return "{$year4digit}-{$new_code}{$customer_code}";
                break;
            default:
                return "{$numbersorting->lettering_code}{$current_year}{$current_month}-{$new_code}";
                break;
        }
    }

    public static function current_code($table = null) {
        
        $numbersorting = NumberSorting::where('table_name', $table)->first();
        if($numbersorting) {
            $old_year   = date('y', strtotime($numbersorting->updated_at));
            $old_month  = date('m', strtotime($numbersorting->updated_at));
            $current_year   = date('y');
            $current_month  = date('m');

            switch ($table) {
                case 'users':
                case 'user_code':
                
                    break;
                default:
    
                    if("{$old_year}{$old_month}" != "{$current_year}{$current_month}") {
                        $numbersorting->current_running_number = 0;
                    }
    
                    break;
            }

            $new_code = self::index_news_id($numbersorting->current_running_number, $numbersorting->digit);
            
            switch ($table) {
                case 'users':
                case 'user_code':
                    return "{$numbersorting->lettering_code}{$new_code}";
                    break;
                default:
                    return "{$numbersorting->lettering_code}{$current_year}{$current_month}-{$new_code}";
                    break;
            }
        }
        
        return null;
    }

    public static function current_code_qt($user = null) {
        
        if(!$user) {
            $user = Auth::user();
        }

        $old_year       = date('y', strtotime($user->updated_at));
        $old_month      = date('m', strtotime($user->updated_at));
        $current_year   = date('y');
        $current_month  = date('m');

        if("{$old_month}{$old_year}" != "{$current_month}{$current_year}") {
            $user->run_qt = 0;
        }

        $new_code = self::index_news_id($user->run_qt, 3);
      
        $code_qt = "{$user->users_qt_code}Q{$current_month}{$current_year}-{$new_code}";

        return $code_qt;
      
    }

    public static function running_code_qt($user = null) {
        
        if(!$user) {
            $user = Auth::user();
        }

        $old_year       = date('y', strtotime($user->updated_at));
        $old_month      = date('m', strtotime($user->updated_at));
        $current_year   = date('y');
        $current_month  = date('m');

        if("{$old_month}{$old_year}" != "{$current_month}{$current_year}") {
            $user->run_qt = 0;
        }

        $new_code = self::index_news_id($user->run_qt, 3);
        $user->run_qt++;
        $user->updated_at = date('Y-m-d h:i:s');
        $user->save();
      
        $code_qt = "{$user->users_qt_code}Q{$current_month}{$current_year}-{$new_code}";

        return $code_qt;
      
    }

    public static function getControllerName() {
        
        $action = request()->route()->getAction();
        $controller = class_basename($action['controller']);
        list($controller, $action) = explode("@", $controller);
        
        return $controller;
    }

    public static function OptionIsEnabled($IsEnabled = "") {

        return static::$OptionIsEnabled[$IsEnabled];
    }

    public static function OptionIsType($IsType = "") {

        return static::$OptionIsType[$IsType];
    }

    public static function OptionIsTypeSupplier($IsType = "") {

        return static::$OptionIsTypeSupplier[$IsType];
    }

    public static function TitleTypeOrderName($IsType = "") {

        return static::$TitleTypeOrderName[$IsType];
    }

    public static function TitleTypeDeliveryNoteName($IsType = "") {

        return static::$TitleTypeDeliveryNoteName[$IsType];
    }
    
    public static function TitleTypeQuotationName($IsType = "") {

        return static::$TitleTypeQuotationName[$IsType];
    }
    
    public static function TitleTypeReservationName($IsType = "") {

        return static::$TitleTypeReservationName[$IsType];
    }
    
    public static function OptionVisitStatus($IsStatus = "") {

        return static::$OptionVisitStatus[$IsStatus];
    }

    public static function OptionMaterialType($IsType = "") {

        return static::$OptionMaterialType[$IsType];
    }

    public static function OptionManhourType($IsType = "") {

        return static::$OptionManhourType[$IsType];
    }

    public static function OptionIoCostSheet($IsType = "") {

        return static::$OptionIoCostSheet[$IsType];
    } 
    
    public static function OptionQuotationType($IsType = "") {

        return static::$OptionQuotationType[$IsType];
    } 

    public static function OptionPoStatus($IsType = "") {

        return static::$OptionPoStatus[$IsType];
    } 

    public static function saveFile($file) {
    
        error_reporting(E_ALL);

        ini_set("display_errors", 1);
        ini_set('memory_limit', '256M');
     
        $result = [];
        if($file) {
            
            $hashName = self::uniqidReal().".".$file->getClientOriginalExtension();
            $destinationPath = 'uploads/'.$hashName;
    
            $file_new = new File();
            $file_new->originalName = $file->getClientOriginalName();
            $file_new->hashName     = $hashName;
            $file_new->size         = $file->getSize();
            $file_new->mimeType     = $file->getMimeType();
            $file_new->extension    = $file->getClientOriginalExtension();
            $file_new->path         = $destinationPath;
            $file_new->created_at   = Carbon::now();
            $file_new->save();
    
            $path = $file->store('uploads', 'public');
            $path = Storage::disk('public')->move($path, $destinationPath);
       
            $result['destinationPath'] = $destinationPath;
            $result['file_new_id'] = $file_new->id;
        }
     
        return $result;
    }

    public static function index_news_id($news_id = 0, $digit) {
        $str_zero = "";
        $news_id = $news_id + 1;

		if($news_id) {
			if($digit > 0) {
				if(strlen(strval($news_id)) > 0){
					$d = $digit - strlen(strval($news_id));
					for ($i=0; $i < $d; $i++) { 
						$str_zero .= "0";
					}
					$str_zero .= strval($news_id);
				}
			}
		}
		return $str_zero;

    }
    
    public function img_no_data($size, $url = null) {

        $result = [];

        if($url == 'null') {
            $result['url'] = $url;
        } elseif($url == 'loading') {
            $result['url']  = $url;
        } else {
            $url = Crypt::decryptString($url);
            $result['url']  = public_path(Storage::disk('local')->url($url));
        }
  
        $result['size'] = $size;

        return view('img-no-data',[
            'result' => $result
        ]);

    }
 

}
