<?php

namespace TechEx\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{DB, Crypt, Storage, Response, Mail};
use TechEx\{Meeting, MeetingStep, MeetingIssues, MeetingIssuesFile, AttendanceMeeting, Board, BoardMeeting, Status, MeetingIssuesVoter, MeetingReport, Organization, Template, File, User, System};
use Collective\Html\FormFacade as Form;
use Validator, Auth, Config;
use TechEx\Http\Controllers\Core;
use Carbon\Carbon;
use LaravelQRCode\Facades\QRCode;
use TechEx\Mail\MeetingInvitation;

class MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $result = [];

        $board_arr = [];
        $board = Board::selection();
        if($board->count() > 0) {
            foreach ($board->get() as $board) {
                $board_arr[$board->id] = $board->name;
            }
        }
        unset($board);

        $organization_arr = [];
        $organization = Organization::selection();
        if($organization->count() > 0) {
            foreach ($organization->get() as $organization) {
                $organization_arr[$organization->id] = $organization->name;
            }
        }
        unset($organization);

        $template_arr = [];
        $template = Template::selection();
        if($template->count() > 0) {
            foreach ($template->get() as $template) {
                $template_arr[$template->id] = $template->name;
            }
        }
        unset($template);
    
        $result = [
            'data' => (object)[
                "meeting_issues" => json_encode([]),
            ],
            'board'         => $board_arr,
            'organization'  => $organization_arr,
            'template'      => $template_arr
        ];

        return view('pages.meeting.create',[
            'result' => $result
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
  
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->action    = 'store';
        $results->redirect_url   = null;

        $validator = Validator::make($request->all(), $this->rules(), $this->messages());
        if($validator->fails()){
            return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
        } else {

            DB::beginTransaction();
            
            try {
              
                $meeting = new Meeting();
                $meeting->meeting_name              = $request->input('meeting_name', '');
                $meeting->agenda                    = $request->input('agenda', '');
                $meeting->date                      = $request->input('date') ? Carbon::createFromFormat('d/m/Y', $request->input('date'))->modify('-543 year')->format('Y-m-d') : Carbon::now();
                $meeting->start_time                = $request->input('start_time') ? Carbon::createFromFormat('H:i', $request->input('start_time'))->format('H:i:s.u') : Carbon::now()->toTimeString();
                $meeting->end_time                  = $request->input('end_time') ? Carbon::createFromFormat('H:i', $request->input('end_time'))->format('H:i:s.u') : Carbon::now()->toTimeString();
                $meeting->board_id                  = $request->input('board_id', null);
                $meeting->meeting_place             = $request->input('meeting_place', '');
                $meeting->information               = $request->input('information', '');
                $meeting->meeting_organization_id   = $request->input('meeting_organization', '');
                $meeting->meeting_organization_name = !empty($organization = Organization::find($request->input('meeting_organization', ''))) ? $organization->name : '';
                $meeting->acceptance_end_date       = $request->input('acceptance_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('acceptance_end_date'))->modify('-543 year')->format('Y-m-d') : Carbon::now();
                $meeting->acceptance_end_time       = $request->input('acceptance_end_time') ? Carbon::createFromFormat('H:i', $request->input('acceptance_end_time'))->format('H:i:s.u') : Carbon::now()->toTimeString();
                $meeting->is_propose                = $request->input('is_propose', 0);
                $meeting->voter_id                  = $request->input('voter_id', 0);
                $meeting->created_by                = $user->id;
                $meeting->created_at                = Carbon::now();
             
                $meeting_step = new MeetingStep();
                $meeting_step->step         = $request->input('current_tabs',  99);
                $meeting_step->created_by   = $user->id;
                $meeting_step->created_at   = Carbon::now();

                if($meeting->save()){
                   if($meeting->meetingstep()->save($meeting_step)) {
                       $results->status         = true;
                       $results->icon           = 'success';
                       $results->message        = 'บันทึกสำเร็จ';
                       $results->done_is_step   = $meeting->meetingstep()->max('step');
                       $results->redirect_url   = route('pages.meeting.index');
                   }
                 
                } else {
                    $results->status        = false;
                    $results->icon          = 'error';
                    $results->message       = 'บันทึกไม่สำเร็จ';
                    $results->redirect_url  = null;
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $result = [];

        $user = Auth::user();

        if(Crypt::decryptString($id)) {
            $meeting = Meeting::find(Crypt::decryptString($id));
       
            $meeting_issues_arr = [];
            $meeting_issues = $meeting->meetingissues()->whereNull('parent')->get();
         
            if($meeting_issues->sortBy->level->count() > 0) {
                foreach ($meeting_issues->sortBy->level->all() as $list) {

                    $file_1 = [];
                    if($list->files()->get()->count() > 0) {
                        foreach ($list->files()->get() as $item) {
                            $files = $item->file()->first();
                            if($files) {
                                $files->urlencode_path = Crypt::encryptString($files->path);
                                $files->path = asset(Storage::disk('local')->url($files->path));
                                $file_1[] = (object)[
                                    'id'   => $files->id,
                                    'name' => $files->originalName,
                                    'size' => $files->size,
                                    'type' => $files->mimeType,
                                    'url'  => $files->path
                                ];
                            }
                        };
                    }

                    $qr_code = route('pages.meeting.qrcode', ['id' => Crypt::encryptString($list->id), 'title' => 'title_voter']);
        
                    $voter_sum_current = $list->voter_sum();

                    $voter_sum_arr = [];
                    if($voter_sum_current) {
                        $voter_sum_arr = (object)[
                            'agree'     => $voter_sum_current->agree,
                            'disagree'  => $voter_sum_current->disagree,
                            'no_comment'=> $voter_sum_current->no_comment,
                        ];
                    }
                    unset($voter_sum_current);

                    $main = (object)[
                        'meeting_issues_id' => Crypt::encryptString($list->id),
                        'title'             => htmlspecialchars_decode($list->title),
                        'detail'            => htmlspecialchars_decode($list->detail),
                        'meeting_record'    => $list->meeting_record,
                        'level'             => $list->level,
                        'is_topic'          => $list->is_topic,
                        'files'             => $file_1,
                        'qr_code'           => $qr_code,
                        'vote_by'           => !empty($list->vote_by) ? $list->vote_by : 3,
                        'voter_sum'         => $voter_sum_arr,
                        'parent'            => []
                    ];

                    unset($qr_code);
                    unset($voter_sum_arr);
                  
                    $level_1 = [];
                    if($list->parent()->get()->count() > 0) {
                        foreach ($list->parent()->get()->all() as $sub) {
                         
                            $file_2 = [];
                            if($sub->files()->get()->count() > 0) {
                                foreach ($sub->files()->get() as $item) {
                                    $files = $item->file()->first();
                                  
                                    if($files) {
                                        $files->urlencode_path = Crypt::encryptString($files->path);
                                        $files->path = asset(Storage::disk('local')->url($files->path));
                                        $file_2[] = (object)[
                                            'id'   => $files->id,
                                            'name' => $files->originalName,
                                            'size' => $files->size,
                                            'type' => $files->mimeType,
                                            'url'  => $files->path
                                        ];
                                    }
                                };
                            }
                         
                            $qr_code = route('pages.meeting.qrcode', ['id' => Crypt::encryptString($sub->id), 'title' => 'title_voter']);

                            $voter_sum_current = $sub->voter_sum();

                            $voter_sum_arr = [];
                            if($voter_sum_current) {
                                $voter_sum_arr = (object)[
                                    'agree'     => $voter_sum_current->agree,
                                    'disagree'  => $voter_sum_current->disagree,
                                    'no_comment'=> $voter_sum_current->no_comment,
                                ];
                            }
                            unset($voter_sum_current);

                            $second = (object)[
                                'meeting_issues_id' => Crypt::encryptString($sub->id),
                                'title'             => htmlspecialchars_decode($sub->title),
                                'detail'            => htmlspecialchars_decode($sub->detail),
                                'meeting_record'    => htmlspecialchars_decode($sub->meeting_record),
                                'level'             => $sub->level,
                                'is_topic'          => $sub->is_topic,
                                'files'             => $file_2,
                                'qr_code'           => $qr_code,
                                'vote_by'           => !empty($sub->vote_by) ? $sub->vote_by : 3,
                                'voter_sum'         => $voter_sum_arr,
                                'parent'            => []
                            ];
        
                            unset($qr_code);
                            unset($voter_sum_arr);
    
                            $level_2 = [];
                            if($sub->parent()->get()->count() > 0) {
                                foreach ($sub->parent()->get()->all() as $last) {

                                    $file_3 = [];
                                    if($last->files()->get()->count() > 0) {
                                        foreach ($last->files()->get() as $item) {
                                            $files = $item->file()->first();
                                          
                                            if($files) {
                                                $files->urlencode_path = Crypt::encryptString($files->path);
                                                $files->path = asset(Storage::disk('local')->url($files->path));
                                                $file_3[] = (object)[
                                                    'id'   => $files->id,
                                                    'name' => $files->originalName,
                                                    'size' => $files->size,
                                                    'type' => $files->mimeType,
                                                    'url'  => $files->path
                                                ];
                                            }
                                        };
                                    }

                                    $qr_code = route('pages.meeting.qrcode', ['id' => Crypt::encryptString($last->id), 'title' => 'title_voter']);

                                    $voter_sum_current = $last->voter_sum();

                                    $voter_sum_arr = [];
                                    if($voter_sum_current) {
                                        $voter_sum_arr = (object)[
                                            'agree'     => $voter_sum_current->agree,
                                            'disagree'  => $voter_sum_current->disagree,
                                            'no_comment'=> $voter_sum_current->no_comment,
                                        ];
                                    }
                                    unset($voter_sum_current);

                                    $level_2[] = (object)[
                                        'meeting_issues_id' => Crypt::encryptString($last->id),
                                        'title'             => htmlspecialchars_decode($last->title),
                                        'detail'            => htmlspecialchars_decode($last->detail),
                                        'meeting_record'    => htmlspecialchars_decode($last->meeting_record),
                                        'level'             => $last->level,
                                        'is_topic'          => $last->is_topic,
                                        'files'             => $file_3,
                                        'qr_code'           => $qr_code,
                                        'vote_by'           => !empty($last->vote_by) ? $last->vote_by : 3,
                                        'voter_sum'         => $voter_sum_arr,
                                        'parent'            => []
                                    ];
                
                                    unset($qr_code);
                                    unset($voter_sum_arr);
                                }
                            }
    
                            $second->parent = $level_2;
                            $level_1[] = $second;
                        }
                    }
    
                    $main->parent = $level_1;
                    $meeting_issues_arr[] = $main;
    
                }
            }

            $board_meeting_arr = [];
            $attendance_meeting_arr = [];
            if($meeting->boardmeeting()->count() > 0) {
                $board_meeting = $meeting->boardmeeting()->get();
                foreach ($board_meeting as $key => $row) {
                    $board_meeting_arr[] = (object)[
                        'board_id'                  => !empty($row->board_id) ? Crypt::encryptString($row->board_id) : '',
                        'title_id'                  => !empty($row->title_id) ? Crypt::encryptString($row->title_id) : '',
                        'organization_id'           => !empty($row->organization_id) ? Crypt::encryptString($row->organization_id) : '',
                        'user_id'                   => !empty($row->user_id) ? Crypt::encryptString($row->user_id) : '',
                        'status_id'                 => !empty($row->status_id) ? Crypt::encryptString($row->status_id) : '',
                        'position_in_meeting_id'    => !empty($row->position_in_meeting_id) ? Crypt::encryptString($row->position_in_meeting_id) : '',
                        'title_name'                => $row->title_name,
                        'organization_name'         => $row->organization_name,
                        'position_in_meeting_name'  => $row->position_in_meeting_name,
                        'first_name'                => $row->first_name,
                        'last_name'                 => $row->last_name,
                        'position'                  => $row->position,
                        'other'                     => $row->other,
                        'email'                     => $row->email,
                        'phone'                     => $row->phone,
                    ];
                }
                unset($board_meeting);
            }

            if($meeting->attendancemeeting()->count() > 0) {
                $attendance_meeting = $meeting->attendancemeeting()->get();
                foreach ($attendance_meeting as $key => $row) {
                    $attendance_meeting_arr[] = (object)[
                        'title_id'                  => !empty($row->title_id) ? Crypt::encryptString($row->title_id) : '',
                        'organization_id'           => !empty($row->organization_id) ? Crypt::encryptString($row->organization_id) : '',
                        'user_id'                   => !empty($row->user_id) ? Crypt::encryptString($row->user_id) : '',
                        'status_id'                 => !empty($row->status_id) ? Crypt::encryptString($row->status_id) : '',
                        'position_in_meeting_id'    => !empty($row->position_in_meeting_id) ? Crypt::encryptString($row->position_in_meeting_id) : '',
                        'title_name'                => $row->title_name,
                        'organization_name'         => $row->organization_name,
                        'position_in_meeting_name'  => $row->position_in_meeting_name,
                        'first_name'                => $row->first_name,
                        'last_name'                 => $row->last_name,
                        'position'                  => $row->position,
                        'other'                     => $row->other,
                        'email'                     => $row->email,
                        'phone'                     => $row->phone,
                    ];
                }
                unset($attendance_meeting);
            }
         
            $board_arr = [];
            $board = Board::selection();
            if($board->count() > 0) {
                foreach ($board->get() as $board) {
                    $board_arr[$board->id] = $board->name;
                }
            }
            unset($board);

            $organization_arr = [];
            $organization = Organization::selection();
            if($organization->count() > 0) {
                foreach ($organization->get() as $organization) {
                    $organization_arr[$organization->id] = $organization->name;
                }
            }
            unset($organization);
            
            $template_arr = [];
            $template = Template::selection();
            if($template->count() > 0) {
                foreach ($template->get() as $template) {
                    $template_arr[$template->id] = $template->name;
                }
            }
            unset($template);

            $result = [
                'data' => (object)[
                    "id"                    => Crypt::encryptString($meeting->id),
                    "channel_id"            => $meeting->id,
                    "meeting_name"          => $meeting->meeting_name,
                    "agenda"                => $meeting->agenda,
                    "date"                  => isset($meeting->date) ? Carbon::createFromFormat('Y-m-d', $meeting->date, 'Asia/Bangkok')->modify('+543 year')->format('d/m/Y') : '',
                    "start_time"            => $meeting->start_time,
                    "end_time"              => $meeting->end_time,
                    "board_id"              => $meeting->board_id,
                    "meeting_place"         => $meeting->meeting_place,
                    "information"           => $meeting->information,
                    "meeting_organization"  => $meeting->meeting_organization_id,
                    "acceptance_end_date"   => isset($meeting->acceptance_end_date) ? Carbon::createFromFormat('Y-m-d', $meeting->acceptance_end_date, 'Asia/Bangkok')->modify('+543 year')->format('d/m/Y') : '',
                    "acceptance_end_time"   => $meeting->acceptance_end_time,
                    "voter_id"              => $meeting->voter_id,
                    "is_propose"            => $meeting->is_propose,
                    "meeting_issues"        => json_encode($meeting_issues_arr),
                    "done_is_step"          => $meeting->meetingstep()->max('step'),
                    "editor"                => !empty($meeting->meetingreport()->first()) ? $meeting->meetingreport()->first()->text : ''
                ],
                'board'                 => $board_arr,
                'organization'          => $organization_arr,
                'board_meeting'         => $board_meeting_arr,
                'attendance_meeting'    => $attendance_meeting_arr,
                'template'              => $template_arr,
            ];
        }

        return view('pages.meeting.edit',[
            'result' => $result
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function voter($id, $title)
    {
        $result = [];

        $user = Auth::user();
     
        if(Crypt::decryptString($id)) {

            $meeting_issues = MeetingIssues::find(Crypt::decryptString($id));
           
            $result = [
                'title'             => $title,
                'detail'            => !empty($meeting_issues->detail) ? $meeting_issues->detail : '',
                'meeting_name'      => !empty($meeting_issues->meeting) ? $meeting_issues->meeting->meeting_name : '',
                'data'              => (object)[
                    'id'                => Crypt::encryptString($meeting_issues->id),
                    'meeting_id'        => Crypt::encryptString($meeting_issues->meeting_id),
                    'meeting_issue_id'  => Crypt::encryptString($meeting_issues->id)
                ]
            ];
            unset($meeting_issues);
        }

        return view('pages.meeting.voter',[
            'result' => $result,
        ]);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function sendvote(Request $request, $id)
    {
        $result = [];

        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->target    = '';
        $results->redirect_url   = null;
        $results->update_url   = null;

        if(Crypt::decryptString($id)) {

            DB::beginTransaction();
            
            try {

                $meeting_issues = MeetingIssues::find(Crypt::decryptString($id));
                $meeting_issues_voter = new MeetingIssuesVoter();
                $meeting_issues_voter->score        = 1;
                $meeting_issues_voter->is_voter     = $request->input('is_voter', '');
                $meeting_issues_voter->created_at   = Carbon::now();
               
                if($meeting_issues->voter()->save($meeting_issues_voter)){
                    $results->status        = true;
                    $results->icon          = 'success';
                    $results->message       = 'ลงคะแนนสำเร็จ';
                    $results->target        = $request->input('target', '');
                    $results->redirect_url  = route('pages.meeting.voter.success');
                    $results->update_url    = route('pages.meeting.voter.update', ['id' => $id]);
                 
                } else {
                    $results->status        = false;
                    $results->icon          = 'error';
                    $results->message       = 'ลงคะแนนไม่สำเร็จ';
                    $results->target        = '';
                    $results->redirect_url  = null;
                    $results->update_url    = null;
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function clearvote(Request $request)
    {
        $result = [];
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->target    = '';
        $results->redirect_url   = null;
        $results->update_url   = null;

        if($request->has('meeting_issues_id') && Crypt::decryptString($request->meeting_issues_id)) {

            DB::beginTransaction();
            
            try {
                $meeting_issues = MeetingIssues::find(Crypt::decryptString($request->meeting_issues_id));
                $meeting_issues->vote_by = !empty($request->vote_by) ? $request->vote_by : 3;
                $meeting_issues->save();

                $meeting_issues->target  = $request->input('target', '');

                if($meeting_issues->voter()->count() > 0) {
                    if($meeting_issues->voter()->delete()){
    
                        $results->status        = true;
                        $results->icon          = 'success';
                        $results->message       = 'ลงคะแนนสำเร็จ';
                   
                        event(new \TechEx\Events\VoterEvent($meeting_issues));
                    }
                } else {

                    $results->status        = true;
                    $results->icon          = 'success';
                    $results->message       = 'ลงคะแนนสำเร็จ';

                    event(new \TechEx\Events\VoterEvent($meeting_issues));  
                }
  
            } catch(ValidationException $e) {
                
                DB::rollback();

                return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);

            } catch (\Exception $e) {

                DB::rollback();
                throw $e;
            }

            DB::commit();
       
            return response()->json($results);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function beautifulformat(Request $request)
    {
        $result = [];
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->txt       = '';
       
        $daysTH = ['Sunday' => 'วันอาทิตย์', 'Monday' => 'วันจันทร์', 'Tuesday' => 'วันอังคาร', 'Wednesday' => 'วันพุธ', 'Thursday' => 'วันพฤหัสบดี', 'Friday' => 'วันศุกร์', 'Saturday' => 'วันเสาร์'];
        $monthTH = [null,'มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $monthTH_brev = [null,'ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];

        if(Crypt::decryptString($request->id)) {

            $meeting = Meeting::find(Crypt::decryptString($request->id));
           
            $dateCarbon = Carbon::createFromFormat('Y-m-d', $meeting->date, 'Asia/Bangkok');
            $date = $daysTH[$dateCarbon->format('l')];
            $date .= "ที่ {$dateCarbon->format('d')} ";
            $date .= "เดือน{$monthTH[$dateCarbon->format('n')]} ";
            $date .= "พ.ศ.{$dateCarbon->modify('+543 year')->format('Y ')} ";
           
     
            $start_time = explode(':', $meeting->start_time);
            $start_time = $start_time[0].'.'.$start_time[1];

            $end_time = explode(':', $meeting->end_time);
            $end_time = $end_time[0].'.'.$end_time[1];

            $time = "{$start_time}-{$end_time} น.";

            $date .= "เวลา {$time}";
          
            $txt = "";
            $txt .= '<p style="text-align:center;">รายงานการประชุม</p>';
            $txt .= '<p style="text-align:center;">'.$meeting->meeting_name.' ครั้งที่ '.$meeting->agenda.'</p>';
            $txt .= '<p style="text-align:center;">'.$date.'</p>';
            $txt .= '<p style="text-align:center;">ณ '.$meeting->meeting_place.'</p>';

            $txt .= '<p style="text-align:justify;">คณะกรรมการ</p>';

            $txt .= '<p>';
            if($meeting->boardmeeting()->count() > 0) {
                $board_meeting = $meeting->boardmeeting()->get();
                foreach ($board_meeting as $key => $row) {
                    $status_name = $row->status()->first() ? $row->status()->first()->name : '';
                    $txt .= "&nbsp;{$row->title_name}{$row->first_name} {$row->last_name} ({$status_name})<br>";
                }
                unset($board_meeting);
            }

            $txt .= '</p>';

            $txt .= '<p style="text-align:justify;">ผู้เข้าร่วมประชุม</p>';

            $txt .= '<p>';

            if($meeting->attendancemeeting()->count() > 0) {
                $attendance_meeting = $meeting->attendancemeeting()->get();
                foreach ($attendance_meeting as $key => $row) {
                    $status_name = $row->status()->first() ? $row->status()->first()->name : '';
                    $txt .= "&nbsp;{$row->title_name}{$row->first_name} {$row->last_name} ({$status_name})<br>";
                }
                unset($attendance_meeting);
            }

            $txt .= '</p>';
    
            if($request->has('input') && count($request->input('input', [])) > 0) {
                foreach ($request->input('input') as $key => $rows) {
               
                    $meeting_issues = MeetingIssues::find(Crypt::decryptString($request->input("input.{$key}.meeting_issues_id", '')));
               
                    $voter_sum_current = $meeting_issues->voter_sum();
                 
                    $agree = 0;
                    $disagree = 0;
                    $no_comment = 0;
                    if($voter_sum_current) {
                        $agree      = !empty($voter_sum_current->agree) ? $voter_sum_current->agree : 0;
                        $disagree   = !empty($voter_sum_current->disagree) ? $voter_sum_current->disagree : 0;
                        $no_comment = !empty($voter_sum_current->no_comment) ? $voter_sum_current->no_comment : 0;
                    }

                    $txt .= "<ol>";
                        $txt .= '<li style="text-align:justify;">';
                        $txt .= $request->input("input.{$key}.value", '');
                        $txt .= '<br>';
                        $txt .= '&nbsp;&nbsp;&nbsp;&nbsp;'.$request->input("input.{$key}.detail", '');

                        if($meeting_issues->vote_by != 3) {
                            $txt .= '<br>';
                            $txt .= '<br>';
                            $txt .= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>มติที่ประชุม</strong>';
                            $txt .= '<br>';
                            $txt .= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>เห็นด้วย '.$agree.' คน ไม่เห็นด้วย '.$disagree.' คน ไม่ลงความเห็น '.$no_comment.' คน</strong>';
                            $txt .= '<br>';
                            $txt .= '<br>';
                        }
                        unset($meeting_issues);

                        if($request->has("input.{$key}.parent") && count($request->input("input.{$key}.parent", [])) > 0) {
                            foreach ($request->input("input.{$key}.parent") as $type => $value) {
    
                                if($type == 'term') {
                                    $txt .= "<ol>";
                                } else {
                                    $txt .= "<ul>";
                                }
    
                                if($request->has("input.{$key}.parent.{$type}") && count($request->input("input.{$key}.parent.{$type}", [])) > 0) {

                                    foreach ($request->input("input.{$key}.parent.{$type}") as $sub_key => $sub) {

                                        $meeting_issues = MeetingIssues::find(Crypt::decryptString($request->input("input.{$key}.parent.{$type}.{$sub_key}.meeting_issues_id", '')));
                                        $voter_sum_current = $meeting_issues->voter_sum();
                    
                                        $agree = 0;
                                        $disagree = 0;
                                        $no_comment = 0;
                                        if($voter_sum_current) {
                                            $agree      = !empty($voter_sum_current->agree) ? $voter_sum_current->agree : 0;
                                            $disagree   = !empty($voter_sum_current->disagree) ? $voter_sum_current->disagree : 0;
                                            $no_comment = !empty($voter_sum_current->no_comment) ? $voter_sum_current->no_comment : 0;
                                        }
                                        unset($meeting_issues);

                                        $txt .= '<li style="text-align:justify;">';
                                        $txt .= $request->input("input.{$key}.parent.{$type}.{$sub_key}.value", "");
                                        $txt .= '<br>';
                                        $txt .= '&nbsp;&nbsp;&nbsp;&nbsp;'.$request->input("input.{$key}.parent.{$type}.{$sub_key}.detail", '');
                                      
                                        if(!empty($meeting_issues) && $meeting_issues->vote_by != 3) {
                                            $txt .= '<br>';
                                            $txt .= '<br>';
                                            $txt .= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>มติที่ประชุม</strong>';
                                            $txt .= '<br>';
                                            $txt .= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>เห็นด้วย '.$agree.' คน ไม่เห็นด้วย '.$disagree.' คน ไม่ลงความเห็น '.$no_comment.' คน</strong>';
                                            $txt .= '<br>';
                                            $txt .= '<br>';
                                        }
                                        unset($meeting_issues);

                                        if($request->has("input.{$key}.parent.{$type}.{$sub_key}.parent.topic") && count($request->input("input.{$key}.parent.{$type}.{$sub_key}.parent.topic", [])) > 0) {
                                            $txt .= "<ul>";
                                            foreach ($request->input("input.{$key}.parent.{$type}.{$sub_key}.parent.topic") as $last_key => $last) {

                                                $meeting_issues = MeetingIssues::find(Crypt::decryptString($request->input("input.{$key}.parent.{$type}.{$sub_key}.parent.topic.{$last_key}.meeting_issues_id", '')));
                                                $voter_sum_current = $meeting_issues->voter_sum();
                            
                                                $agree = 0;
                                                $disagree = 0;
                                                $no_comment = 0;
                                                if($voter_sum_current) {
                                                    $agree      = !empty($voter_sum_current->agree) ? $voter_sum_current->agree : 0;
                                                    $disagree   = !empty($voter_sum_current->disagree) ? $voter_sum_current->disagree : 0;
                                                    $no_comment = !empty($voter_sum_current->no_comment) ? $voter_sum_current->no_comment : 0;
                                                }

                                                $txt .= '<li style="text-align:justify;">';
                                                    $txt .= $request->input("input.{$key}.parent.{$type}.{$sub_key}.parent.topic.{$last_key}.value", "");
                                                    $txt .= '<br>';
                                                    $txt .= '&nbsp;&nbsp;&nbsp;&nbsp;'.$request->input("input.{$key}.parent.{$type}.{$sub_key}.parent.topic.{$last_key}.detail", '');

                                                    if($meeting_issues->vote_by != 3) {
                                                        $txt .= '<br>';
                                                        $txt .= '<br>';
                                                        $txt .= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>มติที่ประชุม</strong>';
                                                        $txt .= '<br>';
                                                        $txt .= '&nbsp;&nbsp;&nbsp;&nbsp;<strong>เห็นด้วย '.$agree.' คน ไม่เห็นด้วย '.$disagree.' คน ไม่ลงความเห็น '.$no_comment.' คน</strong>';
                                                        $txt .= '<br>';
                                                    }
                                                    unset($meeting_issues);
                                                $txt .= '</li>';
                                            }
                                            $txt .= "</ul>";
                                        }
                                        $txt .= '</li>';
                                    }
                                }
    
                                if($type == 'term') {
                                    $txt .= "</ol>";
                                } else {
                                    $txt .= "</ul>";
                                }
                            }
                        }
                        $txt .= '</li>';
                    $txt .= "</ol>";

                }

                $txt .= '<p style="text-align:right;">(.............................................................)</p>';
                $txt .= '<p style="text-align:right;"><strong>ผู้จัดทำการประชุม &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</strong><br><br><br data-cke-filler="true"></p>';
            }
    
            $results->txt = $txt;
            $results->status        = true;
            $results->icon          = 'success';
            $results->message       = 'รูปแบบที่สวยงามสำเร็จ';
        }

        return response()->json($results);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $users = Auth::user();
     
        $results = new \stdClass();
        $results->status    = false;
        $results->icon      = 'warning';
        $results->message   = 'ไม่ได้กำหนด';
        $results->action    = 'update';
        $results->redirect_url   = null;
        $path_unlink  = [];
   
        if(Crypt::decryptString($id)) {
            
            $validator = Validator::make($request->all(), $this->rules(), $this->messages());
            if($validator->fails()){
                return response()->json(['errors' => $validator->errors(), 'rules' => $this->rules()]);
            } else {
    
                DB::beginTransaction();
                
                try {
                  
                    $meeting = Meeting::find(Crypt::decryptString($id));
               
                    if($request->has('current_tabs')) {

                        switch ($request->input('current_tabs')) {
                            case '1':

                                $meeting->meeting_name              = $request->input('meeting_name', '');
                                $meeting->agenda                    = $request->input('agenda', '');
                                $meeting->date                      = $request->input('date') ? Carbon::createFromFormat('d/m/Y', $request->input('date'))->format('Y-m-d') : Carbon::now();
                                $meeting->start_time                = $request->input('start_time') ? Carbon::createFromFormat('H:i', $request->input('start_time'))->format('H:i:s.u') : Carbon::now()->toTimeString();
                                $meeting->end_time                  = $request->input('end_time') ? Carbon::createFromFormat('H:i', $request->input('end_time'))->format('H:i:s.u') : Carbon::now()->toTimeString();
                                $meeting->board_id                  = $request->input('board_id', null);
                                $meeting->meeting_place             = $request->input('meeting_place', '');
                                $meeting->information               = $request->input('information', '');
                                $meeting->meeting_organization_id   = $request->input('meeting_organization', '');
                                $meeting->meeting_organization_name = !empty($organization = Organization::find($request->input('meeting_organization', ''))) ? $organization->name : '';
                                $meeting->acceptance_end_date       = $request->input('acceptance_end_date') ? Carbon::createFromFormat('d/m/Y', $request->input('acceptance_end_date'))->format('Y-m-d') : Carbon::now();
                                $meeting->acceptance_end_time       = $request->input('acceptance_end_time') ? Carbon::createFromFormat('H:i', $request->input('acceptance_end_time'))->format('H:i:s.u') : Carbon::now()->toTimeString();
                                $meeting->is_propose                = $request->input('is_propose', 0);
                                $meeting->voter_id                  = $request->input('voter_id', 0);
                                $meeting->updated_by                = $users->id;
                                $meeting->updated_at                = Carbon::now();
                       
                                if($meeting->save()){
                                 
                                    $results->status        = true;
                                    $results->icon       = 'success';
                                    $results->message       = 'บันทึกสำเร็จ';
                                    $results->redirect_url  = route('pages.meeting.index');
                                } else {
                                    $results->status        = false;
                                    $results->icon          = 'error';
                                    $results->message       = 'บันทึกไม่สำเร็จ';
                                    $results->redirect_url  = null;
                                }
                                break;
                            case '2':
                                
                                $meeting_issues_arr = [];
                                $file_new_arr = [];
                                if($request->has('meeting_issues')) {
                                    if(count($request->input('meeting_issues')) > 0) {
                                        foreach ($lists = $request->input('meeting_issues') as $key => $list) {
                                 
                                            $meeting_issues                 = new MeetingIssues();
                                            $meeting_issues->title          = $request->input("meeting_issues.{$key}.value", '');
                                            $meeting_issues->detail         = $request->input("meeting_issues.{$key}.detail", '');
                                            $meeting_issues->is_topic       = 0;
                                            $meeting_issues->level          = 0;
                                            $meeting_issues->created_by     = $users->id;
                                            $meeting_issues->created_at     = Carbon::now();
                                
                                            $file_1 = [];
                                            if(!empty($request->meeting_issues[$key]['file']) && count($request->meeting_issues[$key]['file']) > 0) {
                                                foreach ($request->meeting_issues[$key]['file'] as $file) {
                                           
                                                    $file_result = Core::saveFile($file);
                                                    $path_unlink[]  = $file_result['destinationPath'];
                                                    $file_new_arr[] = $file_result['file_new_id'];

                                                    $meeting_issues_file = new MeetingIssuesFile();
                                                    $meeting_issues_file->file_id       = $file_result['file_new_id'];
                                                    $meeting_issues_file->created_by    = $users->id;
                                                    $meeting_issues_file->created_at    = Carbon::now();

                                                    $file_1[] = $meeting_issues_file;
                                                }
                                            }

                                            if(!empty($request->meeting_issues[$key]['file_id']) && count($request->meeting_issues[$key]['file_id']) > 0) {
                                                foreach ($request->meeting_issues[$key]['file_id'] as $file_id) {
                                                  
                                                    $file = File::find($file_id);

                                                    $meeting_issues_file = new MeetingIssuesFile();
                                                    $meeting_issues_file->file_id       = $file->id;
                                                    $meeting_issues_file->created_by    = $users->id;
                                                    $meeting_issues_file->created_at    = Carbon::now();

                                                    $file_1[] = $meeting_issues_file;
                                                }
                                            }
                                        
                                            $level_1 = [];
                                            if(count($request->input("meeting_issues.{$key}.parent", [])) > 0) {
                                                foreach ($request->input("meeting_issues.{$key}.parent") as $type => $sub) {

                                                    $is_topic = 0;

                                                    if($type == 'topic') {
                                                        $is_topic = 1;
                                                    }

                                                    if(count($request->input("meeting_issues.{$key}.parent.{$type}", [])) > 0) {
                                                        foreach ($request->input("meeting_issues.{$key}.parent.{$type}") as $key_sub => $item) {

                                                            $file_2 = [];
                                                            if(!empty($request->meeting_issues[$key]['parent'][$type][$key_sub]['file']) && count($request->meeting_issues[$key]['parent'][$type][$key_sub]['file']) > 0) {
                                                                foreach ($request->meeting_issues[$key]['parent'][$type][$key_sub]['file'] as $file) {

                                                                    $file_result = Core::saveFile($file);
                                                                    $path_unlink[]  = $file_result['destinationPath'];
                                                                    $file_new_arr[] = $file_result['file_new_id'];
                
                                                                    $meeting_issues_file = new MeetingIssuesFile();
                                                                    $meeting_issues_file->file_id       = $file_result['file_new_id'];
                                                                    $meeting_issues_file->created_by    = $users->id;
                                                                    $meeting_issues_file->created_at    = Carbon::now();
                
                                                                    $file_2[] = $meeting_issues_file;
                                                                }
                                                            }

                                                            if(!empty($request->meeting_issues[$key]['parent'][$type][$key_sub]['file_id']) && count($request->meeting_issues[$key]['parent'][$type][$key_sub]['file_id']) > 0) {
                                                                foreach ($request->meeting_issues[$key]['parent'][$type][$key_sub]['file_id'] as $file_id) {
                                                                  
                                                                    $file = File::find($file_id);
                
                                                                    $meeting_issues_file = new MeetingIssuesFile();
                                                                    $meeting_issues_file->file_id       = $file->id;
                                                                    $meeting_issues_file->created_by    = $users->id;
                                                                    $meeting_issues_file->created_at    = Carbon::now();
                
                                                                    $file_2[] = $meeting_issues_file;
                                                                }
                                                            }

                                                            $meeting_issues_level_2                 = new MeetingIssues();
                                                            $meeting_issues_level_2->title          = $request->input("meeting_issues.{$key}.parent.{$type}.{$key_sub}.value", ''); 
                                                            $meeting_issues_level_2->detail         = $request->input("meeting_issues.{$key}.parent.{$type}.{$key_sub}.detail", ''); 
                                                            $meeting_issues_level_2->is_topic       = $is_topic;
                                                            $meeting_issues_level_2->level          = $request->input("meeting_issues.{$key}.parent.{$type}.{$key_sub}.level", 0);
                                                            $meeting_issues_level_2->created_by     = $users->id;
                                                            $meeting_issues_level_2->created_at     = Carbon::now();

                                                            $level_2 = [];
                                                            if(count($request->input("meeting_issues.{$key}.parent.{$type}.{$key_sub}.parent", [])) > 0) {
                                                                foreach ($request->input("meeting_issues.{$key}.parent.{$type}.{$key_sub}.parent") as $type_last => $sub_last) {
                                                           
                                                                    if(count($request->input("meeting_issues.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}", [])) > 0) {
                                                                        foreach ($request->input("meeting_issues.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}") as $key_item_last => $item_last) {
                
                                                                            $file_3 = [];
                                                                            if(!empty($request->meeting_issues[$key]['parent'][$type][$key_sub]['parent'][$type_last][$key_item_last]['file']) && count($request->meeting_issues[$key]['parent'][$type][$key_sub]['parent'][$type_last][$key_item_last]['file']) > 0) {
                                                                                foreach ($request->meeting_issues[$key]['parent'][$type][$key_sub]['parent'][$type_last][$key_item_last]['file'] as $file) {
                                                                                
                                                                                    $file_result = Core::saveFile($file);
                                                                                    $path_unlink[]  = $file_result['destinationPath'];
                                                                                    $file_new_arr[] = $file_result['file_new_id'];
                                
                                                                                    $meeting_issues_file = new MeetingIssuesFile();
                                                                                    $meeting_issues_file->file_id       = $file_result['file_new_id'];
                                                                                    $meeting_issues_file->created_by    = $users->id;
                                                                                    $meeting_issues_file->created_at    = Carbon::now();
                                
                                                                                    $file_3[] = $meeting_issues_file;
                                                                                }
                                                                            }

                                                                            if(!empty($request->meeting_issues[$key]['parent'][$type][$key_sub]['parent'][$type_last][$key_item_last]['file_id']) && count($request->meeting_issues[$key]['parent'][$type][$key_sub]['parent'][$type_last][$key_item_last]['file_id']) > 0) {
                                                                                foreach ($request->meeting_issues[$key]['parent'][$type][$key_sub]['parent'][$type_last][$key_item_last]['file_id'] as $file_id) {
                                                                                  
                                                                                    $file = File::find($file_id);
                                
                                                                                    $meeting_issues_file = new MeetingIssuesFile();
                                                                                    $meeting_issues_file->file_id       = $file->id;
                                                                                    $meeting_issues_file->created_by    = $users->id;
                                                                                    $meeting_issues_file->created_at    = Carbon::now();
                                
                                                                                    $file_3[] = $meeting_issues_file;
                                                                                }
                                                                            }

                                                                            $meeting_issues_level_3                 = new MeetingIssues();
                                                                            $meeting_issues_level_3->title          = $request->input("meeting_issues.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}.{$key_item_last}.value", ''); 
                                                                            $meeting_issues_level_3->detail         = $request->input("meeting_issues.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}.{$key_item_last}.detail", ''); 
                                                                            $meeting_issues_level_3->is_topic       = 1;
                                                                            $meeting_issues_level_3->level          = $request->input("meeting_issues.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}.{$key_item_last}.level", 0);
                                                                            $meeting_issues_level_3->created_by     = $users->id;
                                                                            $meeting_issues_level_3->created_at     = Carbon::now();

                                                                            $g_level_2 = (object)[
                                                                                'main'      => $meeting_issues_level_3,
                                                                                'file'      => $file_3
                                                                            ];

                                                                            $level_2[] = $g_level_2;
                        
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            $g_level_1 = (object)[
                                                                'main'      => $meeting_issues_level_2,
                                                                'parent'    => $level_2,
                                                                'file'      => $file_2
                                                            ];

                                                            $level_1[] = $g_level_1;
                                                        }
                                                    }
                                                }
                                            }

                                            $g_level_0 = (object)[
                                                'main'      => $meeting_issues,
                                                'parent'    => $level_1,
                                                'file'      => $file_1
                                            ];

                                            $meeting_issues_arr[] = $g_level_0;
                                        }
                                    }

                                    if(count($meeting_issues_arr) > 0) {
                                        if($meeting->meetingissues()->count() > 0) {
                                            if($meeting->meetingpermissions()->count() > 0) {
                                                $meeting->meetingpermissions()->delete(); 
                                            }

                                            $meeting->meetingissues()->delete();
                                        }

                                        foreach ($meeting_issues_arr as $meeting_issues) {
                                            if($meeting->meetingissues()->save($meeting_issues->main)) {

                                                if(count($meeting_issues->file) > 0) {
                                                    $meeting_issues->main->files()->saveMany($meeting_issues->file);
                                                }

                                                if(count($meeting_issues->parent) > 0) {
                                                    foreach ($meeting_issues->parent as $level_1) {
                                                        $level_1->main->meeting_id = $meeting_issues->main->meeting_id;
                                                        $level_1->main->parent     = $meeting_issues->main->id;
                                                        if($level_1->main->save()) {

                                                            if(count($level_1->file) > 0) {
                                                                $level_1->main->files()->saveMany($level_1->file);
                                                            }

                                                            if(count($level_1->parent) > 0) {
                                                                foreach ($level_1->parent as $level_2) {
                                                                    $level_2->main->meeting_id = $level_1->main->meeting_id;
                                                                    $level_2->main->parent     = $level_1->main->id;
                                                                    if($level_2->main->save()) {
                                                                        if(count($level_2->file) > 0) {
                                                                            $level_2->main->files()->saveMany($level_2->file);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if($meeting->meetingstep()->where('step', 2)->get()->count() > 0) {
                                  
                                        $meeting_step = $meeting->meetingstep()->where('step', 2)->first();
                                        $meeting_step->updated_by   = $users->id;
                                        $meeting_step->updated_at   = Carbon::now();

                                        if($meeting_step->save()) {
                                            $results->status         = true;
                                            $results->icon           = 'success';
                                            $results->message        = 'บันทึกสำเร็จ';
                                            $results->done_is_step   = $meeting->meetingstep()->max('step');
                                            $results->redirect_url   = route('pages.meeting.index');
                                        } else {
                                            $results->status        = false;
                                            $results->icon          = 'error';
                                            $results->message       = 'บันทึกไม่สำเร็จ';
                                            $results->redirect_url  = null;
                                        }
                                    } else {
                                        $meeting_step = new MeetingStep();
                                        $meeting_step->step         = $request->input('current_tabs',  99);
                                        $meeting_step->created_by   = $users->id;
                                        $meeting_step->created_at   = Carbon::now();
                        
                                        if($meeting->meetingstep()->save($meeting_step)) {
                                            $results->status         = true;
                                            $results->icon           = 'success';
                                            $results->message        = 'บันทึกสำเร็จ';
                                            $results->done_is_step   = $meeting->meetingstep()->max('step');
                                            $results->redirect_url   = route('pages.meeting.index');
                                        } else {
                                            $results->status        = false;
                                            $results->icon          = 'error';
                                            $results->message       = 'บันทึกไม่สำเร็จ';
                                            $results->redirect_url  = null;
                                        }
                                    }

                                }
                                break;
                            case '3':
                            
                                $attendance_meeting_arr = [];
                                $board_meeting_arr = [];

                                if($request->has('board')) {
                                    if(count($request->input('board.organization_id')) > 0) {
                                        foreach ($request->input('board.organization_id') as $key => $row) {

                                            $board_meeting = new BoardMeeting();
                                            $board_meeting->board_id                   = !empty($request->input("board.board_id.{$key}")) ? Crypt::decryptString($request->input("board.board_id.{$key}")) : '';
                                            $board_meeting->user_id                    = !empty($request->input("board.user_id.{$key}")) ? Crypt::decryptString($request->input("board.user_id.{$key}")) : '';
                                            $board_meeting->title_id                   = !empty($request->input("board.title_id.{$key}")) ? Crypt::decryptString($request->input("board.title_id.{$key}")) : '';
                                            $board_meeting->organization_id            = !empty($request->input("board.organization_id.{$key}")) ? Crypt::decryptString($request->input("board.organization_id.{$key}")) : '';
                                            $board_meeting->position_in_meeting_id     = !empty($request->input("board.position_in_meeting_id.{$key}")) ? Crypt::decryptString($request->input("board.position_in_meeting_id.{$key}")) : '';
                                            $board_meeting->title_name                 = $request->input("board.title_name.{$key}", '');
                                            $board_meeting->organization_name          = $request->input("board.organization_name.{$key}", '');
                                            $board_meeting->position_in_meeting_name   = $request->input("board.position_in_meeting_name.{$key}", '');
                                            $board_meeting->first_name                 = $request->input("board.first_name.{$key}", '');
                                            $board_meeting->last_name                  = $request->input("board.last_name.{$key}", '');
                                            $board_meeting->position                   = $request->input("board.position_name.{$key}", '');
                                            $board_meeting->other                      = $request->input("board.other.{$key}", '');
                                            $board_meeting->email                      = $request->input("board.email.{$key}", '');
                                            $board_meeting->phone                      = $request->input("board.phone.{$key}", '');
                                            $board_meeting->status_id                  = !empty($request->input("board.status.{$key}")) ? Crypt::decryptString($request->input("board.status.{$key}")) : '';
                                            $board_meeting->created_by                 = $users->id;
                                            $board_meeting->created_at                 = Carbon::now();

                                            $board_meeting_arr[] = $board_meeting;
                                        }
                                    }
                                }
                             
                                if($request->has('attendance')) {
                                    if(count($request->input('attendance.old.organization')) > 0) {
                                        foreach ($request->input('attendance.old.organization') as $key => $row) {

                                            $attendance_meeting = new AttendanceMeeting();
                                            $attendance_meeting->user_id                    = !empty($request->input("attendance.old.name_option.{$key}")) ? Crypt::decryptString($request->input("attendance.old.name_option.{$key}")) : '';
                                            $attendance_meeting->title_id                   = !empty($request->input("attendance.old.title.{$key}")) ? Crypt::decryptString($request->input("attendance.old.title.{$key}")) : '';
                                            $attendance_meeting->organization_id            = !empty($request->input("attendance.old.organization.{$key}")) ? Crypt::decryptString($request->input("attendance.old.organization.{$key}")) : '';
                                            $attendance_meeting->position_in_meeting_id     = !empty($request->input("attendance.old.position_in_meeting.{$key}")) ? Crypt::decryptString($request->input("attendance.old.position_in_meeting.{$key}")) : '';
                                            $attendance_meeting->title_name                 = $request->input("attendance.old.title_name.{$key}", '');
                                            $attendance_meeting->organization_name          = $request->input("attendance.old.organization_name.{$key}", '');
                                            $attendance_meeting->position_in_meeting_name   = $request->input("attendance.old.position_in_meeting_name.{$key}", '');
                                            $attendance_meeting->first_name                 = $request->input("attendance.old.first_name.{$key}", '');
                                            $attendance_meeting->last_name                  = $request->input("attendance.old.last_name.{$key}", '');
                                            $attendance_meeting->position                   = $request->input("attendance.old.position_name.{$key}", '');
                                            $attendance_meeting->other                      = $request->input("attendance.old.other.{$key}", '');
                                            $attendance_meeting->email                      = $request->input("attendance.old.email.{$key}", '');
                                            $attendance_meeting->phone                      = $request->input("attendance.old.phone.{$key}", '');
                                            $attendance_meeting->status_id                  = !empty($request->input("attendance.old.status.{$key}")) ? Crypt::decryptString($request->input("attendance.old.status.{$key}")) : '';
                                            $attendance_meeting->created_by                 = $users->id;
                                            $attendance_meeting->created_at                 = Carbon::now();

                                            $attendance_meeting_arr[] = $attendance_meeting;
                                        }
                                    }
                                }

                                if(count($board_meeting_arr) > 0) {

                                    if($meeting->boardmeeting()->count() > 0) {
                                        $meeting->boardmeeting()->delete();
                                    }

                                    $meeting->boardmeeting()->saveMany($board_meeting_arr);
                                }

                                if(count($attendance_meeting_arr) > 0) {

                                    if($meeting->attendancemeeting()->count() > 0) {
                                        $meeting->attendancemeeting()->delete();
                                    }

                                    $meeting->attendancemeeting()->saveMany($attendance_meeting_arr);
                                }

                                if($meeting->meetingstep()->where('step', 3)->get()->count() > 0) {
                                  
                                    $meeting_step = $meeting->meetingstep()->where('step', 3)->first();
                                    $meeting_step->updated_by   = $users->id;
                                    $meeting_step->updated_at   = Carbon::now();

                                    if($meeting_step->save()) {
                                        $results->status         = true;
                                        $results->icon           = 'success';
                                        $results->message        = 'บันทึกสำเร็จ';
                                        $results->done_is_step   = $meeting->meetingstep()->max('step');
                                        $results->redirect_url   = route('pages.meeting.index');
                                    } else {
                                        $results->status        = false;
                                        $results->icon          = 'error';
                                        $results->message       = 'บันทึกไม่สำเร็จ';
                                        $results->redirect_url  = null;
                                    }
                                } else {
                                    $meeting_step = new MeetingStep();
                                    $meeting_step->step         = $request->input('current_tabs',  99);
                                    $meeting_step->created_by   = $users->id;
                                    $meeting_step->created_at   = Carbon::now();
                    
                                    if($meeting->meetingstep()->save($meeting_step)) {
                                        $results->status         = true;
                                        $results->icon           = 'success';
                                        $results->message        = 'บันทึกสำเร็จ';
                                        $results->done_is_step   = $meeting->meetingstep()->max('step');
                                        $results->redirect_url   = route('pages.meeting.index');
                                    } else {
                                        $results->status        = false;
                                        $results->icon          = 'error';
                                        $results->message       = 'บันทึกไม่สำเร็จ';
                                        $results->redirect_url  = null;
                                    }
                                }

                                break;
                            case '4':
                                if($request->has('meeting_issues_save')) {
                                    if(count($request->input('meeting_issues_save')) > 0) {
                                        foreach ($lists = $request->input('meeting_issues_save') as $key => $list) {

                                            if($request->input("meeting_issues_save.{$key}.meeting_issues_id", '')) {
                                                $meeting_issues = MeetingIssues::find(Crypt::decryptString($request->input("meeting_issues_save.{$key}.meeting_issues_id")));
                                                $meeting_issues->meeting_record = $request->input("meeting_issues_save.{$key}.meeting_record");
                                                $meeting_issues->vote_by        = $request->input("meeting_issues_save.{$key}.vote_by", 3);
                                                $meeting_issues->updated_by     = $users->id;
                                                $meeting_issues->updated_at     = Carbon::now();

                                                $meeting_issues_voter_change_arr = [];
                                                if($request->input("meeting_issues_save.{$key}.vote_by") == 1 ||
                                                    $request->input("meeting_issues_save.{$key}.vote_by") == 3) {

                                                    $meeting_issues_voter_agree = new MeetingIssuesVoter();
                                                    $meeting_issues_voter_agree->score        = $request->input("meeting_issues_save.{$key}.agree", 0);
                                                    $meeting_issues_voter_agree->is_voter     = 1;
                                                    $meeting_issues_voter_agree->created_by   = $users->id;
                                                    $meeting_issues_voter_agree->created_at   = Carbon::now();
                                                    $meeting_issues_voter_change_arr[] = $meeting_issues_voter_agree;

                                                    $meeting_issues_voter_disagree = new MeetingIssuesVoter();
                                                    $meeting_issues_voter_disagree->score        = $request->input("meeting_issues_save.{$key}.disagree", 0);
                                                    $meeting_issues_voter_disagree->is_voter     = 2;
                                                    $meeting_issues_voter_disagree->created_by   = $users->id;
                                                    $meeting_issues_voter_disagree->created_at   = Carbon::now();
                                                    $meeting_issues_voter_change_arr[] = $meeting_issues_voter_disagree;

                                                    $meeting_issues_voter_no_comment = new MeetingIssuesVoter();
                                                    $meeting_issues_voter_no_comment->score        = $request->input("meeting_issues_save.{$key}.no_comment", 0);
                                                    $meeting_issues_voter_no_comment->is_voter     = 3;
                                                    $meeting_issues_voter_no_comment->created_by   = $users->id;
                                                    $meeting_issues_voter_no_comment->created_at   = Carbon::now();
                                                    $meeting_issues_voter_change_arr[] = $meeting_issues_voter_no_comment;

                                                    if(count($meeting_issues_voter_change_arr) > 0) {
                                                        if($meeting_issues->voter()->count() > 0) {
                                                            $meeting_issues->voter()->delete();
                                                        }

                                                        $meeting_issues->voter()->saveMany($meeting_issues_voter_change_arr);
                                                    }
                                                }
                                                unset($meeting_issues_voter_change_arr);

                                                if($meeting_issues->save()) {
                                                    if(count($request->input("meeting_issues_save.{$key}.parent", [])) > 0) {
                                                        foreach ($request->input("meeting_issues_save.{$key}.parent") as $type => $sub) {
        
                                                            $is_topic = 0;
        
                                                            if($type == 'topic') {
                                                                $is_topic = 1;
                                                            }
        
                                                            if(count($request->input("meeting_issues_save.{$key}.parent.{$type}", [])) > 0) {
                                                                foreach ($request->input("meeting_issues_save.{$key}.parent.{$type}") as $key_sub => $item) {
                                                               
                                                                    $meeting_issues_level_2 = MeetingIssues::find(Crypt::decryptString($request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.meeting_issues_id", '')));
                                                                    $meeting_issues_level_2->meeting_record = $request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.meeting_record");
                                                                    $meeting_issues_level_2->vote_by        = $request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.vote_by", 3);
                                                                    $meeting_issues_level_2->updated_by     = $users->id;
                                                                    $meeting_issues_level_2->updated_at     = Carbon::now();

                                                                    $meeting_issues_voter_change_arr = [];
                                                                    if($request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.vote_by") == 1 || 
                                                                        $request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.vote_by") == 3) {
                    
                                                                        $meeting_issues_voter_agree = new MeetingIssuesVoter();
                                                                        $meeting_issues_voter_agree->score        = $request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.agree", 0);
                                                                        $meeting_issues_voter_agree->is_voter     = 1;
                                                                        $meeting_issues_voter_agree->created_by   = $users->id;
                                                                        $meeting_issues_voter_agree->created_at   = Carbon::now();
                                                                        $meeting_issues_voter_change_arr[] = $meeting_issues_voter_agree;
                    
                                                                        $meeting_issues_voter_disagree = new MeetingIssuesVoter();
                                                                        $meeting_issues_voter_disagree->score        = $request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.disagree", 0);
                                                                        $meeting_issues_voter_disagree->is_voter     = 2;
                                                                        $meeting_issues_voter_disagree->created_by   = $users->id;
                                                                        $meeting_issues_voter_disagree->created_at   = Carbon::now();
                                                                        $meeting_issues_voter_change_arr[] = $meeting_issues_voter_disagree;
                    
                                                                        $meeting_issues_voter_no_comment = new MeetingIssuesVoter();
                                                                        $meeting_issues_voter_no_comment->score        = $request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.no_comment", 0);
                                                                        $meeting_issues_voter_no_comment->is_voter     = 3;
                                                                        $meeting_issues_voter_no_comment->created_by   = $users->id;
                                                                        $meeting_issues_voter_no_comment->created_at   = Carbon::now();
                                                                        $meeting_issues_voter_change_arr[] = $meeting_issues_voter_no_comment;
                    
                                                                        if(count($meeting_issues_voter_change_arr) > 0) {
                                                                            if($meeting_issues_level_2->voter()->count() > 0) {
                                                                                $meeting_issues_level_2->voter()->delete();
                                                                            }
                    
                                                                            $meeting_issues_level_2->voter()->saveMany($meeting_issues_voter_change_arr);
                                                                        }
                                                                    }
                                                                    unset($meeting_issues_voter_change_arr);

                                                                    if($meeting_issues_level_2->save()) {

                                                                        if(count($request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.parent", [])) > 0) {
                                                                            foreach ($request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.parent") as $type_last => $sub_last) {
                                                                       
                                                                                if(count($request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}", [])) > 0) {
                                                                                    foreach ($request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}") as $key_item_last => $item_last) {
                            
                                                                                        $meeting_issues_level_3 = MeetingIssues::find(Crypt::decryptString($request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}.{$key_item_last}.meeting_issues_id", '')));
                                                                                        $meeting_issues_level_3->meeting_record = $request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}.{$key_item_last}.meeting_record");
                                                                                        $meeting_issues_level_3->vote_by        = $request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}.{$key_item_last}.vote_by", 3);
                                                                                        $meeting_issues_level_3->updated_by     = $users->id;
                                                                                        $meeting_issues_level_3->updated_at     = Carbon::now();
                                                                                        $meeting_issues_level_3->save();

                                                                                        $meeting_issues_voter_change_arr = [];
                                                                                        if($request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}.{$key_item_last}.vote_by") == 1 ||
                                                                                            $request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}.{$key_item_last}.vote_by") == 3) {
                                        
                                                                                            $meeting_issues_voter_agree = new MeetingIssuesVoter();
                                                                                            $meeting_issues_voter_agree->score        = $request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}.{$key_item_last}.agree", 0);
                                                                                            $meeting_issues_voter_agree->is_voter     = 1;
                                                                                            $meeting_issues_voter_agree->created_by   = $users->id;
                                                                                            $meeting_issues_voter_agree->created_at   = Carbon::now();
                                                                                            $meeting_issues_voter_change_arr[] = $meeting_issues_voter_agree;
                                        
                                                                                            $meeting_issues_voter_disagree = new MeetingIssuesVoter();
                                                                                            $meeting_issues_voter_disagree->score        = $request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}.{$key_item_last}.disagree", 0);
                                                                                            $meeting_issues_voter_disagree->is_voter     = 2;
                                                                                            $meeting_issues_voter_disagree->created_by   = $users->id;
                                                                                            $meeting_issues_voter_disagree->created_at   = Carbon::now();
                                                                                            $meeting_issues_voter_change_arr[] = $meeting_issues_voter_disagree;
                                        
                                                                                            $meeting_issues_voter_no_comment = new MeetingIssuesVoter();
                                                                                            $meeting_issues_voter_no_comment->score        = $request->input("meeting_issues_save.{$key}.parent.{$type}.{$key_sub}.parent.{$type_last}.{$key_item_last}.no_comment", 0);
                                                                                            $meeting_issues_voter_no_comment->is_voter     = 3;
                                                                                            $meeting_issues_voter_no_comment->created_by   = $users->id;
                                                                                            $meeting_issues_voter_no_comment->created_at   = Carbon::now();
                                                                                            $meeting_issues_voter_change_arr[] = $meeting_issues_voter_no_comment;
                                        
                                                                                            if(count($meeting_issues_voter_change_arr) > 0) {
                                                                                                if($meeting_issues_level_3->voter()->count() > 0) {
                                                                                                    $meeting_issues_level_3->voter()->delete();
                                                                                                }
                                        
                                                                                                $meeting_issues_level_3->voter()->saveMany($meeting_issues_voter_change_arr);
                                                                                            }
                                                                                        }
                                                                                        unset($meeting_issues_voter_change_arr);
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if($meeting->meetingstep()->where('step', 4)->get()->count() > 0) {
                                  
                                            $meeting_step = $meeting->meetingstep()->where('step', 4)->first();
                                            $meeting_step->updated_by   = $users->id;
                                            $meeting_step->updated_at   = Carbon::now();
        
                                            if($meeting_step->save()) {
                                                $results->status         = true;
                                                $results->icon           = 'success';
                                                $results->message        = 'บันทึกสำเร็จ';
                                                $results->done_is_step   = $meeting->meetingstep()->max('step');
                                                $results->redirect_url   = route('pages.meeting.index');
                                            } else {
                                                $results->status        = false;
                                                $results->icon          = 'error';
                                                $results->message       = 'บันทึกไม่สำเร็จ';
                                                $results->redirect_url  = null;
                                            }
                                        } else {
                                            $meeting_step = new MeetingStep();
                                            $meeting_step->step         = $request->input('current_tabs',  99);
                                            $meeting_step->created_by   = $users->id;
                                            $meeting_step->created_at   = Carbon::now();
                            
                                            if($meeting->meetingstep()->save($meeting_step)) {
                                                $results->status         = true;
                                                $results->icon           = 'success';
                                                $results->message        = 'บันทึกสำเร็จ';
                                                $results->done_is_step   = $meeting->meetingstep()->max('step');
                                                $results->redirect_url   = route('pages.meeting.index');
                                            } else {
                                                $results->status        = false;
                                                $results->icon          = 'error';
                                                $results->message       = 'บันทึกไม่สำเร็จ';
                                                $results->redirect_url  = null;
                                            }
                                        }
                                    }
                                }
                                break;
                            case '5':

                                $meeting_report =  new MeetingReport();
                                $meeting_report->text         = $request->input('editor', '');
                                $meeting_report->created_by   = $users->id;
                                $meeting_report->created_at   = Carbon::now();

                                if($meeting->meetingreport()->save($meeting_report)) {
                                    if($meeting->meetingstep()->where('step', 5)->get()->count() > 0) {
                                      
                                        $meeting_step = $meeting->meetingstep()->where('step', 5)->first();
                                        $meeting_step->updated_by   = $users->id;
                                        $meeting_step->updated_at   = Carbon::now();
    
                                        if($meeting_step->save()) {
                                            $results->status         = true;
                                            $results->icon           = 'success';
                                            $results->message        = 'บันทึกสำเร็จ';
                                            $results->done_is_step   = $meeting->meetingstep()->max('step');
                                            $results->redirect_url   = route('pages.meeting.index');
                                        } else {
                                            $results->status        = false;
                                            $results->icon          = 'error';
                                            $results->message       = 'บันทึกไม่สำเร็จ';
                                            $results->redirect_url  = null;
                                        }
                                    } else {
                                        $meeting_step = new MeetingStep();
                                        $meeting_step->step         = $request->input('current_tabs',  99);
                                        $meeting_step->created_by   = $users->id;
                                        $meeting_step->created_at   = Carbon::now();
                        
                                        if($meeting->meetingstep()->save($meeting_step)) {
                                            $results->status         = true;
                                            $results->icon           = 'success';
                                            $results->message        = 'บันทึกสำเร็จ';
                                            $results->done_is_step   = $meeting->meetingstep()->max('step');
                                            $results->redirect_url   = route('pages.meeting.index');
                                        } else {
                                            $results->status        = false;
                                            $results->icon          = 'error';
                                            $results->message       = 'บันทึกไม่สำเร็จ';
                                            $results->redirect_url  = null;
                                        }
                                    }
                                }

                                break;
                        }
                    }
      
                } catch(ValidationException $e) {
                    
                    DB::rollback();
    
                    if(count($path_unlink) > 0) {
                        Storage::delete($path_unlink);
                    }

                    return response()->json(['errors' => $e->getErrors(), 'rules' => $this->rules()]);
    
                } catch (\Exception $e) {
    
                    DB::rollback();

                    if(count($path_unlink) > 0) {
                        Storage::delete($path_unlink);
                    }

                    throw $e;
                }
    
                DB::commit();
           
                return response()->json($results);
            }
        } else {

            return response()->json($results);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function rules()
    {
        //
        $rules = [

        ];

        return $rules;
    }

    public function messages()
    {
        $messages = [

        ];
        return $messages;
    }

    public function datatables(Request $request) {

        $user = Auth::user();

        $columns = [
            0 =>'meeting_name',
        ];

        $monthTH_brev = [null,'ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];
        
        $datas = Meeting::getList();

        $totalData = $datas->get()->count();
        $totalFiltered = $totalData;

        $start = $request->input( 'start' );
        $length = $request->input( 'length' );

        if ($request->has('order')) {
            if ($request->input('order.0.column') != '') {

                $orderColumn = $request->input('order.0.column');
                $orderDirection = $request->input('order.0.dir');

                $data_count = $datas;
                $totalFiltered = $data_count->get()->count();

                $datas = $datas->orderBy($columns[intval( $orderColumn )], $orderDirection )->skip($start)->take($length)->get();
            }
        }

        $data = [];
        foreach ( $datas as $key => $col ) {
            $nestedData = [];

            $dateCarbon = Carbon::createFromFormat('Y-m-d', $col->date, 'Asia/Bangkok');
            $date = $dateCarbon->format('d');
            $date .= " {$monthTH_brev[$dateCarbon->format('n')]} ";
            $date .= $dateCarbon->modify('+543 year')->format('Y');
     
            $start_time = explode(':', $col->start_time);
            $start_time = $start_time[0].'.'.$start_time[1];

            $end_time = explode(':', $col->end_time);
            $end_time = $end_time[0].'.'.$end_time[1];

            $time = "{$start_time}-{$end_time} น.";

            $nestedData = (object)[
                'name'          => $col->meeting_name,
                'date'          => $date,
                'time'          => $time,
                'link'          => route('pages.meeting.edit', ['id' => Crypt::encryptString($col->id)]),
            ];

            $data [] = json_encode($nestedData);
        }

        $tableContent = [
                "draw"              => intval($request->input('draw')), 
                "recordsTotal"      => intval($totalData),
                "recordsFiltered"   => intval($totalFiltered),
                "data"              => $data
        ];
        return $tableContent;
    }
    
    public function selectsapi(Request $request) {
     
        $daysTH = ['Sunday' => 'วันอาทิตย์', 'Monday' => 'วันจันทร์', 'Tuesday' => 'วันอังคาร', 'Wednesday' => 'วันพุธ', 'Thursday' => 'วันพฤหัสบดี', 'Friday' => 'วันศุกร์', 'Saturday' => 'วันเสาร์'];
        $monthTH = [null,'มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม'];
        $monthTH_brev = [null,'ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.'];

        $selected_id = !empty($request->selected_id) ? Crypt::decryptString($request->selected_id) : null;

        $datas = Meeting::getList();
   
        if ($request->has('search')) {
            $searchTerm = $request->input('search');
            $datas = $datas->where(function($query) use ($searchTerm) {
                $query->where('meeting_name', 'Like', '%' . $searchTerm . '%' );
            });
        }
     
        $datas = $datas->orderBy("meeting_name", 'ASC' )->take(10)->get();
      
        $option = [];
        if($datas->count() > 0) {
            foreach ($datas as $item) {

                $selected = false;

                if($selected_id == $item->id) {
                    $selected = true;
                }
        
                $dateCarbon = Carbon::createFromFormat('Y-m-d', $item->date, 'Asia/Bangkok');
                $date = $daysTH[$dateCarbon->format('l')];
                $date .= "ที่ {$dateCarbon->format('d')} ";
                $date .= "เดือน{$monthTH[$dateCarbon->format('n')]} ";
                $date .= "พ.ศ.{$dateCarbon->modify('+543 year')->format('Y')} ";

                $start_time = explode(':', $item->start_time);
                $start_time = $start_time[0].'.'.$start_time[1];
    
                $end_time = explode(':', $item->end_time);
                $end_time = $end_time[0].'.'.$end_time[1];
    
                $time = "{$start_time}-{$end_time} น.";
    
                $date .= "เวลา {$time}";

                $option[] = (object)[
                    'id'        => Crypt::encryptString($item->id),
                    'text'      => "{$item->meeting_name} ครั้ง {$item->agenda} {$date}",
                    'selected'  => $selected
                ];
            }
        }

        return Response::json($option);
    }

       
    public function sendmail(Request $request) {
       
        if($request->has('user_id') && $request->has('meeting_id')) {
         
            $user = User::find(Crypt::decryptString($request->user_id));
            $meeting = Meeting::find(Crypt::decryptString($request->meeting_id));
        
            $variable = [
                'system_name_en',
                'smtp_host',
                'smtp_port',
                'mail_accout',
                'mail_password',
                'mail_is_enabled'
            ];
    
            $variable = array_flip($variable);
            $system_mail = System::whereIn('variable', ['system_name_en', 'smtp_host', 'smtp_port', 'mail_accout', 'mail_password', 'mail_is_enabled']);
    
            if($system_mail->count() > 0) {
                foreach ($system_mail->get() as $key => $value) {
                    $variable[$value->variable] = $value->value;
                }
            }
    
            $variable = (object)$variable;
            
            Config::set('mail.driver', 'smtp');
            Config::set('mail.host', $variable->smtp_host);
            Config::set('mail.port', $variable->smtp_port);
            Config::set('mail.username', $variable->mail_accout);
            Config::set('mail.password', $variable->mail_password);
            Config::set('mail.encryption', 'tls');
            Config::set('mail.from.name', $variable->system_name_en);
          
            Mail::to($user->email)->send(new MeetingInvitation($user, $meeting));
    
            if(count(Mail::failures()) > 0) {
    
                $text = "มีความล้มเหลวอย่างน้อยหนึ่งรายการ พวกเขาเป็น : <br />";
             
                foreach(Mail::failures() as $email_address) {
                    $text .= " - $email_address <br />";
                }
                return json_encode(['result' => false, 'messages' => $text]);
            } else {
                return json_encode(['result' => true, 'messages' => 'ไม่มีข้อผิดพลาด, ส่งทั้งหมดสำเร็จ!']);
            }
        }
    }
}
