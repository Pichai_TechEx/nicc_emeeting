<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;

class MeetingIssuesVoter extends Model
{
    protected $table = 'meeting_issues_voter';
}
