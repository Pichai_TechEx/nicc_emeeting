<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\{DB};

class Role extends Model
{
    public function roles_menus_permissions() {
        return $this->hasMany(RolesMenusPermissions::class, 'role_id', 'id');
    }

    public static function getList() {

        $model = Role::select([
            'roles.*',
            DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name) AS updated_name"),
        ])
        ->leftJoin('users', 'users.id', '=', 'roles.updated_by');
       
        return $model;
    }

    public static function selection() {

        $model = Role::select([
            'roles.id',
            'roles.name',
        ])
        ->where('roles.is_enabled', 1);
       
        return $model;
    }
}
