<?php

namespace TechEx\Permissions;

use TechEx\Menu;
use TechEx\Role;

trait HasPermissionsTrait {

    public function roles() {
        return $this->belongsToMany(Role::class, 'users_roles');
     }
  
    public function menus() {
        return $this->belongsToMany(Menu::class, 'roles_menus');
    }

    protected function hasPermissionTo($permission) {
        return $this->hasPermission($permission);
    }

    protected function hasPermission($permission) {
        return (bool) $this->permissions->where('slug', $permission->slug)->count();
    }

    public function hasRole( ... $roles ) {
        foreach ($roles as $role) {
           if ($this->roles->contains('id', $role)) {
              return true;
           }
        }
        return false;
    }
}