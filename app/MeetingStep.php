<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;

class MeetingStep extends Model
{
    protected $table = 'meeting_step';
}
