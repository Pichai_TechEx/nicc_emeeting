<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\{DB};

class PositionInMeeting extends Model
{
    use SoftDeletes;

    protected $table = 'position_in_meeting';

    public static function getList() {

        $model = PositionInMeeting::select([
            'position_in_meeting.*',
            DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name) AS updated_name"),
        ])
        ->leftJoin('users', 'users.id', '=', 'position_in_meeting.updated_by');
       
        return $model;
    }

    public static function selection() {

        $model = PositionInMeeting::select([
            'position_in_meeting.id',
            'position_in_meeting.name',
        ])
        ->where('position_in_meeting.is_enabled', 1)
        ->orderBy('position_in_meeting.sort_no', 'ASC' );
       
        return $model;
    }
}
