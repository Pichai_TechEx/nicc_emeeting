<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;

class System extends Model
{
    protected $table = 'system';
}
