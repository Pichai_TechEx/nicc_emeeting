<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\{DB};

class MeetingIssues extends Model
{
    protected $table = 'meeting_issues';

    
    public function parent() {
        return $this->hasMany(MeetingIssues::class, 'parent', 'id');
    }
    
    public function files() {
        return $this->hasMany(MeetingIssuesFile::class, 'meeting_issues_id', 'id');
    }

    public function voter() {
        return $this->hasMany(MeetingIssuesVoter::class, 'meeting_issues_id', 'id');
    }
    
    public function meeting() {
        return $this->hasOne(Meeting::class, 'id', 'meeting_id');
    }

    public function voter_sum() {
        return $this->voter()->select([
            DB::raw("(SELECT SUM(T1.score) FROM meeting_issues_voter T1 WHERE meeting_issues_voter.meeting_issues_id = T1.meeting_issues_id AND T1.is_voter = 1) AS agree"),
            DB::raw("(SELECT SUM(T2.score) FROM meeting_issues_voter T2 WHERE meeting_issues_voter.meeting_issues_id = T2.meeting_issues_id AND T2.is_voter = 2) AS disagree"),
            DB::raw("(SELECT SUM(T3.score) FROM meeting_issues_voter T3 WHERE meeting_issues_voter.meeting_issues_id = T3.meeting_issues_id AND T3.is_voter = 3) AS no_comment")
        ])->first();
    }
}
