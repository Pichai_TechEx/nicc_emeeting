<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;

class RolesMenusPermissions extends Model
{
    protected $table = 'roles_menus_permissions';

    public $incrementing = false;

    public $timestamps = false;

    public function permission() {
        return $this->hasOne(Permission::class, 'id', 'permission_id');
    }
}
