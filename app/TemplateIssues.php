<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\{DB};

class TemplateIssues extends Model
{
    protected $table = 'template_issues';
}
