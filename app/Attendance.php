<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\{DB};

class Attendance extends Model
{
    public static function getList() {

        $model = Attendance::select([
            'attendances.*',
            'organizations.name AS organization_name',
            DB::raw("(CASE WHEN attendances.is_external = 1 THEN 'บุคคลภายนอก' ELSE 'บุคคลภายใน' END) AS is_external_name"),
            DB::raw("CONCAT(attendances.first_name, CHAR(32), attendances.last_name) AS name"),
            DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name) AS updated_name"),
            DB::raw("(SELECT T2.name FROM organizations T2 WHERE T2.id = attendances.organization_id) AS organizations_name"),
        ])
        ->leftJoin('organizations', 'organizations.id', '=', 'attendances.organization_id')
        ->leftJoin('users', 'users.id', '=', 'attendances.updated_by');
       
        return $model;
    }
}
