<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;

class ProponentTopicFile extends Model
{
    protected $table = 'proponent_topic_file';

    public function file() {
        return $this->hasOne(File::class, 'id', 'file_id');
    }
}
