<?php

namespace TechEx;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use TechEx\Permissions\HasPermissionsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\{DB};

class User extends Authenticatable
{
    use Notifiable;
    use HasPermissionsTrait;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'username', 'email', 'password',
    ];
  
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles() {
        return $this->belongsToMany(Role::class, 'users_roles');
    }

    public function hasPermissionTo($permission) {
        return self::hasPermission($permission);
    }
     
    public function hasPermission($permission) {
      
        $path_name = \Request::route()->getName();
      
        if($path_name) {

            $routes = explode('.', $path_name);
            if(count($routes) > 0) {
                foreach ($routes as $key => $route) {
                    $routes[$key] = "{$route}Controller";
                }
            }
         
            $roles = $this->roles()->first();
            $menu = Menu::whereIn('controller', $routes)->first();
            
            if($menu) {
                $permissions = $roles->roles_menus_permissions()->where('menu_id', $menu->id);
             
                if($permissions->count() > 0) {
                    return (bool) $permissions->where('permission_id', $permission->id)->first();
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static function getList() {

        $model = User::select([
            'users.*',
            DB::raw("(SELECT T0.name FROM titles T0 WHERE T0.id = users.title_id ) AS title_name"),
            DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name) AS name"),
            DB::raw("(SELECT CONCAT(T.first_name, CHAR(32), T.last_name) FROM users T WHERE T.id = users.updated_by) AS updated_name"),
            DB::raw("(SELECT T2.name FROM organizations T2 WHERE T2.id = users.organization_id) AS organizations_name"),
            DB::raw("(SELECT T4.name FROM roles T4 WHERE T4.id = (SELECT T3.role_id FROM users_roles T3 WHERE T3.user_id = users.id)) AS roles_name"),
        ]);
       
        return $model;
    }

    public static function getListOrBoard($attendance_id) {
    
        $model = User::select([
            'users.*',
            DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name) AS name"),
            DB::raw("(SELECT CONCAT(T.first_name, CHAR(32), T.last_name) FROM users T WHERE T.id = users.updated_by) AS updated_name"),
            DB::raw("(SELECT T0.name FROM titles T0 WHERE T0.id = users.title_id ) AS title_name"),
            DB::raw("(SELECT T2.name FROM organizations T2 WHERE T2.id = users.organization_id) AS organizations_name"),
            DB::raw("(SELECT T4.name FROM roles T4 WHERE T4.id = (SELECT T3.role_id FROM users_roles T3 WHERE T3.user_id = users.id)) AS roles_name"),
            DB::raw("(SELECT T5.organization_name FROM boards_attendances T5 WHERE T5.id = '{$attendance_id}' AND T5.user_id = users.id ) AS old_organizations_name"),
            DB::raw("(SELECT T6.position_in_meeting_name FROM boards_attendances T6 WHERE T6.id = '{$attendance_id}' AND T6.user_id = users.id ) AS old_position_in_meeting_name"),
            DB::raw("(SELECT T7.first_name FROM boards_attendances T7 WHERE T7.id = '{$attendance_id}' AND T7.user_id = users.id ) AS old_first_name"),
            DB::raw("(SELECT T8.last_name FROM boards_attendances T8 WHERE T8.id = '{$attendance_id}' AND T8.user_id = users.id ) AS old_last_name"),
            DB::raw("(SELECT T9.position FROM boards_attendances T9 WHERE T9.id = '{$attendance_id}' AND T9.user_id = users.id ) AS old_position"),
            DB::raw("(SELECT T10.other FROM boards_attendances T10 WHERE T10.id = '{$attendance_id}' AND T10.user_id = users.id ) AS old_other"),
            DB::raw("(SELECT T11.email FROM boards_attendances T11 WHERE T11.id = '{$attendance_id}' AND T11.user_id = users.id ) AS old_email"),
            DB::raw("(SELECT T12.phone FROM boards_attendances T12 WHERE T12.id = '{$attendance_id}' AND T12.user_id = users.id ) AS old_phone"),
            DB::raw("(SELECT T13.title_name AS name FROM boards_attendances T13 WHERE T13.id = '{$attendance_id}' AND T13.user_id = users.id ) AS old_title_name"),
            DB::raw("(SELECT CONCAT(T14.first_name, CHAR(32), T14.last_name) AS name FROM boards_attendances T14 WHERE T14.id = '{$attendance_id}' AND T14.user_id = users.id ) AS old_name"),
        ]);
          
        return $model;
    }
}
