<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;

class ProposeTopicProponent extends Model
{
    protected $table = 'propose_topic_proponent';
    
    public function proponenttopic() {
        return $this->hasMany(ProponentTopic::class, 'propose_topic_proponent_id', 'id');
    }
    
}
