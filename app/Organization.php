<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\{DB};


class Organization extends Model
{
    use SoftDeletes;

    public static function getList() {

        $model = Organization::select([
            'organizations.*',
            DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name) AS updated_name"),
        ])
        ->leftJoin('users', 'users.id', '=', 'organizations.updated_by');
       
        return $model;
    }

    public static function selection() {

        $model = Organization::select([
            'organizations.id',
            'organizations.name',
        ])
        ->where('organizations.is_enabled', 1);
       
        return $model;
    }
}
