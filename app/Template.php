<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Template extends Model
{
    use SoftDeletes;

    public static function getList() {

        $model = Template::select(['*']);

        return $model;
    }

    public function templateissues() {
        return $this->hasMany(TemplateIssues::class, 'template_id', 'id');
    }

    public static function selection() {

        $model = Template::select([
            'templates.id',
            'templates.name',
        ])
        ->where('templates.is_enabled', 1);
       
        return $model;
    }
}
