<?php

namespace TechEx\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use TechEx\MeetingIssues;

class VoterEvent implements ShouldBroadcastNow
{
    use Dispatchable, InteractsWithSockets, SerializesModels;


    public $meeting_issues;
    public $target = '';
    public $agree = 0;
    public $disagree = 0;
    public $no_comment = 0;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(MeetingIssues $meeting_issues)
    {

        $this->meeting_issues = $meeting_issues;
        $this->target = $meeting_issues->target;

        $voter_sum_current = $meeting_issues->voter_sum();

        if($voter_sum_current) {
            $this->agree      = $voter_sum_current->agree;
            $this->disagree   = $voter_sum_current->disagree;
            $this->no_comment = $voter_sum_current->no_comment;
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('update.voter.'.$this->meeting_issues->meeting_id);
    }

    public function broadcastAs()
    {
        return 'voter.update';
    }

    // /**
    //  * Get the data to broadcast.
    //  *
    //  * @return array
    //  */
    // public function broadcastWith()
    // {
    //     return $this->serializeData($this->meeting_issues);
    // }
}
