<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;

class ProponentTopic extends Model
{
    protected $table = 'proponent_topic';

    public function files() {
        return $this->hasMany(ProponentTopicFile::class, 'proponent_topic_id', 'id');
    }
}
