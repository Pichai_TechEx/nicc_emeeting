<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\{DB};

class Title extends Model
{
    use SoftDeletes;

    public static function getList() {

        $model = Title::select([
            'titles.*',
            DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name) AS updated_name"),
        ])
        ->leftJoin('users', 'users.id', '=', 'titles.updated_by');
       
        return $model;
    }

    public static function selection() {

        $model = Title::select([
            'titles.id',
            'titles.name',
        ])
        ->where('titles.is_enabled', 1);
       
        return $model;
    }
}
