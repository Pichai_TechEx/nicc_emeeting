<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\{DB};

class Board extends Model
{
    use SoftDeletes;

    public static function getList() {

        $model = Board::select([
            'boards.*',
            DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name) AS updated_name"),
            DB::raw("(SELECT COUNT(T.id) FROM boards_attendances T WHERE T.board_id = boards.id) AS board_count"),
        ])
        ->leftJoin('users', 'users.id', '=', 'boards.updated_by');
       
        return $model;
    }

    public function boardsattendances() {
        return $this->hasMany(BoardsAttendances::class, 'board_id', 'id');
    }

    public function files() {
        return $this->hasMany(BoardsFile::class, 'board_id', 'id');
    }

    public static function selection() {

        $model = Board::select([
            'boards.id',
            'boards.name',
        ])
        ->where('boards.is_enabled', 1);
       
        return $model;
    }
}
