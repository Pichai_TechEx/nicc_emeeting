<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;

class BoardsFile extends Model
{
    protected $table = 'boards_file';

    public function file() {
        return $this->hasOne(File::class, 'id', 'file_id');
    }
    
}
