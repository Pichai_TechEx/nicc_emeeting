<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\{DB};

class Meeting extends Model
{
    use SoftDeletes;

    protected $table = 'meeting';

    public static function getList() {

        $model = Meeting::select([
            'meeting.*',
            DB::raw("(select CONCAT(u.first_name, CHAR(32), u.last_name) FROM users u WHERE u.id = meeting.created_by)  AS meeting_created_by"),
            DB::raw("(select MAX(t.step) FROM meeting_step t WHERE t.meeting_id = meeting.id)  AS step")
        ]);
        
        return $model;
    }

    public function meetingstep() {
        return $this->hasMany(MeetingStep::class, 'meeting_id', 'id');
    }

    public function meetingissues() {
        return $this->hasMany(MeetingIssues::class, 'meeting_id', 'id');
    }

    public function attendancemeeting() {
        return $this->hasMany(AttendanceMeeting::class, 'meeting_id', 'id');
    }

    public function boardmeeting() {
        return $this->hasMany(BoardMeeting::class, 'meeting_id', 'id');
    }

    public function meetingreport() {
        return $this->hasMany(MeetingReport::class, 'meeting_id', 'id');
    }

    public function meetingpermissions() {
        return $this->hasOne(MeetingPermissions::class);
    }

}
