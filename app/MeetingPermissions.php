<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;

class MeetingPermissions extends Model
{
    protected $table = 'meeting_permissions';

    public function meetingpermissionsissues() {
        return $this->hasMany(MeetingPermissionsIssues::class, 'meeting_permissions_id', 'id');
    }

    public static function getList() {

        $model = MeetingPermissions::select([
            'meeting_permissions.id',
            'meeting.meeting_name',
            'meeting.agenda',
            'meeting.date',
            'meeting.meeting_organization_name',
        ])
        ->leftJoin('users', 'users.id', '=', 'meeting_permissions.updated_by')
        ->leftJoin('meeting', 'meeting.id', '=', 'meeting_permissions.meeting_id');

        return $model;
    }
}
