<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\{DB};
use Auth;

class ProposeTopic extends Model
{
    public function proposetopicproponent() {
        return $this->hasMany(ProposeTopicProponent::class, 'propose_topic_id', 'id');
    }

    public function meeting() {
        return $this->hasOne(Meeting::class, 'id', 'meeting_id');
    }

    public static function getList() {

        $user = Auth::user();
       
        $model = ProposeTopic::select([
            'propose_topics.*',
            'meeting.meeting_name',
            'meeting.agenda',
            'meeting.date',
            'meeting.meeting_organization_name',
        ]);
        
        if($user->roles()->first()->id != 2) {
            $model = $model->leftJoin('propose_topic_proponent', 'propose_topics.id', '=', 'propose_topic_proponent.propose_topic_id');
        }

        $model = $model->leftJoin('users', 'users.id', '=', 'propose_topics.updated_by')
        ->leftJoin('meeting', 'meeting.id', '=', 'propose_topics.meeting_id');

        return $model;
    }

    public static function getTopicList() {
     
        $model = ProposeTopic::select([
            'propose_topic_proponent.user_id',
            'proponent_topic.*',
            'propose_topics.id',
            DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name) AS proposer_name"),
        ])
        ->leftJoin('propose_topic_proponent', 'propose_topics.id', '=', 'propose_topic_proponent.propose_topic_id')
        ->leftJoin('proponent_topic', 'propose_topic_proponent.id', '=', 'proponent_topic.propose_topic_proponent_id')
        ->leftJoin('users', 'users.id', '=', 'propose_topic_proponent.user_id');

        return $model;
    }
}
