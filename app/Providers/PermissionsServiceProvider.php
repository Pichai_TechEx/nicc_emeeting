<?php

namespace TechEx\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use TechEx\Permission;
use \Illuminate\Http\Request;

class PermissionsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            
            if(Permission::get()->count() > 0) {
                Permission::get()->map(function($permission){
                    Gate::define($permission->slug, function($user) use ($permission) {
                       return $user->hasPermissionTo($permission);
                    });
                });
            }
            
        } catch (\Exception $e) {
            return [];
        }
        
        Blade::directive('role', function ($role){
            return "<?php if(auth()->check() && auth()->user()->hasRole({$role})) : ?>";
        });

        Blade::directive('elserole', function (){
            return "<?php else : ?>";
        });

        Blade::directive('endrole', function ($role){
            return "<?php endif; ?>";
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
