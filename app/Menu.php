<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\{DB};

class Menu extends Model
{
    use SoftDeletes;

    public static function getList() {

        $model = Menu::select([
            'menus.*',
            DB::raw("CONCAT(users.first_name, CHAR(32), users.last_name) AS updated_name"),
        ])
        ->leftJoin('users', 'users.id', '=', 'menus.updated_by');
       
        return $model;
    }

    public function permissions() {
        return $this->belongsToMany(Permission::class, 'menus_permissions');
    }
}
