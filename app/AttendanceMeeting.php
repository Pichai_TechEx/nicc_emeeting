<?php

namespace TechEx;

use Illuminate\Database\Eloquent\Model;

class AttendanceMeeting extends Model
{
    protected $table = 'attendance_meeting';

    public function status() {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }
}
