/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import $ from 'jquery';
import 'moment';
import 'fullcalendar';
import dt from 'datatables.net-bs4';
import 'datatables.net-autofill-bs4';
import 'datatables.net-buttons-bs4';
import 'datatables.net-buttons/js/buttons.colVis.js';
import 'datatables.net-buttons/js/buttons.html5.js';
import 'datatables.net-buttons/js/buttons.flash.js';
import 'datatables.net-buttons/js/buttons.print.js';
import 'datatables.net-colreorder-bs4';
import 'datatables.net-fixedcolumns-bs4';
import 'datatables.net-fixedheader-bs4';
import 'datatables.net-keytable-bs4';
import 'datatables.net-responsive-bs4';
import 'datatables.net-rowgroup-bs4';
import 'datatables.net-rowreorder-bs4';
import 'datatables.net-scroller-bs4';
import 'datatables.net-searchpanes-bs4';
import 'datatables.net-select-bs4';

import DecoupledEditor from 'script-loader!@ckeditor/ckeditor5-build-decoupled-document/build/ckeditor.js';
// import ExportPdf from '@ckeditor/ckeditor5-export-pdf/src/exportpdf';

// // The editor creator to use.
// import ClassicEditorBase from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';

// import ExportPdf from '@ckeditor/ckeditor5-export-pdf/src/exportpdf';
// import EssentialsPlugin from '@ckeditor/ckeditor5-essentials/src/essentials';
// import UploadAdapterPlugin from '@ckeditor/ckeditor5-adapter-ckfinder/src/uploadadapter';
// import AutoformatPlugin from '@ckeditor/ckeditor5-autoformat/src/autoformat';
// import BoldPlugin from '@ckeditor/ckeditor5-basic-styles/src/bold';
// import ItalicPlugin from '@ckeditor/ckeditor5-basic-styles/src/italic';
// import BlockQuotePlugin from '@ckeditor/ckeditor5-block-quote/src/blockquote';
// // import EasyImagePlugin from '@ckeditor/ckeditor5-easy-image/src/easyimage';
// import HeadingPlugin from '@ckeditor/ckeditor5-heading/src/heading';
// // import ImagePlugin from '@ckeditor/ckeditor5-image/src/image';
// // import ImageCaptionPlugin from '@ckeditor/ckeditor5-image/src/imagecaption';
// // import ImageStylePlugin from '@ckeditor/ckeditor5-image/src/imagestyle';
// // import ImageToolbarPlugin from '@ckeditor/ckeditor5-image/src/imagetoolbar';
// // import ImageUploadPlugin from '@ckeditor/ckeditor5-image/src/imageupload';
// // import LinkPlugin from '@ckeditor/ckeditor5-link/src/link';
// // import ListPlugin from '@ckeditor/ckeditor5-list/src/list';
// import ParagraphPlugin from '@ckeditor/ckeditor5-paragraph/src/paragraph';

// window.DecoupledEditor = ClassicEditorBase;
// window.ExportPdf = ExportPdf;


import 'eonasdan-custom-bootstrap-datetimepicker-with-bootstrap-4';
import 'script-loader!datepicker-bootstrap/js/core';
import Swal from 'script-loader!sweetalert2/dist/sweetalert2.js';

import 'sweetalert2/src/sweetalert2.scss'

import 'select2/dist/js/select2'

import Echo from "laravel-echo";

window.io = require('socket.io-client');

if (typeof io !== 'undefined') {

    window.Echo = new Echo({
        broadcaster: 'socket.io',
        host: window.location.hostname + ':6001',
    });
}

require('datepicker-bootstrap/js/datepicker.min.js');
require('datepicker-bootstrap/js/timepicker.min.js');
require('jquery-steps/build/jquery.steps.min.js');
require('jquery.filer/js/jquery.filer.min.js');
require('jquery.filer/css/jquery.filer.css');
require('jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css');

require('icheck/icheck.min.js');
require('icheck/skins/all.css');

require('bootstrap4-toggle/js/bootstrap4-toggle.min.js');
require('bootstrap4-toggle/css/bootstrap4-toggle.min.css');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});