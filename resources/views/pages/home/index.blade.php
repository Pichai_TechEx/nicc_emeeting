@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row form-group">
        <div class="col">
            <button type="button" class="btn btn-primary" style="background-color: #01579B;border: #024f8b;" onclick="location.href='{{ route('pages.meeting.create') }}'"><i class="fas fa-plus-circle"></i> สร้างวาระการประชุม</button>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-6">
            {!! $calendar->calendar() !!}
        </div>
        <div class="col-md-6">
            <table id="table_id" class="table table-bordered" cellspacing="0" width="100%">
                <thead class="thead-light">
                    <tr>
                        <th scope="col"><i class="fas fa-users"></i> รายละเอียดการประชุม</th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
{!! $calendar->script() !!}
<script>

   $(document).ready( function () {

        var dataTable = $('#table_id').DataTable({
            destroy     : true,
            serverSide  : true,
            paging      : true,
            lengthChange: false,
            searching   : false,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            processing  : true,
            deferLoading: 0,
            pageLength  : 10,
            language : {
                decimal:        "",
                emptyTable:     "ไม่มีข้อมูลในตาราง",
                info:           "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
                infoEmpty:      "แสดง 0 ถึง 0 จาก 0 รายการ",
                infoFiltered:   "(กรองจากรายการทั้งหมด _MAX_ รายการ)",
                infoPostFix:    "",
                thousands:      ",",
                lengthMenu:     "แสดง _MENU_ รายการ",
                loadingRecords: "กำลังโหลด...",
                processing:     "กำลังประมวลผล...",
                search:         "ค้นหา:",
                zeroRecords:    "ไม่พบรายการ",
                paginate: {
                    first:      "หน้าแรก",
                    last:       "หน้าสุดท้าย",
                    next:       "ถัดไป",
                    previous:   "ก่อนหน้า"
                },
                aria: {
                    sortAscending:  ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปมาก",
                    sortDescending: ": เปิดใช้งานเพื่อเรียงคอลัมน์จากมากไปหาน้อย"
                }
            },
            createdRow: async function ( row, data, index ) {
                data = JSON.parse(data);

                var tr = $(row);
                var name =  $('<a/>').attr('href', data.link).addClass('card-link text-primary').append(data.name);
                var datetime =  $('<p/>').addClass('text-dark');
                var date_i =  $('<i/>').addClass('far fa-calendar-alt');
                var date =  data.date;
                var time_i =  $('<i/>').addClass('far fa-clock');

                datetime.append(date_i);
                datetime.append('&nbsp;');
                datetime.append(date);
                datetime.append('&nbsp;');
                datetime.append(time_i);
                datetime.append('&nbsp;');
                datetime.append(data.time);

                tr.find('td').text('');
                tr.find('td').append(name);
                tr.find('td').append(datetime);
          

            },
            drawCallback: function () {
            },
            ajax: function(data, callback, settings) {
            
                $.ajaxSetup({
                    url: "{{ route('pages.meeting.datatables') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : "POST",
                    dataType: 'json',
                    error: function(data){
                        if(data.status == 401) {
                            $('#login_modal').modal('show');
                        }
                        console.log('error');
                    },
                    success: function(data){

                        callback({
                            recordsTotal    : data.recordsTotal,
                            recordsFiltered : data.recordsFiltered,
                            data            : data.data
                        });
                    }
                });
                
                $.ajax({ 
                    data: data
                });
            },
            aoColumnDefs: [
                { targets: [ 0 ], sWidth: '100%' },
            ],
            columns: [
                null,
            ]
        });
        
        dataTable.draw();
    });
</script>
@endsection
