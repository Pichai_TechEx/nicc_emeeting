@extends('layouts.app')
@section('card-header', 'เพิ่มการเผยแพร่หัวข้อการประชุม')
@section('breadcrumbs')
{{ Breadcrumbs::render('meetingpermissions_create') }}
@endsection
@section('content')
@include('layouts.partials.meetingpermissions.card-main')
@endsection
