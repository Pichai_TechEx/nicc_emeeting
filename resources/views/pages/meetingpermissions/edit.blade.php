@extends('layouts.app')
@section('card-header', 'แก้ไข')
@section('breadcrumbs')
{{ Breadcrumbs::render('meetingpermissions_edit', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.meetingpermissions.card-main')
@endsection