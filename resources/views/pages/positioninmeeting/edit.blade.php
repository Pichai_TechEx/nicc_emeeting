@extends('layouts.app')
@section('card-header', 'แก้ไข')
@section('breadcrumbs')
{{ Breadcrumbs::render('positioninmeeting_edit', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.positioninmeeting.card-main')
@endsection