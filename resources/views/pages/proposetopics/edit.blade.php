@extends('layouts.app')
@section('card-header', 'แก้ไข')
@section('breadcrumbs')
{{ Breadcrumbs::render('proposetopics_edit', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.proposetopics.card-main')
@endsection