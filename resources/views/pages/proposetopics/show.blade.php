@extends('layouts.app')
@section('card-header', 'หัวข้อที่ถูกเสนอ')
@section('breadcrumbs')
{{ Breadcrumbs::render('proposetopics_show', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.proposetopics.card-show')
@endsection