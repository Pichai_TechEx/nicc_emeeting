@extends('layouts.app')
@section('card-header', 'เสนอหัวข้อ')
@section('breadcrumbs')
{{ Breadcrumbs::render('proposetopics_propose', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.proposetopics.card-propose')
@endsection
