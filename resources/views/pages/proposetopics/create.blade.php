@extends('layouts.app')
@section('card-header', 'เพิ่มเสนอหัวข้อการประชุม')
@section('breadcrumbs')
{{ Breadcrumbs::render('proposetopics_create') }}
@endsection
@section('content')
@include('layouts.partials.proposetopics.card-main')
@endsection
