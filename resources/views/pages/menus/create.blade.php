@extends('layouts.app')
@section('card-header', 'สร้าง')
@section('breadcrumbs')
{{ Breadcrumbs::render('menus_create') }}
@endsection
@section('content')
@include('layouts.partials.menus.card-main')
@endsection
