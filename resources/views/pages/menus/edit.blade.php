@extends('layouts.app')
@section('card-header', 'แก้ไข')
@section('breadcrumbs')
{{ Breadcrumbs::render('menus_edit', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.menus.card-main')
@endsection