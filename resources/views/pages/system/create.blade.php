@extends('layouts.app')
@section('card-header', 'สร้าง')
@section('breadcrumbs')
{{ Breadcrumbs::render('positioninmeeting_create') }}
@endsection
@section('content')
@include('layouts.partials.positioninmeeting.card-main')
@endsection
