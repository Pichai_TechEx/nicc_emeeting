@extends('layouts.app')
@section('breadcrumbs')
{{ Breadcrumbs::render('system_index') }}
@endsection
@section('content')
@include('layouts.partials.system.card-main')
@endsection