@extends('layouts.app')
@section('card-header', 'แก้ไข')
@section('breadcrumbs')
{{ Breadcrumbs::render('attendances_edit', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.attendances.card-main')
@endsection