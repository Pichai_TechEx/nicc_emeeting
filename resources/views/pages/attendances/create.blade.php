@extends('layouts.app')
@section('card-header', 'สร้าง')
@section('breadcrumbs')
{{ Breadcrumbs::render('attendances_create') }}
@endsection
@section('content')
@include('layouts.partials.attendances.card-main')
@endsection
