@extends('layouts.app')
@section('card-header', 'แก้ไขหน่วยงาน')
@section('breadcrumbs')
{{ Breadcrumbs::render('organizations_edit', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.organizations.card-main')
@endsection