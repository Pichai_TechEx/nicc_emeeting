@extends('layouts.app')
@section('card-header', 'เพิ่มหน่วยงาน')
@section('breadcrumbs')
{{ Breadcrumbs::render('organizations_create') }}
@endsection
@section('content')
@include('layouts.partials.organizations.card-main')
@endsection
