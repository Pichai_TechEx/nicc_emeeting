@extends('layouts.app')
@section('card-header', 'สร้าง')
@section('breadcrumbs')
{{ Breadcrumbs::render('titles_create') }}
@endsection
@section('content')
@include('layouts.partials.titles.card-main')
@endsection
