@extends('layouts.app')
@section('card-header', 'แก้ไข')
@section('breadcrumbs')
{{ Breadcrumbs::render('titles_edit', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.titles.card-main')
@endsection