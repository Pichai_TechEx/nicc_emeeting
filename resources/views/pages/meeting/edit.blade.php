@extends('layouts.app')
@section('breadcrumbs')
{{ Breadcrumbs::render('meeting_edit', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.meeting.card-main')
@endsection