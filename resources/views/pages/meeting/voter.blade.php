@extends('layouts.app')
@section('breadcrumbs')
{{ Breadcrumbs::render('meeting_voter', (!empty($result['data']->id) ? $result['data']->id : ''), (!empty($result['data']->title) ? $result['data']->title : '')) }}
@endsection
@section('content')
@include('layouts.partials.meeting.card-voter')
@endsection
