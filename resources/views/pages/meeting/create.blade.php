@extends('layouts.app')
@section('breadcrumbs')
{{ Breadcrumbs::render('meeting_create') }}
@endsection
@section('content')
@include('layouts.partials.meeting.card-main')
@endsection
