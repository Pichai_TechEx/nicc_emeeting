@extends('layouts.app')
@section('content')
    @section('breadcrumbs')
        {{ Breadcrumbs::render('meeting_index') }}
    @endsection
    @can('search')
        @section('toolbar')
        <div class="row form-group">
            <div class="col-sm-12 col-md-6">
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control" placeholder="Search..." name="search" autocomplete="off">
                    <span class="input-group-append">
                        <button type="button" class="btn btn-info btn-flat" id="search"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="input-group input-group-sm">
                    {{ Form::select('grade', $result['gradeOptions'], $result['gradeOptions_selected'], ['class' => 'form-control form-control-sm select2', 'style' => 'width: 100%;']) }}
                </div>
            </div>
        </div>
        @endsection
    @endcan
    @include('layouts.partials.table')
    @include('layouts.partials.modal-confirm-delete')
@endsection
@section('script')
<script>

    var where_search = "";
    var where_grade_search = "";

    $(document).ready(function() {

        $("input[name='search']").on("keyup", function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                $('#search').click();
            }
        });
        
        $("select[name='grade']").on("change", function(event) {
            setGradeSearch( $(this).val() );
            $('#search').click();
        });

        var dataTable = $('#dataTable').DataTable({
            destroy     : true,
            serverSide  : true,
            paging      : true,
            lengthChange: false,
            searching   : false,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            processing  : true,
            deferLoading: 0,
            pageLength  : 10,
            language : {
                decimal:        "",
                emptyTable:     "ไม่มีข้อมูลในตาราง",
                info:           "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
                infoEmpty:      "แสดง 0 ถึง 0 จาก 0 รายการ",
                infoFiltered:   "(กรองจากรายการทั้งหมด _MAX_ รายการ)",
                infoPostFix:    "",
                thousands:      ",",
                lengthMenu:     "แสดง _MENU_ รายการ",
                loadingRecords: "กำลังโหลด...",
                processing:     "กำลังประมวลผล...",
                search:         "ค้นหา:",
                zeroRecords:    "ไม่พบรายการ",
                paginate: {
                    first:      "หน้าแรก",
                    last:       "หน้าสุดท้าย",
                    next:       "ถัดไป",
                    previous:   "ก่อนหน้า"
                },
                aria: {
                    sortAscending:  ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปมาก",
                    sortDescending: ": เปิดใช้งานเพื่อเรียงคอลัมน์จากมากไปหาน้อย"
                }
            },
            drawCallback: function () {
                $('.dataTables_paginate > .pagination').addClass('pagination-sm pagination-customer');
            },
            ajax: function(data, callback, settings) {
            
                $.ajaxSetup({
                    url: "{{ route('pages.sales.customer.datatables') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : "POST",
                    dataType: 'json',
                    error: function(data){
                        if(data.status == 401) {
                            $('#login_modal').modal('show');
                        }
                        console.log('error');
                    },
                    success: function(data){

                        callback({
                            recordsTotal    : data.recordsTotal,
                            recordsFiltered : data.recordsFiltered,
                            data            : data.data
                        });
                    }
                });
                
                data.search.value   = getSearch();
                data.search.grade   = getGradeSearch();

                $.ajax({ 
                    data: data
                });
            },
            aoColumnDefs: [
                { targets: [ 0 ], sWidth: '18%', sClass: 'text-center' },
                { targets: [ 1 ], sWidth: '18%', sClass: 'text-left' },
                { targets: [ 2 ], sWidth: '18%', sClass: 'text-left' },
                { targets: [ 3 ], sWidth: '10%', sClass: 'text-center' },
                { targets: [ 4 ], sWidth: '18%', sClass: 'text-left' },
                { targets: [ 5 ], sWidth: '18%', sClass: 'text-left' },
                { targets: [ 6 ], sWidth: '10%', sClass: 'text-center all', orderable: false }
            ],
            columns: [
                @if(!empty($result['table_thead']) && count($result['table_thead'])>0)
                    @foreach ($result['table_thead'] as $col)
                        {!! 'null,' !!}
                    @endforeach
                @endif
            ],
            fixedColumns: {
                leftColumns: 4
            }
        });
        
        dataTable.draw();
      
        $('#search').on('click',  function () {
            setSearch($("input[name='search']").val());
            dataTable.ajax.reload();
        });
    });

    function setSearch(search) {
        where_search = search;
    }

    function getSearch() {
        return where_search;
    }

    function setGradeSearch(search) {
        where_grade_search = search;
    }

    function getGradeSearch() {
        return where_grade_search;
    }    
</script>
@endsection   