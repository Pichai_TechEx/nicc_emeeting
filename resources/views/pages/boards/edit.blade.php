@extends('layouts.app')
@section('card-header', 'แก้ไข')
@section('breadcrumbs')
{{ Breadcrumbs::render('boards_edit', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.boards.card-main')
@endsection