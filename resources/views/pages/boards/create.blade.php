@extends('layouts.app')
@section('card-header', 'สร้าง')
@section('breadcrumbs')
{{ Breadcrumbs::render('boards_create') }}
@endsection
@section('content')
@include('layouts.partials.boards.card-main')
@endsection
