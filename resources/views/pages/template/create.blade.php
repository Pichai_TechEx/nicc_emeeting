@extends('layouts.app')
@section('card-header', 'เพิ่มเทมเพลต')
@section('breadcrumbs')
{{ Breadcrumbs::render('template_create') }}
@endsection
@section('content')
@include('layouts.partials.template.card-main')
@endsection
