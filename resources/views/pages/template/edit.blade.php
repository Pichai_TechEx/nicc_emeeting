@extends('layouts.app')
@section('card-header', 'แก้ไขเทมเพลต')
@section('breadcrumbs')
{{ Breadcrumbs::render('template_edit', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.template.card-main')
@endsection