@extends('layouts.app')
@section('card-header', 'สร้าง')
@section('breadcrumbs')
{{ Breadcrumbs::render('usertype_create') }}
@endsection
@section('content')
@include('layouts.partials.usertype.card-main')
@endsection
