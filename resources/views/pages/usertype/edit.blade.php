@extends('layouts.app')
@section('card-header', 'แก้ไข')
@section('breadcrumbs')
{{ Breadcrumbs::render('usertype_edit', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.usertype.card-main')
@endsection