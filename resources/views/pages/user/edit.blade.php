@extends('layouts.app')
@section('card-header', 'แก้ไข')
@section('breadcrumbs')
{{ Breadcrumbs::render('user_edit', $result['data']->id) }}
@endsection
@section('content')
@include('layouts.partials.user.card-main')
@endsection