@extends('layouts.app')
@section('card-header', 'สร้าง')
@section('breadcrumbs')
{{ Breadcrumbs::render('user_create') }}
@endsection
@section('content')
@include('layouts.partials.user.card-main')
@endsection
