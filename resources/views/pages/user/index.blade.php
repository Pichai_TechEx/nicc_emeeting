@extends('layouts.app')
@section('content')
    @section('breadcrumbs')
        {{ Breadcrumbs::render('user_index') }}
    @endsection
    <div class="container">
        <div class="row form-group">
            <div class="col-sm-12 col-md-6">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="ค้นหา..." name="search" autocomplete="off">
                    <span class="input-group-append">
                        <button type="button" class="btn btn-secondary" id="search"><i class="fas fa-search"></i></button>
                    </span>
                </div>
            </div>
        </div>
        <table class="table table-bordered" cellspacing="0" width="100%" id="dataTable" name ="dataTable">
            <thead class="thead-light">
                <tr>
                    <th>ชื่อ - นามสกุล</th>
                    <th>หน่วยงาน</th>
                    <th>ประเภทผู้ใช้งาน</th>
                    <th>วันที่ปรับปรุงข้อมูล</th>
                    <th>ผู้ปรับปรุงข้อมูล</th>
                    <th width="100px;" class="p-1 text-center">
                        <a role="button" class="btn btn-success" href="{{ route('pages.user.create') }}"><i class="fas fa-plus"></i> เพิ่ม</a>
                    </th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
@endsection
@section('script')
<script>

    var where_search = "";

    $(document).ready(function() {

        $("input[name='search']").on("keyup", function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                $('#search').click();
            }
        });

        var dataTable = $('#dataTable').DataTable({
            destroy     : true,
            serverSide  : true,
            paging      : true,
            lengthChange: false,
            searching   : false,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            processing  : true,
            deferLoading: 0,
            pageLength  : 10,
            order: [],
            language : {
                decimal:        "",
                emptyTable:     "ไม่มีข้อมูลในตาราง",
                info:           "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
                infoEmpty:      "แสดง 0 ถึง 0 จาก 0 รายการ",
                infoFiltered:   "(กรองจากรายการทั้งหมด _MAX_ รายการ)",
                infoPostFix:    "",
                thousands:      ",",
                lengthMenu:     "แสดง _MENU_ รายการ",
                loadingRecords: "กำลังโหลด...",
                processing:     "กำลังประมวลผล...",
                search:         "ค้นหา:",
                zeroRecords:    "ไม่พบรายการ",
                paginate: {
                    first:      "หน้าแรก",
                    last:       "หน้าสุดท้าย",
                    next:       "ถัดไป",
                    previous:   "ก่อนหน้า"
                },
                aria: {
                    sortAscending:  ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปมาก",
                    sortDescending: ": เปิดใช้งานเพื่อเรียงคอลัมน์จากมากไปหาน้อย"
                }
            },
            drawCallback: function () {
                
            },
            ajax: function(data, callback, settings) {
            
                $.ajaxSetup({
                    url: "{{ route('pages.user.datatables') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : "POST",
                    dataType: 'json',
                    error: function(data){
                        if(data.status == 401) {
                            $('#login_modal').modal('show');
                        }
                        console.log('error');
                    },
                    success: function(data){

                        callback({
                            recordsTotal    : data.recordsTotal,
                            recordsFiltered : data.recordsFiltered,
                            data            : data.data
                        });
                    }
                });
                
                data.search.value   = getSearch();

                $.ajax({ 
                    data: data
                });
            },
            aoColumnDefs: [
                { targets: [ 0 ], sWidth: '20%' },
                { targets: [ 1 ], sWidth: '25%' },
                { targets: [ 2 ], sWidth: '20%' },
                { targets: [ 3 ], sWidth: '20%' },
                { targets: [ 4 ], sWidth: '20%' },
                { targets: [ 5 ], sWidth: '8%', sClass: 'text-center all', orderable: false }
            ],
            columns: [
                null,
                null,
                null,
            ]
        });
        
        dataTable.draw();
      
        $('#search').on('click',  function () {
            setSearch($("input[name='search']").val());
            dataTable.ajax.reload();
        });
    });

    function setSearch(search) {
        where_search = search;
    }

    function getSearch() {
        return where_search;
    }
    $(document).on('click', '.btn-delete', function () {

        var form    = $(this).find('form').get(0);
        var url     = $(form).attr('action');
        var method  = $(form).attr('method');
        var data 	= new FormData(form);

        Swal.fire({
            text: "ต้องการลบรายการนี้ ใช่หรือไม่ ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#38c172',
            cancelButtonColor: '#e3342f',
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ยกเลิก'
        }).then((result) => {
      

        if (result.isConfirmed) {

            $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                    Swal.fire({
                        text: data.message,
                        icon: data.icon,
                        confirmButtonText: 'ปิด'
                    });

                    if(data.status) {
                        $('#search').click();
                    }

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {
                        $('#login_modal').modal('show');
                    },
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }
            });
        
        }
        })
    });
</script>
@endsection   