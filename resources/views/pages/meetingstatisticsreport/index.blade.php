@extends('layouts.app')
@section('content')
    @section('breadcrumbs')
        {{ Breadcrumbs::render('meetingstatisticsreport_index') }}
    @endsection
    <div class="container">
        @can('search')
            <div class="card mb-3">
                <div class="card-header">
                    <i class="fas fa-search"></i> รายงานสถิติการจัดประชุม
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        {!! Html::decode(Form::label('meeting_name', 'ชื่อเรื่องการประชุม :', ['class' => 'col-sm-3 col-form-label text-right'])) !!}
                        <div class="col-sm-3">
                            {{ Form::text('meeting_name', old('meeting_name') ? old('meeting_name') : ( !empty($result['data']->meeting_name) ? $result['data']->meeting_name : '' ), ['class' => 'form-control '.($errors->has('meeting_name') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุข้อมูล', 'autocomplete' => "off", 'required' => true]) }}
                            <span class="form-group text-danger vld validate-meeting_name" style="display:none">
                                <small><i class="validate-meeting_name-i"></i></small>
                            </span>
                        </div>
                        {!! Html::decode(Form::label('meeting_place', 'สถานะที่ประชุม :', ['class' => 'col-sm-2 col-form-label text-right'])) !!}
                        <div class="col-sm-3">
                            {{ Form::text('meeting_place', null, ['class' => 'form-control', 'placeholder' => 'ระบุการค้นหา สถานะที่ประชุม', 'autocomplete' => "off"]) }}
                            <span class="form-group text-danger vld validate-meeting_place" style="display:none">
                                <small><i class="validate-meeting_place-i"></i></small>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        {{Form::label('date', 'วันที่ประชุม :', ['class' => 'col-sm-3 col-form-label text-right'])}}
                        <div class="col-sm-3">
                            {{ Form::text('date', null, ['class' => 'datepicker', 'placeholder' => 'DD/MM/YYYY', 'autocomplete' => "off"]) }}
                            <span class="form-group text-danger vld validate-date" style="display:none">
                                <small><i class="validate-date-i"></i></small>
                            </span>
                        </div>
                        {{Form::label('acceptance_end_date', 'ถึงวันที่ :', ['class' => 'col-sm-2 col-form-label text-right'])}}
                        <div class="col-sm-3">
                            {{ Form::text('acceptance_end_date', null, ['class' => 'datepicker', 'placeholder' => 'DD/MM/YYYY', 'autocomplete' => "off"]) }}
                            <span class="form-group text-danger vld validate-date" style="display:none">
                                <small><i class="validate-date-i"></i></small>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        {{Form::label('start_time', 'เวลาเริ่ม :', ['class' => 'col-sm-3 col-form-label text-right'])}}
                        <div class="col-sm-3">
                            <div class="form-group">
                                <div class="input-group">
                                    {{ Form::text('start_time', null, ['class' => 'form-control timepicker', 'placeholder' => 'DD/MM/YYYY', 'autocomplete' => "off"]) }}
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="far fa-clock"></i></span>
                                    </div>
                                </div>
                            </div>
                            <span class="form-group text-danger vld validate-start_time" style="display:none">
                                <small><i class="validate-start_time-i"></i></small>
                            </span>
                        </div>
                        {{Form::label('end_time', 'เวลาสิ้นสุด :', ['class' => 'col-sm-2 col-form-label text-right'])}}
                        <div class="col-sm-3">
                            <div class="form-group">
                                <div class="input-group">
                                    {{ Form::text('end_time', null, ['class' => 'form-control timepicker', 'placeholder' => 'DD/MM/YYYY', 'autocomplete' => "off"]) }}
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="far fa-clock"></i></span>
                                    </div>
                                </div>
                            </div>
                            <span class="form-group text-danger vld validate-end_time" style="display:none">
                                <small><i class="validate-end_time-i"></i></small>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Html::decode(Form::label('meeting_organization_name', 'หน่วยงานที่จัดการประชุม :', ['class' => 'col-sm-3 col-form-label text-right'])) !!}
                        <div class="col-sm-3">
                            {{ Form::text('meeting_organization_name', null, ['class' => 'form-control', 'placeholder' => 'กรุณาระบุข้อมูล', 'autocomplete' => "off"]) }}
                            <span class="form-group text-danger vld validate-meeting_organization_name" style="display:none">
                                <small><i class="validate-meeting_organization_name-i"></i></small>
                            </span>
                        </div>
                        {!! Html::decode(Form::label('meeting_organizer_name', 'ชื่อผู้จัดการประชุม :', ['class' => 'col-sm-2 col-form-label text-right'])) !!}
                        <div class="col-sm-3">
                            {{ Form::text('meeting_organizer_name', null, ['class' => 'form-control', 'placeholder' => 'กรุณาระบุข้อมูล', 'autocomplete' => "off"]) }}
                            <span class="form-group text-danger vld validate-agenda" style="display:none">
                                <small><i class="validate-agenda-i"></i></small>
                            </span>
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Html::decode(Form::label(null, 'รูปแบบรายงาน :', ['class' => 'col-sm-3 col-form-label text-right'])) !!}
                        <div class="col-sm-5 pt-2">
                            <div class="form-check-inline">
                                <input class="form-check-input icheck-blue" type="radio" name="extension" id="extension1" value="xls" checked>
                                <label class="form-check-label ml-2" for="extension1">.xls</label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input icheck-blue" type="radio" name="extension" id="extension2" value="doc">
                                <label class="form-check-label ml-2" for="extension2">.doc</label>
                            </div>
                            <div class="form-check-inline">
                                <input class="form-check-input icheck-blue" type="radio" name="extension" id="extension3" value="docx">
                                <label class="form-check-label ml-2" for="extension3">.docx</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                <div class="btn-group" role="group" aria-label="ค้นหา">
                                    <button type="button" class="btn btn-info" id="search"><i class="fas fa-search"></i> ค้นหา</button>
                                    <button type="button" class="btn btn btn-warning"><i class="fas fa-sync-alt"></i> เคลียร์</button>
                                    <button type="button" class="btn btn-light" onclick="report();"><i class="fas fa-file-export"></i> นำออก</button>
                                    <button type="button" class="btn btn-primary"><i class="fas fa-print"></i> พิมพ์</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endcan
        <table class="table table-bordered" cellspacing="0" width="100%" id="dataTable" name ="dataTable">
            <thead class="thead-light">
                <tr>
                    <th>วันที่ประชุม</th>
                    <th>เวลาเริ่ม - เวลาสิ้นสุด</th>
                    <th>ชื่อเรื่องการจัดประชุม</th>
                    <th>สถานะที่ประชุม</th>
                    <th>หน่วยงานที่จัดการประชุม</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
    {{ Form::open(['route' => 'pages.export', 'method' => 'GET', 'class' => 'hidden', 'id' => 'form-export', 'enctype' => 'multipart/form-data']) }}
    {{-- <input type="hidden" name="cc" value="cc"> --}}
    {{ Form::close() }}
@endsection
@section('script')
<script>

    var where_meeting_name = "";
    var where_meeting_place = "";
    var where_meeting_organization_name = "";
    var where_meeting_organizer_name = "";

    $(document).ready(function() {

        $('input[type=checkbox].icheck-green, input[type=radio].icheck-green').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
     
        $('input[type=checkbox].icheck-yellow, input[type=radio].icheck-yellow').iCheck({
            checkboxClass: 'icheckbox_square-yellow',
            radioClass: 'iradio_square-yellow',
        });
     
        $('input[type=checkbox].icheck-blue, input[type=radio].icheck-blue').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
        });

        $.each($('.timepicker'), function (i, e) { 
            $(this).datetimepicker({
                format: 'HH:mm',
                icons : {
                    time: 'far fa-clock',
                    date: 'far fa-calendar-alt',
                    up: 'fas fa-chevron-up',
                    down: 'fas fa-chevron-down',
                    previous: 'fas fa-chevron-left',
                    next: 'fas fa-chevron-right',
                }
            });
        });

        gj.core.messages['th-th'] = {
            monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
            monthShortNames: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
            weekDaysMin: ["อา", "จ", "อ", "พ", "พฤ", "ศ", "ส"],
            weekDaysShort: ["อา", "จ", "อ", "พ", "พฤ", "ศ", "ส"],
            weekDays: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์"],
            ok: 'ตกลง',
            cancel: 'ยกเลิก',
            titleFormat: 'mmmm yyyy'
        };
 
        $.each($('.datepicker'), function (i, e) { 
            var date = new Date();
            $(this).datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                format: 'dd/mm/yyyy',
                locale: 'th-th',     
                value: $(this).val() ? $(this).val() : ( parseInt(date.getDate(), 10) < 10 ? '0' : '' ) + (date.getDate()).toString() + '/' + ( (parseInt(date.getMonth(), 10) + 1) < 10 ? '0' : '' ) + (parseInt(date.getMonth(), 10) + 1).toString() + '/' + (parseInt(date.getFullYear(), 10) + 543)
            });
        });

        $("input[name='meeting_name'], input[name='meeting_place'], input[name='meeting_organization_name'], input[name='meeting_organizer_name']").on("keyup", function(event) {
            if (event.keyCode === 13) {
                event.preventDefault();
                $('#search').click();
            }
        });

        var dataTable = $('#dataTable').DataTable({
            destroy     : true,
            serverSide  : true,
            paging      : true,
            lengthChange: false,
            searching   : false,
            ordering    : true,
            info        : true,
            autoWidth   : true,
            processing  : true,
            deferLoading: 0,
            pageLength  : 10,
            order: [],
            language : {
                decimal:        "",
                emptyTable:     "ไม่มีข้อมูลในตาราง",
                info:           "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
                infoEmpty:      "แสดง 0 ถึง 0 จาก 0 รายการ",
                infoFiltered:   "(กรองจากรายการทั้งหมด _MAX_ รายการ)",
                infoPostFix:    "",
                thousands:      ",",
                lengthMenu:     "แสดง _MENU_ รายการ",
                loadingRecords: "กำลังโหลด...",
                processing:     "กำลังประมวลผล...",
                search:         "ค้นหา:",
                zeroRecords:    "ไม่พบรายการ",
                paginate: {
                    first:      "หน้าแรก",
                    last:       "หน้าสุดท้าย",
                    next:       "ถัดไป",
                    previous:   "ก่อนหน้า"
                },
                aria: {
                    sortAscending:  ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปมาก",
                    sortDescending: ": เปิดใช้งานเพื่อเรียงคอลัมน์จากมากไปหาน้อย"
                }
            },
            drawCallback: function () {
                
            },
            ajax: function(data, callback, settings) {
            
                $.ajaxSetup({
                    url: "{{ route('pages.meetingstatisticsreport.datatables') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : "POST",
                    dataType: 'json',
                    error: function(data){
                        if(data.status == 401) {
                            $('#login_modal').modal('show');
                        }
                        console.log('error');
                    },
                    success: function(data){

                        callback({
                            recordsTotal    : data.recordsTotal,
                            recordsFiltered : data.recordsFiltered,
                            data            : data.data
                        });
                    }
                });
                
                data.search.meeting_name                = getMeetingName();
                data.search.meeting_place               = getMeetingPlace();
                data.search.meeting_organization_name   = getMeetingOrganizationName();
                data.search.meeting_organizer_name      = getMeetingOrganizerName();

                $.ajax({ 
                    data: data
                });
            },
            aoColumnDefs: [
                { targets: [ 0 ], sWidth: '20%' },
                { targets: [ 1 ], sWidth: '20%' },
                { targets: [ 2 ], sWidth: '20%' },
                { targets: [ 3 ], sWidth: '20%' },
                { targets: [ 4 ], sClass: '20%' }
            ],
            columns: [
                null,
                null,
                null,
                null,
                null,
            ]
        });
        
        dataTable.draw();
      
        $('#search').on('click',  function () {

            setMeetingName($("input[name='meeting_name']").val());
            setMeetingPlace($("input[name='meeting_place']").val());
            setMeetingOrganizationName($("input[name='meeting_organization_name']").val());
            setMeetingOrganizerName($("input[name='meeting_organizer_name']").val());

            dataTable.ajax.reload();
        });
    });

    function setMeetingName(meeting_name) {
        where_meeting_name = meeting_name;
    }

    function getMeetingName() {
        return where_meeting_name;
    }

    function setMeetingPlace(meeting_place) {
        where_meeting_place = meeting_place;
    }

    function getMeetingPlace() {
        return where_meeting_place;
    }

    function setMeetingOrganizationName(meeting_organization_name) {
        where_meeting_organization_name = meeting_organization_name;
    }

    function getMeetingOrganizationName() {
        return where_meeting_organization_name;
    }

    function setMeetingOrganizerName(meeting_organizer_name) {
        where_meeting_organizer_name = meeting_organizer_name;
    }

    function getMeetingOrganizerName() {
        return where_meeting_organizer_name;
    }

    $(document).on('click', '.btn-delete', function () {

        var form    = $(this).find('form').get(0);
        var url     = $(form).attr('action');
        var method  = $(form).attr('method');
        var data 	= new FormData(form);

        Swal.fire({
            text: "ต้องการลบรายการนี้ ใช่หรือไม่ ?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#38c172',
            cancelButtonColor: '#e3342f',
            confirmButtonText: 'ยืนยัน',
            cancelButtonText: 'ยกเลิก'
        }).then((result) => {
      

        if (result.isConfirmed) {

            $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                    Swal.fire({
                        text: data.message,
                        icon: data.icon,
                        confirmButtonText: 'ปิด'
                    });

                    if(data.status) {
                        $('#search').click();
                    }

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {
                        $('#login_modal').modal('show');
                    },
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }
            });
        
        }
        })
    });

    function report() {
       
        $("#form-export").submit();
        // pages.meetingstatisticsreport.report
    }
</script>
@endsection   