@extends('layouts.app')
@section('content')
@section('breadcrumbs')
  {{ Breadcrumbs::render('errors_520') }}
@endsection
<div class="error-page">
  <h2 class="headline text-danger">520</h2>

  <div class="error-content">
    <h3><i class="fa fa-warning text-danger"></i> Oops! Web server is returning an unknown error.</h3>

    <p>
      We will work on fixing that right away.
      Meanwhile, you may <a href="{{ route('home') }}">return to home.</a>
    </p>
    <span class="fa-stack fa-lg">
      <i class="fa fa-internet-explorer text-muted fa-stack-2x"></i>
      <i class="fa fa-check fa-stack-1x text-success"></i>
    </span>
    <span class="fa-stack fa-lg">
      <i class="fa fa-long-arrow-right"></i>
    </span>
    <span class="fa-stack fa-lg">
      <i class="fa fa-cloud text-muted fa-stack-2x"></i>
      <i class="fa fa-check fa-stack-1x text-success"></i>
    </span>
    <span class="fa-stack fa-lg">
      <i class="fa fa-long-arrow-right"></i>
    </span>
    <span class="fa-stack fa-lg">
      <i class="fa fa fa-server fa-stack-1x"></i>
      <i class="fa fa-ban fa-stack-2x text-danger"></i>
    </span>
  </div>
</div>
@endsection
@section('script')
<script>

   
</script>
@endsection   