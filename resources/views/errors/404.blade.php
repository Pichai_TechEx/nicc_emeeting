@extends('layouts.app')
@section('content')
@section('breadcrumbs')
  {{ Breadcrumbs::render('errors_404') }}
@endsection
<div class="error-page">
  <h2 class="headline text-warning">404</h2>

  <div class="error-content">
    <h3><i class="fa fa-warning text-warning"></i> Oops! Page not found.</h3>
    <p>
      We could not find the page you were looking for.
      Meanwhile, you may <a href="{{ route('home') }}">return to home.</a>
    </p>
  </div>
</div>
@endsection
@section('script')
<script>

   
</script>
@endsection   