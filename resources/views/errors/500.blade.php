@extends('layouts.app')
@section('content')
@section('breadcrumbs')
  {{ Breadcrumbs::render('errors_500') }}
@endsection
<div class="error-page">
  <h2 class="headline text-danger">500</h2>

  <div class="error-content">
    <h3><i class="fa fa-warning text-danger"></i> Oops! Something went wrong.</h3>

    <p>
      We will work on fixing that right away.
      Meanwhile, you may <a href="{{ route('home') }}">return to home.</a>
    </p>

  
  </div>
</div>
@endsection
@section('script')
<script>

   
</script>
@endsection   