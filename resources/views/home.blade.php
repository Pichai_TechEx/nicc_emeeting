@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row form-group">
        <div class="col">
            <button type="button" class="btn btn-sm btn-primary" style="background-color: #01579B;border: #024f8b;"><i class="fas fa-plus-circle"></i> สร้างวาระการประชุม</button>
            <button type="button" class="btn btn-sm btn-primary"><i class="fas fa-filter"></i> กรอกหัวข้อจากการประชุม</button>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-6">
            {!! $calendar->calendar() !!}
        </div>
        <div class="col-md-6">
            <table id="table_id" class="table">
                <thead>
                    <tr>
                        <th scope="col"><i class="fas fa-users"></i> รายละเอียดการประชุม</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <p class="text-primary">ทดสอบการประชุม</p>
                            <p class="text-dark"><i class="far fa-calendar-alt"></i> 21 พ.ค. 2563  <i class="far fa-clock"></i> 09.00-12.00 น.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="text-primary">ทดสอบการประชุม</p>
                            <p class="text-dark"><i class="far fa-calendar-alt"></i> 21 พ.ค. 2563  <i class="far fa-clock"></i> 09.00-12.00 น.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="text-primary">ทดสอบการประชุม</p>
                            <p class="text-dark"><i class="far fa-calendar-alt"></i> 21 พ.ค. 2563  <i class="far fa-clock"></i> 09.00-12.00 น.</p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p class="text-primary">ทดสอบการประชุม</p>
                            <p class="text-dark"><i class="far fa-calendar-alt"></i> 21 พ.ค. 2563  <i class="far fa-clock"></i> 09.00-12.00 น.</p>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
@section('script')
{!! $calendar->script() !!}
<script>
   $(document).ready( function () {
        $('#table_id').DataTable({
            destroy     : true,
            serverSide  : false,
            paging      : true,
            info        : true,
            lengthChange: false,
            searching   : false,
            ordering    : true,
            autoWidth   : true,
            deferLoading: 0
        });
    });
</script>
@endsection
