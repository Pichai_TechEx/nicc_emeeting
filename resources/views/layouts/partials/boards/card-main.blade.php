<div class="container">
    @if(Request::is('boards/create'))
    {{ Form::open(['route' => 'pages.boards.store', 'method' => 'POST', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif

    @if(Request::is('boards/*/edit'))
    {{ Form::open(['route' => ['pages.boards.update', 'id' => ( !empty($result['data']->id) ? $result['data']->id : '' )], 'method' => 'PATCH', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif
    {{ Form::hidden('lang', 'th') }}
    {{ Form::hidden('id', !empty($result['data']->id) ? $result['data']->id : '') }}

    <div class="card mb-4">
        <div class="card-header">
            <p class="font-weight-bold mb-0">@yield('card-header')คณะกรรมการ</p>
          </div>
        <div class="card-body">
            <div class="form-group row">
                {{ Form::label('name', 'ชื่อคณะกรรมการ :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-7">
                    {{ Form::text('name', old('name') ? old('name') : ( !empty($result['data']->name) ? $result['data']->name : '' ), ['class' => 'form-control '.($errors->has('name') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{Form::label('start_date', 'วาระดำรงตำแหน่ง :', ['class' => 'col-sm-3 col-form-label text-right'])}}
                <div class="col-sm-3">
                    {{ Form::text('start_date', old('start_start_date') ? old('start_date') : ( !empty($result['data']->start_date) ? $result['data']->start_date : '' ), ['class' => 'datepicker '.($errors->has('start_date') ? 'is-invalid' : ''), 'placeholder' => 'DD/MM/YYYY', 'autocomplete' => "off", 'required' => true]) }}
                    <span class="form-group text-danger vld validate-start_date" style="display:none">
                        <small><i class="validate-start_date-i"></i></small>
                    </span>
                </div>
                {{Form::label('end_date', 'ถึง :', ['class' => 'col-sm-1 col-form-label text-right'])}}
                <div class="col-sm-3">
                    {{ Form::text('end_date', old('end_date') ? old('end_date') : ( !empty($result['data']->end_date) ? $result['data']->end_date : '' ), ['class' => 'datepicker '.($errors->has('end_date') ? 'is-invalid' : ''), 'placeholder' => 'DD/MM/YYYY', 'autocomplete' => "off", 'required' => true]) }}
                    <span class="form-group text-danger vld validate-end_date" style="display:none">
                        <small><i class="validate-end_date-i"></i></small>
                    </span>
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('detail', 'รายละเอียด :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-7">
                    {{ Form::textarea('detail', old('detail') ? old('detail') : ( !empty($result['data']->detail) ? $result['data']->detail : '' ), ['class' => 'form-control '.($errors->has('detail') ? 'is-invalid' : ''), 'rows' => 3, 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 col-form-label text-right">เปิด/ปิดการใช้งาน :</label>
                <div class="col-sm-4">
                    {{ Form::hidden('is_enabled', 0) }}
                    {{ Form::checkbox('is_enabled', 1, old('is_enabled') == 1 ? true : ( !empty($result['data']->is_enabled) && $result['data']->is_enabled == 1 ? true : '' ), ['data-toggle' => 'toggle', 'data-on' => 'เปิด', 'data-off' => 'ปิด', 'data-onstyle' => 'success', 'data-offstyle' => 'danger', 'data-size' => 'sm']) }}
                </div>
            </div>
    
        </div>
    </div>
    <div class="card mb-4">
        <div class="card-header pb-2 d-flex justify-content-between">
            <p class="font-weight-bold mb-0">เอกสารแต่งตั้งคณะกรรมการ</p> 
            <label for="btn-file-upload" class="btn-upload btn-sm mb-0"><i class="fas fa-file-upload"></i> เลือก</label>
        </div>
        <div class="card-body">
            <input type="file" id="btn-file-upload" name="fileToUpload" hidden/>
        </div>
    </div>
    <div class="card mb-4">
        <div class="card-header">
            <p class="font-weight-bold mb-0">รายชื่อคณะกรรมการ (เพิ่มรายชื่อที่มีในระบบ)</p>
          </div>
        <div class="card-body">
            <table class="table table-bordered" cellspacing="0" width="100%" id="dataTable" name="dataTable">
                <thead class="thead-light">
                    <tr>
                        <th style="width : 3rem;">ลำดับ</th>
                        <th style="width : 10rem;">หน่วยงาน</th>
                        <th style="width : 15rem;">ตำแหน่งที่แต่งตั้ง</th>
                        <th>ชื่อ - นามสกุล</th>
                        <th style="width : 12rem;">ตำแหน่งที่ประชุม</th>
                        <th style="width : 12rem;">อีเมล</th>
                        <th style="width : 10rem;">เบอร์ติดต่อ</th>
                        <th>อื่นๆ</th>
                        <th style="width : 3rem;" class="p-1 text-center">
                            <button type="button" class="btn btn-success"><i class="fas fa-user-plus"></i></button>
                        </th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <p class="font-weight-bold mb-0">รายชื่อคณะกรรมการ (เพิ่มรายชื่อที่ยังไม่มีในระบบ)</p>
        </div>
        <div class="card-body">
            <table class="table table-bordered" cellspacing="0" width="100%" id="dataTableNew" name="dataTableNew">
                <thead class="thead-light">
                    <tr>
                        <th style="width : 3rem;">ลำดับ</th>
                        <th style="width : 10rem;">หน่วยงาน</th>
                        <th style="width : 10rem;">ตำแหน่งที่แต่งตั้ง</th>
                        <th style="width : 15rem;">คำนำหน้า</th>
                        <th style="width : 15rem;">ชื่อ</th>
                        <th style="width : 15rem;">นามสกุล</th>
                        <th style="width : 12rem;">ตำแหน่งที่ประชุม</th>
                        <th style="width : 12rem;">อีเมล</th>
                        <th style="width : 10rem;">เบอร์ติดต่อ</th>
                        <th style="width : 10rem;">อื่นๆ</th>
                        <th style="width : 3rem;" class="p-1 text-center">
                            <button type="button" class="btn btn-info"><i class="fas fa-user-plus"></i></button>
                        </th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-between">
                <a role="button" class="btn btn-dark" href="{{ route('pages.boards.index') }}"><i class="far fa-arrow-alt-circle-left"></i> ย้อนกลับ</a>
                <button type="submit" class="btn btn-success"><i class="far fa-save"></i> บันทึก</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@section('script')
<script type="text/javascript">

    String.prototype.hashCodeStringOnly = function () {
        var length              = 20;
        var result              = '';
        var characters          = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength    = characters.length;

        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

   $(document).ready( function () {

        var my_edit = JSON.parse('{!! !empty($result["boardsattendances"]) ? $result["boardsattendances"] : [] !!}');
  
        $('.toggle-group').find('label').removeAttr('for');
   

        $('input[type=checkbox].icheck-green, input[type=radio].icheck-green').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
     
        $('input[type=checkbox].icheck-yellow, input[type=radio].icheck-yellow').iCheck({
            checkboxClass: 'icheckbox_square-yellow',
            radioClass: 'iradio_square-yellow',
        });
     
        $('input[type=checkbox].icheck-blue, input[type=radio].icheck-blue').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
        });
  
        gj.core.messages['th-th'] = {
            monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
            monthShortNames: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
            weekDaysMin: ["อา", "จ", "อ", "พ", "พฤ", "ศ", "ส"],
            weekDaysShort: ["อา", "จ", "อ", "พ", "พฤ", "ศ", "ส"],
            weekDays: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์"],
            ok: 'ตกลง',
            cancel: 'ยกเลิก',
            titleFormat: 'mmmm yyyy'
        };

        $.each($('.datepicker'), function (i, e) { 
            var date = new Date();
            $(this).datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                format: 'dd/mm/yyyy',
                locale: 'th-th',     
                value: $(this).val() ? $(this).val() : ( parseInt(date.getDate(), 10) < 10 ? '0' : '' ) + (date.getDate()).toString() + '/' + ( (parseInt(date.getMonth(), 10) + 1) < 10 ? '0' : '' ) + (parseInt(date.getMonth(), 10) + 1).toString() + '/' + (parseInt(date.getFullYear(), 10) + 543)
            });
        });

       var dataTable = $('#dataTable').DataTable({
            destroy     : true,
            serverSide  : false,
            paging      : false,
            info        : false,
            lengthChange: false,
            searching   : false,
            ordering    : false,
            autoWidth   : true,
            deferLoading: 0,
            language : {
                decimal:        "",
                emptyTable:     "ไม่มีข้อมูลในตาราง",
                info:           "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
                infoEmpty:      "แสดง 0 ถึง 0 จาก 0 รายการ",
                infoFiltered:   "(กรองจากรายการทั้งหมด _MAX_ รายการ)",
                infoPostFix:    "",
                thousands:      ",",
                lengthMenu:     "แสดง _MENU_ รายการ",
                loadingRecords: "กำลังโหลด...",
                processing:     "กำลังประมวลผล...",
                search:         "ค้นหา:",
                zeroRecords:    "ไม่พบรายการ",
                paginate: {
                    first:      "หน้าแรก",
                    last:       "หน้าสุดท้าย",
                    next:       "ถัดไป",
                    previous:   "ก่อนหน้า"
                },
                aria: {
                    sortAscending:  ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปมาก",
                    sortDescending: ": เปิดใช้งานเพื่อเรียงคอลัมน์จากมากไปหาน้อย"
                }
            },
            createdRow: async function ( row, data, index ) {

                var tr = $(row);
                
                    tr.find("select[name*='name_option']").select2({
                        ajax: {
                            url: "{{ route('pages.user.selectsapi') }}",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type : "POST",
                            dataType: 'json',
                            data: function (params) {
                                var query = {
                                    search: params.term,
                                }

                                return query;
                            },
                            processResults: function (data) {
                                return {
                                    results: data
                                };
                            }
                        },
                        templateSelection: function(data) {
                            tr.find("td:eq(1) span").text(data.org);
                            tr.find("td:eq(1) input[name*='organization']").not("td:eq(1) input[name*='organization_name']").val(data.organization_id);
                            tr.find("td:eq(1) input[name*='organization_name']").val(data.org);
                            tr.find("td:eq(2) span").text(data.position);
                            tr.find("td:eq(2) input").val(data.position);
                            tr.find("td:eq(3) input[name*='title']").not("td:eq(3) input[name*='title_name']").val(data.title_id);
                            tr.find("td:eq(3) input[name*='title_name']").val(data.title_name);
                            tr.find("td:eq(3) input[name*='first_name']").val(data.first_name);
                            tr.find("td:eq(3) input[name*='last_name']").val(data.last_name);
                            tr.find("td:eq(4) input[name*='position_in_meeting_name']").val(data.position_in_meeting_name);
                            tr.find("td:eq(5) span").text(data.email);
                            tr.find("td:eq(5) input").val(data.email);
                            tr.find("td:eq(6) span").text(data.tel);
                            tr.find("td:eq(6) input").val(data.tel);
                            tr.find("td:eq(7) input").val(data.other);

                            return data.text;
                        },
                        placeholder: {
                            id: '-1',
                            text: 'กรุณาเลือกข้อมูล'
                        },
                        width: '100%',
                        theme: 'bootstrap4',
                    });

                    tr.find("select[name*='name_option']").prop("disabled", false);
                    tr.find("select[name*='name_option']").val('-1').trigger('change');
                    tr.find("select[name*='name_option']").on('select2:select', async function (e) {

                        var val = e.params.data;

                        tr.find("select[name*='position_in_meeting']").select2({
                            data : await position_in_meeting_selects(val.position_in_meeting_id),
                            placeholder: {
                                id: '-1',
                                text: 'กรุณาเลือกข้อมูล'
                            },
                            width: '100%',
                            theme: 'bootstrap4',
                            templateSelection: function(data) {
                                tr.find("td:eq(4) input[name*='position_in_meeting_name']").val(data.text);
                                return data.text;
                            },
                        });
                    });

                    tr.find("select[name*='position_in_meeting']").select2({
                        data : await position_in_meeting_selects(),
                        placeholder: {
                            id: '-1',
                            text: 'กรุณาเลือกข้อมูล'
                        },
                        width: '100%',
                        theme: 'bootstrap4',
                        templateSelection: function(data) {
                            tr.find("td:eq(4) input[name*='position_in_meeting_name']").val(data.text);
                            return data.text;
                        },
                    });
                    
                    tr.find("select[name*='position_in_meeting']").prop("disabled", false);
                    tr.find("select[name*='position_in_meeting']").val('-1').trigger('change');
                    if(tr.find("select[name*='name_option']").data().edit) {
         
                        tr.find("select[name*='name_option']").select2({
                            data : await selectsapi(tr.find("select[name*='name_option']").data().edit, tr.find("select[name*='name_option']").data().attendance, $(document).find("input[name='id']").val(), true),
                            ajax: {
                                url: "{{ route('pages.user.selectsapi') }}",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type : "POST",
                                dataType: 'json',
                                data: function (params) {
                                    var query = {
                                        search: params.term,
                                    }
  
                                    return query;
                                },
                                processResults: function (data) {
                                    return {
                                        results: data
                                    };
                                }
                            },
                            templateSelection: function(data) {
                                tr.find("td:eq(1) span").text(data.org);
                                tr.find("td:eq(1) input[name*='organization']").not("td:eq(1) input[name*='organization_name']").val(data.organization_id);
                                tr.find("td:eq(1) input[name*='organization_name']").val(data.org);
                                tr.find("td:eq(2) span").text(data.position);
                                tr.find("td:eq(2) input").val(data.position);
                                tr.find("td:eq(3) input[name*='title']").not("td:eq(3) input[name*='title_name']").val(data.title_id);
                                tr.find("td:eq(3) input[name*='title_name']").val(data.title_name);
                                tr.find("td:eq(3) input[name*='first_name']").val(data.first_name);
                                tr.find("td:eq(3) input[name*='last_name']").val(data.last_name);
                                tr.find("td:eq(4) input[name*='position_in_meeting_name']").val(data.position_in_meeting_name);
                                tr.find("td:eq(5) span").text(data.email);
                                tr.find("td:eq(5) input").val(data.email);
                                tr.find("td:eq(6) span").text(data.tel);
                                tr.find("td:eq(6) input").val(data.tel);
                                tr.find("td:eq(7) input").val(data.other);

                                return data.text;
                            },
                            placeholder: {
                                id: '-1',
                                text: 'กรุณาเลือกข้อมูล'
                            },
                            width: '100%',
                            theme: 'bootstrap4',
                        });
                    }

                    if(tr.find("select[name*='position_in_meeting']").data().edit) {
                        tr.find("select[name*='position_in_meeting']").select2({
                            data : await position_in_meeting_selects(tr.find("select[name*='position_in_meeting']").data().edit),
                            placeholder: {
                                id: '-1',
                                text: 'กรุณาเลือกข้อมูล'
                            },
                            width: '100%',
                            theme: 'bootstrap4',
                            templateSelection: function(data) {
                                tr.find("td:eq(4) input[name*='position_in_meeting_name']").val(data.text);
                                return data.text;
                            },
                        });
                    }

            },
            drawCallback: function () {
        
            },
            ajax: function(data, callback, settings) {
           
            },
            aoColumnDefs: [
                { targets: [ 0, 1, 2, 3, 4, 5, 6, 7 ], orderable: false },
                { targets: [ 8 ], sClass: 'text-center all', orderable: false }
            ],
            columns: [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            ]
        });
        
        dataTable.draw();
        var header = $(dataTable.table().header());

        header.find('button').on('click', function () {
            dataTable.row.add( [
                '',
                $('<div>').append($('<span/>').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][organization][]' }).clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][organization_name][]' }).clone()).html(),
                $('<div>').append($('<span/>').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][position_name][]' }).clone()).html(),
                $('<div>').append($('<select/>').attr({ 'name' : 'attendance[old][name_option][]' }).addClass('form-control').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][title][]' }).clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][title_name][]' }).clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][first_name][]' }).clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][last_name][]' }).clone()).html(),
                $('<div>').append($('<select/>').attr({ 'name' : 'attendance[old][position_in_meeting][]' }).addClass('form-control').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][position_in_meeting_name][]' }).clone()).html(),
                $('<div>').append($('<span/>').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][email][]' }).clone()).html(),
                $('<div>').append($('<span/>').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][phone][]' }).clone()).html(),
                $('<div>').append($('<input/>').attr({ 'name' : 'attendance[old][other][]' }).addClass('form-control').clone()).html(),
                $('<div>').append($('<button/>').attr({ 'type' : 'button' }).addClass('btn btn-danger datatable-delete').append($('<i/>').addClass('fas fa-user-minus')).clone()).html(),
            ] ).draw( false );

            dataTable.column(0).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        });

       var dataTableNew = $('#dataTableNew').DataTable({
            destroy     : true,
            serverSide  : false,
            paging      : false,
            info        : false,
            lengthChange: false,
            searching   : false,
            ordering    : false,
            autoWidth   : true,
            deferLoading: 0,
            createdRow: async function ( row, data, index ) {

                var tr = $(row);
                
                    tr.find("select[name*='organization']").select2({
                        data : await organizations_selects(),
                        placeholder: {
                            id: '-1',
                            text: 'กรุณาเลือกข้อมูล'
                        },
                        width: '100%',
                        theme: 'bootstrap4',
                    });

                    tr.find("select[name*='organization']").prop("disabled", false);
                    tr.find("select[name*='organization']").val('-1').trigger('change');
                    tr.find("select[name*='organization']").on('select2:select', async function (e) {
                        var val = e.params.data;
                        tr.find("input[name*='organization_name']").val(val.text);
                    });
                    
                    tr.find("select[name*='title']").select2({
                        data : await titles_selects(),
                        placeholder: {
                            id: '-1',
                            text: 'กรุณาเลือกข้อมูล'
                        },
                        width: '100%',
                        theme: 'bootstrap4',
                    });

                    tr.find("select[name*='title']").prop("disabled", false);
                    tr.find("select[name*='title']").val('-1').trigger('change');
                    tr.find("select[name*='title']").on('select2:select', async function (e) {
                        var val = e.params.data;
                        tr.find("input[name*='title_name']").val(val.text);
                    });

                    tr.find("select[name*='position_in_meeting']").select2({
                        data : await position_in_meeting_selects(),
                        placeholder: {
                            id: '-1',
                            text: 'กรุณาเลือกข้อมูล'
                        },
                        width: '100%',
                        theme: 'bootstrap4',
                    });
                    
                    tr.find("select[name*='position_in_meeting']").prop("disabled", false);
                    tr.find("select[name*='position_in_meeting']").val('-1').trigger('change');

                    tr.find("select[name*='position_in_meeting']").on('select2:select', async function (e) {
                        var val = e.params.data;
                        tr.find("input[name*='position_in_meeting_name']").val(val.text);
                    });
            },
            drawCallback: function () {
        
            },
            ajax: function(data, callback, settings) {
           
            },
            aoColumnDefs: [
                { targets: [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ], orderable: false },
                { targets: [ 10 ], sClass: 'text-center all' }
            ],
            columns: [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            ]
        });
        
        dataTableNew.draw();
        var headerNew = $(dataTableNew.table().header());

        headerNew.find('button').on('click', function () {
            dataTableNew.row.add( [
                '',
                $('<div>').append($('<select/>').attr({ 'name' : 'attendance[news][organization][]' }).addClass('form-control').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[news][organization_name][]' }).clone()).html(),
                $('<div>').append($('<input/>').attr({ 'name' : 'attendance[news][position][]' }).addClass('form-control').clone()).html(),
                $('<div>').append($('<select/>').attr({ 'name' : 'attendance[news][title][]' }).addClass('form-control').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[news][title_name][]' }).clone()).html(),
                $('<div>').append($('<input/>').attr({ 'name' : 'attendance[news][first_name][]' }).addClass('form-control').clone()).html(),
                $('<div>').append($('<input/>').attr({ 'name' : 'attendance[news][last_name][]' }).addClass('form-control').clone()).html(),
                $('<div>').append($('<select/>').attr({ 'name' : 'attendance[news][position_in_meeting][]' }).addClass('form-control').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[news][position_in_meeting_name][]' }).clone()).html(),
                $('<div>').append($('<input/>').attr({ 'name' : 'attendance[news][email][]' }).addClass('form-control').clone()).html(),
                $('<div>').append($('<input/>').attr({ 'name' : 'attendance[news][phone][]' }).addClass('form-control').clone()).html(),
                $('<div>').append($('<input/>').attr({ 'name' : 'attendance[news][other][]' }).addClass('form-control').clone()).html(),
                $('<div>').append($('<button/>').attr({ 'type' : 'button' }).addClass('btn btn-danger datatable-delete').append($('<i/>').addClass('fas fa-user-minus')).clone()).html(),
            ] ).draw( false );

            dataTableNew.column(0).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });
        });

        if(my_edit.length > 0) {
            $.each(my_edit, function (i, e) { 
                dataTable.row.add( [
                    '',
                    $('<div>').append($('<span/>').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][organization][]' }).clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][organization_name][]' }).clone()).html(),
                    $('<div>').append($('<span/>').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][position_name][]' }).clone()).html(),
                    $('<div>').append($('<select/>').attr({ 'name' : 'attendance[old][name_option][]', 'data-edit' : e.user_id, 'data-attendance' : e.id }).addClass('form-control').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][title][]' }).clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][title_name][]' }).clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][first_name][]' }).clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][last_name][]' }).clone()).html(),
                    $('<div>').append($('<select/>').attr({ 'name' : 'attendance[old][position_in_meeting][]', 'data-edit' : e.position_in_meeting_id }).addClass('form-control').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][position_in_meeting_name][]' }).clone()).html(),
                    $('<div>').append($('<span/>').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][email][]' }).clone()).html(),
                    $('<div>').append($('<span/>').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][phone][]' }).clone()).html(),
                    $('<div>').append($('<input/>').attr({ 'name' : 'attendance[old][other][]', 'value' : e.other }).addClass('form-control').clone()).html(),
                    $('<div>').append($('<button/>').attr({ 'type' : 'button' }).addClass('btn btn-danger datatable-delete').append($('<i/>').addClass('fas fa-user-minus')).clone()).html(),
                ] ).draw( false );
    
                dataTable.column(0).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                });
                    
            });
        }

        $(document).on('click', '.datatable-delete', function () {
            switch ($(this).parents('table').attr('name')) {
                case 'dataTable':
                    dataTable.row($(this).parents('tr')).remove().draw();

                    dataTable.column(0).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                    break;
                case 'dataTableNew':
                    
                    dataTableNew.row($(this).parents('tr')).remove().draw();

                    dataTableNew.column(0).nodes().each( function (cell, i) {
                        cell.innerHTML = i+1;
                    });
                    break;
            }
        });

        var jFiler_item = $('<li/>').addClass('jFiler-item p-2');
        var jFiler_item_container = $('<div/>').addClass('jFiler-item-container');
        var jFiler_item_inner = $('<div/>').addClass('jFiler-item-inner');
        var jFiler_item_icon = $('<div/>').addClass('jFiler-item-icon float-left text-dark').text("{"+"{" + "fi-icon" + "}"+"}");
        var jFiler_item_info = $('<div/>').addClass('jFiler-item-info float-left');
        var jFiler_item_title = $('<div/>').addClass('jFiler-item-title').append($('<a/>').attr({'href' : "" ,'download': "{"+"{" + "fi-name" + "}"+"}"}).text("{"+"{" + "fi-name | limitTo:35" + "}"+"}"));
        var jFiler_item_others = $('<div/>').addClass('jFiler-item-others');
        var jFiler_item_status = $('<span/>').addClass('jFiler-item-status');

        var jFiler_item_assets = $('<div/>').addClass('jFiler-item-assets');
        var jFiler_ul = $('<ul/>').addClass('list-inline');
        var jFiler_li = $('<li/>');
        var jFiler_a = $('<a/>').addClass('icon-jfi-trash jFiler-item-trash-action');

        jFiler_li.append(jFiler_a);

        jFiler_ul.append(jFiler_li);

        jFiler_item_assets.append(jFiler_ul);

        jFiler_item_others.append($('<span/>').text("size : {"+"{" + "fi-size2" + "}"+"}"));
        jFiler_item_others.append($('<span/>').text("type : {"+"{" + "fi-extension" + "}"+"}"));
        jFiler_item_others.append(jFiler_item_status);

        jFiler_item_info.append(jFiler_item_title);
        jFiler_item_info.append(jFiler_item_others);
        jFiler_item_info.append(jFiler_item_assets);

        jFiler_item_inner.append(jFiler_item_icon);
        jFiler_item_inner.append(jFiler_item_info);

        jFiler_item_container.append(jFiler_item_inner);

        jFiler_item.append(jFiler_item_container);

        var files = JSON.parse('{!! !empty($result["files"]) ? $result["files"] : "[]" !!}');

        if(files.length > 0) {
            $.each(files, function (i, e) {
                $('#form-submit').append($('<input/>').attr({ 'type' : 'hidden', 'name' : 'file_id[]', 'value' : e.id }));
            });
        }

        $('#btn-file-upload').filer({
            limit: 5,
            maxSize: 10,
            extensions: ["pdf", "doc", "docx", "xls", "xlsx"],
            showThumbs: true,
            templates : {
                item: $('<div>').append(jFiler_item.clone()).html(),
                itemAppend: $('<div>').append(jFiler_item.clone()).html()
            },
            files : files,
            beforeRender : function (itemEl, inputEl) {
                    
                inputEl.change(function() {
                    var fileToUpload = $(this).prop('files');
                    if(fileToUpload.length > 0 ) {
                        Object.keys(fileToUpload).forEach(file => {
                            var lastModified = fileToUpload[file].lastModified;
                            var lastModifiedDate = fileToUpload[file].lastModifiedDate;
                            var name = fileToUpload[file].name;
                            var size = fileToUpload[file].size;
                            var type = fileToUpload[file].type;
                            var fileReader = new FileReader();
                                fileReader.readAsDataURL(fileToUpload[file]);
                                fileReader.onload = function(fileLoadedEvent){
                                    var _a = itemEl.find('ul.jFiler-items-list li.jFiler-item').eq(file).find('.jFiler-item-title a');
                                    _a.attr({'href' : fileReader.result, 'download' : _a.text()});
                                };
                        });
                    }
                });
            },
            onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
                console.log(inputEl)
                if($("input[name^='file_id'][value='" + file.id + "']").length > 0) {
                    $("input[name^='file_id'][value='" + file.id + "']").remove();
                }
            }
        });    

        $('#btn-file-upload').parent().find('.jFiler-input').addClass('d-none');

        $('#btn-file-upload').on('change',function() {
            if($("input[name^='file_id']").length > 0) {
                $.each($("input[name^='file_id']"), function (i, e) { 
                    $(this).remove();
                });
            }
        });

        if(files.length > 0) {
            $.each(files, function (i, e) {
                var _a = $('#form-submit').find('ul.jFiler-items-list li.jFiler-item').eq(i).find('.jFiler-item-title a');
                _a.attr({'href' : e.url, 'download' : e.name});
            });
        }

        $("#form-submit").on('submit', function (e) {

            e.preventDefault();

            var self = $(this);
            var _btn = self.find("button[type='submit']");

            var _span = $('<span/>').addClass('spinner-border spinner-border-sm mb-2').attr({'role' : 'status', 'aria-hidden' : true});
            var _span_txt = $('<span/>').append('&nbsp;Loading...');

            var _i_default = $('<i/>').addClass('far fa-save');

            _btn.attr('disabled', false);
            
            _btn.attr('disabled', true);
            _btn.find('i').remove();
            _btn.text('');
            _btn.append(_span);
            _btn.append(_span_txt);

            var method 		= $(this).attr('method');
            var url 		= $(this).attr('action');
            var data 		= new FormData(this);

            $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData: false,
                success		: function(data) {

                    var element = null;
                    if(data.errors) {

                        $(".back").click();

                        $.each(data.errors, function(key, value) {

                            if(data.rules[key]) {
                                delete data.rules[key];
                            }

                            $(".validate-" + key).show();
                            $(".validate-" + key + "-i").text(value);
                        });

                        element = Object.keys(data.errors)[0];
                    }

                    if(data.rules) {
                        $.each(data.rules, function(key, value){
                            $(".validate-" + key).hide();
                        });
                    }

                    if(element) {
                        $("*[name='" + element + "']").focus();
                        return;
                    }

                    if(data.status) {

                        $('.vld').hide();

                    }

                    Swal.fire({
                        position: 'center',
                        icon: data.icon,
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                       if(result.isDismissed && data.redirect_url != '') {
                            window.location.href = data.redirect_url;
                       }
                    });

                    _btn.attr('disabled', false);
                    _btn.find('span').remove();
                    _btn.append(_i_default);
                    _btn.append('&nbsp;บันทึก');

                    

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {
                        $('#login_modal').modal('show');
                    },
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {
                        alert("500");

                        _btn.attr('disabled', false);
                        _btn.find('span').remove();
                        _btn.append(_i_default);
                        _btn.append('&nbsp;บันทึก');
                    }			
                }
            });
        });
    });

    function organizations_selects(val = '') {
        var method 		= "POST";
        var url 		= "{{ route('pages.organizations.selects') }}";
        var data 		= new FormData();
            data.append('selected_id', val);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }

    function titles_selects(val = '') {
        var method 		= "POST";
        var url 		= "{{ route('pages.titles.selects') }}";
        var data 		= new FormData();
            data.append('selected_id', val);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }

    function position_in_meeting_selects(val = '') {
        var method 		= "POST";
        var url 		= "{{ route('pages.positioninmeeting.selects') }}";
        var data 		= new FormData();
            data.append('selected_id', val);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }

    function selectsapi(val = '', attendance_id = '', board_id = '',is_edit = false) {
        var method 		= "POST";
        var url 		= "{{ route('pages.user.selectsapi') }}";
        var data 		= new FormData();
            data.append('selected_id', val);
            data.append('attendance_id', attendance_id);
            data.append('board_id', board_id);
            data.append('is_edit', is_edit);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }
</script>
@endsection