<div class="container">
    @if(Request::is('positioninmeeting/create'))
    {{ Form::open(['route' => 'pages.positioninmeeting.store', 'method' => 'POST', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif

    @if(Request::is('positioninmeeting/*/edit'))
    {{ Form::open(['route' => ['pages.positioninmeeting.update', 'id' => ( !empty($result['data']->id) ? $result['data']->id : '' )], 'method' => 'PATCH', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif
    {{ Form::hidden('lang', 'th') }}

    <div class="card">
        <div class="card-header">
            @yield('card-header')
          </div>
        <div class="card-body">
            <div class="form-group row">
                {{ Form::label('name', 'ตำแหน่งในที่ประชุม :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-4">
                    {{ Form::text('name', old('name') ? old('name') : ( !empty($result['data']->name) ? $result['data']->name : '' ), ['class' => 'form-control '.($errors->has('name') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                </div>
            </div>
            @if ($errors->has('name'))
                <div class="form-group row">
                    <div class="offset-sm-3 col-sm-4 text-danger">
                        <small><i>{{ $errors->first('name') }}</i></small>
                    </div>
                </div>
            @endif
            <div class="form-group row">
                {{ Form::label('description', 'รายละเอียด :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-4">
                    {{ Form::textarea('description', old('description') ? old('description') : ( !empty($result['data']->description) ? $result['data']->description : '' ), ['class' => 'form-control '.($errors->has('description') ? 'is-invalid' : ''), 'rows' => 3, 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('sort_no', 'ลำดับการแสดงข้อมูล :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-4">
                    {{ Form::text('sort_no', old('sort_no') ? old('sort_no') : ( !empty($result['data']->sort_no) ? $result['data']->sort_no : '' ), ['class' => 'form-control '.($errors->has('sort_no') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 col-form-label text-right">เปิด/ปิดการใช้งาน :</label>
                <div class="col-sm-4">
                    {{ Form::hidden('is_enabled', 0) }}
                    {{ Form::checkbox('is_enabled', 1, old('is_enabled') == 1 ? true : ( !empty($result['data']->is_enabled) && $result['data']->is_enabled == 1 ? true : '' ), ['data-toggle' => 'toggle', 'data-on' => 'เปิด', 'data-off' => 'ปิด', 'data-onstyle' => 'success', 'data-offstyle' => 'danger', 'data-size' => 'sm']) }}
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-between">
                <a role="button" class="btn btn-dark" href="{{ route('pages.positioninmeeting.index') }}"><i class="far fa-arrow-alt-circle-left"></i> ย้อนกลับ</a>
                <button type="submit" class="btn btn-success"><i class="far fa-save"></i> บันทึก</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@section('script')
<script type="text/javascript">

    String.prototype.hashCodeStringOnly = function () {
        var length              = 20;
        var result              = '';
        var characters          = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength    = characters.length;

        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

   $(document).ready( function () {

        $('.toggle-group').find('label').removeAttr('for');

        $('input[type=checkbox].icheck-green, input[type=radio].icheck-green').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
     
        $('input[type=checkbox].icheck-yellow, input[type=radio].icheck-yellow').iCheck({
            checkboxClass: 'icheckbox_square-yellow',
            radioClass: 'iradio_square-yellow',
        });
     
        $('input[type=checkbox].icheck-blue, input[type=radio].icheck-blue').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
        });

        $('.btn-proposer-add').on('click', function () {
            var proposer = $('.proposer').clone();
            proposer.removeClass('proposer');
            proposer.addClass('parent-proposer');
            proposer.find('label').remove();
            proposer.find('div:eq(0)').addClass('offset-sm-2');
            proposer.find('input').val('');
            proposer.find('button').removeClass('btn-success');
            proposer.find('button i').removeClass('fa-user-plus');
            proposer.find('button').removeClass('btn-proposer-add');
            proposer.find('button').addClass('btn-danger btn-proposer-delete');
            proposer.find('button i').addClass('fa-user-minus');

            $('.end-positioninmeeting').before(proposer);
        });
  
        $('.list-term-btn').on('click', function () {
            create_term();
        });


        $("#form-submit").on('submit', function (e) {

            e.preventDefault();

            var self = $(this);
            var _btn = self.find("button[type='submit']");

            var _span = $('<span/>').addClass('spinner-border spinner-border-sm mb-2').attr({'role' : 'status', 'aria-hidden' : true});
            var _span_txt = $('<span/>').append('&nbsp;Loading...');

            var _i_default = $('<i/>').addClass('far fa-save');

            _btn.attr('disabled', false);
            
            _btn.attr('disabled', true);
            _btn.find('i').remove();
            _btn.text('');
            _btn.append(_span);
            _btn.append(_span_txt);

            var method 		= $(this).attr('method');
            var url 		= $(this).attr('action');
            var data 		= new FormData(this);

            $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData: false,
                success		: function(data) {

                    var element = null;
                    if(data.errors) {

                        $(".back").click();

                        $.each(data.errors, function(key, value) {

                            if(data.rules[key]) {
                                delete data.rules[key];
                            }

                            $(".validate-" + key).show();
                            $(".validate-" + key + "-i").text(value);
                        });

                        element = Object.keys(data.errors)[0];
                    }

                    if(data.rules) {
                        $.each(data.rules, function(key, value){
                            $(".validate-" + key).hide();
                        });
                    }

                    if(element) {
                        $("*[name='" + element + "']").focus();
                        return;
                    }

                    if(data.status) {

                        $('.vld').hide();

                    }

                    Swal.fire({
                        position: 'center',
                        icon: data.icon,
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                       if(result.isDismissed && data.redirect_url != '') {
                            window.location.href = data.redirect_url;
                       }
                    });

                    _btn.attr('disabled', false);
                    _btn.find('span').remove();
                    _btn.append(_i_default);
                    _btn.append('&nbsp;บันทึก');

                    

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {
                        $('#login_modal').modal('show');
                    },
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {
                        alert("500");

                        _btn.attr('disabled', false);
                        _btn.find('span').remove();
                        _btn.append(_i_default);
                        _btn.append('&nbsp;บันทึก');
                    }			
                }
            });
        });
    });
</script>
@endsection