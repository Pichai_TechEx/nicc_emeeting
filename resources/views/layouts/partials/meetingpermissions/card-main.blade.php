<div class="container">
    @if(Request::is('meetingpermissions/create'))
    {{ Form::open(['route' => 'pages.meetingpermissions.store', 'method' => 'POST', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif

    @if(Request::is('meetingpermissions/*/edit'))
    {{ Form::open(['route' => ['pages.meetingpermissions.update', 'id' => ( !empty($result['data']->id) ? $result['data']->id : '' )], 'method' => 'PATCH', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif
    {{ Form::hidden('lang', 'th') }}

    <div class="card">
        <div class="card-header">
            @yield('card-header')
        </div>
        <div class="card-body">
            <div class="form-group row">
                {{ Form::label('name', 'ชื่อการประชุม :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-8">
                    {{ Form::select('meeting_id', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true , 'data-edit' => ( !empty($result['data']->meeting_id) ? $result['data']->meeting_id : '' ) ]) }}
                </div>
            </div>
            @php
                $first_id = time();
            @endphp
            <div class="form-group row div-template">
                <div class="offset-sm-3 col-sm-8">
                    <fieldset class="scheduler-border">
                        <legend class="scheduler-border">สิทธิ์การเห็นหัวข้อ</legend>
                        <div class="form-group row mater">
                            {{ Form::label(null, 'หัวข้อ :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                            <div class="col-sm-8">
                                <div class="input-group">
                                    {{ Form::select('meeting_issues['.$first_id.'][id]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'disabled' => true, 'data-meeting_id' => ( !empty($result['data']->meeting_id) ? $result['data']->meeting_id : '' ), 'data-edit' => ( !empty($result['data']->meeting_permissions) && count($result['data']->meeting_permissions) > 0 ? $result['data']->meeting_permissions[0]['meeting_issues_id'] : '' )]) }}
                                    <div class="input-group-append">
                                        <button class="btn btn-success add-box" type="button"><i class="fas fa-list-ul"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if (!empty($result['data']->meeting_permissions[0]['permissions']) && count($result['data']->meeting_permissions[0]['permissions']) > 0)
                            @foreach ($result['data']->meeting_permissions[0]['permissions'] as $key => $item)
                                @if ($key == 0)
                                <div class="form-group row mater">
                                    {{ Form::label('sort_no', 'กำหนดสิทธิ์ :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                    <div class="col-sm-4">
                                        {{ Form::select('meeting_select_issues['.$first_id.'][organization][]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'data-edit' => $item->organization_id ]) }}
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            {{ Form::select('meeting_select_issues['.$first_id.'][name_option][]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'data-edit' => $item->user_id ]) }}
                                            <div class="input-group-append">
                                                <button class="btn btn-success add-topic" type="button" data-id="{{ $first_id }}"><i class="fas fa-user-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="form-group row">
                                    <div class="col-sm-4 offset-sm-3">
                                        {{ Form::select('meeting_select_issues['.$first_id.'][organization][]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'data-edit' => $item->organization_id ]) }}
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="input-group">
                                            {{ Form::select('meeting_select_issues['.$first_id.'][name_option][]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'data-edit' => $item->user_id ]) }}
                                            <div class="input-group-append">
                                                <button class="btn btn-danger delete-topic" type="button" data-id="{{ $first_id }}"><i class="fas fa-user-minus"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                            @endforeach
                        @else
                            <div class="form-group row mater">
                                {{ Form::label('sort_no', 'กำหนดสิทธิ์ :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                <div class="col-sm-4">
                                    {{ Form::select('meeting_select_issues['.$first_id.'][organization][]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true ]) }}
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group">
                                        {{ Form::select('meeting_select_issues['.$first_id.'][name_option][]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true ]) }}
                                        <div class="input-group-append">
                                            <button class="btn btn-success add-topic" type="button" data-id="{{ $first_id }}"><i class="fas fa-user-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </fieldset>
                </div>
            </div>
            @if (!empty($result['data']->meeting_permissions) && count($result['data']->meeting_permissions) > 0)
                @foreach ($result['data']->meeting_permissions as $key => $item)
                    @if ($key > 0)
                        @php
                            $last_id = time() + $key;
                        @endphp
                        <div class="form-group row">
                            <div class="offset-sm-3 col-sm-8">
                                <fieldset class="scheduler-border">
                                    <legend class="scheduler-border">สิทธิ์การเห็นหัวข้อ</legend>
                                    <div class="form-group row mater">
                                        {{ Form::label(null, 'หัวข้อ :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                        <div class="col-sm-8">
                                            <div class="input-group">
                                                {{ Form::select('meeting_issues['.$last_id.'][id]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'disabled' => true, 'data-meeting_id' => ( !empty($result['data']->meeting_id) ? $result['data']->meeting_id : '' ), 'data-edit' => $item['meeting_issues_id'] ]) }}
                                                <div class="input-group-append">
                                                    <button class="btn btn-danger delete-box" type="button"><i class="fas fa-times"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @if (count($item['permissions']) > 0)
                                        @foreach ($item['permissions'] as $sub_key => $sub)
                                            @if ($sub_key == 0)
                                            <div class="form-group row mater">
                                                {{ Form::label('sort_no', 'กำหนดสิทธิ์ :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                                <div class="col-sm-4">
                                                    {{ Form::select('meeting_select_issues['.$last_id.'][organization][]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'data-edit' => $sub->organization_id ]) }}
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                        {{ Form::select('meeting_select_issues['.$last_id.'][name_option][]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'data-edit' => $sub->user_id ]) }}
                                                        <div class="input-group-append">
                                                            <button class="btn btn-success add-topic" type="button" data-id="{{ $last_id }}"><i class="fas fa-user-plus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @else
                                            <div class="form-group row">
                                                <div class="col-sm-4 offset-sm-3">
                                                    {{ Form::select('meeting_select_issues['.$last_id.'][organization][]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'data-edit' => $sub->organization_id ]) }}
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="input-group">
                                                        {{ Form::select('meeting_select_issues['.$last_id.'][name_option][]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'data-edit' => $sub->user_id ]) }}
                                                        <div class="input-group-append">
                                                            <button class="btn btn-danger delete-topic" type="button" data-id="{{ $last_id }}"><i class="fas fa-user-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        @endforeach
                                    @else
                                        <div class="form-group row mater">
                                            {{ Form::label('sort_no', 'กำหนดสิทธิ์ :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                            <div class="col-sm-4">
                                                {{ Form::select('meeting_select_issues['.$last_id.'][organization][]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true ]) }}
                                            </div>
                                            <div class="col-sm-4">
                                                <div class="input-group">
                                                    {{ Form::select('meeting_select_issues['.$last_id.'][name_option][]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true ]) }}
                                                    <div class="input-group-append">
                                                        <button class="btn btn-success add-topic" type="button" data-id="{{ $last_id }}"><i class="fas fa-user-plus"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </fieldset>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-between">
                <a role="button" class="btn btn-dark" href="{{ route('pages.meetingpermissions.index') }}"><i class="far fa-arrow-alt-circle-left"></i> ย้อนกลับ</a>
                <button type="submit" class="btn btn-success"><i class="far fa-save"></i> บันทึก</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@section('script')
<script type="text/javascript">

    String.prototype.hashCodeStringOnly = function () {
        var length              = 20;
        var result              = '';
        var characters          = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength    = characters.length;

        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

   $(document).ready( function () {

        $.each($('.timepicker'), function (i, e) { 
            $(this).datetimepicker({
                format: 'HH:mm',
                icons : {
                    time: 'far fa-clock',
                    date: 'far fa-calendar-alt',
                    up: 'fas fa-chevron-up',
                    down: 'fas fa-chevron-down',
                    previous: 'fas fa-chevron-left',
                    next: 'fas fa-chevron-right',
                }
            });
       });

        $.each($('.datepicker'), function (i, e) { 
            $(this).datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                format: 'dd/mm/yyyy',
            });
        });

        (async () => {
            $("select[name*='meeting_id']").select2({
                data : await selects_meeting_api($("select[name*='meeting_id']").data().edit),
                ajax: {
                    url: "{{ route('pages.meeting.selectsapi') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : "POST",
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                },
                templateSelection: function(data) {
                    return data.text;
                },
                placeholder: {
                    id: '-1',
                    text: 'กรุณาเลือกข้อมูล'
                },
                width: '100%',
                theme: 'bootstrap4',
            });

            $("select[name*='meeting_id']").prop("disabled", false);

            if(!$("select[name*='meeting_id']").data().edit) {
                $("select[name*='meeting_id']").val('-1').trigger('change');
            } else {

                $.each($("select[name*='meeting_issues']"), async function (i, e) { 
                     
                    $(this).select2({
                        data : await meeting_issues_selects($(this).data().meeting_id, $(this).data().edit),
                        placeholder: {
                            id: '-1',
                            text: 'กรุณาเลือกข้อมูล'
                        },
                        width: 'auto',
                        dropdownAutoWidth: true,
                        theme: 'bootstrap4',
                    });
    
                    $(this).prop("disabled", false);
    
                    if(!$(this).data().edit) {
                        $(this).val('-1').trigger('change');
                    }
                });
            }

            $("select[name*='meeting_id']").on('select2:select', function (e) {

                Swal.fire({
                    text: "การเปลี่ยนชื่อการประชุมทุกครั้งจะเคลียร์ค่าสิทธิ์ใหม่ ต้องการเปลี่ยน ใช่หรือไม่?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#38c172',
                    cancelButtonColor: '#e3342f',
                    confirmButtonText: 'ยืนยัน',
                    cancelButtonText: 'ปิด'
                }).then(async (result) => {
                    if (result.isConfirmed) {

                        var val = e.params.data;
                        
                        if ($("select[name*='meeting_issues']:eq(0)").data('select2')) {
                            $("select[name*='meeting_issues']:eq(0)").select2("destroy");  
                            $("select[name*='meeting_issues']:eq(0)").find("option").remove();
                        }

                        $("select[name*='meeting_issues']:eq(0)").attr({"data-meeting_id" : val.id});
                        $("select[name*='meeting_issues']:eq(0)").select2({
                            data : await meeting_issues_selects(val.id),
                            placeholder: {
                                id: '-1',
                                text: 'กรุณาเลือกข้อมูล'
                            },
                            width: 'auto',
                            dropdownAutoWidth: true,
                            theme: 'bootstrap4',
                        });

                        $("select[name*='meeting_issues']:eq(0)").prop("disabled", false);
                        $("select[name*='meeting_issues']:eq(0)").val('-1').trigger('change');
                        $("select[name*='organization']:eq(0)").val('-1').trigger('change');
                        $("select[name*='name_option']:eq(0)").val('-1').trigger('change');

                        $.each($(".div-template").parent().find(".form-group").not(".form-group:eq(0),.form-group.div-template, .form-group.mater"), function (i, e) { 
                           $(this).remove();  
                        });
                    }
                });
            });
       })();

        $.each($("select[name*='organization']"), async function (i, e) {
            $(this).select2({
                data : await selects_organization_api($(this).data().edit),
                ajax: {
                    url: "{{ route('pages.organizations.selectsapi') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : "POST",
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                },
                templateSelection: function(data) {
                    return data.text;
                },
                placeholder: {
                    id: '-1',
                    text: 'กรุณาเลือกข้อมูล'
                },
                width: '100%',
                theme: 'bootstrap4',
            });

            $(this).prop("disabled", false);

            if(!$(this).data().edit) {
                $(this).val('-1').trigger('change');
            }
        });

        $.each($("select[name*='name_option']"), async function (i, e) {
     
            $(this).select2({
                data : await selectsapi($(this).data().edit),
                ajax: {
                    url: "{{ route('pages.user.selectsapi') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : "POST",
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                },
                templateSelection: function(data) {
                    return data.text;
                },
                placeholder: {
                    id: '-1',
                    text: 'กรุณาเลือกข้อมูล'
                },
                width: 'auto',
			    dropdownAutoWidth: true,
                theme: 'bootstrap4',
            });

            $(this).prop("disabled", false);

            if(!$(this).data().edit) {
                $(this).val('-1').trigger('change');
            }
       });

        $('.toggle-group').find('label').removeAttr('for');

        $('input[type=checkbox].icheck-green, input[type=radio].icheck-green').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
     
        $('input[type=checkbox].icheck-yellow, input[type=radio].icheck-yellow').iCheck({
            checkboxClass: 'icheckbox_square-yellow',
            radioClass: 'iradio_square-yellow',
        });
     
        $('input[type=checkbox].icheck-blue, input[type=radio].icheck-blue').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
        });
  
        $('.add-box').on('click', async function () {
            var _id = Date.now();
            var _div = $(".div-template").clone();
                _div.removeClass('div-template');
                _div.find('fieldset').find('.form-group.row').not('.form-group.row.mater').remove();
                _div.find('.form-group.row.mater:eq(0)').find('button').removeClass('btn-success').removeClass('add-box').addClass('btn-danger delete-box').find('i').remove();
                _div.find('.form-group.row.mater:eq(0)').find('button').append($("<i/>").addClass('fas fa-times'));
                _div.find('.form-group.row.mater:eq(0)').find('button').attr({ 'data-id' : _id });
            
            var _box1 = _div.find('.col-sm-4:eq(0)');
            var _box2 = _div.find('.col-sm-4:eq(1)');
             
                _box1.find('select').remove();
                _box1.find('span').remove();
                _box2.find('select').remove();
                _box2.find('span').remove();

                _box1.append($("<select/>").attr({'name' : 'meeting_select_issues[' + _id + '][organization][]', 'autocomplete' : 'off', 'required' : true}).addClass('form-control'));
                _box2.find(".input-group").append($("<select/>").attr({'name' : 'meeting_select_issues[' + _id + '][name_option][]', 'autocomplete' : 'off', 'required' : true}).addClass('form-control'));
                _box2.find(".input-group").append(_box2.find(".input-group-append"));


            var meeting_id = _div.find("select[name*='meeting_issues']").data().meeting_id;
            var _box_meeting_issues = _div.find("select[name*='meeting_issues']").parent();
                _box_meeting_issues.find('select').remove();
                _box_meeting_issues.find('span').remove();
                _box_meeting_issues.append($("<select/>").attr({'name' : 'meeting_issues[' + _id + '][id]', 'autocomplete' : 'off', 'required' : true, 'disabled' : true, 'data-meeting_id' : meeting_id}).addClass('form-control'));
                _box_meeting_issues.append(_box_meeting_issues.find('.input-group-append'));

            _box_meeting_issues.find("select[name*='meeting_issues']").select2({
                data : await meeting_issues_selects(meeting_id),
                placeholder: {
                    id: '-1',
                    text: 'กรุณาเลือกข้อมูล'
                },
                width: 'auto',
                dropdownAutoWidth: true,
                theme: 'bootstrap4',
            });

            _box_meeting_issues.find("select[name*='meeting_issues']").prop("disabled", false);

            if(!_box_meeting_issues.find("select[name*='meeting_issues']").data().edit) {
                _box_meeting_issues.find("select[name*='meeting_issues']").val('-1').trigger('change');
            }

            _div.find("select[name*='meeting_issues']").parent().find('input-group-append');

            ( async () => {
                _div.find("select[name*='organization']").select2({
                    data : await selects_organization_api(),
                    ajax: {
                        url: "{{ route('pages.organizations.selectsapi') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type : "POST",
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                            }

                            return query;
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        }
                    },
                    templateSelection: function(data) {
                        return data.text;
                    },
                    placeholder: {
                        id: '-1',
                        text: 'กรุณาเลือกข้อมูล'
                    },
                    width: '100%',
                    theme: 'bootstrap4',
                });

                _div.find("select[name*='organization']").prop("disabled", false);

                if(!_div.find("select[name*='organization']").data().edit) {
                    _div.find("select[name*='organization']").val('-1').trigger('change');
                }
            })();

            ( async () => {
                _div.find("select[name*='name_option']").select2({
                    data : await selectsapi(),
                    ajax: {
                        url: "{{ route('pages.user.selectsapi') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type : "POST",
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                            }

                            return query;
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        }
                    },
                    templateSelection: function(data) {
                        return data.text;
                    },
                    placeholder: {
                        id: '-1',
                        text: 'กรุณาเลือกข้อมูล'
                    },
                    width: 'auto',
                    dropdownAutoWidth: true,
                    theme: 'bootstrap4',
                });

                _div.find("select[name*='name_option']").prop("disabled", false);

                if(!_div.find("select[name*='name_option']").data().edit) {
                    _div.find("select[name*='name_option']").val('-1').trigger('change');
                }
            })();
           

            $(".card-body").append(_div);
        });
  
        $(document).on('click', '.add-topic', function () {
            var _id_topic = $(this).data().id;
            var _div = $(this).parent().parent().parent().parent().clone();
                _div.find('label').remove();
                _div.removeClass('mater');
                _div.find('.add-topic').removeClass('btn-success add-topic').addClass('btn-danger delete-topic').find('i').removeClass('fas fa-user-plus').addClass('fas fa-user-minus');

            var _box1 = _div.find('.col-sm-4:eq(0)').addClass('offset-sm-3');
            var _box2 = _div.find('.col-sm-4:eq(1)');
                _box1.find('select').remove();
                _box1.find('span').remove();
                _box2.find('select').remove();
                _box2.find('span').remove();

                _box1.append($("<select/>").attr({'name' : 'meeting_select_issues[' + _id_topic + '][organization][]', 'autocomplete' : 'off', 'required' : true}).addClass('form-control'));
                _box2.find(".input-group").append($("<select/>").attr({'name' : 'meeting_select_issues[' + _id_topic + '][name_option][]', 'autocomplete' : 'off', 'required' : true}).addClass('form-control'));
                _box2.find(".input-group").append(_box2.find(".input-group-append"));

            ( async () => {
                _div.find("select[name*='organization']").select2({
                    data : await selects_organization_api(),
                    ajax: {
                        url: "{{ route('pages.organizations.selectsapi') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type : "POST",
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                            }

                            return query;
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        }
                    },
                    templateSelection: function(data) {
                        return data.text;
                    },
                    placeholder: {
                        id: '-1',
                        text: 'กรุณาเลือกข้อมูล'
                    },
                    width: '100%',
                    theme: 'bootstrap4',
                });

                _div.find("select[name*='organization']").prop("disabled", false);

                if(!_div.find("select[name*='organization']").data().edit) {
                    _div.find("select[name*='organization']").val('-1').trigger('change');
                }
            })();

            ( async () => {
                _div.find("select[name*='name_option']").select2({
                    data : await selectsapi(),
                    ajax: {
                        url: "{{ route('pages.user.selectsapi') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type : "POST",
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                            }

                            return query;
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        }
                    },
                    templateSelection: function(data) {
                        return data.text;
                    },
                    placeholder: {
                        id: '-1',
                        text: 'กรุณาเลือกข้อมูล'
                    },
                    width: 'auto',
                    dropdownAutoWidth: true,
                    theme: 'bootstrap4',
                });

                _div.find("select[name*='name_option']").prop("disabled", false);

                if(!_div.find("select[name*='name_option']").data().edit) {
                    _div.find("select[name*='name_option']").val('-1').trigger('change');
                }
            })();
           
            $(this).parent().parent().parent().parent().parent().append(_div);
            
        });

        $(document).on('click', '.delete-topic', function () {
            $(this).parent().parent().parent().parent().remove();
        });

        $(document).on('click', '.delete-box', function () {
            $(this).parents('fieldset').parent().parent().remove();
        });

        $("#form-submit").on('submit', function (e) {

            e.preventDefault();

            var self = $(this);
            var _btn = self.find("button[type='submit']");

            var _span = $('<span/>').addClass('spinner-border spinner-border-sm mb-2').attr({'role' : 'status', 'aria-hidden' : true});
            var _span_txt = $('<span/>').append('&nbsp;Loading...');

            var _i_default = $('<i/>').addClass('far fa-save');

            _btn.attr('disabled', false);
            
            _btn.attr('disabled', true);
            _btn.find('i').remove();
            _btn.text('');
            _btn.append(_span);
            _btn.append(_span_txt);

            var method 		= $(this).attr('method');
            var url 		= $(this).attr('action');
            var data 		= new FormData(this);

            $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData: false,
                success		: function(data) {

                    var element = null;
                    if(data.errors) {

                        $(".back").click();

                        $.each(data.errors, function(key, value) {

                            if(data.rules[key]) {
                                delete data.rules[key];
                            }

                            $(".validate-" + key).show();
                            $(".validate-" + key + "-i").text(value);
                        });

                        element = Object.keys(data.errors)[0];
                    }

                    if(data.rules) {
                        $.each(data.rules, function(key, value){
                            $(".validate-" + key).hide();
                        });
                    }

                    if(element) {
                        $("*[name='" + element + "']").focus();
                        return;
                    }

                    if(data.status) {

                        $('.vld').hide();

                    }

                    Swal.fire({
                        position: 'center',
                        icon: data.icon,
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                       if(result.isDismissed && data.redirect_url != '') {
                            window.location.href = data.redirect_url;
                       }
                    });

                    _btn.attr('disabled', false);
                    _btn.find('span').remove();
                    _btn.append(_i_default);
                    _btn.append('&nbsp;บันทึก');

                    

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {
                        $('#login_modal').modal('show');
                    },
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {
                        alert("500");

                        _btn.attr('disabled', false);
                        _btn.find('span').remove();
                        _btn.append(_i_default);
                        _btn.append('&nbsp;บันทึก');
                    }			
                }
            });
        });
    });

    function selects_meeting_api(val = '') {
        var method 		= "POST";
        var url 		= "{{ route('pages.meeting.selectsapi') }}";
        var data 		= new FormData();
            data.append('selected_id', val);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }

    function selects_organization_api(val = '') {
        var method 		= "POST";
        var url 		= "{{ route('pages.organizations.selectsapi') }}";
        var data 		= new FormData();
            data.append('selected_id', val);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }

    function selectsapi(val = '') {
        var method 		= "POST";
        var url 		= "{{ route('pages.user.selectsapi') }}";
        var data 		= new FormData();
            data.append('selected_id', val);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }

    function meeting_issues_selects(meeting_id, selected_id = '') {
        var method 		= "POST";
        var url 		= "{{ route('pages.meetingpermissions.selects') }}";
        var data 		= new FormData();
            data.append('meeting_id', meeting_id);
            data.append('selected_id', selected_id);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }
</script>
@endsection