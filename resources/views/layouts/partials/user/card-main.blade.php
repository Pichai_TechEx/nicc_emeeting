<div class="container-fluid">
    @if(Request::is('user/create'))
    {{ Form::open(['route' => 'pages.user.store', 'method' => 'POST', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif

    @if(Request::is('user/*/edit'))
    {{ Form::open(['route' => ['pages.user.update', 'id' => ( !empty($result['data']->id) ? $result['data']->id : '' )], 'method' => 'PATCH', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif
    {{ Form::hidden('lang', 'th') }}
    {{ Form::hidden('cerrent_tab', '1') }}
    {{ Form::hidden('is_enabled', 1) }}
    <div class="row">
        <div class="col-sm-4 col-md-4 col-xl-2">
            <div class="card">
                <div class="card-header">
                    ตั้งค่าผู้ใช้งาน
                </div>
                <div class="nav flex-column nav-pills p-1" role="tablist" aria-orientation="vertical">
                    <a class="nav-link mt-1 active" href="#config-profile" data-toggle="pill" role="tab" aria-controls="config-profile" aria-selected="true" data-tab="1">
                        ข้อมูลส่วนตัว
                    </a>
                    <a class="nav-link mt-1" href="#config-account" data-toggle="pill" role="tab" aria-controls="config-account" aria-selected="false" data-tab="2">
                        ชื่อผู้ใช้งานและรหัสผ่าน
                    </a>
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-lg-8">
            <div class="tab-content">
                <div class="tab-pane fade show active" id="config-profile" role="tabpanel" aria-labelledby="config-profile-tab">
                    <div class="card">
                        <div class="card-header">
                            ข้อมูลส่วนตัว
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                {{ Form::label('name', 'ประเภทผู้ใช้งาน :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                <div class="col-sm-8">
                                    {{ Form::select('role', $result['role'], old('role') ? old('role') : ( !empty($result['data']->role) ? $result['data']->role : null ), ['class' => 'form-control '.($errors->has('role') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาเลือก', 'autocomplete' => "off", 'required' => true]) }}
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('title_id', 'คำนำหน้า :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                <div class="col-sm-2">
                                    {{ Form::select('title_id', $result['title'], old('title_id') ? old('title_id') : ( !empty($result['data']->title_id) ? $result['data']->title_id : null ), ['class' => 'form-control '.($errors->has('title_id') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาเลือก', 'autocomplete' => "off", 'required' => true]) }}
                                </div>
                                {{ Form::label('first_name', 'ชื่อ :', ['class' => 'col-sm-2 col-form-label text-right']) }}
                                <div class="col-sm-2">
                                    {{ Form::text('first_name', old('first_name') ? old('first_name') : ( !empty($result['data']->first_name) ? $result['data']->first_name : '' ), ['class' => 'form-control '.($errors->has('first_name') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุชื่อ', 'autocomplete' => "off"]) }}
                                </div>
                                <div class="col-sm-2">
                                    {{ Form::text('last_name', old('last_name') ? old('last_name') : ( !empty($result['data']->last_name) ? $result['data']->last_name : '' ), ['class' => 'form-control '.($errors->has('last_name') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุนามสกุล', 'autocomplete' => "off"]) }}
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('organization_id', 'หน่วยงาน :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                <div class="col-sm-2">
                                    {{ Form::select('organization_id', $result['org'], old('organization_id') ? old('organization_id') : ( !empty($result['data']->organization_id) ? $result['data']->organization_id : null ), ['class' => 'form-control '.($errors->has('organization_id') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาเลือก', 'autocomplete' => "off", 'required' => true]) }}
                                </div>
                                {{ Form::label('position', 'ตำแหน่ง :', ['class' => 'col-sm-2 col-form-label text-right']) }}
                                <div class="col-sm-4">
                                    {{ Form::text('position', old('position') ? old('position') : ( !empty($result['data']->position) ? $result['data']->position : '' ), ['class' => 'form-control '.($errors->has('position') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุชื่อ', 'autocomplete' => "off"]) }}
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('email', 'อีเมล :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                <div class="col-sm-2">
                                    {{ Form::text('email', old('email') ? old('email') : ( !empty($result['data']->email) ? $result['data']->email : '' ), ['class' => 'form-control '.($errors->has('email') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุชื่อ', 'autocomplete' => "off"]) }}
                                </div>
                                {{ Form::label('phone', 'เบอร์ติดต่อ :', ['class' => 'col-sm-2 col-form-label text-right']) }}
                                <div class="col-sm-4">
                                    {{ Form::text('phone', old('phone') ? old('phone') : ( !empty($result['data']->phone) ? $result['data']->phone : '' ), ['class' => 'form-control '.($errors->has('phone') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุชื่อ', 'autocomplete' => "off"]) }}
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex justify-content-between">
                                <a role="button" class="btn btn-dark" href="{{ route('pages.user.index') }}">ยกเลิก</a>
                                <button type="submit" class="btn btn-success"><i class="far fa-save"></i> บันทึก</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="config-account" role="tabpanel" aria-labelledby="config-account-tab">
                    <div class="card">
                        <div class="card-header">
                            ชื่อผู้ใช้งานและรหัสผ่าน
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                {{ Form::label('note', 'Username :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                <div class="col-sm-8">
                                    {{ Form::text('username', old('username') ? old('username') : ( !empty($result['data']->username) ? $result['data']->username : '' ), ['class' => 'form-control '.($errors->has('username') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุชื่อ', 'autocomplete' => "off"]) }}
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('note', 'Password :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                <div class="col-sm-8">
                                    {{ Form::password('password', ['class' => 'form-control '.($errors->has('password') ? 'is-invalid' : ''), 'placeholder' => '********', 'autocomplete' => "off"]) }}
                                </div>
                            </div>
                            <div class="form-group row">
                                {{ Form::label('note', 'Confirm Password :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                <div class="col-sm-8">
                                    {{ Form::password('confirmed', ['class' => 'form-control '.($errors->has('confirmed') ? 'is-invalid' : ''), 'placeholder' => '********', 'autocomplete' => "off"]) }}
                                </div>
                            </div>
                            <div class="form-group row mb-4">
                                <label for="is_login" class="col-sm-3 col-form-label text-right">เปิด/ปิด การเข้าสู่ระบบ (สำหรับคณะกรรมการ/ผู้เข้าร่วมประชุม) :</label>
                                <div class="col-sm-4">
                                    {{ Form::hidden('is_login', 0) }}
                                    {{ Form::checkbox('is_login', 1, old('is_login') == 1 ? true : ( !empty($result['data']->is_login) && $result['data']->is_login == 1 ? true : '' ), ['data-toggle' => 'toggle', 'data-on' => 'เปิด', 'data-off' => 'ปิด', 'data-onstyle' => 'success', 'data-offstyle' => 'danger', 'data-size' => 'sm']) }}
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex justify-content-between">
                                <a role="button" class="btn btn-dark" href="{{ route('pages.user.index') }}">ยกเลิก</a>
                                <button type="submit" class="btn btn-success"><i class="far fa-save"></i> บันทึก</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@section('script')
<script type="text/javascript">

    String.prototype.hashCodeStringOnly = function () {
        var length              = 20;
        var result              = '';
        var characters          = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength    = characters.length;

        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

   $(document).ready( function () {

        $('.toggle-group').find('label').removeAttr('for');

        $('input[type=checkbox].icheck-green, input[type=radio].icheck-green').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
     
        $('input[type=checkbox].icheck-yellow, input[type=radio].icheck-yellow').iCheck({
            checkboxClass: 'icheckbox_square-yellow',
            radioClass: 'iradio_square-yellow',
        });
     
        $('input[type=checkbox].icheck-blue, input[type=radio].icheck-blue').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
        });

        $('.btn-proposer-add').on('click', function () {
            var proposer = $('.proposer').clone();
            proposer.removeClass('proposer');
            proposer.addClass('parent-proposer');
            proposer.find('label').remove();
            proposer.find('div:eq(0)').addClass('offset-sm-2');
            proposer.find('input').val('');
            proposer.find('button').removeClass('btn-success');
            proposer.find('button i').removeClass('fa-user-plus');
            proposer.find('button').removeClass('btn-proposer-add');
            proposer.find('button').addClass('btn-danger btn-proposer-delete');
            proposer.find('button i').addClass('fa-user-minus');

            $('.end-user').before(proposer);
        });
  
        $('.list-term-btn').on('click', function () {
            create_term();
        });


        $("#form-submit").on('submit', function (e) {

            e.preventDefault();

            var self = $(this);
            var _btn = self.find("button[type='submit']");

            var _span = $('<span/>').addClass('spinner-border spinner-border-sm mb-2').attr({'role' : 'status', 'aria-hidden' : true});
            var _span_txt = $('<span/>').append('&nbsp;Loading...');

            var _i_default = $('<i/>').addClass('far fa-save');

            _btn.attr('disabled', false);
            
            _btn.attr('disabled', true);
            _btn.find('i').remove();
            _btn.text('');
            _btn.append(_span);
            _btn.append(_span_txt);

            var method 		= $(this).attr('method');
            var url 		= $(this).attr('action');
            var data 		= new FormData(this);

            $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData: false,
                success		: function(data) {

                    var element = null;
                    if(data.errors) {

                        $(".back").click();

                        $.each(data.errors, function(key, value) {

                            if(data.rules[key]) {
                                delete data.rules[key];
                            }

                            $(".validate-" + key).show();
                            $(".validate-" + key + "-i").text(value);
                        });

                        element = Object.keys(data.errors)[0];
                    }

                    if(data.rules) {
                        $.each(data.rules, function(key, value){
                            $(".validate-" + key).hide();
                        });
                    }

                    if(element) {
                        $("*[name='" + element + "']").focus();
                        return;
                    }

                    if(data.status) {

                        $('.vld').hide();

                    }

                    Swal.fire({
                        position: 'center',
                        icon: data.icon,
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                       if(result.isDismissed && data.redirect_url != '') {
                            window.location.href = data.redirect_url;
                       }
                    });

                    _btn.attr('disabled', false);
                    _btn.find('span').remove();
                    _btn.append(_i_default);
                    _btn.append('&nbsp;บันทึก');

                    

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {
                        $('#login_modal').modal('show');
                    },
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {
                        alert("500");

                        _btn.attr('disabled', false);
                        _btn.find('span').remove();
                        _btn.append(_i_default);
                        _btn.append('&nbsp;บันทึก');
                    }			
                }
            });
        });
    });
</script>
@endsection