<div class="container-fluid">
    {{ Form::open(['route' => ['pages.system.update'], 'method' => 'PATCH', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
        {{ Form::hidden('cerrent_tab', '1') }}
        {{ Form::hidden('file_id', ( !empty($result['data']->file_id) ? $result['data']->file_id : '' )) }}
        <div class="row">
            <div class="col-sm-4 col-md-4 col-xl-2">
                <div class="card">
                    <div class="card-header">
                        ตั้งค่าระบบ
                    </div>
                    <div class="nav flex-column nav-pills p-1" role="tablist" aria-orientation="vertical">
                        <a class="nav-link mt-1 active" href="#config-system" data-toggle="pill" role="tab" aria-controls="config-system" aria-selected="true" data-tab="1">
                            ทั่วไป
                        </a>
                        <a class="nav-link mt-1" href="#config-mail" data-toggle="pill" role="tab" aria-controls="config-mail" aria-selected="false" data-tab="2">
                            อีเมลที่ใช้ในการจัดส่งข้อมูล
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 col-lg-8">
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="config-system" role="tabpanel" aria-labelledby="config-system-tab">
                        <div class="card">
                            <div class="card-header">
                                ทั่วไป
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group row">
                                            {{ Form::label('system_name_th', 'ชื่อระบบ (ภาษาไทย) :', ['class' => 'col-sm-5 col-form-label text-right']) }}
                                            <div class="col-sm-7">
                                                {{ Form::text('system_name_th', old('system_name_th') ? old('system_name_th') : ( !empty($result['data']->system_name_th) ? $result['data']->system_name_th : '' ), ['class' => 'form-control '.($errors->has('system_name_th') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {{ Form::label('system_name_en', 'ชื่อระบบ (ภาษาอังกฤษ) :', ['class' => 'col-sm-5 col-form-label text-right']) }}
                                            <div class="col-sm-7">
                                                {{ Form::text('system_name_en', old('system_name_en') ? old('system_name_en') : ( !empty($result['data']->system_name_en) ? $result['data']->system_name_en : '' ), ['class' => 'form-control '.($errors->has('system_name_en') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {{ Form::label('system_short_name_th', 'ชื่อย่อระบบ (ภาษาไทย) :', ['class' => 'col-sm-5 col-form-label text-right']) }}
                                            <div class="col-sm-7">
                                                {{ Form::text('system_short_name_th', old('system_short_name_th') ? old('system_short_name_th') : ( !empty($result['data']->system_short_name_th) ? $result['data']->system_short_name_th : '' ), ['class' => 'form-control '.($errors->has('system_short_name_th') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {{ Form::label('system_short_name_en', 'ชื่อย่อระบบ (ภาษาอังกฤษ) :', ['class' => 'col-sm-5 col-form-label text-right']) }}
                                            <div class="col-sm-7">
                                                {{ Form::text('system_short_name_en', old('system_short_name_en') ? old('system_short_name_en') : ( !empty($result['data']->system_short_name_en) ? $result['data']->system_short_name_en : '' ), ['class' => 'form-control '.($errors->has('system_short_name_en') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="card mx-auto" style="width: 275px;">
                                            <img src="{{  ( !empty($result['data']->path) ? $result['data']->path : asset('images/no-image-found.jpg') ) }}" id="box-file-upload" class="rounded" alt="logo">
                                            <div class="card-body m-auto">
                                                <input type="file" id="btn-file-upload" name="fileToUpload" hidden/>
                                                <label for="btn-file-upload" class="btn-upload"><i class="fas fa-file-upload"></i> เลือก</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-success"><i class="far fa-save"></i> บันทึก</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="config-mail" role="tabpanel" aria-labelledby="config-mail-tab">
                        <div class="card">
                            <div class="card-header">
                                อีเมลที่ใช้ในการจัดส่งข้อมูล
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    {{ Form::label('smtp_host', 'SMTP Host :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                    <div class="col-sm-4">
                                        {{ Form::text('smtp_host', old('smtp_host') ? old('smtp_host') : ( !empty($result['data']->smtp_host) ? $result['data']->smtp_host : '' ), ['class' => 'form-control '.($errors->has('smtp_host') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('smtp_port', 'SMTP Port :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                    <div class="col-sm-4">
                                        {{ Form::text('smtp_port', old('smtp_port') ? old('smtp_port') : ( !empty($result['data']->smtp_port) ? $result['data']->smtp_port : '' ), ['class' => 'form-control '.($errors->has('smtp_port') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('mail_accout', 'Mail Accout :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                    <div class="col-sm-4">
                                        {{ Form::text('mail_accout', old('mail_accout') ? old('mail_accout') : ( !empty($result['data']->mail_accout) ? $result['data']->mail_accout : '' ), ['class' => 'form-control '.($errors->has('mail_accout') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    {{ Form::label('mail_password', 'Mail Password :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                                    <div class="col-sm-4">
                                        {{ Form::text('mail_password', old('mail_password') ? old('mail_password') : ( !empty($result['data']->mail_password) ? $result['data']->mail_password : '' ), ['class' => 'form-control '.($errors->has('mail_password') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label text-right">เปิด/ปิดการใช้งาน :</label>
                                    <div class="col-sm-4">
                                        {{ Form::hidden('mail_is_enabled', 0) }}
                                        {{ Form::checkbox('mail_is_enabled', 1, old('mail_is_enabled') == 1 ? true : ( !empty($result['data']->mail_is_enabled) && $result['data']->mail_is_enabled == 1 ? true : '' ), ['data-toggle' => 'toggle', 'data-on' => 'เปิด', 'data-off' => 'ปิด', 'data-onstyle' => 'success', 'data-offstyle' => 'danger', 'data-size' => 'sm']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="d-flex justify-content-end">
                                    <button type="submit" class="btn btn-success"><i class="far fa-save"></i> บันทึก</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {{ Form::close() }}
</div>
@section('script')
<script type="text/javascript">

    String.prototype.hashCodeStringOnly = function () {
        var length              = 20;
        var result              = '';
        var characters          = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength    = characters.length;

        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

   $(document).ready( function () {

        $('.toggle-group').find('label').removeAttr('for');
   

        $('input[type=checkbox].icheck-green, input[type=radio].icheck-green').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
     
        $('input[type=checkbox].icheck-yellow, input[type=radio].icheck-yellow').iCheck({
            checkboxClass: 'icheckbox_square-yellow',
            radioClass: 'iradio_square-yellow',
        });
     
        $('input[type=checkbox].icheck-blue, input[type=radio].icheck-blue').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
        });

        $('a[data-toggle="pill"]').on('shown.bs.tab', function (e) {
            $(document).find("input[name='cerrent_tab']").val($(e.target).data().tab);
        });

        $("#btn-file-upload").change(function() {
            var item = this;
            var fileToUpload = $(this).prop('files');
            if(fileToUpload.length > 0 ) {
                Object.keys(fileToUpload).forEach(file => {
                    var lastModified = fileToUpload[file].lastModified;
                    var lastModifiedDate = fileToUpload[file].lastModifiedDate;
                    var name = fileToUpload[file].name;
                    var size = fileToUpload[file].size;
                    var type = fileToUpload[file].type;
                    var fileReader = new FileReader();
                        fileReader.readAsDataURL(fileToUpload[file]);
                        fileReader.onload = function(fileLoadedEvent){
                            var img = new Image();
                                img.src = fileReader.result;
                            var path_img = (window.URL || window.webkitURL || window || {}).createObjectURL(fileToUpload[file]);
                            var MAX_WIDTH = 275;
                            var MAX_HEIGHT = 275;

                                img.onload = function() {
                                    var width = this.width;
                                    var height = this.height;
                                    var canvas = document.createElement("canvas");
                                    var ctx = canvas.getContext("2d");
                                        ctx.drawImage(img, 0, 0);

                                    var ctx = canvas.getContext("2d");

                                    if (width > height) {
                                        canvas.width    = MAX_WIDTH;
                                        canvas.height   = MAX_HEIGHT;
                                
                                        ctx.drawImage(img, 0, 0, MAX_WIDTH, MAX_HEIGHT);
                                    } else {
                                     
                                        canvas.width    = MAX_WIDTH;
                                        canvas.height   = MAX_HEIGHT;

                                        ctx.fillStyle = "000000";
                                        ctx.fillRect(0, 0, MAX_WIDTH, MAX_HEIGHT);
                                        ctx.drawImage(img, (MAX_WIDTH - (MAX_HEIGHT - (MAX_HEIGHT * 0.25)))/2, 0, (MAX_HEIGHT - (MAX_HEIGHT * 0.25)), MAX_HEIGHT);
                                    }

                                    var dataurl = canvas.toDataURL(type);
                                    fetch(dataurl).then(res => res.blob()).then(blob => {
                                        var blobUrl = (window.URL || window.webkitURL || window || {}).createObjectURL(blob);
                                        $(document).find("#box-file-upload").attr('src', blobUrl);
                                    });
                                };
                        };
                });
            }
        });

        $("#form-submit").on('submit', function (e) {

            e.preventDefault();

            var self = $(this);
            var _btn = self.find("button[type='submit']");

            var _span = $('<span/>').addClass('spinner-border spinner-border-sm mb-2').attr({'role' : 'status', 'aria-hidden' : true});
            var _span_txt = $('<span/>').append('&nbsp;Loading...');

            var _i_default = $('<i/>').addClass('far fa-save');

            _btn.attr('disabled', false);
            
            _btn.attr('disabled', true);
            _btn.find('i').remove();
            _btn.text('');
            _btn.append(_span);
            _btn.append(_span_txt);

            var method 		= $(this).attr('method');
            var url 		= $(this).attr('action');
            var data 		= new FormData(this);

            $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData: false,
                success		: function(data) {

                    var element = null;
                    if(data.errors) {

                        $(".back").click();

                        $.each(data.errors, function(key, value) {

                            if(data.rules[key]) {
                                delete data.rules[key];
                            }

                            $(".validate-" + key).show();
                            $(".validate-" + key + "-i").text(value);
                        });

                        element = Object.keys(data.errors)[0];
                    }

                    if(data.rules) {
                        $.each(data.rules, function(key, value){
                            $(".validate-" + key).hide();
                        });
                    }

                    if(element) {
                        $("*[name='" + element + "']").focus();
                        return;
                    }

                    if(data.status) {

                        $('.vld').hide();

                    }

                    Swal.fire({
                        position: 'center',
                        icon: data.icon,
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                       if(result.isDismissed && data.redirect_url != '') {
                            window.location.href = data.redirect_url;
                       }
                    });

                    _btn.attr('disabled', false);
                    _btn.find('span').remove();
                    _btn.append(_i_default);
                    _btn.append('&nbsp;บันทึก');

                    

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {
                        $('#login_modal').modal('show');
                    },
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {
                        alert("500");

                        _btn.attr('disabled', false);
                        _btn.find('span').remove();
                        _btn.append(_i_default);
                        _btn.append('&nbsp;บันทึก');
                    }			
                }
            });
        });
    });
</script>
@endsection