<div class="{{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step == 2 ? 'container-fluid' : 'container' ) : 'container' }} ">
    @if(Request::is('meeting/create'))
    {{ Form::open(['route' => 'pages.meeting.store', 'method' => 'POST', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif

    @if(Request::is('meeting/*/edit'))
    {{ Form::open(['route' => ['pages.meeting.update', 'id' => ( !empty($result['data']->id) ? $result['data']->id : '' )], 'method' => 'PATCH', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif
    {{ Form::hidden('current_tabs', !empty($result['data']->done_is_step) ? ($result['data']->done_is_step + 1) : 1) }}

    @php
        $step = !empty($result['data']->done_is_step) ? ($result['data']->done_is_step + 1) : 1;
        $step_text = "";
        switch ($step) {
            case 1:
                $step_text = "หน้าปกการประชุม";
                break;
            case 2:
                $step_text = "รายละเอียดวาระการประชุม";
                break;
            case 3:
                $step_text = "ส่งเชิญผู้เข้าร่วมประชุม";
                break;
            case 4:
                $step_text = "บันทึกการประชุม";
                break;
            case 5:
            case 6:
                $step_text = "จัดทำรายงานการประชุม";
                break;
        }
    @endphp
    <div class="card">
        <div class="card-header">
            <p class="font-weight-bold mb-0 card-header-main">{{ $step_text }}</p> 
        </div>
        <div class="card-body">
            <div class="tab-content">
                <div class="tab-pane fade {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step == 0 ? 'show active' : '' ) : 'show active' }}" id="tab-1">
                    @include('layouts.partials.meeting.card-tabs-1')
                </div>
                <div class="tab-pane fade {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step == 1 ? 'show active' : '' ) : '' }}" id="tab-2">
                    @include('layouts.partials.meeting.card-tabs-2')
                </div>
                <div class="tab-pane fade {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step == 2 ? 'show active' : '' ) : '' }}" id="tab-3">
                    @include('layouts.partials.meeting.card-tabs-3')
                </div>
                <div class="tab-pane fade {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step == 3 ? 'show active' : '' ) : '' }}" id="tab-4">
                    @include('layouts.partials.meeting.card-tabs-4')
                </div>
                <div class="tab-pane fade {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step >= 4 ? 'show active' : '' ) : '' }}" id="tab-5">
                    @include('layouts.partials.meeting.card-tabs-5')
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-between">
                <a role="button" class="btn btn-dark" href="{{ route('home') }}"><i class="far fa-arrow-alt-circle-left"></i> ย้อนกลับ</a>
                <button type="submit" class="btn btn-success" {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step == 5 ? 'disabled' : '' ) : '' }} ><i class="far fa-save"></i> บันทึก</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>

<div class="position-fixed listStep">
    <div class="card">
        <div class="card-header">
            <span class="d-inline-block">สิ่งที่ต้องดำเนินการ</span>
            <button type="button" data-toggle="collapse" class="btn btn-sm btn-list-step float-right" href="#listStep" role="button" aria-expanded="false" aria-controls="listStep"><i class="fas fa-list-ul"></i></button>
        </div>
        <ul class="list-group list-group-flush collapse show" id="listStep">
            <li class="list-group-item {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step >= 0 ? '' : '' ) : '' }}">
                <input type="radio" class="icheck-green" {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step >= 1 ? 'checked' : 'checked disabled' ) : 'checked disabled' }}> <a href="#tab-1" data-current_tabs="1" class="card-link {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step == 0 ? 'active' : '' ) : 'active' }}" data-toggle="tab" role="tab" aria-controls="contact" aria-selected="true">หน้าปกการประชุม</a>
            </li>
            <li class="list-group-item {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step >= 1 ? '' : 'disabled' ) : 'disabled' }}">
                <input type="radio" class="icheck-green" {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step >= 2 ? 'checked' : 'checked disabled' ) : 'checked disabled' }}> <a href="#tab-2" data-current_tabs="2" class="card-link {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step == 1 ? 'active' : '' ) : 'disabled' }}" data-toggle="tab" role="tab" aria-controls="contact" aria-selected="false">รายละเอียดวาระการประชุม</a>
            </li>
            <li class="list-group-item {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step >= 2 ? '' : 'disabled' ) : 'disabled' }}">
                <input type="radio" class="icheck-green" {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step >= 3 ? 'checked' : 'checked disabled' ) : 'checked disabled' }}> <a href="#tab-3" data-current_tabs="3" class="card-link {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step == 2 ? 'active' : '' ) : 'disabled' }}" data-toggle="tab" role="tab" aria-controls="contact" aria-selected="false">ส่งเชิญผู้เข้าร่วมประชุม</a>
            </li>
            <li class="list-group-item {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step >= 3 ? '' : 'disabled' ) : 'disabled' }}">
                <input type="radio" class="icheck-green" {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step >= 4 ? 'checked' : 'checked disabled' ) : 'checked disabled' }}> <a href="#tab-4" data-current_tabs="4" class="card-link {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step == 3 ? 'active' : '' ) : 'disabled' }}" data-toggle="tab" role="tab" aria-controls="contact" aria-selected="false">บันทึกการประชุม</a>
            </li>
            <li class="list-group-item {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step >= 4 ? '' : 'disabled' ) : 'disabled' }}">
                <input type="radio" class="icheck-green" {{ !empty($result['data']->done_is_step) ? ( (int)$result['data']->done_is_step == 5 ? 'checked' : 'checked disabled' ) : 'checked disabled' }}> <a href="#tab-5" data-current_tabs="5" class="card-link {{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step >= 4 ? 'active' : '' ) : 'disabled' }}" data-toggle="tab" role="tab" aria-controls="contact" aria-selected="false">จัดทำรายงานการประชุม</a>
            </li>
        </ul>
    </div>
</div>
@section('script')
<script type="text/javascript">

    var boards_id = '';

    String.prototype.hashCodeStringOnly = function () {
        var length              = 20;
        var result              = '';
        var characters          = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength    = characters.length;

        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

   $(document).ready( function () {

        var my_edit = JSON.parse('{!! !empty($result["data"]->meeting_issues) ? $result["data"]->meeting_issues : "[]" !!}');

        if(my_edit.length > 0) {
            $.each(my_edit, function (i, e) {
                create_term(e.title, e.detail, e.files, e.parent);
                create_save_term(e.meeting_issues_id, e.title, e.detail, e.meeting_record, e.files, e.parent, e.qr_code, e.vote_by, e.voter_sum);
            });
        }

        $("select[name='board_id']").on('change', function () {
            setBoardId($(this).val());
            dataTable_tab_3_1.ajax.reload();
        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
     
            if($(e.target).parent().find('input').prop('checked') && !$(e.target).parent().find('input').prop('disabled')){
                // $("button[type='submit']").prop('disabled', true);
            } else {
                $("button[type='submit']").prop('disabled', false);
            }

            var tabsss = $($(e.target).attr('href')).parents('main').find('div.container:eq(0)');
            var tabxxx = $($(e.target).attr('href')).parents('main').find('div.container-fluid:eq(0)');

            if($(e.target).attr('href') == "#tab-3") {
                tabsss.removeClass('container');
                tabsss.addClass('container-fluid');
            } else {
                if(tabxxx.hasClass('container-fluid')) {
                    tabxxx.removeClass('container-fluid');
                    tabxxx.addClass('container');
                }
            }

            // container-fluid
            $(".card-header-main").text($(e.target).text());
            $("input[type='hidden'][name='current_tabs']").val($(e.target).data().current_tabs);
        })

       $('#listStep').on('hidden.bs.collapse', function () {
            $(this).parent().find('.d-inline-block').removeClass('d-inline-block').hide();
            $(this).find('.list-group-item').hide();
            $(this).parent().css({
                'width' : '5rem',
                'overflow' : 'hidden', 
                '-webkit-transition' : 'width 0.5s ease-in-out', 
                '-moz-transition' : 'width 0.5s ease-in-out', 
                '-o-transition' : 'width 0.5s ease-in-out', 
                'transition' : 'width 0.5s ease-in-out'
            });
        });

        $('#listStep').on('shown.bs.collapse', function () {
        
            $(this).parent().css('width', '18rem');
            
            setTimeout(() => {
                $(this).find('.list-group-item').show();
                $(this).parent().find('span').addClass('d-inline-block');
            }, 300);
        });
        
        $.each($('.timepicker'), function (i, e) { 
            $(this).datetimepicker({
                format: 'HH:mm',
                icons : {
                    time: 'far fa-clock',
                    date: 'far fa-calendar-alt',
                    up: 'fas fa-chevron-up',
                    down: 'fas fa-chevron-down',
                    previous: 'fas fa-chevron-left',
                    next: 'fas fa-chevron-right',
                }
            });
        });

        gj.core.messages['th-th'] = {
            monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
            monthShortNames: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
            weekDaysMin: ["อา", "จ", "อ", "พ", "พฤ", "ศ", "ส"],
            weekDaysShort: ["อา", "จ", "อ", "พ", "พฤ", "ศ", "ส"],
            weekDays: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์"],
            ok: 'ตกลง',
            cancel: 'ยกเลิก',
            titleFormat: 'mmmm yyyy'
        };
 
        $.each($('.datepicker'), function (i, e) { 
            var date = new Date();
            $(this).datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                format: 'dd/mm/yyyy',
                locale: 'th-th',     
                value: $(this).val() ? $(this).val() : ( parseInt(date.getDate(), 10) < 10 ? '0' : '' ) + (date.getDate()).toString() + '/' + ( (parseInt(date.getMonth(), 10) + 1) < 10 ? '0' : '' ) + (parseInt(date.getMonth(), 10) + 1).toString() + '/' + (parseInt(date.getFullYear(), 10) + 543)
            });
        });

        $('input[type=checkbox].icheck-green, input[type=radio].icheck-green').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
     
        $('input[type=checkbox].icheck-yellow, input[type=radio].icheck-yellow').iCheck({
            checkboxClass: 'icheckbox_square-yellow',
            radioClass: 'iradio_square-yellow',
        });
     
        $('input[type=checkbox].icheck-blue, input[type=radio].icheck-blue').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
        });

        $('.btn-proposer-add').on('click', function () {
            var proposer = $('.proposer').clone();
            proposer.removeClass('proposer');
            proposer.addClass('parent-proposer');
            proposer.find('label').remove();
            proposer.find('div:eq(0)').addClass('offset-sm-2');
            proposer.find('input').val('');
            proposer.find('button').removeClass('btn-success');
            proposer.find('button i').removeClass('fa-user-plus');
            proposer.find('button').removeClass('btn-proposer-add');
            proposer.find('button').addClass('btn-danger btn-proposer-delete');
            proposer.find('button i').addClass('fa-user-minus');

            $('.end-meeting').before(proposer);
        });
  
        $('.list-term-btn').on('click', function () {
            create_term();
        });
  
        $(document).on('click', '.delete-li-btn', function () {
            var id = $(this).data().id;
            var level = $(this).data().level;
            var start = $('.list-term li').index($(this).parents('li'));
            var end = $('.list-term li').index($("[data-to='" + id + "']"));
            var _length = $('.list-term li').length;

            $.each($('.list-term li'), function (i, e) { 
                 if(i >= start && i <= end) {
                     $(this).remove();

                     if(i == end) {
                        $.each($('[data-term]'), function (index, element) {
                            no = index + 1;
                            $(this).data().term = no;

                            is_active = 0;

                            btn_2 = $(this).parent().find('a:eq(1)');
                            btn_3 = $(this).parent().find('a:eq(2)');
                            

                            if(btn_2.hasClass( 'disabled' )) {
                                is_active = 2;
                            }

                            if(btn_3.hasClass( 'disabled' )) {
                                is_active = 1;
                            }

                            $(this).parents('li').find('.title-agenda').text("วาระที่ " + no);

                            if($("[data-secondary='" + $(this).data().id + "']").length > 0) {
                                $.each($("[data-secondary='" + $(this).data().id + "']"), function (k, l) {
                                    switch (is_active) {
                                        case 1:
                                            $(this).find('.title-agenda').text(k + 1);
                                            break;
                                        case 2:
                                            $(this).find('.title-agenda').text("วาระที่ " + no + "." + (k + 1));

                                            if($("[data-secondary='" + $(l).find('.sub-topics').data().id + "']").length > 0) {
                                                $.each($("[data-secondary='" + $(l).find('.sub-topics').data().id + "']"), function (index_t, element_t) {
                                                    $(this).find('.title-agenda').text(index_t + 1);
                                                });
                                            }
                                            break;
                                    }
                                });
                            } else {
                                btn_2.removeAttr('tabindex, aria-disabled').removeClass( 'disabled d-none' );
                                btn_3.removeAttr('tabindex, aria-disabled').removeClass( 'disabled d-none' );
                            }
                        });
                     }
                 }
            });
        });

        $(document).on('click', '.sub-agenda, .sub-topics', function () {
            switch ($(this).hasClass('sub-agenda')) {
                case true:
                    create_sub(this);
                    break;
                default:
                    modal_propose_topics(this);
                    break;
            }
        });

        $("#form-submit").on('submit', function (e) {

            e.preventDefault();

            var self = $(this);
            var _btn = self.find("button[type='submit']");

            var _span = $('<span/>').addClass('spinner-border spinner-border-sm mb-2').attr({'role' : 'status', 'aria-hidden' : true});
            var _span_txt = $('<span/>').append('&nbsp;Loading...');

            var _i_default = $('<i/>').addClass('far fa-save');

            _btn.attr('disabled', false);
            
            _btn.attr('disabled', true);
            _btn.find('i').remove();
            _btn.text('');
            _btn.append(_span);
            _btn.append(_span_txt);

            var method 		= $(this).attr('method');
            var url 		= $(this).attr('action');
            var data 		= new FormData(this);

            if(window.editor.getData()) {
                data.append('editor', window.editor.getData());
            }

            $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData: false,
                success		: function(data) {

                    var element = null;
                    if(data.errors) {

                        $(".back").click();

                        $.each(data.errors, function(key, value) {

                            if(data.rules[key]) {
                                delete data.rules[key];
                            }

                            $(".validate-" + key).show();
                            $(".validate-" + key + "-i").text(value);
                        });

                        element = Object.keys(data.errors)[0];
                    }

                    if(data.rules) {
                        $.each(data.rules, function(key, value){
                            $(".validate-" + key).hide();
                        });
                    }

                    if(element) {
                        $("*[name='" + element + "']").focus();
                        return;
                    }

                    if(data.status) {

                        $('.vld').hide();

                    }

                    Swal.fire({
                        position: 'center',
                        icon: data.icon,
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                       if(result.isDismissed && data.redirect_url != '') {
                           if(data.action == 'store') {
                               window.location.href = data.redirect_url;
                           } else {
                                location.reload();
                           }
                       }
                    });

                    _btn.attr('disabled', false);
                    _btn.find('span').remove();
                    _btn.append(_i_default);
                    _btn.append('&nbsp;บันทึก');

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {
                        $('#login_modal').modal('show');
                    },
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {
                        alert("500");

                        _btn.attr('disabled', false);
                        _btn.find('span').remove();
                        _btn.append(_i_default);
                        _btn.append('&nbsp;บันทึก');
                    }			
                }
            });
        });

        $('.load-template-btn').on('click', function () {

            Swal.fire({
                text: "ต้องการโหลดรูปแบบใช่หรือไม่?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#38c172',
                cancelButtonColor: '#e3342f',
                confirmButtonText: 'ยืนยัน',
                cancelButtonText: 'ปิด'
            }).then((result) => {
                if (result.isConfirmed) {

                    var method 		= "POST";
                    var url 		= "{{ route('pages.template.calljson') }}";
                    var data 		= new FormData();
                        data.append('template_id', $("select[name='template_meeting_id'] option:selected").val());

                        $.ajax({
                            url			: url,
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            type		: method,
                            dataType	: "json",
                            data		: data,
                            mimeTypes	: "multipart/form-data",
                            contentType	: false,
                            cache		: false,
                            processData	: false,
                            success		: function(data) {
                                if(data.status) {

                                    $('.list-term li').remove();
                                    if(data.data.length > 0) {
                                        $.each(data.data, function (i, e) {
                                            create_term(e.title);
                                        });
                                    }
                                }
                            }, statusCode: {
                                400: function(data) {alert("400");},
                                401: function(data) {alert("401");},
                                403: function(data) {alert("403");},
                                404: function(data) {alert("404");},
                                500: function(data) {alert("500");}			
                            }        
                        });
                }
            });
        });

        var dataTable_tab_3_1 = $('#table-tab-3-1').DataTable({
            destroy     : true,
            serverSide  : false,
            paging      : false,
            info        : false,
            lengthChange: false,
            searching   : false,
            ordering    : false,
            autoWidth   : true,
            deferLoading: 0,
            createdRow: async function ( row, data, index ) {
                var tr = $(row);
                    tr.find("select[name*='status']").select2({
                        data : await status_selects((tr.find("select[name*='status']").data().edit ? tr.find("select[name*='status']").data().edit : '')),
                        placeholder: {
                            id: '-1',
                            text: 'กรุณาเลือกข้อมูล'
                        },
                        width: '100%',
                        theme: 'bootstrap4',
                    });

                tr.find("select[name*='status']").prop("disabled", false);

                if(!tr.find("select[name*='status']").data().edit) {
                    tr.find("select[name*='status']").val('-1').trigger('change');
                }

            },
            drawCallback: function () {
                $('.dataTables_paginate > .pagination').addClass('pagination-sm pagination-customer');
            },
            ajax: function(data, callback, settings) {
            
                $.ajaxSetup({
                    url: "{{ route('pages.boards.datatablesselect') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : "POST",
                    dataType: 'json',
                    error: function(data){
                        if(data.status == 401) {
                            $('#login_modal').modal('show');
                        }
                        console.log('error');
                    },
                    success: function(data) {

                        callback({
                            recordsTotal    : data.recordsTotal,
                            recordsFiltered : data.recordsFiltered,
                            data            : data.data
                        });
                    }
                });

                data.board_id = getBoardId();
                
                $.ajax({ 
                    data: data
                });
            },
            aoColumnDefs: [
            ],
            columns: [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null
            ]
        }).draw(false);
        
        dataTable_tab_3_1.draw();

        if(dataTable_tab_3_1.rows().count() == 0 && $("select[name='board_id'] option:selected").length > 0) {
            setBoardId($("select[name='board_id'] option:selected").val());
            dataTable_tab_3_1.ajax.reload();
        }

        var dataTable_tab_3_2 = $('#table-tab-3-2').DataTable({
            destroy     : true,
            serverSide  : false,
            paging      : false,
            info        : false,
            lengthChange: false,
            searching   : false,
            ordering    : false,
            autoWidth   : true,
            deferLoading: 0,
            createdRow: async function ( row, data, index ) {

                var tr = $(row);

                    tr.find("select[name*='status']").select2({
                        data : await status_selects((tr.find("select[name*='status']").data().edit ? tr.find("select[name*='status']").data().edit : '')),
                        placeholder: {
                            id: '-1',
                            text: 'กรุณาเลือกข้อมูล'
                        },
                        width: '100%',
                        theme: 'bootstrap4',
                    });

                    tr.find("select[name*='status']").prop("disabled", false);

                    if(!tr.find("select[name*='status']").data().edit) {
                        tr.find("select[name*='status']").val('-1').trigger('change');
                    }
                   
                    tr.find("select[name*='position_in_meeting']").prop("disabled", false);

                    if(!tr.find("select[name*='position_in_meeting']").data().edit) {
                        tr.find("select[name*='position_in_meeting']").val('-1').trigger('change');
                    }

                    if(tr.find("select[name*='name_option']").data().edit) {
                        tr.find("select[name*='name_option']").select2({
                            data : await selectsapi(tr.find("select[name*='name_option']").data().edit, '', '', true),
                            ajax: {
                                url: "{{ route('pages.user.selectsapi') }}",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type : "POST",
                                dataType: 'json',
                                data: function (params) {
                                    var query = {
                                        search: params.term,
                                        is_participant: 1,
                                    }
  
                                    return query;
                                },
                                processResults: function (data) {
                                    return {
                                        results: data
                                    };
                                }
                            },
                            templateSelection: function(data) {
                                tr.find("td:eq(1) span").text(data.org);
                                tr.find("td:eq(1) input[name*='organization']").not("td:eq(1) input[name*='organization_name']").val(data.organization_id);
                                tr.find("td:eq(1) input[name*='organization_name']").val(data.org);
                                tr.find("td:eq(2) span").text(data.position);
                                tr.find("td:eq(2) input").val(data.position);
                                tr.find("td:eq(3) input[name*='title']").not("td:eq(3) input[name*='title_name']").val(data.title_id);
                                tr.find("td:eq(3) input[name*='title_name']").val(data.title_name);
                                tr.find("td:eq(3) input[name*='first_name']").val(data.first_name);
                                tr.find("td:eq(3) input[name*='last_name']").val(data.last_name);
                                tr.find("td:eq(4) input[name*='position_in_meeting_name']").val(data.position_in_meeting_name);
                                tr.find("td:eq(5) span").text(data.email);
                                tr.find("td:eq(5) input").val(data.email);
                                tr.find("td:eq(6) span").text(data.tel);
                                tr.find("td:eq(6) input").val(data.tel);
                                tr.find("td:eq(7) input").val(data.other);

                                return data.text;
                            },
                            placeholder: {
                                id: '-1',
                                text: 'กรุณาเลือกข้อมูล'
                            },
                            width: '100%',
                            theme: 'bootstrap4',
                        });
                    } else {

                        tr.find("select[name*='name_option']").select2({
                            ajax: {
                                url: "{{ route('pages.user.selectsapi') }}",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type : "POST",
                                dataType: 'json',
                                data: function (params) {
                                    var query = {
                                        search: params.term,
                                        is_participant: 1,
                                    }

                                    return query;
                                },
                                processResults: function (data) {
                                    return {
                                        results: data
                                    };
                                }
                            },
                            templateSelection: function(data) {
                                tr.find("td:eq(1) span").text(data.org);
                                tr.find("td:eq(1) input[name*='organization']").not("td:eq(1) input[name*='organization_name']").val(data.organization_id);
                                tr.find("td:eq(1) input[name*='organization_name']").val(data.org);
                                tr.find("td:eq(2) span").text(data.position);
                                tr.find("td:eq(2) input").val(data.position);
                                tr.find("td:eq(3) input[name*='title']").not("td:eq(3) input[name*='title_name']").val(data.title_id);
                                tr.find("td:eq(3) input[name*='title_name']").val(data.title_name);
                                tr.find("td:eq(3) input[name*='first_name']").val(data.first_name);
                                tr.find("td:eq(3) input[name*='last_name']").val(data.last_name);
                                tr.find("td:eq(4) input[name*='position_in_meeting_name']").val(data.position_in_meeting_name);
                                tr.find("td:eq(5) span").text(data.email);
                                tr.find("td:eq(5) input").val(data.email);
                                tr.find("td:eq(6) span").text(data.tel);
                                tr.find("td:eq(6) input").val(data.tel);
                                tr.find("td:eq(7) input").val(data.other);

                                return data.text;
                            },
                            placeholder: {
                                id: '-1',
                                text: 'กรุณาเลือกข้อมูล'
                            },
                            width: '100%',
                            theme: 'bootstrap4',
                        });

                        tr.find("select[name*='name_option']").on('select2:select', async function (e) {

                            var val = e.params.data;

                            tr.find("select[name*='position_in_meeting']").select2({
                                data : await position_in_meeting_selects(val.position_in_meeting_id),
                                placeholder: {
                                    id: '-1',
                                    text: 'กรุณาเลือกข้อมูล'
                                },
                                width: '100%',
                                theme: 'bootstrap4',
                                templateSelection: function(data) {
                                    tr.find("td:eq(4) input[name*='position_in_meeting_name']").val(data.text);
                                    return data.text;
                                },
                            });
                        });
                    }

                    if(tr.find("select[name*='position_in_meeting']").data().edit) {
                        tr.find("select[name*='position_in_meeting']").select2({
                            data : await position_in_meeting_selects(tr.find("select[name*='position_in_meeting']").data().edit),
                            placeholder: {
                                id: '-1',
                                text: 'กรุณาเลือกข้อมูล'
                            },
                            width: '100%',
                            theme: 'bootstrap4',
                            templateSelection: function(data) {
                                tr.find("td:eq(4) input[name*='position_in_meeting_name']").val(data.text);
                                return data.text;
                            },
                        });
                    } else {

                        tr.find("select[name*='position_in_meeting']").select2({
                            data : await position_in_meeting_selects(),
                            placeholder: {
                                id: '-1',
                                text: 'กรุณาเลือกข้อมูล'
                            },
                            width: '100%',
                            theme: 'bootstrap4',
                            templateSelection: function(data) {
                                tr.find("td:eq(4) input[name*='position_in_meeting_name']").val(data.text);
                                return data.text;
                            },
                        });
                    }

                    tr.find("select[name*='position_in_meeting']").prop("disabled", false);

                    if(!tr.find("select[name*='position_in_meeting']").data().edit) {
                        tr.find("select[name*='position_in_meeting']").val('-1').trigger('change');
                    }

                    tr.find("select[name*='name_option']").prop("disabled", false);

                    if(!tr.find("select[name*='name_option']").data().edit) {
                        tr.find("select[name*='name_option']").val('-1').trigger('change');
                    }

                    tr.find("select[name*='status']").removeAttr('data-edit');
                    tr.find("select[name*='name_option']").removeAttr('data-edit');
                    tr.find("select[name*='position_in_meeting']").removeAttr('data-edit');
            },
            drawCallback: function () {
        
            },
            aoColumnDefs: [
                { targets: [ 0, 1, 2, 3, 4, 5, 6, 7, 8 ], orderable: false },
                { targets: [ 9 ], sClass: 'text-center all', orderable: false }
            ],
            columns: [
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
            ]
        });
        
        dataTable_tab_3_2.draw();

        var header = $(dataTable_tab_3_2.table().header());

        header.find('button').on('click', function () {

            var _option_dropdown = $('<div/>').addClass('dropdown');
            var _option_dropdown_button = $('<button/>').attr({'type' : 'button', 'data-toggle' : 'dropdown', 'aria-haspopup' : true, 'aria-expanded' : false}).addClass('btn btn-secondary option dropdown-toggle').text('ดำเนินการ');
            var _option_dropdown_menu = $('<div/>').addClass('dropdown-menu');
            var _option_dropdown_menu_a_email = $('<a/>').addClass('dropdown-item text-dark').append($('<i/>').addClass('far fa-envelope'));
                _option_dropdown_menu_a_email.append('&nbsp;ส่งอีเมล');
            var _option_dropdown_menu_a_delete = $('<a/>').addClass('dropdown-item text-danger datatable-delete').append($('<i/>').addClass('far fa-trash-alt'));
                _option_dropdown_menu_a_delete.append('&nbsp;ลบ');

                _option_dropdown_menu.append(_option_dropdown_menu_a_email);
                _option_dropdown_menu.append(_option_dropdown_menu_a_delete);

                _option_dropdown.append(_option_dropdown_button);
                _option_dropdown.append(_option_dropdown_menu);

            dataTable_tab_3_2.row.add( [
                '',
                $('<div>').append($('<span/>').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][organization][]' }).clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][organization_name][]' }).clone()).html(),
                $('<div>').append($('<span/>').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][position_name][]' }).clone()).html(),
                $('<div>').append($('<select/>').attr({ 'name' : 'attendance[old][name_option][]' }).addClass('form-control').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][title][]' }).clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][title_name][]' }).clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][first_name][]' }).clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][last_name][]' }).clone()).html(),
                $('<div>').append($('<select/>').attr({ 'name' : 'attendance[old][position_in_meeting][]' }).addClass('form-control').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][position_in_meeting_name][]' }).clone()).html(),
                $('<div>').append($('<span/>').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][email][]' }).clone()).html(),
                $('<div>').append($('<span/>').clone()).html() + $('<div>').append($('<input/>').attr({'type' : 'hidden', 'name' : 'attendance[old][phone][]' }).clone()).html(),
                $('<div>').append($('<input/>').attr({ 'name' : 'attendance[old][other][]', 'placeholder' : 'รายละเอียดอื่นๆ' }).addClass('form-control').clone()).html(),
                $('<div>').append($('<select/>').attr({ 'name' : 'attendance[old][status][]' }).addClass('form-control').clone()).html(),
                $('<div>').append(_option_dropdown.clone()).html(),
            ] ).draw( false );

            dataTable_tab_3_2.column(0).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            });

        });

        $(document).on('click', '.datatable-delete', function () {
            dataTable_tab_3_2.row($(this).parents('tr')).remove().draw();
        });

        var template = "";
            template += '<p style="text-align:center;">รายงานการประชุม</p>';
            template += '<p style="text-align:center;">ชื่อการประชุม {} ครั้งที่ {}</p>';
            template += '<p style="text-align:center;">วันที่ {date}</p>';
            
  
        DecoupledEditor
        .create( document.querySelector( '.document-editor__editable' ), {
     
        } )
        .then( editor => {
            const toolbarContainer = document.querySelector( '.document-editor__toolbar' );
            

            toolbarContainer.appendChild( editor.ui.view.toolbar.element );

            window.editor = editor;
            window.editor.setData('{!! !empty($result["data"]->editor) ? $result["data"]->editor : '' !!}');
        } )
        .catch( err => {
            console.error( err );
        } );

        for (var index = 0; index < parseInt("{{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step ) : 0 }}"); index++) {
            $.each($("#tab-"+ (index + 1).toString()).find("input[type='text'], input[type='radio'], textarea, select"), function (i, e) { 
                // $(this).prop({'readonly': true, 'disabled' : true});
            });

            if(parseInt("{{ !empty($result['data']->done_is_step) ? ( $result['data']->done_is_step ) : 0 }}") == 4 && index == 3) {
                beautifulformat();
            }
        }
        delete index;
    });

    // TABS 2 START
    function create_term(value = '', value_detail = '', files = [], parent = []) {
        var no_term = $('.term-main').length;
            no_term++;
     
            var hash_code = String.prototype.hashCodeStringOnly();
            var title = "วาระที่ " + no_term;
            var li = $('<li/>').addClass('list-group-item border-0');
            var li_break = $('<li/>').attr('data-to', hash_code).addClass('d-none term-main');
            var box = $('<div/>').addClass('row main-agenda border-0');
            var box_textarea = $('<div/>').addClass('row border-0 mt-3');
            var box_file = $('<div/>').addClass('row border-0 mt-3');
            var tag = $('<div/>').addClass('input-group-prepend');
            var tag_title = $('<span/>').addClass('input-group-text title-agenda');
    
            var detail = $('<div/>').addClass('col-sm-10 box-agenda');
            var detail_f = $('<div/>').addClass('col-sm-2 pr-0');
            var detail_textarea = $('<div/>').attr({'id' : hash_code}).addClass('col-sm-10 offset-sm-2 collapse' + (value_detail ? ' show ' : ''));
            var detail_file = $('<div/>').attr({'id' : 'file' + hash_code}).addClass('col-sm-10 offset-sm-2');

            var div_input_group = $('<div/>').addClass('input-group');
            
            var btn_1 = $('<label/>').attr('for', 'file_' + hash_code).addClass('dropdown-item');
                btn_1.append($('<i/>').addClass('fas fa-file-alt'));
                btn_1.append('&nbsp;แนบไฟล์');
            var btn_2 = $('<a/>').attr({'href' : 'JavaScript:void(0);', 'data-id' : hash_code, 'data-main_name' : 'meeting_issues[' + hash_code + '][parent][topic]', 'data-level' : 0}).addClass('dropdown-item sub-topics');
                btn_2.append($('<i/>').addClass('fas fa-plus'));
                btn_2.append('&nbsp;เพิ่มหัวข้อ');
            var btn_3 = $('<a/>').attr({'href' : 'JavaScript:void(0);', 'data-id' : hash_code, 'data-main_name' : 'meeting_issues[' + hash_code + '][parent][term]', 'data-level' : 0, 'data-term' : no_term}).addClass('dropdown-item sub-agenda');
                btn_3.append($('<i/>').addClass('fas fa-search-plus'));
                btn_3.append('&nbsp;เพิ่มวาระย่อย');
            var btn_4 = $('<a/>').attr({'href' : '#' + hash_code, 'role' : 'button', 'data-toggle' : 'collapse', 'aria-expanded' : false, 'aria-controls' : hash_code}).addClass('dropdown-item detail-btn');
                btn_4.append($('<i/>').addClass('fas fa-comment-dots'));
                btn_4.append('&nbsp;เพิ่มรายละเอียด');
            var btn_5 = $('<a/>').attr({'href' : 'JavaScript:void(0);', 'data-id' : hash_code, 'data-level' : 0}).addClass('dropdown-item delete-li-btn');
                btn_5.append($('<i/>').addClass('far fa-times-circle'));
                btn_5.append('&nbsp;ลบหัวข้อ');

            var input = $('<input/>').attr({'type' : 'text', 'name' : 'meeting_issues[' + hash_code + '][value]', 'placeholder' : 'กรุณาระบุวาระการประชุม'}).addClass('form-control').val(value);
            var input_level = $('<input/>').attr({'type' : 'hidden', 'name' : 'meeting_issues[' + hash_code + '][level]', 'value' : 0});

            var textarea_input_group = $('<div/>').addClass('input-group');
            var textarea = $('<textarea/>').attr({'type' : 'text', 'name' : 'meeting_issues[' + hash_code + '][detail]', 'placeholder' : 'กรุณาระบุรายละเอียด'}).addClass('form-control').val(value_detail);

            var file = $('<input/>').attr({'type' : 'file', 'name' : 'meeting_issues[' + hash_code + '][file][]', 'placeholder' : 'กรุณาระบุรายละเอียด', 'id' : 'file_' + hash_code, 'data-check' : 'meeting_issues[' + hash_code + '][file_id]', 'accept' : "application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/pdf"}).addClass('form-control');

            textarea_input_group.append(textarea);

            //
            var jFiler_item = $('<li/>').addClass('jFiler-item p-2');
            var jFiler_item_container = $('<div/>').addClass('jFiler-item-container');
            var jFiler_item_inner = $('<div/>').addClass('jFiler-item-inner');
            var jFiler_item_icon = $('<div/>').addClass('jFiler-item-icon float-left text-dark').text("{"+"{" + "fi-icon" + "}"+"}");
            var jFiler_item_info = $('<div/>').addClass('jFiler-item-info float-left');
            var jFiler_item_title = $('<div/>').addClass('jFiler-item-title').append($('<a/>').attr({'href' : "" ,'download': "{"+"{" + "fi-name" + "}"+"}"}).text("{"+"{" + "fi-name | limitTo:35" + "}"+"}"));
            var jFiler_item_others = $('<div/>').addClass('jFiler-item-others');
            var jFiler_item_status = $('<span/>').addClass('jFiler-item-status');

            var jFiler_item_assets = $('<div/>').addClass('jFiler-item-assets');
            var jFiler_ul = $('<ul/>').addClass('list-inline');
            var jFiler_li = $('<li/>');
            var jFiler_a = $('<a/>').addClass('icon-jfi-trash jFiler-item-trash-action');

            jFiler_li.append(jFiler_a);

            jFiler_ul.append(jFiler_li);

            jFiler_item_assets.append(jFiler_ul);

            jFiler_item_others.append($('<span/>').text("size : {"+"{" + "fi-size2" + "}"+"}"));
            jFiler_item_others.append($('<span/>').text("type : {"+"{" + "fi-extension" + "}"+"}"));
            jFiler_item_others.append(jFiler_item_status);

            jFiler_item_info.append(jFiler_item_title);
            jFiler_item_info.append(jFiler_item_others);
            jFiler_item_info.append(jFiler_item_assets);

            jFiler_item_inner.append(jFiler_item_icon);
            jFiler_item_inner.append(jFiler_item_info);

            jFiler_item_container.append(jFiler_item_inner);

            jFiler_item.append(jFiler_item_container);
            //

            var dropdown = $('<div/>').addClass('input-group-append dropleft');
            var dropdown_btn = $('<button/>').attr({'type' : 'button',  'data-toggle' : 'dropdown', 'aria-haspopup' : true, 'aria-expanded' : false }).addClass('btn btn-outline-secondary rounded-right');
            var dropdown_btn_i = $('<i/>').addClass('fas fa-ellipsis-v');
            var dropdown_menu = $('<div/>').addClass('dropdown-menu').attr({'aria-labelledby' : 'dropdown-option'});
           
            dropdown_menu.append(btn_1);
            dropdown_menu.append(btn_2);
            dropdown_menu.append(btn_3);
            dropdown_menu.append(btn_4);
            dropdown_menu.append(btn_5);

            dropdown_btn.append(dropdown_btn_i);

            dropdown.append(dropdown_btn);
            dropdown.append(dropdown_menu);

            div_input_group.append(input);
            div_input_group.append(input_level);
            div_input_group.append(dropdown);
      
            tag_title.append(title);

            box.append(detail_f.append(tag_title));
            box.append(detail);
            box_textarea.append(detail_textarea);
            box_file.append(detail_file);

            detail.append(div_input_group);

            detail_textarea.append(textarea_input_group);

            detail_file.append(file);

            li.append(box);
            li.append(box_textarea);
            li.append(box_file);

            if(files.length > 0) {
                $.each(files, function (i, e) {
                    box_file.append($('<input/>').attr({ 'type' : 'hidden', 'name' : 'meeting_issues[' + hash_code + '][file_id][]', 'value' : e.id }));
                });
            }
    
            file.filer({
                limit: 5,
                maxSize: 10,
                extensions: ["pdf", "doc", "docx", "xls", "xlsx"],
                showThumbs: true,
                templates : {
                    item: $('<div>').append(jFiler_item.clone()).html(),
                    itemAppend: $('<div>').append(jFiler_item.clone()).html()
                },
                files : files,//1
                beforeRender : function (itemEl, inputEl) {
                       
                    inputEl.change(function() {
                        var fileToUpload = $(this).prop('files');
                        if(fileToUpload.length > 0 ) {
                            Object.keys(fileToUpload).forEach(file => {
                                var lastModified = fileToUpload[file].lastModified;
                                var lastModifiedDate = fileToUpload[file].lastModifiedDate;
                                var name = fileToUpload[file].name;
                                var size = fileToUpload[file].size;
                                var type = fileToUpload[file].type;
                                var fileReader = new FileReader();
                                    fileReader.readAsDataURL(fileToUpload[file]);
                                    fileReader.onload = function(fileLoadedEvent){
                                        var _a = itemEl.find('ul.jFiler-items-list li.jFiler-item').eq(file).find('.jFiler-item-title a');
                                        _a.attr({'href' : fileReader.result, 'download' : _a.text()});
                                    };
                            });
                        }
                    });
                },
                onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
                    if($("input[name^='" + inputEl.data().check + "'][value='" + file.id + "']").length > 0) {
                        $("input[name^='" + inputEl.data().check + "'][value='" + file.id + "']").remove();
                    }
                }
            });    

            file.parent().find('.jFiler-input').addClass('d-none');

            file.on('change',function() {
                if($("input[name^='" + $(this).data().check + "']").length > 0) {
                    $.each($("input[name^='" + $(this).data().check + "']"), function (i, e) { 
                        $(this).remove();
                    });
                }
            });

            if(files.length > 0) {
                $.each(files, function (i, e) {
                    var _a = box_file.find('ul.jFiler-items-list li.jFiler-item').eq(i).find('.jFiler-item-title a');
                    _a.attr({'href' : e.url, 'download' : e.name});
                });
            }

            $('.list-term').append(li);
            $('.list-term').append(li_break);

            if(parent.length > 0) {
                $.each(parent, function (i, e) { 
                    if(e.is_topic == "1") {
                        create_sub($(btn_2).get(0), e.title, e.detail, e.files, e.parent);
                    } else {
                        create_sub($(btn_3).get(0), e.title, e.detail, e.files, e.parent);
                    }
                });
            }
    }

    function create_sub(elment, value= '', value_detail= '', files = [], parent = []) {
      
        var is_topics = false;

        var title = "";
        
        var id = $(elment).data().id;
        var level = $(elment).data().level + 1;
        var term_main = $(elment).data().term;
        var main_name = $(elment).data().main_name;
        
        var term_secondary = $("[data-secondary='" + id + "']").length + 1;
    
        var hash_code = String.prototype.hashCodeStringOnly();

        switch ($(elment).hasClass('sub-agenda')) {
            case true:
                title = "วาระที่ " + term_main + "." + term_secondary;
                break;
            default:
                title = "ข้อ " + term_secondary;
                is_topics = true;
                break;
        }

        if(level == 1) {
            if(is_topics) {
                tag_link = $(elment).parent().find('a:eq(1)');
                tag_link.attr({'tabindex' : '-1', 'aria-disabled' : true}).addClass('d-none disabled');
            } else {
                tag_link = $(elment).parent().find('a:eq(0)');
                tag_link.attr({'tabindex' : '-1', 'aria-disabled' : true}).addClass('d-none disabled');
            }
        }

        var li = $('<li/>').attr('data-secondary', id).addClass('list-group-item border-0');
        var li_break = $('<li/>').attr('data-to', hash_code).addClass('d-none' + (is_topics ? ' topics' : ' term') );
        var box = $('<div/>').addClass('row main-agenda border-0');
        var box_textarea = $('<div/>').addClass('row border-0 mt-3');
        var box_file = $('<div/>').addClass('row border-0 mt-3');
        var tag = $('<div/>').addClass('input-group-prepend');
        var tag_title = $('<span/>').addClass('input-group-text title-agenda');
    
        var detail = $('<div/>').addClass('col-sm-10 box-agenda');
        var detail_f = $('<div/>').addClass('col-sm-2 pr-0');
        var detail_textarea = $('<div/>').attr({'id' : hash_code}).addClass('col-sm-10 offset-sm-2 collapse' + (value_detail ? ' show ' : ''));
        var detail_file = $('<div/>').attr({'id' : 'file' + hash_code}).addClass('col-sm-10 offset-sm-2');

        var div_input_group = $('<div/>').addClass('input-group');

        var btn_1 = $('<label/>').attr('for', 'file_' + hash_code).addClass('dropdown-item');
            btn_1.append($('<i/>').addClass('fas fa-file-alt'));
            btn_1.append('&nbsp;แนบไฟล์');
        var btn_2 = $('<a/>').attr({'href' : 'JavaScript:void(0);', 'data-id' : hash_code, 'data-main_name' : main_name + '[' + hash_code + '][parent][topic]', 'data-level' : level, 'tabindex' : (level >= 2 || is_topics ? '-1' : '' ), 'aria-disabled' : (level >= 2 || is_topics ? true : false ) }).addClass('dropdown-item sub-topics' + (level >= 2 || is_topics ? ' disabled d-none' : '' ));
            btn_2.append($('<i/>').addClass('fas fa-plus'));
            btn_2.append('&nbsp;เพิ่มหัวข้อ');
        var btn_3 = $('<a/>').attr({'href' : 'JavaScript:void(0);', 'data-id' : hash_code, 'data-main_name' : main_name + '[' + hash_code + '][parent][term]', 'data-level' : level, 'tabindex' : (level >= 1 ? '-1' : '' ), 'aria-disabled' : (level >= 1 ? true : false ) }).addClass('dropdown-item sub-agenda' + (level >= 1 ? ' disabled d-none' : '' ));
            btn_3.append($('<i/>').addClass('fas fa-search-plus'));
            btn_3.append('&nbsp;เพิ่มวาระย่อย');
        var btn_4 = $('<a/>').attr({'href' : '#' + hash_code, 'role' : 'button', 'data-toggle' : 'collapse', 'aria-expanded' : false, 'aria-controls' : hash_code}).addClass('dropdown-item detail-btn');
            btn_4.append($('<i/>').addClass('fas fa-comment-dots'));
            btn_4.append('&nbsp;เพิ่มรายละเอียด');
        var btn_5 = $('<a/>').attr({'href' : 'JavaScript:void(0);', 'data-id' : hash_code, 'data-level' : level}).addClass('dropdown-item delete-li-btn');
            btn_5.append($('<i/>').addClass('far fa-times-circle'));
            btn_5.append('&nbsp;ลบหัวข้อ');

        var input = $('<input/>').attr({'type' : 'text', 'name' : main_name + '[' + hash_code + '][value]', 'placeholder' : 'กรุณาระบุวาระการประชุม'}).addClass('form-control').val(value);
        var input_level = $('<input/>').attr({'type' : 'hidden', 'name' : main_name + '[' + hash_code + '][level]', 'value' : level});

        var textarea_input_group = $('<div/>').addClass('input-group');
     
        var textarea = $('<textarea/>').attr({'name' : main_name + '[' + hash_code + '][detail]', 'placeholder' : 'กรุณาระบุรายละเอียด'}).addClass('form-control').val(value_detail);

        var file = $('<input/>').attr({'type' : 'file', 'name' : main_name + '[' + hash_code + '][file][]', 'placeholder' : 'กรุณาระบุรายละเอียด', 'id' : 'file_' + hash_code, 'data-check' : main_name + '[' + hash_code + '][file_id]', 'accept' : "application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/pdf"}).addClass('form-control');

        textarea_input_group.append(textarea);

        //
        var jFiler_item = $('<li/>').addClass('jFiler-item p-2');
        var jFiler_item_container = $('<div/>').addClass('jFiler-item-container');
        var jFiler_item_inner = $('<div/>').addClass('jFiler-item-inner');
        var jFiler_item_icon = $('<div/>').addClass('jFiler-item-icon float-left text-dark').text("{"+"{" + "fi-icon" + "}"+"}");
        var jFiler_item_info = $('<div/>').addClass('jFiler-item-info float-left');
        var jFiler_item_title = $('<div/>').addClass('jFiler-item-title').append($('<a/>').attr({'href' : "" ,'download': "{"+"{" + "fi-name" + "}"+"}"}).text("{"+"{" + "fi-name | limitTo:35" + "}"+"}"));
        var jFiler_item_others = $('<div/>').addClass('jFiler-item-others');
        var jFiler_item_status = $('<span/>').addClass('jFiler-item-status');

        var jFiler_item_assets = $('<div/>').addClass('jFiler-item-assets');
        var jFiler_ul = $('<ul/>').addClass('list-inline');
        var jFiler_li = $('<li/>');
        var jFiler_a = $('<a/>').addClass('icon-jfi-trash jFiler-item-trash-action');

        jFiler_li.append(jFiler_a);

        jFiler_ul.append(jFiler_li);

        jFiler_item_assets.append(jFiler_ul);

        jFiler_item_others.append($('<span/>').text("size : {"+"{" + "fi-size2" + "}"+"}"));
        jFiler_item_others.append($('<span/>').text("type : {"+"{" + "fi-extension" + "}"+"}"));
        jFiler_item_others.append(jFiler_item_status);

        jFiler_item_info.append(jFiler_item_title);
        jFiler_item_info.append(jFiler_item_others);
        jFiler_item_info.append(jFiler_item_assets);

        jFiler_item_inner.append(jFiler_item_icon);
        jFiler_item_inner.append(jFiler_item_info);

        jFiler_item_container.append(jFiler_item_inner);

        jFiler_item.append(jFiler_item_container);
        //

        var dropdown = $('<div/>').addClass('input-group-append dropleft');
        var dropdown_btn = $('<button/>').attr({'type' : 'button', 'data-toggle' : 'dropdown', 'aria-haspopup' : true, 'aria-expanded' : false }).addClass('btn btn-outline-secondary rounded-right');
        var dropdown_btn_i = $('<i/>').addClass('fas fa-ellipsis-v');
        var dropdown_menu = $('<div/>').addClass('dropdown-menu').attr({'aria-labelledby' : 'dropdown-option'});
        
        dropdown_menu.append(btn_1);
        dropdown_menu.append(btn_2);
        dropdown_menu.append(btn_3);
        dropdown_menu.append(btn_4);
        dropdown_menu.append(btn_5);

        dropdown_btn.append(dropdown_btn_i);

        dropdown.append(dropdown_btn);
        dropdown.append(dropdown_menu);

        // div_input_group.append(tag);
        div_input_group.append(input);
        div_input_group.append(input_level);
        div_input_group.append(dropdown);
    
        tag_title.append(title);
        // tag.append(tag_title);

        box.append(detail_f.append(tag_title));
        box.append(detail);
        box_textarea.append(detail_textarea);
        box_file.append(detail_file);

        detail.append(div_input_group);

        detail_textarea.append(textarea_input_group);

        detail_file.append(file);

        li.append(box);
        li.append(box_textarea);
        li.append(box_file);
      
        $.each(files, function (i, e) {
            box_file.append($('<input/>').attr({ 'type' : 'hidden', 'name' : main_name + '[' + hash_code + '][file_id][]', 'value' : e.id }));
        });
   
        file.filer({
            limit: 5,
            maxSize: 10,
            extensions: ["pdf", "doc", "docx", "xls", "xlsx"],
            showThumbs: true,
            templates : {
                item: $('<div>').append(jFiler_item.clone()).html(),
                itemAppend: $('<div>').append(jFiler_item.clone()).html()
            },
            files : files,//2
            beforeRender : function (itemEl, inputEl) {
                    
                inputEl.change(function() {
                    var fileToUpload = $(this).prop('files');
                    if(fileToUpload.length > 0 ) {
                        Object.keys(fileToUpload).forEach(file => {
                            var lastModified = fileToUpload[file].lastModified;
                            var lastModifiedDate = fileToUpload[file].lastModifiedDate;
                            var name = fileToUpload[file].name;
                            var size = fileToUpload[file].size;
                            var type = fileToUpload[file].type;
                            var fileReader = new FileReader();
                                fileReader.readAsDataURL(fileToUpload[file]);
                                fileReader.onload = function(fileLoadedEvent){
                                    var _a = itemEl.find('ul.jFiler-items-list li.jFiler-item').eq(file).find('.jFiler-item-title a');
                                    _a.attr({'href' : fileReader.result, 'download' : _a.text()});
                                };
                        });
                    }
                });
            },
            onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
                if($("input[name^='" + inputEl.data().check + "'][value='" + file.id + "']").length > 0) {
                    $("input[name^='" + inputEl.data().check + "'][value='" + file.id + "']").remove();
                }
            }
        });    

        file.parent().find('.jFiler-input').addClass('d-none');

        file.on('change',function() {
            if($("input[name^='" + $(this).data().check + "']").length > 0) {
                $.each($("input[name^='" + $(this).data().check + "']"), function (i, e) { 
                    $(this).remove();
                });
            }
        });

        if(files.length > 0) {
            $.each(files, function (i, e) {
                var _a = box_file.find('ul.jFiler-items-list li.jFiler-item').eq(i).find('.jFiler-item-title a');
                _a.attr({'href' : e.url, 'download' : e.name});
            });
        }

        var last_box_sub = null;
        var last_detail_sub = null;

        var last_textarea_box_sub = null;
        var last_textarea_detail_sub = null;

        var last_file_box_sub = null;
        var last_file_detail_sub = null;
    
        for (var index = 0; index < level; index++) {
            
            var box_sub = $('<div/>').addClass('row main-agenda border-0');
            var detail_sub = $('<div/>').addClass('offset-sm-2 col-sm-10 box-agenda');
            
            var textarea_box_sub = $('<div/>').addClass('row main-agenda border-0');
            var textarea_detail_sub = $('<div/>').addClass('offset-sm-2 col-sm-10 box-agenda');
            
            var file_box_sub = $('<div/>').addClass('row main-agenda border-0');
            var file_detail_sub = $('<div/>').addClass('offset-sm-2 col-sm-10 box-agenda');

            box_sub.append(detail_sub);
            textarea_box_sub.append(textarea_detail_sub);
            file_box_sub.append(file_detail_sub);

            if(last_box_sub == null) {
                last_box_sub = box_sub;
                last_detail_sub = detail_sub;
                
                last_textarea_box_sub = textarea_box_sub;
                last_textarea_detail_sub = textarea_detail_sub;

                last_file_box_sub = file_box_sub;
                last_file_detail_sub = file_detail_sub;
            } else {
                box_sub.append(detail_sub);
                textarea_box_sub.append(textarea_detail_sub);
                file_box_sub.append(file_detail_sub);
            
                if(last_detail_sub.find('div:last').length> 0) {
                    last_detail_sub.find('div:last').append(box_sub);
                    last_textarea_detail_sub.find('div:last').append(textarea_box_sub);
                    last_file_detail_sub.find('div:last').append(file_box_sub);
                } else {

                    last_detail_sub.append(box_sub);
                    last_textarea_detail_sub.append(textarea_box_sub);
                    last_file_detail_sub.append(file_box_sub);
                }

                last_box_sub.append(last_detail_sub);
                last_textarea_box_sub.append(last_textarea_detail_sub);
                last_file_box_sub.append(last_file_detail_sub);
            }
    
            if(level == index + 1) {
                if((index + 1) > 1) {
                    detail_sub.append(box);
                    textarea_detail_sub.append(box_textarea);
                    file_detail_sub.append(box_file);

                    li.append(last_box_sub);
                    li.append(last_textarea_box_sub);
                    li.append(last_file_box_sub);
                    
                } else {

                    last_detail_sub.append(box);
                    last_box_sub.append(last_detail_sub);

                    last_textarea_detail_sub.append(box_textarea);
                    last_textarea_box_sub.append(last_textarea_detail_sub);

                    last_file_detail_sub.append(box_file);
                    last_file_box_sub.append(last_file_detail_sub);

                    li.append(last_box_sub);
                    li.append(last_textarea_box_sub);
                    li.append(last_file_box_sub);
                }

                $("[data-to='" + id + "']").before(li);
                $("[data-to='" + id + "']").before(li_break);

            }
        }

        if(parent.length > 0) {
            $.each(parent, function (i, e) { 
                if(e.is_topic == "1") {
                    create_sub($(btn_2).get(0), e.title, e.detail, e.files, e.parent);
                } else {
                    create_sub($(btn_3).get(0), e.title, e.detail, e.files, e.parent);
                }
            });
        }
    }

    // TABS 2 END

    // TABS 4 START
    function create_save_term(meeting_issues_id = '', value = '', value_detail = '', value_meeting_record = '', files = [], parent = [], qr_code = '', vote_by = 3, voter_sum = {}) {
        var no_term = $('.save-term-main').length;
            no_term++;
       
            var hash_code = String.prototype.hashCodeStringOnly();
            var title = "วาระที่ " + no_term;
            var li = $('<li/>').addClass('list-group-item border-0');
            var li_break = $('<li/>').attr('data-to', hash_code).addClass('d-none save-term-main');
            var box = $('<div/>').addClass('row main-agenda border-0');
            var box_textarea = $('<div/>').addClass('row border-0 mt-3 ' + (value_detail ? '' : 'd-none') );
            var box_file = $('<div/>').addClass('row border-0 mt-3');
            var tag = $('<div/>').addClass('input-group-prepend');
            var tag_title = $('<span/>').addClass('input-group-text title-agenda');
    
            var detail = $('<div/>').addClass('col-sm-10 box-agenda');
            var detail_f = $('<div/>').addClass('col-sm-2 pr-0');
            var detail_textarea = $('<div/>').attr({'id' : hash_code}).addClass('col-sm-10 offset-sm-2');
            var detail_file = $('<div/>').attr({'id' : 'file' + hash_code}).addClass('col-sm-10 offset-sm-2');

            var input_1 = $('<input/>').attr({'type' : 'hidden', 'name' : 'meeting_issues_save[' + hash_code + '][meeting_issues_id]'}).val(meeting_issues_id);
            var input_2 = $('<input/>').attr({'type' : 'hidden', 'data-id' : hash_code, 'data-main_name' : 'meeting_issues_save[' + hash_code + '][parent][topic]', 'data-level' : 0});
            var input_3 = $('<input/>').attr({'type' : 'hidden', 'data-id' : hash_code, 'data-main_name' : 'meeting_issues_save[' + hash_code + '][parent][term]', 'data-level' : 0, 'data-term' : no_term}).addClass('save-sub-agenda');

            var input = $('<input/>').attr({'type' : 'text', 'name' : 'meeting_issues_save[' + hash_code + '][value]', 'placeholder' : 'กรุณาระบุวาระการประชุม', 'readonly' : true}).addClass('form-control').val(value);
            var input_level = $('<input/>').attr({'type' : 'hidden', 'name' : 'meeting_issues_save[' + hash_code + '][level]', 'value' : 0});

            var textarea_input_group = $('<div/>').addClass('input-group');
            var textarea = $('<textarea/>').attr({'name' : 'meeting_issues_save[' + hash_code + '][detail]', 'placeholder' : 'กรุณาระบุรายละเอียด', 'readonly' : true}).addClass('form-control').val(value_detail);

            var file = $('<input/>').attr({'type' : 'file', 'name' : 'meeting_issues_save[' + hash_code + '][file][]', 'placeholder' : 'กรุณาระบุรายละเอียด', 'id' : 'file_' + hash_code, 'accept' : "application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/pdf"}).addClass('form-control');

            var box_meeting_record_row = $('<div/>').addClass('row border-0 mt-3');
            var box_meeting_record_col = $('<div/>').addClass('col-sm-10 offset-sm-2');
            var box_meeting_record_form_group = $('<div/>').addClass('form-group');
            var box_meeting_record_label = $('<label/>').text('บันทึกการประชุม');
            var box_meeting_record_textarea = $('<textarea/>').attr({'name' : 'meeting_issues_save[' + hash_code + '][meeting_record]', 'placeholder' : 'กรุณาระบุรายละเอียด'}).val(value_meeting_record).addClass('form-control');

            box_meeting_record_form_group.append(box_meeting_record_label);
            box_meeting_record_form_group.append(box_meeting_record_textarea);

            box_meeting_record_col.append(box_meeting_record_form_group);

            box_meeting_record_row.append(box_meeting_record_col);

            textarea_input_group.append(textarea);

            if(parent.length > 0) {
                $.each(parent, function (i, e) { 
                    parent[i].title_voter = title + " > {title}";
                });
            }

            // ลงมติ

            var box_voter_row = $('<div/>').addClass('row border-0 mt-3');
            var box_voter_col = $('<div/>').addClass('col-sm-10 offset-sm-2');

            var box_voter_fieldset = $('<fieldset/>').addClass('scheduler-border');
            var box_voter_legend = $('<legend/>').addClass('scheduler-border').text('ลงมติ :');

            var box_voter_form_group1 = $('<div/>').addClass('form-group row');
            var box_voter_label1 = $('<label/>').addClass('col-sm-2 col-form-label text-right').text('ผู้ลงมติ :');
            var box_voter_div1 = $('<div/>').addClass('col-sm-7');

            $.each([
                'เลขาฯ',
                'ผู้เข้าร่วมการประชุม',
                'ไม่มีการลงมติฯ'
            ], function (i, e) { 
                var _div = $('<div/>').addClass('form-check form-check-inline  mt-1');
                var _input = $('<input/>').attr({ 'type' : 'radio', 'name' : 'meeting_issues_save[' + hash_code + '][vote_by]', 'value' : ( i + 1 ), 'checked' : ( i + 1 ) == vote_by ? true : false, 'data-active' : ( i + 1 ) == vote_by ? 'active' : '' }).addClass('form-check-input icheck-blue vote_by-' + hash_code);
                var _label = $('<label/>').addClass('form-check-label pl-2 mr-4').text(e);
                    _div.append(_input);
                    _div.append(_label);

                    box_voter_div1.append(_div);
            });

            var box_voter_form_group2_div_qr = $('<div/>').addClass('col-sm-3 text-right');
            var box_voter_form_group2_btn_qr = $('<button/>').attr({'type' : 'button', 'onclick' : "openQRCode('" + qr_code + "', '" + title + " : " + value + "')", 'disabled' : ( vote_by == 2 ? false : true )}).addClass('btn btn-outline-secondary');
            var box_voter_form_group2_btn_qr_i = $('<i/>').addClass('fas fa-qrcode');

            box_voter_form_group2_btn_qr.append(box_voter_form_group2_btn_qr_i);
            box_voter_form_group2_btn_qr.append(" QR Code");

            box_voter_form_group2_div_qr.append(box_voter_form_group2_btn_qr);
            
            var box_voter_form_group2 = $('<div/>').addClass('form-group row');
            var box_voter_label2 = $('<label/>').addClass('col-sm-2 col-form-label text-right').text('ลงมติการประชุม :');
            var box_voter_input_target = $('<input/>').attr({'type' : 'hidden', 'data-target_uptime' : title + " : " + value});

            box_voter_form_group2.append(box_voter_label2);
            box_voter_form_group2.append(box_voter_input_target);

            $.each([
                {text : 'เห็นด้วย', name : 'agree'},
                {text : 'ไม่เห็นด้วย', name : 'disagree' },
                {text : 'ไม่ลงความคิดเห็น', name : 'no_comment'}
            ], function (i, e) { 
                var _input = $('<input/>').attr({ 'type' : 'text', 'autocomplete' : 'off', 'placeholder' : '0', 'name' : 'meeting_issues_save[' + hash_code + '][' + e.name + ']', readonly : ( vote_by == 1 ? false : true ) }).val((typeof voter_sum[e.name] != 'undefined' ? voter_sum[e.name] : 0)).addClass('form-control ');
                var _label = $('<label/>').addClass('col-sm-2 col-form-label text-right').text(e.text);
                var _div = $('<div/>').addClass('col-sm-1');
                    _div.append(_input);

                    box_voter_form_group2.append(_label);
                    box_voter_form_group2.append(_div);
            });

            var box_voter_form_group2_div = $('<div/>').addClass('col-sm-1 text-right');
            var box_voter_form_group2_btn = $('<button/>').attr({'type' : 'button'}).addClass('btn btn-outline-secondary');
            var box_voter_form_group2_btn_i = $('<i/>').addClass('fas fa-sync-alt');

            box_voter_form_group2_btn.append(box_voter_form_group2_btn_i);
            box_voter_form_group2_div.append(box_voter_form_group2_btn);
            // box_voter_form_group2.append(box_voter_form_group2_div);

            box_voter_form_group1.append(box_voter_label1);
            box_voter_form_group1.append(box_voter_div1);
            box_voter_form_group1.append(box_voter_form_group2_div_qr);

            box_voter_fieldset.append(box_voter_legend);
            box_voter_fieldset.append(box_voter_form_group1);
            box_voter_fieldset.append(box_voter_form_group2);

            box_voter_col.append(box_voter_fieldset);

            box_voter_row.append(box_voter_col);

            // จบ ลงมติ

            //
            var jFiler_item = $('<li/>').addClass('jFiler-item p-2');
            var jFiler_item_container = $('<div/>').addClass('jFiler-item-container');
            var jFiler_item_inner = $('<div/>').addClass('jFiler-item-inner');
            var jFiler_item_icon = $('<div/>').addClass('jFiler-item-icon float-left text-dark').text("{"+"{" + "fi-icon" + "}"+"}");
            var jFiler_item_info = $('<div/>').addClass('jFiler-item-info float-left');
            var jFiler_item_title = $('<div/>').addClass('jFiler-item-title').append($('<a/>').attr({'href' : "" ,'download': "{"+"{" + "fi-name" + "}"+"}"}).text("{"+"{" + "fi-name | limitTo:35" + "}"+"}"));
            var jFiler_item_others = $('<div/>').addClass('jFiler-item-others');
            var jFiler_item_status = $('<span/>').addClass('jFiler-item-status');

            var jFiler_item_assets = $('<div/>').addClass('jFiler-item-assets');
            var jFiler_ul = $('<ul/>').addClass('list-inline');
            var jFiler_li = $('<li/>');
            var jFiler_a = $('<a/>').addClass('icon-jfi-trash jFiler-item-trash-action');

            jFiler_li.append(jFiler_a);

            jFiler_ul.append(jFiler_li);

            jFiler_item_assets.append(jFiler_ul);

            jFiler_item_others.append($('<span/>').text("size : {"+"{" + "fi-size2" + "}"+"}"));
            jFiler_item_others.append($('<span/>').text("type : {"+"{" + "fi-extension" + "}"+"}"));
            jFiler_item_others.append(jFiler_item_status);

            jFiler_item_info.append(jFiler_item_title);
            jFiler_item_info.append(jFiler_item_others);
            jFiler_item_info.append(jFiler_item_assets);

            jFiler_item_inner.append(jFiler_item_icon);
            jFiler_item_inner.append(jFiler_item_info);

            jFiler_item_container.append(jFiler_item_inner);

            jFiler_item.append(jFiler_item_container);
            //
      
            tag_title.append(title);

            box.append(detail_f.append(tag_title));
            box.append(detail);
            box_textarea.append(detail_textarea);
            box_file.append(detail_file);

            detail.append(input);
            detail.append(input_level);
            detail.append(input_1);
            detail.append(input_2);
            detail.append(input_3);

            detail_textarea.append(textarea_input_group);

            detail_file.append(file);

            li.append(box);
            li.append(box_textarea);
            li.append(box_file);
            li.append(box_meeting_record_row);
            li.append(box_voter_row);

            file.filer({
                limit: 5,
                maxSize: 10,
                extensions: ["pdf", "doc", "docx", "xls", "xlsx"],
                showThumbs: true,
                templates : {
                    item: $('<div>').append(jFiler_item.clone()).html(),
                    itemAppend: $('<div>').append(jFiler_item.clone()).html()
                },
                files : files,//3
                beforeRender : function (itemEl, inputEl) {
                       
                    inputEl.change(function() {
                        var fileToUpload = $(this).prop('files');
                        if(fileToUpload.length > 0 ) {
                            Object.keys(fileToUpload).forEach(file => {
                                var lastModified = fileToUpload[file].lastModified;
                                var lastModifiedDate = fileToUpload[file].lastModifiedDate;
                                var name = fileToUpload[file].name;
                                var size = fileToUpload[file].size;
                                var type = fileToUpload[file].type;
                                var fileReader = new FileReader();
                                    fileReader.readAsDataURL(fileToUpload[file]);
                                    fileReader.onload = function(fileLoadedEvent){
                                        var _a = itemEl.find('ul.jFiler-items-list li.jFiler-item').eq(file).find('.jFiler-item-title a');
                                        _a.attr({'href' : fileReader.result, 'download' : _a.text()});
                                    };
                            });
                        }
                    });
                },
                onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
                    if($("input[name^='" + inputEl.data().check + "'][value='" + file.id + "']").length > 0) {
                        $("input[name^='" + inputEl.data().check + "'][value='" + file.id + "']").remove();
                    }
                }
            });    

            file.parent().find('.jFiler-input').addClass('d-none');

            file.on('change',function() {
                if($("input[name^='" + $(this).data().check + "']").length > 0) {
                    $.each($("input[name^='" + $(this).data().check + "']"), function (i, e) { 
                        $(this).remove();
                    });
                }
            });

            if(files.length > 0) {
                $.each(files, function (i, e) {
                    var _a = box_file.find('ul.jFiler-items-list li.jFiler-item').eq(i).find('.jFiler-item-title a');
                    _a.attr({'href' : e.url, 'download' : e.name});
                });
            }

            $('.save-list-term').append(li);
            $('.save-list-term').append(li_break);

            $(".vote_by-" + hash_code).on('ifChanged', function(event) {

                var btn_qr_code = $(this).parent().parent().parent().parent().find('button');
                var fieldset = $(this).parent().parent().parent().parent().parent();
                var input_checked = $(this);

                if(input_checked.prop('checked') ) {
                    if(!input_checked.attr("data-active")) {

                        Swal.fire({
                            title: 'การเปลี่ยนแปลงผู้ลงมติ',
                            text: "ทุกครั้งที่มีการเปลี่ยนแปลงจะเป็นการเคลียร์ ค่าผลลงมติใหม่ ต้องการเปลี่ยนแปลงใช่หรือไม่?",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonColor: '#38c172',
                            cancelButtonColor: '#e3342f',
                            confirmButtonText: 'ยืนยัน',
                            cancelButtonText: 'ปิด'
                        }).then((result) => {
                            if (result.isConfirmed) {

                                var index = input_checked.parent().parent().parent().find("input[name*='vote_by']").index(input_checked);
                                var inputLength = input_checked.parent().parent().parent().find("input[name*='vote_by']").length - 1;
                                var vote_by;
                                $.each(input_checked.parent().parent().parent().find("input[name*='vote_by']"), function (i, e) { 
                                    if(index == i) {
                                        $(this).attr('data-active', 'active');
                                        vote_by = $(this).val();
                                    } else {
                                        $(this).attr('data-active', '');
                                    }

                                    if(inputLength === i) {

                                        var method 		= "POST";
                                        var url 		= "{{ route('pages.meeting.voter.clear') }}";
                                        
                                        var formData 		= new FormData();
                                            formData.append('meeting_issues_id', input_checked.parents('li').find("input[name*='meeting_issues_id']").val());
                                            formData.append('target', input_checked.parents('li').find("input[data-target_uptime]").attr('data-target_uptime'));
                                            formData.append('vote_by', vote_by);
                
                                        $.ajax({
                                            url			: url,
                                            headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            },
                                            type		: method,
                                            dataType	: "json",
                                            data		: formData,
                                            mimeTypes	: "multipart/form-data",
                                            contentType	: false,
                                            cache		: false,
                                            processData	: false,
                                            success		: function(data) {
                                                if(data.status) {
                                                    switch (parseInt(input_checked.val())) {
                                                        case 1:
                                                            btn_qr_code.prop('disabled', true);
                                                            $.each(fieldset.find("input[name*='agree'], input[name*='disagree'], input[name*='no_comment']"), function (i, e) { 
                                                                $(this).prop('disabled', false);
                                                                $(this).prop('readonly', false);
                                                            });
                                                            break;
                                                        case 2:
                                                            btn_qr_code.prop('disabled', false);
                                                            $.each(fieldset.find("input[name*='agree'], input[name*='disagree'], input[name*='no_comment']"), function (i, e) { 
                                                                $(this).prop('disabled', true);
                                                                $(this).prop('readonly', true);
                                                            });
                                                            break;
                                                        case 3:
                                                            btn_qr_code.prop('disabled', true);
                                                            $.each(fieldset.find("input[name*='agree'], input[name*='disagree'], input[name*='no_comment']"), function (i, e) { 
                                                                $(this).prop('disabled', true);
                                                                $(this).prop('readonly', true);
                                                            });
                                                            break;
                                                    }
                                                }
                                            }, statusCode: {
                                                400: function(data) {alert("400");},
                                                401: function(data) {alert("401");},
                                                403: function(data) {alert("403");},
                                                404: function(data) {alert("404");},
                                                500: function(data) {alert("500");}			
                                            }        
                                        });
                                    }
                                });
                            } else {
                                input_checked.parent().parent().parent().find("input[name*='vote_by'][data-active='active']").iCheck('check');
                            }
                        });
                    }
                }
            });

            if(parent.length > 0) {
                $.each(parent, function (i, e) { 
                    if(e.is_topic == "1") {
                        create_save_sub($(input_2).get(0), e.meeting_issues_id, e.title, e.detail, e.meeting_record, e.files, e.parent, e.qr_code, e.title_voter, e.vote_by, e.voter_sum);
                    } else {
                        create_save_sub($(input_3).get(0), e.meeting_issues_id, e.title, e.detail, e.meeting_record, e.files, e.parent, e.qr_code, e.title_voter, e.vote_by, e.voter_sum);
                    }
                });
            }
    }

    function create_save_sub(elment, meeting_issues_id= '', value= '', value_detail= '', value_meeting_record = '', files = [], parent = [], qr_code = '', title_voter = '', vote_by = 3, voter_sum = {}) {
      
        var is_topics = false;

        var title = "";
        
        var id = $(elment).data().id;
        var level = $(elment).data().level + 1;
        var term_main = $(elment).data().term;
        var main_name = $(elment).data().main_name;

        var term_secondary = $("[data-secondary='" + id + "']").length + 1;
    
        var hash_code = String.prototype.hashCodeStringOnly();

        switch ($(elment).hasClass('save-sub-agenda')) {
            case true:
                $(elment).parent().parent().parent().find('div.row.border-0.mt-3').eq(3).addClass('d-none');
                title = "วาระที่ " + term_main + "." + term_secondary;
                break;
            default:
                title = "ข้อ " + term_secondary;
                is_topics = true;
                break;
        }

        if(level == 1) {
            if(is_topics) {
                tag_link = $(elment).parent().find('a:eq(1)');
                tag_link.attr({'tabindex' : '-1', 'aria-disabled' : true}).addClass('disabled');
            } else {
                tag_link = $(elment).parent().find('a:eq(0)');
                tag_link.attr({'tabindex' : '-1', 'aria-disabled' : true}).addClass('disabled');
            }
        }

        var li = $('<li/>').attr('data-secondary', id).addClass('list-group-item border-0');
        var li_break = $('<li/>').attr('data-to', hash_code).addClass('d-none' + (is_topics ? ' topics' : ' term') );
        var box = $('<div/>').addClass('row save-main-agenda border-0');
        var box_textarea = $('<div/>').addClass('row border-0 mt-3 ' + (value_detail ? '' : 'd-none'));
        var box_file = $('<div/>').addClass('row border-0 mt-3');
        var tag = $('<div/>').addClass('input-group-prepend');
        var tag_title = $('<span/>').addClass('input-group-text title-agenda');
    
        var detail = $('<div/>').addClass('col-sm-10 box-agenda');
        var detail_f = $('<div/>').addClass('col-sm-2 pr-0');
        var detail_textarea = $('<div/>').attr({'id' : hash_code}).addClass('col-sm-10 offset-sm-2');
        var detail_file = $('<div/>').attr({'id' : 'file' + hash_code}).addClass('col-sm-10 offset-sm-2');

        var input_1 = $('<input/>').attr({'type' : 'hidden', 'name' : main_name + '[' + hash_code + '][meeting_issues_id]'}).val(meeting_issues_id);
        var input_2 = $('<input/>').attr({'type' : 'hidden', 'data-id' : hash_code, 'data-main_name' : main_name + '[' + hash_code + '][parent][topic]', 'data-level' : level, 'tabindex' : (level >= 2 || is_topics ? '-1' : '' ), 'aria-disabled' : (level >= 2 || is_topics ? true : false ) });
        var input_3 = $('<input/>').attr({'type' : 'hidden', 'data-id' : hash_code, 'data-main_name' : main_name + '[' + hash_code + '][parent][term]', 'data-level' : level, 'tabindex' : (level >= 1 ? '-1' : '' ), 'aria-disabled' : (level >= 1 ? true : false ) });

        var input = $('<input/>').attr({'name' : main_name + '[' + hash_code + '][value]', 'placeholder' : 'กรุณาระบุวาระการประชุม', 'readonly' : true}).addClass('form-control').val(value);
        var input_level = $('<input/>').attr({'type' : 'hidden', 'name' : main_name + '[' + hash_code + '][level]', 'value' : level});

        var textarea_input_group = $('<div/>').addClass('input-group');
     
        var textarea = $('<textarea/>').attr({'name' : main_name + '[' + hash_code + '][detail]', 'placeholder' : 'กรุณาระบุรายละเอียด', 'readonly' : true}).addClass('form-control').val(value_detail);

        var file = $('<input/>').attr({'type' : 'file', 'name' : main_name + '[' + hash_code + '][file][]', 'placeholder' : 'กรุณาระบุรายละเอียด', 'id' : 'file_' + hash_code, 'accept' : "application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/pdf"}).addClass('form-control');

        var box_meeting_record_row = $('<div/>').addClass('row border-0 mt-3');
        var box_meeting_record_col = $('<div/>').addClass('col-sm-10 offset-sm-2');
        var box_meeting_record_form_group = $('<div/>').addClass('form-group');
        var box_meeting_record_label = $('<label/>').text('บันทึกการประชุม');
        var box_meeting_record_textarea = $('<textarea/>').attr({'name' : main_name + '[' + hash_code + '][meeting_record]', 'placeholder' : 'กรุณาระบุรายละเอียด'}).val(value_meeting_record).addClass('form-control');


        box_meeting_record_form_group.append(box_meeting_record_label);
        box_meeting_record_form_group.append(box_meeting_record_textarea);

        box_meeting_record_col.append(box_meeting_record_form_group);

        box_meeting_record_row.append(box_meeting_record_col);

        textarea_input_group.append(textarea);
 
        if(typeof parent == 'object' && parent.length > 0) {
            $.each(parent, function (i, e) {
                parent[i].title_voter = title_voter.replace('{title}', title) + " > {title}";
            });
        }

        // ลงมติ
     
        var box_voter_row = $('<div/>').addClass('row border-0 mt-3 ' + ( is_topics ? 'd-none' : '' ));
        var box_voter_col = $('<div/>').addClass('col-sm-10 offset-sm-2');

        var box_voter_fieldset = $('<fieldset/>').addClass('scheduler-border');
        var box_voter_legend = $('<legend/>').addClass('scheduler-border').text('ลงมติ :');

        var box_voter_form_group1 = $('<div/>').addClass('form-group row');
        var box_voter_label1 = $('<label/>').addClass('col-sm-2 col-form-label text-right').text('ผู้ลงมติ :');
        var box_voter_div1 = $('<div/>').addClass('col-sm-7');

        $.each([
            'เลขาฯ',
            'ผู้เข้าร่วมการประชุม',
            'ไม่มีการลงมติฯ'
        ], function (i, e) { 
            var _div = $('<div/>').addClass('form-check form-check-inline  mt-1');
            var _input = $('<input/>').attr({ 'type' : 'radio', 'name' : main_name + '[' + hash_code + '][vote_by]', 'value' : ( i + 1 ), 'checked' : ( i + 1 ) == vote_by ? true : false, 'data-active' : ( i + 1 ) == vote_by ? 'active' : '' }).addClass('form-check-input icheck-blue vote_by-' + hash_code);
            var _label = $('<label/>').addClass('form-check-label pl-2 mr-4').text(e);
                _div.append(_input);
                _div.append(_label);

                box_voter_div1.append(_div);
        });
     
        var box_voter_form_group2_div_qr = $('<div/>').addClass('col-sm-3 text-right');
        var box_voter_form_group2_btn_qr = $('<button/>').attr({'type' : 'button', 'onclick' : "openQRCode('" + qr_code + "', '" + title_voter.replace('{title}', title) + " : " + value + "')", 'disabled' : ( vote_by == 2 ? false : true )}).addClass('btn btn-outline-secondary');
        var box_voter_form_group2_btn_qr_i = $('<i/>').addClass('fas fa-qrcode');

        box_voter_form_group2_btn_qr.append(box_voter_form_group2_btn_qr_i);
        box_voter_form_group2_btn_qr.append(" QR Code");

        box_voter_form_group2_div_qr.append(box_voter_form_group2_btn_qr);
        
        var box_voter_form_group2 = $('<div/>').addClass('form-group row');
        var box_voter_label2 = $('<label/>').addClass('col-sm-2 col-form-label text-right').text('ลงมติการประชุม :');
        var box_voter_input_target = $('<input/>').attr({'type' : 'hidden', 'data-target_uptime' : title_voter.replace('{title}', title) + " : " + value});

        box_voter_form_group2.append(box_voter_label2);
        box_voter_form_group2.append(box_voter_input_target);

        $.each([
            {text : 'เห็นด้วย', name : 'agree'},
            {text : 'ไม่เห็นด้วย', name : 'disagree' },
            {text : 'ไม่ลงความคิดเห็น', name : 'no_comment'}
        ], function (i, e) { 
            var _input = $('<input/>').attr({ 'type' : 'text', 'autocomplete' : 'off', 'placeholder' : '0', 'name' : main_name + '[' + hash_code + '][' + e.name + ']', readonly : ( vote_by == 1 ? false : true ) }).val((typeof voter_sum[e.name] != 'undefined' ? voter_sum[e.name] : 0)).addClass('form-control ');
            var _label = $('<label/>').addClass('col-sm-2 col-form-label text-right').text(e.text);
            var _div = $('<div/>').addClass('col-sm-1');
                _div.append(_input);

                box_voter_form_group2.append(_label);
                box_voter_form_group2.append(_div);
        });

        var box_voter_form_group2_div = $('<div/>').addClass('col-sm-1 text-right');
        var box_voter_form_group2_btn = $('<button/>').attr({'type' : 'button'}).addClass('btn btn-outline-secondary');
        var box_voter_form_group2_btn_i = $('<i/>').addClass('fas fa-sync-alt');

        box_voter_form_group2_btn.append(box_voter_form_group2_btn_i);
        box_voter_form_group2_div.append(box_voter_form_group2_btn);
        // box_voter_form_group2.append(box_voter_form_group2_div);

        box_voter_form_group1.append(box_voter_label1);
        box_voter_form_group1.append(box_voter_div1);
        box_voter_form_group1.append(box_voter_form_group2_div_qr);

        box_voter_fieldset.append(box_voter_legend);
        box_voter_fieldset.append(box_voter_form_group1);
        box_voter_fieldset.append(box_voter_form_group2);

        box_voter_col.append(box_voter_fieldset);

        box_voter_row.append(box_voter_col);

        // จบ ลงมติ

        //
        var jFiler_item = $('<li/>').addClass('jFiler-item p-2');
        var jFiler_item_container = $('<div/>').addClass('jFiler-item-container');
        var jFiler_item_inner = $('<div/>').addClass('jFiler-item-inner');
        var jFiler_item_icon = $('<div/>').addClass('jFiler-item-icon float-left text-dark').text("{"+"{" + "fi-icon" + "}"+"}");
        var jFiler_item_info = $('<div/>').addClass('jFiler-item-info float-left');
        var jFiler_item_title = $('<div/>').addClass('jFiler-item-title').append($('<a/>').attr({'href' : "" ,'download': "{"+"{" + "fi-name" + "}"+"}"}).text("{"+"{" + "fi-name | limitTo:35" + "}"+"}"));
        var jFiler_item_others = $('<div/>').addClass('jFiler-item-others');
        var jFiler_item_status = $('<span/>').addClass('jFiler-item-status');

        var jFiler_item_assets = $('<div/>').addClass('jFiler-item-assets');
        var jFiler_ul = $('<ul/>').addClass('list-inline');
        var jFiler_li = $('<li/>');
        var jFiler_a = $('<a/>').addClass('icon-jfi-trash jFiler-item-trash-action');

        jFiler_li.append(jFiler_a);

        jFiler_ul.append(jFiler_li);

        jFiler_item_assets.append(jFiler_ul);

        jFiler_item_others.append($('<span/>').text("size : {"+"{" + "fi-size2" + "}"+"}"));
        jFiler_item_others.append($('<span/>').text("type : {"+"{" + "fi-extension" + "}"+"}"));
        jFiler_item_others.append(jFiler_item_status);

        jFiler_item_info.append(jFiler_item_title);
        jFiler_item_info.append(jFiler_item_others);
        jFiler_item_info.append(jFiler_item_assets);

        jFiler_item_inner.append(jFiler_item_icon);
        jFiler_item_inner.append(jFiler_item_info);

        jFiler_item_container.append(jFiler_item_inner);

        jFiler_item.append(jFiler_item_container);
        //

        tag_title.append(title);

        box.append(detail_f.append(tag_title));
        box.append(detail);
        box_textarea.append(detail_textarea);
        box_file.append(detail_file);

        detail.append(input);
        detail.append(input_level);
        detail.append(input_1);
        detail.append(input_2);
        detail.append(input_3);

        detail_textarea.append(textarea_input_group);

        detail_file.append(file);

        li.append(box);
        li.append(box_textarea);
        li.append(box_file);
        li.append(box_meeting_record_row);
        li.append(box_voter_row);

        file.filer({
            limit: 5,
            maxSize: 10,
            extensions: ["pdf", "doc", "docx", "xls", "xlsx"],
            showThumbs: true,
            templates : {
                item: $('<div>').append(jFiler_item.clone()).html(),
                itemAppend: $('<div>').append(jFiler_item.clone()).html()
            },
            files : files,//4
            beforeRender : function (itemEl, inputEl) {
                    
                inputEl.change(function() {
                    var fileToUpload = $(this).prop('files');
                    if(fileToUpload.length > 0 ) {
                        Object.keys(fileToUpload).forEach(file => {
                            var lastModified = fileToUpload[file].lastModified;
                            var lastModifiedDate = fileToUpload[file].lastModifiedDate;
                            var name = fileToUpload[file].name;
                            var size = fileToUpload[file].size;
                            var type = fileToUpload[file].type;
                            var fileReader = new FileReader();
                                fileReader.readAsDataURL(fileToUpload[file]);
                                fileReader.onload = function(fileLoadedEvent){
                                    var _a = itemEl.find('ul.jFiler-items-list li.jFiler-item').eq(file).find('.jFiler-item-title a');
                                    _a.attr({'href' : fileReader.result, 'download' : _a.text()});
                                };
                        });
                    }
                });
            },
            onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
                if($("input[name^='" + inputEl.data().check + "'][value='" + file.id + "']").length > 0) {
                    $("input[name^='" + inputEl.data().check + "'][value='" + file.id + "']").remove();
                }
            }
        });    

        file.parent().find('.jFiler-input').addClass('d-none');

        file.on('change',function() {
            if($("input[name^='" + $(this).data().check + "']").length > 0) {
                $.each($("input[name^='" + $(this).data().check + "']"), function (i, e) { 
                    $(this).remove();
                });
            }
        });

        if(files.length > 0) {
            $.each(files, function (i, e) {
                var _a = box_file.find('ul.jFiler-items-list li.jFiler-item').eq(i).find('.jFiler-item-title a');
                _a.attr({'href' : e.url, 'download' : e.name});
            });
        }

        var last_box_sub = null;
        var last_detail_sub = null;

        var last_textarea_box_sub = null;
        var last_textarea_detail_sub = null;

        var last_file_box_sub = null;
        var last_file_detail_sub = null;

        var last_meeting_record_box_sub = null;
        var last_meeting_record_detail_sub = null;

        var last_box_voter_box_sub = null;
        var last_box_voter_detail_sub = null;
    
        for (var index = 0; index < level; index++) {
            
            var box_sub = $('<div/>').addClass('row save-main-agenda border-0');
            var detail_sub = $('<div/>').addClass('offset-sm-2 col-sm-10 box-agenda');
            
            var textarea_box_sub = $('<div/>').addClass('row save-main-agenda border-0');
            var textarea_detail_sub = $('<div/>').addClass('offset-sm-2 col-sm-10 box-agenda');
            
            var file_box_sub = $('<div/>').addClass('row save-main-agenda border-0');
            var file_detail_sub = $('<div/>').addClass('offset-sm-2 col-sm-10 box-agenda');
            
            var meeting_record_box_sub = $('<div/>').addClass('row save-main-agenda border-0');
            var meeting_record_detail_sub = $('<div/>').addClass('offset-sm-2 col-sm-10 box-agenda');
            
            var box_voter_box_sub = $('<div/>').addClass('row save-main-agenda border-0');
            var box_voter_detail_sub = $('<div/>').addClass('offset-sm-2 col-sm-10 box-agenda');

            box_sub.append(detail_sub);
            textarea_box_sub.append(textarea_detail_sub);
            file_box_sub.append(file_detail_sub);
            meeting_record_box_sub.append(meeting_record_detail_sub);
            box_voter_box_sub.append(box_voter_detail_sub);

            if(last_box_sub == null) {
                last_box_sub = box_sub;
                last_detail_sub = detail_sub;
                
                last_textarea_box_sub = textarea_box_sub;
                last_textarea_detail_sub = textarea_detail_sub;

                last_file_box_sub = file_box_sub;
                last_file_detail_sub = file_detail_sub;

                last_meeting_record_box_sub = meeting_record_box_sub;
                last_meeting_record_detail_sub = meeting_record_detail_sub;

                last_box_voter_box_sub = box_voter_box_sub;
                last_box_voter_detail_sub = box_voter_detail_sub;
            } else {
                box_sub.append(detail_sub);
                textarea_box_sub.append(textarea_detail_sub);
                file_box_sub.append(file_detail_sub);
                meeting_record_box_sub.append(meeting_record_detail_sub);
                box_voter_box_sub.append(box_voter_detail_sub);
            
                if(last_detail_sub.find('div:last').length> 0) {
                    last_detail_sub.find('div:last').append(box_sub);
                    last_textarea_detail_sub.find('div:last').append(textarea_box_sub);
                    last_file_detail_sub.find('div:last').append(file_box_sub);
                    last_meeting_record_detail_sub.find('div:last').append(meeting_record_box_sub);
                    last_box_voter_detail_sub.find('div:last').append(box_voter_box_sub);
                } else {

                    last_detail_sub.append(box_sub);
                    last_textarea_detail_sub.append(textarea_box_sub);
                    last_file_detail_sub.append(file_box_sub);
                    last_meeting_record_detail_sub.append(meeting_record_box_sub);
                    last_box_voter_detail_sub.append(box_voter_box_sub);
                }

                last_box_sub.append(last_detail_sub);
                last_textarea_box_sub.append(last_textarea_detail_sub);
                last_file_box_sub.append(last_file_detail_sub);
                last_meeting_record_box_sub.append(last_meeting_record_detail_sub);
                last_box_voter_box_sub.append(last_box_voter_detail_sub);
            }
    
            if(level == index + 1) {
                if((index + 1) > 1) {
                    detail_sub.append(box);
                    textarea_detail_sub.append(box_textarea);
                    file_detail_sub.append(box_file);
                    meeting_record_detail_sub.append(box_meeting_record_row);
                    box_voter_detail_sub.append(box_voter_row);

                    li.append(last_box_sub);
                    li.append(last_textarea_box_sub);
                    li.append(last_file_box_sub);
                    li.append(last_meeting_record_box_sub);
                    li.append(last_box_voter_box_sub);
                    
                } else {

                    last_detail_sub.append(box);
                    last_box_sub.append(last_detail_sub);

                    last_textarea_detail_sub.append(box_textarea);
                    last_textarea_box_sub.append(last_textarea_detail_sub);

                    last_file_detail_sub.append(box_file);
                    last_file_box_sub.append(last_file_detail_sub);

                    last_meeting_record_detail_sub.append(box_meeting_record_row);
                    last_meeting_record_box_sub.append(last_meeting_record_detail_sub);

                    last_box_voter_detail_sub.append(box_voter_row);
                    last_box_voter_box_sub.append(last_box_voter_detail_sub);

                    li.append(last_box_sub);
                    li.append(last_textarea_box_sub);
                    li.append(last_file_box_sub);
                    li.append(last_meeting_record_box_sub);
                    li.append(last_box_voter_box_sub);
                }

                $("[data-to='" + id + "']").before(li);
                $("[data-to='" + id + "']").before(li_break);

            }
        }

        $(".vote_by-" + hash_code).on('ifChanged', function(event) {

            var btn_qr_code = $(this).parent().parent().parent().parent().find('button');
            var fieldset = $(this).parent().parent().parent().parent().parent();
            var input_checked = $(this);

            if(input_checked.prop('checked') ) {
                if(!input_checked.attr("data-active")) {

                    Swal.fire({
                        title: 'การเปลี่ยนแปลงผู้ลงมติ',
                        text: "ทุกครั้งที่มีการเปลี่ยนแปลงจะเป็นการเคลียร์ ค่าผลลงมติใหม่ ต้องการเปลี่ยนแปลงใช่หรือไม่?",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#38c172',
                        cancelButtonColor: '#e3342f',
                        confirmButtonText: 'ยืนยัน',
                        cancelButtonText: 'ปิด'
                    }).then((result) => {
                        if (result.isConfirmed) {

                            var index = input_checked.parent().parent().parent().find("input[name*='vote_by']").index(input_checked);
                            var inputLength = input_checked.parent().parent().parent().find("input[name*='vote_by']").length - 1;
                            var vote_by;
                            $.each(input_checked.parent().parent().parent().find("input[name*='vote_by']"), function (i, e) { 
                                if(index == i) {
                                    $(this).attr('data-active', 'active');
                                    vote_by = $(this).val();
                                } else {
                                    $(this).attr('data-active', '');
                                }

                                if(inputLength === i) {

                                    var method 		= "POST";
                                    var url 		= "{{ route('pages.meeting.voter.clear') }}";
                                    
                                    var formData 		= new FormData();
                                        formData.append('meeting_issues_id', input_checked.parents('li').find("input[name*='meeting_issues_id']").val());
                                        formData.append('target', input_checked.parents('li').find("input[data-target_uptime]").attr('data-target_uptime'));
                                        formData.append('vote_by', vote_by);
            
                                    $.ajax({
                                        url			: url,
                                        headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        type		: method,
                                        dataType	: "json",
                                        data		: formData,
                                        mimeTypes	: "multipart/form-data",
                                        contentType	: false,
                                        cache		: false,
                                        processData	: false,
                                        success		: function(data) {
                                            if(data.status) {
                                                switch (parseInt(input_checked.val())) {
                                                    case 1:
                                                        btn_qr_code.prop('disabled', true);
                                                        $.each(fieldset.find("input[name*='agree'], input[name*='disagree'], input[name*='no_comment']"), function (i, e) { 
                                                            $(this).prop('disabled', false);
                                                            $(this).prop('readonly', false);
                                                        });
                                                        break;
                                                    case 2:
                                                        btn_qr_code.prop('disabled', false);
                                                        $.each(fieldset.find("input[name*='agree'], input[name*='disagree'], input[name*='no_comment']"), function (i, e) { 
                                                            $(this).prop('disabled', true);
                                                            $(this).prop('readonly', true);
                                                        });
                                                        break;
                                                    case 3:
                                                        btn_qr_code.prop('disabled', true);
                                                        $.each(fieldset.find("input[name*='agree'], input[name*='disagree'], input[name*='no_comment']"), function (i, e) { 
                                                            $(this).prop('disabled', true);
                                                            $(this).prop('readonly', true);
                                                        });
                                                        break;
                                                }
                                            }
                                        }, statusCode: {
                                            400: function(data) {alert("400");},
                                            401: function(data) {alert("401");},
                                            403: function(data) {alert("403");},
                                            404: function(data) {alert("404");},
                                            500: function(data) {alert("500");}			
                                        }        
                                    });
                                }
                            });
                        } else {
                            input_checked.parent().parent().parent().find("input[name*='vote_by'][data-active='active']").iCheck('check');
                        }
                    });
                }
            }
        });
   
        if(typeof parent == 'object' && parent.length > 0) {
            $.each(parent, function (i, e) { 
                if(e.is_topic == "1") {
                    create_save_sub($(input_2).get(0), e.meeting_issues_id, e.title, e.detail, e.meeting_record, e.parent, e.qr_code, e.title_voter, e.vote_by, e.voter_sum);
                } else {
                    create_save_sub($(input_3).get(0), e.meeting_issues_id, e.title, e.detail, e.meeting_record, e.parent, e.qr_code, e.title_voter, e.vote_by, e.voter_sum);
                }
            });
        }
    }

    // TABS 4 END
    function selectsapi(val = '') {
        var method 		= "POST";
        var url 		= "{{ route('pages.attendances.selectsapi') }}";
        var data 		= new FormData();
            data.append('selected_id', val);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }

    function setBoardId(val) {
        boards_id = val;
    }
    function getBoardId() {
        return boards_id;
    }

    function position_in_meeting_selects(val = '') {
        var method 		= "POST";
        var url 		= "{{ route('pages.positioninmeeting.selects') }}";
        var data 		= new FormData();
            data.append('selected_id', val);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }

    function selectsapi(val = '', attendance_id = '', board_id = '',is_edit = false) {
        var method 		= "POST";
        var url 		= "{{ route('pages.user.selectsapi') }}";
        var data 		= new FormData();
            data.append('selected_id', val);
            data.append('attendance_id', attendance_id);
            data.append('board_id', board_id);
            data.append('is_edit', is_edit);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }

    function status_selects(val = '') {
        var method 		= "POST";
        var url 		= "{{ route('pages.statuses.selects') }}";
        var data 		= new FormData();
            data.append('selected_id', val);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }

    function openQRCode(url, title) {

        var _div_modal = $('<div/>').attr({ 'tabindex' : '-1' }).addClass('modal');
        var _div_modal_dialog = $('<div/>').addClass('modal-dialog modal-dialog-centered modal-lg');
        var _div_modal_content= $('<div/>').addClass('modal-content');
        var _div_modal_header = $('<div/>').addClass('modal-header');
        var _h5 = $('<h5/>').addClass('modal-title').text('QR Code');
        var _div_modal_body = $('<div/>').addClass('modal-body');
        var _div_modal_footer = $('<div/>').addClass('modal-footer');
        var _btn_close = $('<button>').attr({ 'type' : 'button', 'data-dismiss' : 'modal' }).addClass('btn btn-danger btn-block').text('ปิด');
        var _img_qr_code = $('<img/>').attr({'src' : url.replace('title_voter', encodeURIComponent(title)), 'alt' : 'qr-code'}).addClass('rounded mx-auto d-block');
           
            _div_modal_header.append(_h5);

            _div_modal_footer.append(_btn_close);

            _div_modal_body.append(_img_qr_code);

            _div_modal_content.append(_div_modal_header);
            _div_modal_content.append(_div_modal_body);
            _div_modal_content.append(_div_modal_footer);

            _div_modal_dialog.append(_div_modal_content);

            _div_modal.append(_div_modal_dialog);

            $(document).find('body').append(_div_modal);
            
            _div_modal.modal({
                backdrop: 'static',
                keyboard: false,
                show: false
            });

            _div_modal.on('hidden.bs.modal', function (e) {
                _div_modal.remove();
            });

            _div_modal.modal('show');

    }

    window.Echo.channel("update.voter.{{ !empty($result['data']->channel_id)  ? $result['data']->channel_id : '' }}")
    .listen('.voter.update', (e) => {
        items = $(document).find("[data-target_uptime='" + e.target + "']").parent();
        items.find("input[name*='agree']").val(e.agree);
        items.find("input[name*='disagree']").val(e.disagree);
        items.find("input[name*='no_comment']").val(e.no_comment);
    });

function beautifulformat() {
    var method 		= "POST";
    var url 		= "{{ route('pages.meeting.beautifulformat') }}";
    var data 		= new FormData();

    $.each($(".save-list-term li input[name*='meeting_issues_id'], .save-list-term li input[name*='value'], .save-list-term li textarea[name*='detail']"), function (i, e) { 
        name = $(this).attr('name').replace('meeting_issues_save', '');
        data.append('input' + name, $(this).val());
    });

    data.append('id', "{{ ( !empty($result['data']->id) ? $result['data']->id : '' ) }}");

    $.ajax({
        url			: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type		: method,
        dataType	: "json",
        data		: data,
        mimeTypes	: "multipart/form-data",
        contentType	: false,
        cache		: false,
        processData	: false,
        success		: function(data) {
            window.editor.setData( data.txt );
        }, statusCode: {
            400: function(data) {alert("400");},
            401: function(data) {alert("401");},
            403: function(data) {alert("403");},
            404: function(data) {alert("404");},
            500: function(data) {alert("500");}			
        }        
    });
}

function modal_propose_topics(element) {

    var _div_modal = $("<div/>").attr({ 'tabindex' : '-1', 'role' : 'dialog' }).addClass('modal');
    var _div_modal_dialog = $("<div/>").attr({ 'role' : 'document' }).addClass('modal-dialog modal-xl');
    var _div_modal_content = $("<div/>").addClass('modal-content');
    var _div_modal_header = $("<div/>").addClass('modal-header');
    var _h5_modal_title = $("<h5/>").addClass('modal-title').text('รายการเสนอหัวข้อการประชุม');
    var _button_close = $("<button/>").attr({ 'type' : 'button', 'data-dismiss' : 'modal', 'aria-label' : 'Close' }).addClass('close').append($("<span/>").attr({ 'aria-hidden' : true }).append("&times;"));
    var _div_modal_body = $("<div/>").addClass('modal-body');
    var _div_modal_footer = $("<div/>").addClass('modal-footer');
    var _button_modal_footer_custom = $("<button/>").attr({ 'type' : 'button' }).addClass('btn btn-success').text('กำหนดเอง');
    var _button_modal_footer_confirm = $("<button/>").attr({ 'type' : 'button' }).addClass('btn btn-primary').text('ยืนยัน');
    var _button_modal_footer_close = $("<button/>").attr({ 'type' : 'button', 'data-dismiss' : 'modal' }).addClass('btn btn-secondary').text('ยกเลิก');

    _div_modal_header.append(_h5_modal_title);
    _div_modal_header.append(_button_close);

    _div_modal_footer.append(_button_modal_footer_custom);
    _div_modal_footer.append(_button_modal_footer_confirm);
    _div_modal_footer.append(_button_modal_footer_close);

    _div_modal_content.append(_div_modal_header);
    _div_modal_content.append(_div_modal_body);
    _div_modal_content.append(_div_modal_footer);

    _div_modal_dialog.append(_div_modal_content);
    _div_modal.append(_div_modal_dialog);

    $('body').append(_div_modal);

    _div_modal.modal({
        backdrop: 'static',
        keyboard: false,
        show: false
    });

    _div_modal.on('hidden.bs.modal', function (e) {
        _div_modal.remove();
    });

    var _table = $("<table/>").attr({ 'cellspacing' : 0, 'width' : '100%' }).addClass('table table-bordered');
    var _thead = $("<thead/>").addClass('thead-light');
    var _tbody = $("<tbody/>");
    var _tr = $("<tr/>");
    var _th = [
        $("<input/>").attr({ 'type' : 'checkbox' }),
        'ชื่อหัวข้อการประชุม',
        'รายละเอียด',
        'เอกสารแนบประกอบ',
        'ผู้เสนอเรื่อง',
    ];

    $.each(_th, function (i, e) { 
        _tr.append($('<th/>').append(e));
    });

    _thead.append(_tr);

    _table.append(_thead);
    _table.append(_tbody);

    _div_modal_body.append(_table);

    var _dataTable = _table.DataTable({
        destroy     : true,
        serverSide  : false,
        paging      : false,
        info        : false,
        lengthChange: false,
        searching   : false,
        ordering    : false,
        autoWidth   : true,
        deferLoading: 0,
        createdRow: async function ( row, data, index ) {

            var tr = $(row);
            
            tr.find('input[type=checkbox]').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
            
            tr.find('.iCheck-helper').css('position', 'relative');
                
        },
        drawCallback: function (settings) {
           
        },
        initComplete: function(settings, json) {
            var api = this.api();
            if(api.rows().count() > 0) {
                _div_modal.modal('show');
            } else {
                _div_modal.remove();
                create_sub(element);
            }
        },
        ajax: function(data, callback, settings) {
            
            $.ajaxSetup({
                url: "{{ route('pages.proposetopics.meetingdatatables') }}",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type : "POST",
                dataType: 'json',
                error: function(data){
                    if(data.status == 401) {
                        $('#login_modal').modal('show');
                    }
                    console.log('error');
                },
                success: function(data){

                    callback({
                        recordsTotal    : data.recordsTotal,
                        recordsFiltered : data.recordsFiltered,
                        data            : data.data
                    });
                }
            });
            
            data.meeting_id   = "{{ ( !empty($result['data']->id) ? $result['data']->id : '' ) }}";

            $.ajax({ 
                data: data
            });
        },
        aoColumnDefs: [
            { targets: [ 0, 1, 2, 3 ], orderable: false },
            { targets: [ 4 ], sClass: 'text-center all', orderable: false }
        ],
        columns: [
            null,
            null,
            null,
            null,
            null,
            
        ]
    });

    _dataTable.draw();

    var header = $(_dataTable.table().header());
        header.find("[type='checkbox']").iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });

    _button_modal_footer_confirm.on('click', function () {
        _dataTable.rows().every(async function ( rowIdx, tableLoop, rowLoop ) {
            var tr = $(this.node());
           
            if(tr.find("input[type='checkbox']").prop('checked')) {
                create_sub(element, tr.find("td:eq(1)").text(), tr.find("td:eq(2)").text(), JSON.parse(tr.find("td:eq(3) span").text() ? tr.find("td:eq(3) span").text() : "[]"));
            }

            if(_dataTable.rows().count() == (rowIdx + 1)) {
                _div_modal.modal('hide');
            }
        });
    });

    _button_modal_footer_custom.on('click', function () {
        create_sub(element);
        _div_modal.modal('hide');
    });
    
}

function sendMail(element, target) {
    var user_id = [];
    var table = $(element).parent().find('table').DataTable();
    var current = $(element).parent().parent().parent().parent().find("input[name*='user_id']").val();
   
    if(target == 'ALL') {
        table.rows().every(async function ( rowIdx, tableLoop, rowLoop ) {
            var tr = $(this.node());
            var result = await mail(tr.find("input[name*='user_id']").val());

            if(result.result) {
                tr.addClass('table-success');
            } else {
                tr.addClass('table-danger');
            }
        });
    } else {
        (async () => {
            var result = await mail(current);
            if(result.result) {
                $(element).parent().parent().parent().parent().addClass('table-success');
            } else {
                $(element).parent().parent().parent().parent().addClass('table-danger');
            }
        })();
    }
}

function mail(user_id) {
    var method 		= "POST";
    var url 		= "{{ route('pages.meeting.sendmail') }}";
    var data 		= new FormData();
        data.append('user_id', user_id);
        data.append('meeting_id', "{{ ( !empty($result['data']->id) ? $result['data']->id : '' ) }}");

        return $.ajax({
            url			: url,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type		: method,
            dataType	: "json",
            data		: data,
            mimeTypes	: "multipart/form-data",
            contentType	: false,
            cache		: false,
            processData	: false,
            success		: function(data) {
                console.log(data);
            }, statusCode: {
                400: function(data) {alert("400");},
                401: function(data) {alert("401");},
                403: function(data) {alert("403");},
                404: function(data) {alert("404");},
                500: function(data) {alert("500");}			
            }        
        });
}
</script>
@endsection