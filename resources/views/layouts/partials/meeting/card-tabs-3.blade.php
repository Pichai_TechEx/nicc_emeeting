<fieldset class="scheduler-border position-relative">
    <legend class="scheduler-border">รายชื่อคณะกรรมการ</legend>
    <div class="form-group row mt-2">    
        <div class="col">
            <table class="table table-bordered" cellspacing="0" width="100%" id="table-tab-3-1" name="table-tab-3-1">
                <thead class="thead-light">
                    <tr>
                        <th>ลำดับ</th>
                        <th>หน่วยงาน</th>
                        <th>ตำแหน่งที่แต่งตั้ง</th>
                        <th>ชื่อนามสกุล</th>
                        <th>ตำแหน่งที่ประชุม</th>
                        <th>อีเมล</th>
                        <th>เบอร์ติดต่อ</th>
                        <th>อื่นๆ</th>
                        <th>สถานะเข้าประชุม</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if (!empty($result['board_meeting']) && count($result['board_meeting']) > 0)
                        @php
                            $rowNo = 1;
                        @endphp
                        @foreach ($result['board_meeting'] as $row)
                            <tr>
                                <td>{{ $rowNo++ }}</td>
                                <td>
                                    {{ !empty($row->organization_name) ? $row->organization_name : '' }}
                                    <input type="hidden" name="board[board_id][]" value="{{ !empty($row->board_id) ? $row->board_id : '' }}">
                                    <input type="hidden" name="board[organization_id][]" value="{{ !empty($row->organization_id) ? $row->organization_id : '' }}">
                                    <input type="hidden" name="board[organization_name][]" value="{{ !empty($row->organization_name) ? $row->organization_name : '' }}">
                                </td>
                                <td>
                                    {{ !empty($row->position) ? $row->position : '' }}
                                    <input type="hidden" name="board[position_name][]" value="{{ !empty($row->position) ? $row->position : '' }}">
                                </td>
                                <td>
                                    {{ !empty($row->first_name) ? "{$row->first_name} {$row->last_name}" : '' }}
                                    <input type="hidden" name="board[user_id][]" value="{{ !empty($row->user_id) ? $row->user_id : '' }}">
                                    <input type="hidden" name="board[title_id][]" value="{{ !empty($row->title_id) ? $row->title_id : '' }}">
                                    <input type="hidden" name="board[title_name][]" value="{{ !empty($row->title_name) ? $row->title_name : '' }}">
                                    <input type="hidden" name="board[first_name][]" value="{{ !empty($row->first_name) ? $row->first_name : '' }}">
                                    <input type="hidden" name="board[last_name][]" value="{{ !empty($row->last_name) ? $row->last_name : '' }}">
                                </td>
                                <td>
                                    {{ !empty($row->position_in_meeting_name) ? $row->position_in_meeting_name : '' }}
                                    <input type="hidden" name="board[position_in_meeting_id][]" value="{{ !empty($row->position_in_meeting_id) ? $row->position_in_meeting_id : '' }}">
                                    <input type="hidden" name="board[position_in_meeting_name][]" value="{{ !empty($row->position_in_meeting_name) ? $row->position_in_meeting_name : '' }}">
                                </td>
                                <td>
                                    {{ !empty($row->email) ? $row->email : '' }}
                                    <input type="hidden" name="board[email][]" value="{{ !empty($row->email) ? $row->email : '' }}">
                                </td>
                                <td>
                                    {{ !empty($row->phone) ? $row->phone : '' }}
                                    <input type="hidden" name="board[phone][]" value="{{ !empty($row->phone) ? $row->phone : '' }}">
                                </td>
                                <td>
                                    <input type="text" name="board[other][]" value="{{ !empty($row->other) ? $row->other : '' }}" class="form-control" placeholder="รายละเอียดอื่นๆ">
                                </td>
                                <td>
                                    <select name="board[status][]" data-edit="{{ !empty($row->status_id) ? $row->status_id : '' }}" class="form-control" placeholder="กรุณาเลือก">
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-secondary option dropdown-toggle">ดำเนินการ</button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item text-dark" onclick="sendMail(this, 'CURRENT')"><i class="far fa-envelope"></i> ส่งอีเมล</a>
                                            <a class="dropdown-item text-danger datatable-delete"><i class="far fa-trash-alt"></i> ลบ</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <button class="btn btn-primary" type="button" style="position : absolute;right : 10px;top : -5px;outline : 5px;" onClick="sendMail(this, 'ALL')" disabled><i class="far fa-envelope"></i> ส่งอีเมลทั้งหมด</button>
</fieldset>
<fieldset class="scheduler-border position-relative">
    <legend class="scheduler-border">ผู้เข้าร่วมประชุม</legend>
    <div class="form-group row mt-2">    
        <div class="col">
            <table class="table table-bordered" cellspacing="0" width="100%" id="table-tab-3-2" name="table-tab-3-2">
                <thead class="thead-light">
                    <tr>
                        <th>ลำดับ</th>
                        <th>หน่วยงาน</th>
                        <th>ตำแหน่งที่แต่งตั้ง</th>
                        <th>ชื่อนามสกุล</th>
                        <th>ตำแหน่งที่ประชุม</th>
                        <th>อีเมล</th>
                        <th>เบอร์ติดต่อ</th>
                        <th>อื่นๆ</th>
                        <th>สถานะเข้าประชุม</th>
                        <th><button type="button" class="btn btn-success"><i class="fas fa-user-plus"></i></button></th>
                    </tr>
                </thead>
                <tbody>
                    @if (!empty($result['attendance_meeting']) && count($result['attendance_meeting']) > 0)
                        @php
                            $rowNo = 1;
                        @endphp
                        @foreach ($result['attendance_meeting'] as $row)
                            <tr>
                                <td>{{ $rowNo++ }}</td>
                                <td>
                                    <span>{{ !empty($row->organization_name) ? $row->organization_name : '' }}</span>
                                    <input type="hidden" name="attendance[old][organization][]" value="{{ !empty($row->organization_id) ? $row->organization_id : '' }}">
                                    <input type="hidden" name="attendance[old][organization_name][]" value="{{ !empty($row->organization_name) ? $row->organization_name : '' }}">
                                </td>
                                <td>
                                    <span>{{ !empty($row->position) ? $row->position : '' }}</span>
                                    <input type="hidden" name="attendance[old][position_name][]" value="{{ !empty($row->position) ? $row->position : '' }}">
                                </td>
                                <td>
                                    <select name="attendance[old][name_option][]" data-edit="{{ !empty($row->user_id) ? $row->user_id : '' }}" class="form-control" placeholder="กรุณาเลือก">
                                    <input type="hidden" name="attendance[old][title][]" value="{{ !empty($row->title_id) ? $row->title_id : '' }}">
                                    <input type="hidden" name="attendance[old][title_name][]" value="{{ !empty($row->title_name) ? $row->title_name : '' }}">
                                    <input type="hidden" name="attendance[old][first_name][]" value="{{ !empty($row->first_name) ? $row->first_name : '' }}">
                                    <input type="hidden" name="attendance[old][last_name][]" value="{{ !empty($row->last_name) ? $row->last_name : '' }}">
                                </td>
                                <td>
                                    <select name="attendance[old][position_in_meeting][]" data-edit="{{ !empty($row->position_in_meeting_id) ? $row->position_in_meeting_id : '' }}" class="form-control" placeholder="กรุณาเลือก">
                                    <input type="hidden" name="attendance[old][position_in_meeting_name][]" value="{{ !empty($row->position_in_meeting_name) ? $row->position_in_meeting_name : '' }}">
                                </td>
                                <td>
                                    <span>{{ !empty($row->email) ? $row->email : '' }}</span>
                                    <input type="hidden" name="attendance[old][email][]" value="{{ !empty($row->email) ? $row->email : '' }}">
                                </td>
                                <td>
                                    <span>{{ !empty($row->phone) ? $row->phone : '' }}</span>
                                    <input type="hidden" name="attendance[old][phone][]" value="{{ !empty($row->phone) ? $row->phone : '' }}">
                                </td>
                                <td>
                                    <input type="text" name="attendance[old][other][]" value="{{ !empty($row->other) ? $row->other : '' }}" class="form-control" placeholder="รายละเอียดอื่นๆ">
                                </td>
                                <td>
                                    <select name="attendance[old][status][]" data-edit="{{ !empty($row->status_id) ? $row->status_id : '' }}" class="form-control" placeholder="กรุณาเลือก">
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-secondary option dropdown-toggle">ดำเนินการ</button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item text-dark"><i class="far fa-envelope"></i> อีเมล</a>
                                            <a class="dropdown-item text-danger datatable-delete"><i class="far fa-trash-alt"></i> ลบ</a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
    <button class="btn btn-primary" type="button" style="position : absolute;right : 10px;top : -5px;outline : 5px;" onClick="sendMail(this, 'ALL')" ><i class="far fa-envelope"></i> ส่งอีเมลทั้งหมด</button>
</fieldset>