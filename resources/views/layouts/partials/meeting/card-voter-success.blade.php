@extends('layouts.app')
@section('breadcrumbs')
{{ Breadcrumbs::render('meeting_voter_success') }}
@endsection
@section('content')
<div class="container">
    <div class="alert alert-success" role="alert">
        <h4 class="alert-heading"><i class="far fa-smile"></i> ขอบคุณผู้เข้าร่วมประชุมทุกท่านที่ลงคะแนน !</h4>
        <hr>
        <p class="mb-0"><a href="{{ route('home') }}" class="alert-link">กลับสู่หน้าหลัก <i class="fas fa-sign-out-alt"></i></a></p>
      </div>
</div>
@endsection