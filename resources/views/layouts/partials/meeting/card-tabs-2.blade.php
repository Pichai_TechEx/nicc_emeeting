<div class="form-group row">
    {!! Html::decode(Form::label('template_meeting_id', 'สร้างวาระการประชุม :', ['class' => 'col-sm-2 col-form-label text-right'])) !!}
    <div class="col-sm-3">
        {{ Form::select("template_meeting_id", (!empty($result['template']) ? $result['template'] : []), old('template_meeting_id') ? old('template_meeting_id') : ( !empty($result['data']->template_meeting_id) ? $result['data']->template_meeting_id : null ) ,[ 'class' => 'form-control'] ) }}
        <span class="form-group text-danger vld validate-credit" style="display:none">
            <small><i class="validate-credit-i"></i></small>
        </span>
    </div>
    <div class="col-sm-6">
        <div class="input-group">
            <button type="button" class="btn btn-sm btn-secondary mr-2 load-template-btn">โหลดรูปแบบ</button>
            <button type="button" class="btn btn-info list-term-btn">เพิ่มวาระ</button>
            <div class="input-group-prepend">
                <span class="input-group-text bg-transparent border-0">(ถ้าหากต้องการเพิ่มวาระ)</span>
            </div>
        </div>
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-12">
        <div class="card border-0">
            <ul class="list-group list-term main-agenda"></ul>
        </div>
    </div>
</div>