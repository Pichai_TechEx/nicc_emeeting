<div class="form-group row">
    {!! Html::decode(Form::label('meeting_name', 'ชื่อการประชุม <em class="text-danger">*</em> :', ['class' => 'col-sm-2 col-form-label text-right'])) !!}
    <div class="col-sm-5">
        {{ Form::text('meeting_name', old('meeting_name') ? old('meeting_name') : ( !empty($result['data']->meeting_name) ? $result['data']->meeting_name : '' ), ['class' => 'form-control '.($errors->has('meeting_name') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุข้อมูล', 'autocomplete' => "off", 'required' => true]) }}
        <span class="form-group text-danger vld validate-meeting_name" style="display:none">
            <small><i class="validate-meeting_name-i"></i></small>
        </span>
    </div>
    {!! Html::decode(Form::label('agenda', 'ครั้งที่ <em class="text-danger">*</em> :', ['class' => 'col-sm-1 col-form-label text-right'])) !!}
    <div class="col-sm-3">
        {{ Form::text('agenda', old('agenda') ? old('agenda') : ( !empty($result['data']->agenda) ? $result['data']->agenda : '' ), ['class' => 'form-control '.($errors->has('agenda') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุข้อมูล', 'autocomplete' => "off", 'required' => true]) }}
        <span class="form-group text-danger vld validate-agenda" style="display:none">
            <small><i class="validate-agenda-i"></i></small>
        </span>
    </div>
</div>
<div class="form-group row">
    {{Form::label('date', 'วันที่ :', ['class' => 'col-sm-2 col-form-label text-right'])}}
    <div class="col-sm-2">
        {{ Form::text('date', old('start_date') ? old('date') : ( !empty($result['data']->date) ? $result['data']->date : '' ), ['class' => 'datepicker '.($errors->has('date') ? 'is-invalid' : ''), 'placeholder' => 'DD/MM/YYYY', 'autocomplete' => "off", 'required' => true]) }}
        <span class="form-group text-danger vld validate-date" style="display:none">
            <small><i class="validate-date-i"></i></small>
        </span>
    </div>
    {{Form::label('start_time', 'เวลาเริ่ม :', ['class' => 'col-sm-1 col-form-label text-right'])}}
    <div class="col-sm-2">
        <div class="form-group">
            <div class="input-group">
                {{ Form::text('start_time', old('start_time') ? old('start_time') : ( !empty($result['data']->start_time) ? $result['data']->start_time : '' ), ['class' => 'form-control timepicker '.($errors->has('start_time') ? 'is-invalid' : ''), 'placeholder' => 'DD/MM/YYYY', 'autocomplete' => "off", 'required' => true]) }}
                <div class="input-group-append">
                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                </div>
            </div>
        </div>
        <span class="form-group text-danger vld validate-start_time" style="display:none">
            <small><i class="validate-start_time-i"></i></small>
        </span>
    </div>
    {{Form::label('end_time', 'เวลาสิ้นสุด :', ['class' => 'col-sm-2 col-form-label text-right'])}}
    <div class="col-sm-2">
        <div class="form-group">
            <div class="input-group">
                {{ Form::text('end_time', old('end_time') ? old('end_time') : ( !empty($result['data']->end_time) ? $result['data']->end_time : '' ), ['class' => 'form-control timepicker '.($errors->has('end_time') ? 'is-invalid' : ''), 'placeholder' => 'DD/MM/YYYY', 'autocomplete' => "off", 'required' => true]) }}
                <div class="input-group-append">
                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                </div>
            </div>
        </div>
        <span class="form-group text-danger vld validate-end_time" style="display:none">
            <small><i class="validate-end_time-i"></i></small>
        </span>
    </div>
</div>
<div class="form-group row">
    {!! Html::decode(Form::label('board_id', 'ชื่อคณะกรรมการ :', ['class' => 'col-sm-2 col-form-label text-right'])) !!}
    <div class="col-sm-4">
        {{ Form::select('board_id', (!empty($result['board']) ? $result['board'] : []), old('board_id') ? old('board_id') : ( !empty($result['data']->board_id) ? $result['data']->board_id : null ), ['class' => 'form-control '.($errors->has('board_id') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุข้อมูล', 'autocomplete' => "off", 'required' => false]) }}
        <span class="form-group text-danger vld validate-board_id" style="display:none">
            <small><i class="validate-board_id-i"></i></small>
        </span>
    </div>
    {!! Html::decode(Form::label('meeting_place', 'สถานที่การประชุม <em class="text-danger">*</em> :', ['class' => 'col-sm-2 col-form-label text-right'])) !!}
    <div class="col-sm-3">
        {{ Form::text('meeting_place', old('meeting_place') ? old('meeting_place') : ( !empty($result['data']->meeting_place) ? $result['data']->meeting_place : '' ), ['class' => 'form-control '.($errors->has('meeting_place') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุข้อมูล', 'autocomplete' => "off", 'required' => true]) }}
        <span class="form-group text-danger vld validate-meeting_place" style="display:none">
            <small><i class="validate-meeting_place-i"></i></small>
        </span>
    </div>
</div>
<div class="form-group row end-meeting">
    {{Form::label('acceptance_end_date', 'วันที่สิ้นสุดการตอบรับการประชุม :', ['class' => 'col-sm-2 col-form-label text-right'])}}
    <div class="col-sm-2">
        {{ Form::text('acceptance_end_date', old('acceptance_end_date') ? old('acceptance_end_date') : ( !empty($result['data']->acceptance_end_date) ? $result['data']->acceptance_end_date : '' ), ['class' => 'datepicker '.($errors->has('acceptance_end_date') ? 'is-invalid' : ''), 'placeholder' => 'DD/MM/YYYY', 'autocomplete' => "off", 'required' => true]) }}
        <span class="form-group text-danger vld validate-acceptance_end_date" style="display:none">
            <small><i class="validate-acceptance_end_date-i"></i></small>
        </span>
    </div>
    {{Form::label('acceptance_end_time', 'เวลา :', ['class' => 'col-sm-1 col-form-label text-right'])}}
    <div class="col-sm-2">
        <div class="input-group">
            {{ Form::text('acceptance_end_time', old('acceptance_end_time') ? old('acceptance_end_time') : ( !empty($result['data']->acceptance_end_time) ? $result['data']->acceptance_end_time : '' ), ['class' => 'form-control timepicker '.($errors->has('acceptance_end_time') ? 'is-invalid' : ''), 'placeholder' => 'DD/MM/YYYY', 'autocomplete' => "off", 'required' => true]) }}
            <div class="input-group-append">
                <span class="input-group-text"><i class="far fa-clock"></i></span>
            </div>
        </div>
        <span class="form-group text-danger vld validate-acceptance_end_time" style="display:none">
            <small><i class="validate-acceptance_end_time-i"></i></small>
        </span>
    </div>
</div>
<div class="form-group row">
    {{ Form::label('information', 'หมายเหตุ :', ['class' => 'col-sm-2 col-form-label text-right']) }}
    <div class="col-sm-9">
        {{ Form::textarea('information', old('information') ? old('information') : ( !empty($result['data']->information) ? $result['data']->information : '' ), ['class' => 'form-control '.($errors->has('information') ? 'is-invalid' : ''), 'rows' => 3, 'placeholder' => 'กรุณาระบุข้อมูล', 'autocomplete' => "off", 'readonly' => false, 'maxlength' => 1000]) }}
        <span class="form-group text-danger vld validate-information" style="display:none">
            <small><i class="validate-information-i"></i></small>
        </span>
    </div>
</div>
<div class="form-group row">
    {{ Form::label('meeting_organization', 'หน่วยงานจัดการประชุม :', ['class' => 'col-sm-2 col-form-label text-right']) }}
    <div class="col-sm-4">
        {{ Form::select('meeting_organization', (!empty($result['organization']) ? $result['organization'] : []), old('meeting_organization') ? old('meeting_organization') : ( !empty($result['data']->meeting_organization) ? $result['data']->meeting_organization : null ), ['class' => 'form-control '.($errors->has('meeting_organization') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุข้อมูล', 'autocomplete' => "off", 'required' => false]) }}
        <span class="form-group text-danger vld validate-meeting_organization" style="display:none">
            <small><i class="validate-meeting_organization-i"></i></small>
        </span>
    </div>
</div>