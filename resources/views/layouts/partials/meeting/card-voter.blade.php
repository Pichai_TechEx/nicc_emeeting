<div class="container">
    {{ Form::open(['route' => ['pages.meeting.sendvote', 'id' => ( !empty($result['data']->id) ? $result['data']->id : '' )], 'method' => 'PUT', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    {{ Form::hidden('is_voter', ( !empty($result['data']->is_voter) ? $result['data']->is_voter : '' )) }}
    {{ Form::hidden('target', ( !empty($result['title']) ? $result['title'] : '' )) }}

    <div class="card">
        <div class="card-header">
            <p class="font-weight-bold mb-0">ลงมติการประชุม : {{ $result['meeting_name'] }} ( {{ $result['title'] }} )</p>
          </div>
          <div class="card-body pb-0">
            <fieldset class="scheduler-border">
                <legend class="scheduler-border">รายละเอียด :</legend>
                @if (!empty($result['data']->detail))
                <p class="card-text">{{ $result['data']->detail }}</p>
                @else
                <p class="card-text"><i class="text-muted">- ไม่ระบุ</i></p>
                    
                @endif
            </fieldset>
          </div>
          <div class="card-body" style="height : 18rem;">
            <div class="form-group row h-100">
                <div class="col-sm-4">
                    <button type="button" class="btn btn-block btn-outline-success h-100">
                        <span class="fa-stack fa-2x">
                            <i class="far fa-check-circle fa-stack-2x"></i> 
                        </span>
                        <span class="font-weight-bold d-block mt-2" style="font-size: 2rem;">เห็นด้วย</span>
                    </button>
                </div>
                <div class="col-sm-4">
                    <button type="button" class="btn btn-block btn-outline-danger h-100">
                        <span class="fa-stack fa-2x">
                            <i class="far fa-times-circle fa-stack-2x"></i>
                        </span>
                        <span class="font-weight-bold d-block mt-2" style="font-size: 2rem;">ไม่เห็นด้วย</span>
                    </button>
                </div>
                <div class="col-sm-4">
                    <button type="button" class="btn btn-block btn-outline-secondary h-100">
                        <span class="fa-stack fa-2x">
                            <i class="fas fa-check fa-stack-1x"></i>
                            <i class="fas fa-ban fa-stack-2x"></i>
                        </span>
                        <span class="font-weight-bold d-block mt-2" style="font-size: 2rem;">ไม่ลงความคิดเห็น</span>
                    </button>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success btn-lg btn-block"><i class="fas fa-vote-yea"></i> ลงมติ</button>
        </div>
    </div>
    {{ Form::close() }}
</div>
@section('script')
<script type="text/javascript">

 

   $(document).ready( function () {

        $('button.h-100').on('click', function() {
            var index_click = $('button.h-100').index(this);
            $.each($('button.h-100'), function (i, e) { 
                 if(index_click == i) {
                    $(this).addClass('active');
                    $("input[name='is_voter']").val((index_click + 1));
                 } else {
                    $(this).removeClass('active');
                 }
            });
        });

        $("#form-submit").on('submit', function (e) {

            e.preventDefault();

            var self = $(this);
            var _btn = self.find("button[type='submit']");

            var _span = $('<span/>').addClass('spinner-border spinner-border-sm mb-2').attr({'role' : 'status', 'aria-hidden' : true});
            var _span_txt = $('<span/>').append('&nbsp;Loading...');

            var _i_default = $('<i/>').addClass('far fa-save');

            _btn.attr('disabled', false);
            
            _btn.attr('disabled', true);
            _btn.find('i').remove();
            _btn.text('');
            _btn.append(_span);
            _btn.append(_span_txt);

            var method 		= $(this).attr('method');
            var url 		= $(this).attr('action');
            var data 		= new FormData(this);

            $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData: false,
                success		: function(data) {

                    var element = null;
                    if(data.errors) {

                        $(".back").click();

                        $.each(data.errors, function(key, value) {

                            if(data.rules[key]) {
                                delete data.rules[key];
                            }

                            $(".validate-" + key).show();
                            $(".validate-" + key + "-i").text(value);
                        });

                        element = Object.keys(data.errors)[0];
                    }

                    if(data.rules) {
                        $.each(data.rules, function(key, value){
                            $(".validate-" + key).hide();
                        });
                    }

                    if(element) {
                        $("*[name='" + element + "']").focus();
                        return;
                    }

                    if(data.status) {

                        $('.vld').hide();

                    }

                    Swal.fire({
                        position: 'center',
                        icon: data.icon,
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                        if(result.isDismissed && data.redirect_url != '') {

                            var method 		= "POST";
                            var url 		= data.update_url;
                            var redirect_url= data.redirect_url;
                          
                            var formData 		= new FormData();
                                formData.append('target', data.target);

                            $.ajax({
                                url			: url,
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                type		: method,
                                dataType	: "json",
                                data		: formData,
                                mimeTypes	: "multipart/form-data",
                                contentType	: false,
                                cache		: false,
                                processData	: false,
                                success		: function(data) {
                                    if(data.status) {
                                        window.location.href = redirect_url;
                                    }
                                }, statusCode: {
                                    400: function(data) {alert("400");},
                                    401: function(data) {alert("401");},
                                    403: function(data) {alert("403");},
                                    404: function(data) {alert("404");},
                                    500: function(data) {alert("500");}			
                                }        
                            });
                        }
                    });

                    _btn.attr('disabled', false);
                    _btn.find('span').remove();
                    _btn.append(_i_default);
                    _btn.append('&nbsp;บันทึก');

                    

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {
                        $('#login_modal').modal('show');
                    },
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {
                        alert("500");

                        _btn.attr('disabled', false);
                        _btn.find('span').remove();
                        _btn.append(_i_default);
                        _btn.append('&nbsp;บันทึก');
                    }			
                }
            });
        });
    });
</script>
@endsection