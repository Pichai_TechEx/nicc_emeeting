<div class="container">
    @if(Request::is('attendances/create'))
    {{ Form::open(['route' => 'pages.attendances.store', 'method' => 'POST', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif

    @if(Request::is('attendances/*/edit'))
    {{ Form::open(['route' => ['pages.attendances.update', 'id' => ( !empty($result['data']->id) ? $result['data']->id : '' )], 'method' => 'PATCH', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif
    {{ Form::hidden('lang', 'th') }}

    <div class="card">
        <div class="card-header">
            @yield('card-header')ผู้เข้าร่วมประชุม
          </div>
        <div class="card-body">
            <div class="form-group row">
                {{ Form::label('name', 'ประเภทสมาชิก :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-4">
                    {{ Form::hidden('is_external', 0) }}
                    {{ Form::checkbox("is_external",  1, old('is_external') == 1 ? true : ( !empty($result['data']->is_external) && $result['data']->is_external == 1 ? true : '' ), ['class' => 'icheck-blue']) }}
                    {{ Form::label('', 'บุคคลากรภายนอก', ['class' => 'col-form-label ml-3']) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('title', 'คำนำหน้า :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-3">
                    {{ Form::select('title', $result['title'], old('title') ? old('title') : ( !empty($result['data']->title) ? $result['data']->title : '0' ), ['class' => 'form-control '.($errors->has('title') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off", 'required' => true]) }}
                </div>
                {{ Form::label('first_name', 'ชื่อ :', ['class' => 'col-sm-1 col-form-label text-right']) }}
                <div class="col-sm-2">
                    {{ Form::text('first_name', old('first_name') ? old('first_name') : ( !empty($result['data']->first_name) ? $result['data']->first_name : '' ), ['class' => 'form-control '.($errors->has('first_name') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุชื่อ', 'autocomplete' => "off"]) }}
                </div>
                <div class="col-sm-2">
                    {{ Form::text('last_name', old('last_name') ? old('last_name') : ( !empty($result['data']->last_name) ? $result['data']->last_name : '' ), ['class' => 'form-control '.($errors->has('last_name') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุนามสกุล', 'autocomplete' => "off"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('org', 'หน่วยงาน :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-3">
                    {{ Form::select('org', $result['org'], old('org') ? old('org') : ( !empty($result['data']->org) ? $result['data']->org : '0' ), ['class' => 'form-control '.($errors->has('org') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off", 'required' => true]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('position', 'ตำแหน่ง :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-3">
                    {{ Form::text('position', old('position') ? old('position') : ( !empty($result['data']->position) ? $result['data']->position : '' ), ['class' => 'form-control '.($errors->has('position') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุชื่อ', 'autocomplete' => "off"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('email', 'อีเมล :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-3">
                    {{ Form::text('email', old('email') ? old('email') : ( !empty($result['data']->email) ? $result['data']->email : '' ), ['class' => 'form-control '.($errors->has('email') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุชื่อ', 'autocomplete' => "off"]) }}
                </div>
                {{ Form::label('phone', 'เบอร์ติดต่อ :', ['class' => 'col-sm-2 col-form-label text-right']) }}
                <div class="col-sm-2">
                    {{ Form::text('phone', old('phone') ? old('phone') : ( !empty($result['data']->phone) ? $result['data']->phone : '' ), ['class' => 'form-control '.($errors->has('phone') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุชื่อ', 'autocomplete' => "off"]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('other', 'อื่นๆ :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-3">
                    {{ Form::text('other', old('other') ? old('other') : ( !empty($result['data']->other) ? $result['data']->other : '' ), ['class' => 'form-control '.($errors->has('other') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุชื่อ', 'autocomplete' => "off"]) }}
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 col-form-label text-right">เปิด/ปิดการใช้งาน :</label>
                <div class="col-sm-4">
                    {{ Form::hidden('is_enabled', 0) }}
                    {{ Form::checkbox('is_enabled', 1, old('is_enabled') == 1 ? true : ( !empty($result['data']->is_enabled) && $result['data']->is_enabled == 1 ? true : '' ), ['data-toggle' => 'toggle', 'data-on' => 'เปิด', 'data-off' => 'ปิด', 'data-onstyle' => 'success', 'data-offstyle' => 'danger', 'data-size' => 'sm']) }}
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-between">
                <a role="button" class="btn btn-dark" href="{{ route('pages.attendances.index') }}"><i class="far fa-arrow-alt-circle-left"></i> ย้อนกลับ</a>
                <button type="submit" class="btn btn-success"><i class="far fa-save"></i> บันทึก</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@section('script')
<script type="text/javascript">

    String.prototype.hashCodeStringOnly = function () {
        var length              = 20;
        var result              = '';
        var characters          = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength    = characters.length;

        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

   $(document).ready( function () {

        $('.toggle-group').find('label').removeAttr('for');
   

        $('input[type=checkbox].icheck-green, input[type=radio].icheck-green').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
     
        $('input[type=checkbox].icheck-yellow, input[type=radio].icheck-yellow').iCheck({
            checkboxClass: 'icheckbox_square-yellow',
            radioClass: 'iradio_square-yellow',
        });
     
        $('input[type=checkbox].icheck-blue, input[type=radio].icheck-blue').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
        });

        $('.btn-proposer-add').on('click', function () {
            var proposer = $('.proposer').clone();
            proposer.removeClass('proposer');
            proposer.addClass('parent-proposer');
            proposer.find('label').remove();
            proposer.find('div:eq(0)').addClass('offset-sm-2');
            proposer.find('input').val('');
            proposer.find('button').removeClass('btn-success');
            proposer.find('button i').removeClass('fa-user-plus');
            proposer.find('button').removeClass('btn-proposer-add');
            proposer.find('button').addClass('btn-danger btn-proposer-delete');
            proposer.find('button i').addClass('fa-user-minus');

            $('.end-attendances').before(proposer);
        });
  
        $('.list-term-btn').on('click', function () {
            create_term();
        });


        $("#form-submit").on('submit', function (e) {

            e.preventDefault();

            var self = $(this);
            var _btn = self.find("button[type='submit']");

            var _span = $('<span/>').addClass('spinner-border spinner-border-sm mb-2').attr({'role' : 'status', 'aria-hidden' : true});
            var _span_txt = $('<span/>').append('&nbsp;Loading...');

            var _i_default = $('<i/>').addClass('far fa-save');

            _btn.attr('disabled', false);
            
            _btn.attr('disabled', true);
            _btn.find('i').remove();
            _btn.text('');
            _btn.append(_span);
            _btn.append(_span_txt);

            var method 		= $(this).attr('method');
            var url 		= $(this).attr('action');
            var data 		= new FormData(this);

            $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData: false,
                success		: function(data) {

                    var element = null;
                    if(data.errors) {

                        $(".back").click();

                        $.each(data.errors, function(key, value) {

                            if(data.rules[key]) {
                                delete data.rules[key];
                            }

                            $(".validate-" + key).show();
                            $(".validate-" + key + "-i").text(value);
                        });

                        element = Object.keys(data.errors)[0];
                    }

                    if(data.rules) {
                        $.each(data.rules, function(key, value){
                            $(".validate-" + key).hide();
                        });
                    }

                    if(element) {
                        $("*[name='" + element + "']").focus();
                        return;
                    }

                    if(data.status) {

                        $('.vld').hide();

                    }

                    Swal.fire({
                        position: 'center',
                        icon: data.icon,
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                       if(result.isDismissed && data.redirect_url != '') {
                            window.location.href = data.redirect_url;
                       }
                    });

                    _btn.attr('disabled', false);
                    _btn.find('span').remove();
                    _btn.append(_i_default);
                    _btn.append('&nbsp;บันทึก');

                    

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {
                        $('#login_modal').modal('show');
                    },
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {
                        alert("500");

                        _btn.attr('disabled', false);
                        _btn.find('span').remove();
                        _btn.append(_i_default);
                        _btn.append('&nbsp;บันทึก');
                    }			
                }
            });
        });
    });
</script>
@endsection