<div class="container">
    @if(Request::is('template/create'))
    {{ Form::open(['route' => 'pages.template.store', 'method' => 'POST', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif

    @if(Request::is('template/*/edit'))
    {{ Form::open(['route' => ['pages.template.update', 'id' => ( !empty($result['data']->id) ? $result['data']->id : '' )], 'method' => 'PATCH', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif
    {{ Form::hidden('lang', 'th') }}

    <div class="card mb-4">
        <div class="card-header">
            <p class="font-weight-bold mb-0">@yield('card-header')</p> 
        </div>
        <div class="card-body">
            <div class="form-group row">
                {!! Html::decode(Form::label('name', 'ชื่อเทมเพลต <em class="text-danger">*</em> :', ['class' => 'col-sm-3 col-form-label text-right'])) !!}
                <div class="col-sm-4">
                    {{ Form::text('name', old('name') ? old('name') : ( !empty($result['data']->name) ? $result['data']->name : '' ), ['class' => 'form-control '.($errors->has('name') ? 'is-invalid' : ''), 'placeholder' => 'Please enter', 'autocomplete' => "off", 'required' => true]) }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('note', 'หมายเหตุ :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-4">
                    {{ Form::textarea('note', old('note') ? old('note') : ( !empty($result['data']->note) ? $result['data']->note : '' ), ['class' => 'form-control '.($errors->has('note') ? 'is-invalid' : ''), 'rows' => 3, 'placeholder' => 'Please enter', 'autocomplete' => "off"]) }}
                </div>
            </div>
            <div class="form-group row">
                <label for="inputPassword" class="col-sm-3 col-form-label text-right">เปิด/ปิดการใช้งาน :</label>
                <div class="col-sm-4">
                    {{ Form::hidden('is_enabled', 0) }}
                    {{ Form::checkbox('is_enabled', 1, old('is_enabled') == 1 ? true : ( !empty($result['data']->is_enabled) && $result['data']->is_enabled == 1 ? true : '' ), ['data-toggle' => 'toggle', 'data-on' => 'เปิด', 'data-off' => 'ปิด', 'data-onstyle' => 'success', 'data-offstyle' => 'danger', 'data-size' => 'sm']) }}
                </div>
            </div>
        </div>
    </div>
    <div class="card mb-4">
        <div class="card-header pb-2 d-flex justify-content-between">
            <p class="font-weight-bold mb-0">รายละเอียดวาระการประชุม</p> 
            <button type="button" class="btn btn-sm btn-secondary list-term-btn">เพิ่มวาระ</button>
        </div>
        <div class="card-body">
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="card border-0">
                        <ul class="list-group list-term main-agenda"></ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <p class="font-weight-bold mb-0">ลายน้ำ</p> 
        </div>
        <div class="card-body">
            <div class="form-group row">
                {!! Html::decode(Form::label('text_watermark', 'ลายน้ำในการส่งออกข้อมูล :', ['class' => 'col-sm-4 col-form-label text-right'])) !!}
                <div class="col-sm-5">
                    {{ Form::text('text_watermark', old('text_watermark') ? old('text_watermark') : ( !empty($result['data']->text_watermark) ? $result['data']->text_watermark : '' ), ['class' => 'form-control '.($errors->has('text_watermark') ? 'is-invalid' : ''), 'placeholder' => 'กรุณาระบุข้อมูล', 'autocomplete' => "off", 'required' => true]) }}
                    <span class="form-group text-danger vld validate-text_watermark" style="display:none">
                        <small><i class="validate-text_watermark-i"></i></small>
                    </span>
                </div>
                <div class="col-sm-1">
                    <label for="btn-file-upload" class="btn-upload mb-0"><i class="fas fa-file-upload"></i> เลือก</label>
                </div>
            </div>
            <div class="form-group row">
                <div class="offset-sm-4 col-sm-6">
                    <input type="file" id="btn-file-upload" name="fileToUpload" hidden/>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-between">
                <a role="button" class="btn btn-dark" href="{{ route('pages.template.index') }}"><i class="far fa-arrow-alt-circle-left"></i> ย้อนกลับ</a>
                <button type="submit" class="btn btn-success"><i class="far fa-save"></i> บันทึก</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@section('script')
<script type="text/javascript">

    String.prototype.hashCodeStringOnly = function () {
        var length              = 20;
        var result              = '';
        var characters          = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength    = characters.length;

        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

   $(document).ready( function () {

        var my_edit = JSON.parse('{!! !empty($result["template_issues"]) ? $result["template_issues"] : "[]" !!}');

        if(my_edit.length > 0) {
            $.each(my_edit, function (i, e) {
                create_term(e.title);
            });
        }

        $('.toggle-group').find('label').removeAttr('for');

        $('.list-term-btn').on('click', function () {
            create_term();
        });

        $("#form-submit").on('submit', function (e) {

            e.preventDefault();

            var self = $(this);
            var _btn = self.find("button[type='submit']");

            var _span = $('<span/>').addClass('spinner-border spinner-border-sm mb-2').attr({'role' : 'status', 'aria-hidden' : true});
            var _span_txt = $('<span/>').append('&nbsp;Loading...');

            var _i_default = $('<i/>').addClass('far fa-save');

            _btn.attr('disabled', false);
            
            _btn.attr('disabled', true);
            _btn.find('i').remove();
            _btn.text('');
            _btn.append(_span);
            _btn.append(_span_txt);

            var method 		= $(this).attr('method');
            var url 		= $(this).attr('action');
            var data 		= new FormData(this);

            $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData: false,
                success		: function(data) {

                    var element = null;
                    if(data.errors) {

                        $(".back").click();

                        $.each(data.errors, function(key, value) {

                            if(data.rules[key]) {
                                delete data.rules[key];
                            }

                            $(".validate-" + key).show();
                            $(".validate-" + key + "-i").text(value);
                        });

                        element = Object.keys(data.errors)[0];
                    }

                    if(data.rules) {
                        $.each(data.rules, function(key, value){
                            $(".validate-" + key).hide();
                        });
                    }

                    if(element) {
                        $("*[name='" + element + "']").focus();
                        return;
                    }

                    if(data.status) {

                        $('.vld').hide();

                    }

                    Swal.fire({
                        position: 'center',
                        icon: data.icon,
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                       if(result.isDismissed && data.redirect_url != '') {
                            window.location.href = data.redirect_url;
                       }
                    });

                    _btn.attr('disabled', false);
                    _btn.find('span').remove();
                    _btn.append(_i_default);
                    _btn.append('&nbsp;บันทึก');

                    

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {
                        $('#login_modal').modal('show');
                    },
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {
                        alert("500");

                        _btn.attr('disabled', false);
                        _btn.find('span').remove();
                        _btn.append(_i_default);
                        _btn.append('&nbsp;บันทึก');
                    }			
                }
            });
        });

        $(document).on('click', '.delete-li-btn', function () {
            var id = $(this).data().id;
            var level = $(this).data().level;
            var start = $('.list-term li').index($(this).parents('li'));
            var end = $('.list-term li').index($("[data-to='" + id + "']"));
            var _length = $('.list-term li').length;

            $.each($('.list-term li'), function (i, e) { 
                 if(i >= start && i <= end) {
                     $(this).remove();

                     if(i == end) {
                        $.each($('[data-term]'), function (index, element) {
                            no = index + 1;
                            $(this).data().term = no;

                            $(this).parents('li').find('.title-agenda').text("วาระที่ " + no);
                        });
                     }
                 }
            });
        });

        var jFiler_item = $('<li/>').addClass('jFiler-item p-2');
        var jFiler_item_container = $('<div/>').addClass('jFiler-item-container');
        var jFiler_item_inner = $('<div/>').addClass('jFiler-item-inner');
        var jFiler_item_icon = $('<div/>').addClass('jFiler-item-icon float-left text-dark').text("{"+"{" + "fi-icon" + "}"+"}");
        var jFiler_item_info = $('<div/>').addClass('jFiler-item-info float-left');
        var jFiler_item_title = $('<div/>').addClass('jFiler-item-title').append($('<a/>').attr({'href' : "" ,'download': "{"+"{" + "fi-name" + "}"+"}"}).text("{"+"{" + "fi-name | limitTo:35" + "}"+"}"));
        var jFiler_item_others = $('<div/>').addClass('jFiler-item-others');
        var jFiler_item_status = $('<span/>').addClass('jFiler-item-status');

        var jFiler_item_assets = $('<div/>').addClass('jFiler-item-assets');
        var jFiler_ul = $('<ul/>').addClass('list-inline');
        var jFiler_li = $('<li/>');
        var jFiler_a = $('<a/>').addClass('icon-jfi-trash jFiler-item-trash-action');

        jFiler_li.append(jFiler_a);

        jFiler_ul.append(jFiler_li);

        jFiler_item_assets.append(jFiler_ul);

        jFiler_item_others.append($('<span/>').text("size : {"+"{" + "fi-size2" + "}"+"}"));
        jFiler_item_others.append($('<span/>').text("type : {"+"{" + "fi-extension" + "}"+"}"));
        jFiler_item_others.append(jFiler_item_status);

        jFiler_item_info.append(jFiler_item_title);
        jFiler_item_info.append(jFiler_item_others);
        jFiler_item_info.append(jFiler_item_assets);

        jFiler_item_inner.append(jFiler_item_icon);
        jFiler_item_inner.append(jFiler_item_info);

        jFiler_item_container.append(jFiler_item_inner);

        jFiler_item.append(jFiler_item_container);

        var files = JSON.parse('{!! !empty($result["files"]) ? $result["files"] : "[]" !!}');

        if(files.length > 0) {
            $.each(files, function (i, e) {
                $('#form-submit').append($('<input/>').attr({ 'type' : 'hidden', 'name' : 'file_id[]', 'value' : e.id }));
            });
        }

        $('#btn-file-upload').filer({
            limit: 5,
            maxSize: 10,
            extensions: ["gif", "jpg", "่jpeg", "tif", "png"],
            showThumbs: true,
            templates : {
                item: $('<div>').append(jFiler_item.clone()).html(),
                itemAppend: $('<div>').append(jFiler_item.clone()).html()
            },
            files : files,
            beforeRender : function (itemEl, inputEl) {
                    
                inputEl.change(function() {
                    var fileToUpload = $(this).prop('files');
                    if(fileToUpload.length > 0 ) {
                        Object.keys(fileToUpload).forEach(file => {
                            var lastModified = fileToUpload[file].lastModified;
                            var lastModifiedDate = fileToUpload[file].lastModifiedDate;
                            var name = fileToUpload[file].name;
                            var size = fileToUpload[file].size;
                            var type = fileToUpload[file].type;
                            var fileReader = new FileReader();
                                fileReader.readAsDataURL(fileToUpload[file]);
                                fileReader.onload = function(fileLoadedEvent){
                                    var _a = itemEl.find('ul.jFiler-items-list li.jFiler-item').eq(file).find('.jFiler-item-title a');
                                    _a.attr({'href' : fileReader.result, 'download' : _a.text()});
                                };
                        });
                    }
                });
            },
            onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
                console.log(inputEl)
                if($("input[name^='file_id'][value='" + file.id + "']").length > 0) {
                    $("input[name^='file_id'][value='" + file.id + "']").remove();
                }
            }
        });    

        $('#btn-file-upload').parent().find('.jFiler-input').addClass('d-none');

        $('#btn-file-upload').on('change',function() {
            if($("input[name^='file_id']").length > 0) {
                $.each($("input[name^='file_id']"), function (i, e) { 
                    $(this).remove();
                });
            }
        });

        if(files.length > 0) {
            $.each(files, function (i, e) {
                var _a = $('#form-submit').find('ul.jFiler-items-list li.jFiler-item').eq(i).find('.jFiler-item-title a');
                _a.attr({'href' : e.url, 'download' : e.name});
            });
        }
    });

    function create_term(value = '') {
        var no_term = $('.term-main').length;
            no_term++;
     
        var hash_code = String.prototype.hashCodeStringOnly();
        var title = "วาระที่ " + no_term;
        var li = $('<li/>').addClass('list-group-item border-0');
        var li_break = $('<li/>').attr('data-to', hash_code).addClass('d-none term-main');
        var box = $('<div/>').addClass('row main-agenda border-0');
        var tag = $('<div/>').addClass('input-group-prepend');
        var tag_title = $('<span/>').addClass('input-group-text title-agenda');

        var detail = $('<div/>').addClass('col-sm-10 box-agenda');
        var detail_f = $('<div/>').addClass('col-sm-2 pr-0');

        var div_input_group = $('<div/>').addClass('input-group');

        var input = $('<input/>').attr({'type' : 'text', 'name' : 'template_issues[' + hash_code + '][value]', 'placeholder' : 'กรุณาระบุวาระการประชุม'}).addClass('form-control').val(value);
        var input_level = $('<input/>').attr({'type' : 'hidden', 'name' : 'template_issues[' + hash_code + '][level]', 'value' : 0});

        var dropdown = $('<div/>').addClass('input-group-append dropup');
        var dropdown_btn = $('<button/>').attr({'type' : 'button', 'data-id' : hash_code, 'data-level' : 0, 'data-term' : no_term}).addClass('btn btn-outline-secondary rounded-right delete-li-btn');
        var dropdown_btn_i = $('<i/>').addClass('fas fa-times');
        
            dropdown_btn.append(dropdown_btn_i);

            dropdown.append(dropdown_btn);

            div_input_group.append(input);
            div_input_group.append(input_level);
            div_input_group.append(dropdown);
        
            tag_title.append(title);

            box.append(detail_f.append(tag_title));
            box.append(detail);

            detail.append(div_input_group);

            li.append(box);

            $('.list-term').append(li);
            $('.list-term').append(li_break);

    }
</script>
@endsection