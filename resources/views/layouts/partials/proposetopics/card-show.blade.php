<div class="container">
    <div class="card">
        <div class="card-header">
            @yield('card-header')
        </div>
        <div class="card-body">
            <div class="form-group row">
                {{ Form::label('', 'ชื่อการประชุม :', ['class' => 'col-sm-2 col-form-label text-right']) }}
                <div class="col-sm-3">
                    <span class="form-control-plaintext">{{ !empty($result['data']->meeting_name) ? $result['data']->meeting_name : '' }}</span>
                </div>
                {{ Form::label('', 'วันที่และเวลาประชุม :', ['class' => 'col-sm-2 col-form-label text-right']) }}
                <div class="col-sm-5">
                    <span class="form-control-plaintext">{{ !empty($result['data']->date) ? $result['data']->date : '' }}</span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <hr>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="card border-0">
                        <table class="table table-bordered" cellspacing="0" width="100%">
                            <thead class="thead-light">
                                <tr>
                                    <th>ชื่อหัวข้อการประชุม</th>
                                    <th>รายละเอียด</th>
                                    <th>เอกสารแนบประกอบ</th>
                                    <th>ผู้เสนอเรื่อง</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-between">
                <a role="button" class="btn btn-dark" href="{{ route('pages.proposetopics.index') }}"><i class="far fa-arrow-alt-circle-left"></i> ย้อนกลับ</a>
                <button type="submit" class="btn btn-success"><i class="far fa-save"></i> บันทึก</button>
            </div>
        </div>
    </div>
</div>
@section('script')
<script type="text/javascript">

    $(document).ready( function () {
        var _dataTable = $("table").DataTable({
            destroy     : true,
            serverSide  : false,
            paging      : false,
            info        : false,
            lengthChange: false,
            searching   : false,
            ordering    : false,
            autoWidth   : true,
            deferLoading: 0,
            language : {
                decimal:        "",
                emptyTable:     "ไม่มีข้อมูลในตาราง",
                info:           "แสดง _START_ ถึง _END_ จาก _TOTAL_ รายการ",
                infoEmpty:      "แสดง 0 ถึง 0 จาก 0 รายการ",
                infoFiltered:   "(กรองจากรายการทั้งหมด _MAX_ รายการ)",
                infoPostFix:    "",
                thousands:      ",",
                lengthMenu:     "แสดง _MENU_ รายการ",
                loadingRecords: "กำลังโหลด...",
                processing:     "กำลังประมวลผล...",
                search:         "ค้นหา:",
                zeroRecords:    "ไม่พบรายการ",
                paginate: {
                    first:      "หน้าแรก",
                    last:       "หน้าสุดท้าย",
                    next:       "ถัดไป",
                    previous:   "ก่อนหน้า"
                },
                aria: {
                    sortAscending:  ": เปิดใช้งานเพื่อจัดเรียงคอลัมน์จากน้อยไปมาก",
                    sortDescending: ": เปิดใช้งานเพื่อเรียงคอลัมน์จากมากไปหาน้อย"
                }
            },
            createdRow: async function ( row, data, index ) {

                var tr = $(row);
                
            },
            drawCallback: function () {
        
            },
            ajax: function(data, callback, settings) {
                
                $.ajaxSetup({
                    url: "{{ route('pages.proposetopics.meetingdatatables') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : "POST",
                    dataType: 'json',
                    error: function(data){
                        if(data.status == 401) {
                            $('#login_modal').modal('show');
                        }
                        console.log('error');
                    },
                    success: function(data){

                        callback({
                            recordsTotal    : data.recordsTotal,
                            recordsFiltered : data.recordsFiltered,
                            data            : data.data
                        });
                    }
                });
                
                data.meeting_id = "{{ ( !empty($result['data']->meeting_id) ? $result['data']->meeting_id : '' ) }}";
                data.is_show    = true;

                $.ajax({ 
                    data: data
                });
            },
            aoColumnDefs: [
                { targets: [ 0, 1, 2, 3 ], orderable: false },
            ],
            columns: [
                null,
                null,
                null,
                null,
            ]
        });

        _dataTable.draw();
    });

</script>
@endsection