<div class="container">
    @if(Request::is('proposetopics/create'))
    {{ Form::open(['route' => 'pages.proposetopics.store', 'method' => 'POST', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif

    @if(Request::is('proposetopics/*/edit'))
    {{ Form::open(['route' => ['pages.proposetopics.update', 'id' => ( !empty($result['data']->id) ? $result['data']->id : '' )], 'method' => 'PATCH', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif
    {{ Form::hidden('lang', 'th') }}

    <div class="card">
        <div class="card-header">
            @yield('card-header')
        </div>
        <div class="card-body">
            <div class="form-group row">
                {{ Form::label('name', 'ชื่อการประชุม :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-8">
                    {{ Form::select('meeting_id', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true , 'data-edit' => ( !empty($result['data']->meeting_id) ? $result['data']->meeting_id : '' ) ]) }}
                </div>
            </div>
            <div class="form-group row">
                {{Form::label('end_date', 'วันที่สิ้นสุดการเสนอเรื่อง :', ['class' => 'col-sm-3 col-form-label text-right'])}}
                <div class="col-sm-3">
                    {{ Form::text('end_date', old('end_date') ? old('end_date') : ( !empty($result['data']->end_date) ? $result['data']->end_date : '' ), ['class' => 'datepicker '.($errors->has('end_date') ? 'is-invalid' : ''), 'placeholder' => 'DD/MM/YYYY', 'autocomplete' => "off", 'required' => true]) }}
                    <span class="form-group text-danger vld validate-end_date" style="display:none">
                        <small><i class="validate-end_date-i"></i></small>
                    </span>
                </div>
                {{Form::label('end_time', 'เวลา :', ['class' => 'col-sm-2 col-form-label text-right'])}}
                <div class="col-sm-3">
                    <div class="form-group">
                        <div class="input-group">
                            {{ Form::text('end_time', old('end_time') ? old('end_time') : ( !empty($result['data']->end_time) ? $result['data']->end_time : '' ), ['class' => 'form-control timepicker '.($errors->has('end_time') ? 'is-invalid' : ''), 'placeholder' => 'DD/MM/YYYY', 'autocomplete' => "off", 'required' => true]) }}
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="far fa-clock"></i></span>
                            </div>
                        </div>
                    </div>
                    <span class="form-group text-danger vld validate-end_time" style="display:none">
                        <small><i class="validate-end_time-i"></i></small>
                    </span>
                </div>
            </div>
            <div class="form-group row div-template">
                {{ Form::label('sort_no', 'ผู้เสนอเรื่อง :', ['class' => 'col-sm-3 col-form-label text-right']) }}
                <div class="col-sm-4">
                    {{ Form::select('organization[]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'data-edit' => ( !empty($result['data']->propose_topic_proponent) && count($result['data']->propose_topic_proponent) > 0 ? \Illuminate\Support\Facades\Crypt::encryptString($result['data']->propose_topic_proponent[0]->organization_id) : '' ) ]) }}
                </div>
                <div class="col-sm-4">
                    <div class="input-group">
                        {{ Form::select('name_option[]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'data-edit' => ( !empty($result['data']->propose_topic_proponent) && count($result['data']->propose_topic_proponent) > 0 ? \Illuminate\Support\Facades\Crypt::encryptString($result['data']->propose_topic_proponent[0]->user_id) : '' )]) }}
                        <div class="input-group-append">
                            <button class="btn btn-success add-topic" type="button"><i class="fas fa-user-plus"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            @if (!empty($result['data']->propose_topic_proponent) && count($result['data']->propose_topic_proponent) > 0)
                @foreach ($result['data']->propose_topic_proponent as $key => $item)
                    @if ($key > 0)
                        <div class="form-group row">
                            <div class="col-sm-4 offset-sm-3">
                                {{ Form::select('organization[]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'data-edit' => \Illuminate\Support\Facades\Crypt::encryptString($item->organization_id) ]) }}
                            </div>
                            <div class="col-sm-4">
                                <div class="input-group">
                                    {{ Form::select('name_option[]', [], null, ['class' => 'form-control', 'autocomplete' => "off", 'required' => true, 'data-edit' => \Illuminate\Support\Facades\Crypt::encryptString($item->user_id) ]) }}
                                    <div class="input-group-append">
                                        <button class="btn btn-danger delete-topic" type="button"><i class="fas fa-user-minus"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @endif
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-between">
                <a role="button" class="btn btn-dark" href="{{ route('pages.proposetopics.index') }}"><i class="far fa-arrow-alt-circle-left"></i> ย้อนกลับ</a>
                <button type="submit" class="btn btn-success"><i class="far fa-save"></i> บันทึก</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@section('script')
<script type="text/javascript">

    String.prototype.hashCodeStringOnly = function () {
        var length              = 20;
        var result              = '';
        var characters          = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength    = characters.length;

        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

   $(document).ready( function () {

        $.each($('.timepicker'), function (i, e) { 
            $(this).datetimepicker({
                format: 'HH:mm',
                icons : {
                    time: 'far fa-clock',
                    date: 'far fa-calendar-alt',
                    up: 'fas fa-chevron-up',
                    down: 'fas fa-chevron-down',
                    previous: 'fas fa-chevron-left',
                    next: 'fas fa-chevron-right',
                }
            });
        });

        gj.core.messages['th-th'] = {
            monthNames: ["มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"],
            monthShortNames: ["ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."],
            weekDaysMin: ["อา", "จ", "อ", "พ", "พฤ", "ศ", "ส"],
            weekDaysShort: ["อา", "จ", "อ", "พ", "พฤ", "ศ", "ส"],
            weekDays: ["อาทิตย์", "จันทร์", "อังคาร", "พุธ", "พฤหัส", "ศุกร์", "เสาร์"],
            ok: 'ตกลง',
            cancel: 'ยกเลิก',
            titleFormat: 'mmmm yyyy'
        };
 
        $.each($('.datepicker'), function (i, e) { 
            var date = new Date();
            $(this).datepicker({
                uiLibrary: 'bootstrap4',
                iconsLibrary: 'fontawesome',
                format: 'dd/mm/yyyy',
                locale: 'th-th',     
                value: $(this).val() ? $(this).val() : ( parseInt(date.getDate(), 10) < 10 ? '0' : '' ) + (date.getDate()).toString() + '/' + ( (parseInt(date.getMonth(), 10) + 1) < 10 ? '0' : '' ) + (parseInt(date.getMonth(), 10) + 1).toString() + '/' + (parseInt(date.getFullYear(), 10) + 543)
            });
        });

        (async () => {
            $("select[name*='meeting_id']").select2({
                data : await selects_meeting_api($("select[name*='meeting_id']").data().edit),
                ajax: {
                    url: "{{ route('pages.meeting.selectsapi') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : "POST",
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                },
                templateSelection: function(data) {
                    console.log(data);

                    return data.text;
                },
                placeholder: {
                    id: '-1',
                    text: 'กรุณาเลือกข้อมูล'
                },
                width: '100%',
                theme: 'bootstrap4',
            });

            $("select[name*='meeting_id']").prop("disabled", false);

            if(!$("select[name*='meeting_id']").data().edit) {
                $("select[name*='meeting_id']").val('-1').trigger('change');
            }
       })();

        $.each($("select[name*='organization']"), async function (i, e) {
            $(this).select2({
                data : await selects_organization_api($(this).data().edit),
                ajax: {
                    url: "{{ route('pages.organizations.selectsapi') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : "POST",
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                },
                templateSelection: function(data) {
                    console.log(data);

                    return data.text;
                },
                placeholder: {
                    id: '-1',
                    text: 'กรุณาเลือกข้อมูล'
                },
                width: '100%',
                theme: 'bootstrap4',
            });

            $(this).prop("disabled", false);

            if(!$(this).data().edit) {
                $(this).val('-1').trigger('change');
            }
        });

        $.each($("select[name*='name_option']"), async function (i, e) {
            console.log($(this).data().edit);
            $(this).select2({
                data : await selectsapi($(this).data().edit),
                ajax: {
                    url: "{{ route('pages.user.selectsapi') }}",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    type : "POST",
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                        }

                        return query;
                    },
                    processResults: function (data) {
                        return {
                            results: data
                        };
                    }
                },
                templateSelection: function(data) {
                    return data.text;
                },
                placeholder: {
                    id: '-1',
                    text: 'กรุณาเลือกข้อมูล'
                },
                width: 'auto',
			    dropdownAutoWidth: true,
                theme: 'bootstrap4',
            });

            $(this).prop("disabled", false);

            if(!$(this).data().edit) {
                $(this).val('-1').trigger('change');
            }
       });

        $('.toggle-group').find('label').removeAttr('for');

        $('input[type=checkbox].icheck-green, input[type=radio].icheck-green').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
     
        $('input[type=checkbox].icheck-yellow, input[type=radio].icheck-yellow').iCheck({
            checkboxClass: 'icheckbox_square-yellow',
            radioClass: 'iradio_square-yellow',
        });
     
        $('input[type=checkbox].icheck-blue, input[type=radio].icheck-blue').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
        });
  
        $('.add-topic').on('click', function () {
            var _div = $(".div-template").clone();
                _div.removeClass('div-template');

            var _box1 = _div.find('.col-sm-4:eq(0)').addClass('offset-sm-3');
            var _box2 = _div.find('.col-sm-4:eq(1)');
                _div.find('label').remove();
                _div.find('.add-topic').removeClass('btn-success add-topic').addClass('btn-danger delete-topic').find('i').removeClass('fas fa-user-plus').addClass('fas fa-user-minus');
                _box1.find('select').remove();
                _box1.find('span').remove();
                _box2.find('select').remove();
                _box2.find('span').remove();

                _box1.append($("<select/>").attr({'name' : 'organization[]', 'autocomplete' : 'off', 'required' : true}).addClass('form-control'));
                _box2.find(".input-group").append($("<select/>").attr({'name' : 'name_option[]', 'autocomplete' : 'off', 'required' : true}).addClass('form-control'));
                _box2.find(".input-group").append(_box2.find(".input-group-append"));

            ( async () => {
                _div.find("select[name*='organization']").select2({
                    data : await selects_organization_api(),
                    ajax: {
                        url: "{{ route('pages.organizations.selectsapi') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type : "POST",
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                            }

                            return query;
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        }
                    },
                    templateSelection: function(data) {
                        return data.text;
                    },
                    placeholder: {
                        id: '-1',
                        text: 'กรุณาเลือกข้อมูล'
                    },
                    width: '100%',
                    theme: 'bootstrap4',
                });

                _div.find("select[name*='organization']").prop("disabled", false);

                if(!_div.find("select[name*='organization']").data().edit) {
                    _div.find("select[name*='organization']").val('-1').trigger('change');
                }
            })();

            ( async () => {
                _div.find("select[name*='name_option']").select2({
                    data : await selectsapi(),
                    ajax: {
                        url: "{{ route('pages.user.selectsapi') }}",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type : "POST",
                        dataType: 'json',
                        data: function (params) {
                            var query = {
                                search: params.term,
                            }

                            return query;
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        }
                    },
                    templateSelection: function(data) {
                        return data.text;
                    },
                    placeholder: {
                        id: '-1',
                        text: 'กรุณาเลือกข้อมูล'
                    },
                    width: 'auto',
                    dropdownAutoWidth: true,
                    theme: 'bootstrap4',
                });

                _div.find("select[name*='name_option']").prop("disabled", false);

                if(!_div.find("select[name*='name_option']").data().edit) {
                    _div.find("select[name*='name_option']").val('-1').trigger('change');
                }
            })();
           

            $(".card-body").append(_div);
        });

        $(document).on('click', '.delete-topic', function () {
            $(this).parents('.form-group').remove();
        });

        $("#form-submit").on('submit', function (e) {

            e.preventDefault();

            var self = $(this);
            var _btn = self.find("button[type='submit']");

            var _span = $('<span/>').addClass('spinner-border spinner-border-sm mb-2').attr({'role' : 'status', 'aria-hidden' : true});
            var _span_txt = $('<span/>').append('&nbsp;Loading...');

            var _i_default = $('<i/>').addClass('far fa-save');

            _btn.attr('disabled', false);
            
            _btn.attr('disabled', true);
            _btn.find('i').remove();
            _btn.text('');
            _btn.append(_span);
            _btn.append(_span_txt);

            var method 		= $(this).attr('method');
            var url 		= $(this).attr('action');
            var data 		= new FormData(this);

            $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData: false,
                success		: function(data) {

                    var element = null;
                    if(data.errors) {

                        $(".back").click();

                        $.each(data.errors, function(key, value) {

                            if(data.rules[key]) {
                                delete data.rules[key];
                            }

                            $(".validate-" + key).show();
                            $(".validate-" + key + "-i").text(value);
                        });

                        element = Object.keys(data.errors)[0];
                    }

                    if(data.rules) {
                        $.each(data.rules, function(key, value){
                            $(".validate-" + key).hide();
                        });
                    }

                    if(element) {
                        $("*[name='" + element + "']").focus();
                        return;
                    }

                    if(data.status) {

                        $('.vld').hide();

                    }

                    Swal.fire({
                        position: 'center',
                        icon: data.icon,
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                       if(result.isDismissed && data.redirect_url != '') {
                            window.location.href = data.redirect_url;
                       }
                    });

                    _btn.attr('disabled', false);
                    _btn.find('span').remove();
                    _btn.append(_i_default);
                    _btn.append('&nbsp;บันทึก');

                    

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {
                        $('#login_modal').modal('show');
                    },
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {
                        alert("500");

                        _btn.attr('disabled', false);
                        _btn.find('span').remove();
                        _btn.append(_i_default);
                        _btn.append('&nbsp;บันทึก');
                    }			
                }
            });
        });
    });

    function selects_meeting_api(val = '') {
        var method 		= "POST";
        var url 		= "{{ route('pages.meeting.selectsapi') }}";
        var data 		= new FormData();
            data.append('selected_id', val);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }

    function selects_organization_api(val = '') {
        var method 		= "POST";
        var url 		= "{{ route('pages.organizations.selectsapi') }}";
        var data 		= new FormData();
            data.append('selected_id', val);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }

    function selectsapi(val = '') {
        var method 		= "POST";
        var url 		= "{{ route('pages.user.selectsapi') }}";
        var data 		= new FormData();
            data.append('selected_id', val);

            return $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                dataType	: "json",
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData	: false,
                success		: function(data) {

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {alert("401");},
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {alert("500");}			
                }        
            });
    }
</script>
@endsection