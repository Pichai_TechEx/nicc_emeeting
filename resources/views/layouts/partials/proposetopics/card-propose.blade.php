<div class="container">
    @if(Request::is('proposetopics/*/propose'))
    {{ Form::open(['route' => ['pages.proposetopics.send', 'id' => ( !empty($result['data']->id) ? $result['data']->id : '' )], 'method' => 'PATCH', 'class' => 'steps', 'id' => 'form-submit', 'enctype' => 'multipart/form-data']) }}
    @endif

    {{ Form::hidden('lang', 'th') }}

    <div class="card">
        <div class="card-header">
            @yield('card-header')
        </div>
        <div class="card-body">
            <div class="form-group row">
                {{ Form::label('', 'ชื่อการประชุม :', ['class' => 'col-sm-2 col-form-label text-right']) }}
                <div class="col-sm-3">
                    <span class="form-control-plaintext">{{ !empty($result['data']->meeting_name) ? $result['data']->meeting_name : '' }}</span>
                </div>
                {{ Form::label('', 'วันที่และเวลาประชุม :', ['class' => 'col-sm-2 col-form-label text-right']) }}
                <div class="col-sm-5">
                    <span class="form-control-plaintext">{{ !empty($result['data']->date) ? $result['data']->date : '' }}</span>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12 text-right">
                    <button type="button" class="btn btn-success list-term-btn"><i class="fas fa-plus"></i> เพิ่มหัวข้อ</button>
                    <hr>
                </div>
                
            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <div class="card border-0">
                        <ul class="list-group list-term main-agenda"></ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-between">
                <a role="button" class="btn btn-dark" href="{{ route('pages.proposetopics.index') }}"><i class="far fa-arrow-alt-circle-left"></i> ย้อนกลับ</a>
                <button type="submit" class="btn btn-success"><i class="far fa-save"></i> บันทึก</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>
@section('script')
<script type="text/javascript">

    String.prototype.hashCodeStringOnly = function () {
        var length              = 20;
        var result              = '';
        var characters          = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        var charactersLength    = characters.length;

        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

   $(document).ready( function () {

        $('.list-term-btn').on('click', function () {
            create_term();
        });

        var my_edit = JSON.parse('{!! !empty($result["data"]->proponent_topic) ? $result["data"]->proponent_topic : "[]" !!}');

        if(my_edit.length > 0) {
            $.each(my_edit, function (i, e) {
                create_term(e.title, e.detail, e.files, e.parent);
            });
        }

        $(document).on('click', '.delete-li-btn', function () {
            var id = $(this).data().id;
            var level = $(this).data().level;
            var start = $('.list-term li').index($(this).parents('li'));
            var end = $('.list-term li').index($("[data-to='" + id + "']"));
            var _length = $('.list-term li').length;

            $.each($('.list-term li'), function (i, e) { 
                 if(i >= start && i <= end) {
                     $(this).remove();

                     if(i == end) {
                        $.each($('[data-term]'), function (index, element) {
                            no = index + 1;
                            $(this).data().term = no;

                            $(this).parents('li').find('.title-agenda').text("ข้อ " + no);

                        });
                     }
                 }
            });
        });


        $("#form-submit").on('submit', function (e) {

            e.preventDefault();

            var self = $(this);
            var _btn = self.find("button[type='submit']");

            var _span = $('<span/>').addClass('spinner-border spinner-border-sm mb-2').attr({'role' : 'status', 'aria-hidden' : true});
            var _span_txt = $('<span/>').append('&nbsp;Loading...');

            var _i_default = $('<i/>').addClass('far fa-save');

            _btn.attr('disabled', false);
            
            _btn.attr('disabled', true);
            _btn.find('i').remove();
            _btn.text('');
            _btn.append(_span);
            _btn.append(_span_txt);

            var method 		= $(this).attr('method');
            var url 		= $(this).attr('action');
            var data 		= new FormData(this);

            $.ajax({
                url			: url,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type		: method,
                data		: data,
                mimeTypes	: "multipart/form-data",
                contentType	: false,
                cache		: false,
                processData: false,
                success		: function(data) {

                    var element = null;
                    if(data.errors) {

                        $(".back").click();

                        $.each(data.errors, function(key, value) {

                            if(data.rules[key]) {
                                delete data.rules[key];
                            }

                            $(".validate-" + key).show();
                            $(".validate-" + key + "-i").text(value);
                        });

                        element = Object.keys(data.errors)[0];
                    }

                    if(data.rules) {
                        $.each(data.rules, function(key, value){
                            $(".validate-" + key).hide();
                        });
                    }

                    if(element) {
                        $("*[name='" + element + "']").focus();
                        return;
                    }

                    if(data.status) {

                        $('.vld').hide();

                    }

                    Swal.fire({
                        position: 'center',
                        icon: data.icon,
                        title: data.message,
                        showConfirmButton: false,
                        timer: 1500
                    }).then((result) => {
                       if(result.isDismissed && data.redirect_url != '') {
                            window.location.href = data.redirect_url;
                       }
                    });

                    _btn.attr('disabled', false);
                    _btn.find('span').remove();
                    _btn.append(_i_default);
                    _btn.append('&nbsp;บันทึก');

                    

                }, statusCode: {
                    400: function(data) {alert("400");},
                    401: function(data) {
                        $('#login_modal').modal('show');
                    },
                    403: function(data) {alert("403");},
                    404: function(data) {alert("404");},
                    500: function(data) {
                        alert("500");

                        _btn.attr('disabled', false);
                        _btn.find('span').remove();
                        _btn.append(_i_default);
                        _btn.append('&nbsp;บันทึก');
                    }			
                }
            });
        });
    });

    function create_term(value = '', value_detail = '', files = []) {
        var no_term = $('.term-main').length;
            no_term++;
     
            var hash_code = String.prototype.hashCodeStringOnly();
            var title = "ข้อ " + no_term;
            var li = $('<li/>').addClass('list-group-item border-0');
            var li_break = $('<li/>').attr('data-to', hash_code).addClass('d-none term-main');
            var box = $('<div/>').addClass('row main-agenda border-0');
            var box_textarea = $('<div/>').addClass('row border-0 mt-3');
            var box_file = $('<div/>').addClass('row border-0 mt-3');
            var tag = $('<div/>').addClass('input-group-prepend');
            var tag_title = $('<span/>').addClass('input-group-text title-agenda');
    
            var detail = $('<div/>').addClass('col-sm-11 box-agenda');
            var detail_f = $('<div/>').addClass('col-sm-1 pr-0');
            var detail_textarea = $('<div/>').attr({'id' : hash_code}).addClass('col-sm-11 offset-sm-1 collapse' + (value_detail ? ' show ' : ''));
            var detail_file = $('<div/>').attr({'id' : 'file' + hash_code}).addClass('col-sm-11 offset-sm-1');

            var div_input_group = $('<div/>').addClass('input-group');
            
            var btn_1 = $('<label/>').attr('for', 'file_' + hash_code).addClass('dropdown-item');
                btn_1.append($('<i/>').addClass('fas fa-file-alt'));
                btn_1.append('&nbsp;แนบไฟล์');
            var btn_2 = $('<a/>').attr({'href' : '#' + hash_code, 'role' : 'button', 'data-toggle' : 'collapse', 'aria-expanded' : false, 'aria-controls' : hash_code}).addClass('dropdown-item detail-btn');
                btn_2.append($('<i/>').addClass('fas fa-comment-dots'));
                btn_2.append('&nbsp;เพิ่มรายละเอียด');
            var btn_3 = $('<a/>').attr({'href' : 'JavaScript:void(0);', 'data-id' : hash_code, 'data-level' : 0, 'data-term' : no_term}).addClass('dropdown-item delete-li-btn');
                btn_3.append($('<i/>').addClass('far fa-times-circle'));
                btn_3.append('&nbsp;ลบหัวข้อ');

            var input = $('<input/>').attr({'type' : 'text', 'name' : 'meeting_issues[' + hash_code + '][value]', 'placeholder' : 'กรุณาระบุวาระการประชุม'}).addClass('form-control').val(value);
            var input_level = $('<input/>').attr({'type' : 'hidden', 'name' : 'meeting_issues[' + hash_code + '][level]', 'value' : 0});

            var textarea_input_group = $('<div/>').addClass('input-group');
            var textarea = $('<textarea/>').attr({'type' : 'text', 'name' : 'meeting_issues[' + hash_code + '][detail]', 'placeholder' : 'กรุณาระบุรายละเอียด'}).addClass('form-control').val(value_detail);

            var file = $('<input/>').attr({'type' : 'file', 'name' : 'meeting_issues[' + hash_code + '][file][]', 'placeholder' : 'กรุณาระบุรายละเอียด', 'id' : 'file_' + hash_code}).addClass('form-control');

            textarea_input_group.append(textarea);

            //
            var jFiler_item = $('<li/>').addClass('jFiler-item');
            var jFiler_item_container = $('<div/>').addClass('jFiler-item-container');
            var jFiler_item_inner = $('<div/>').addClass('jFiler-item-inner');
            var jFiler_item_icon = $('<div/>').addClass('jFiler-item-icon float-left').text("{"+"{" + "fi-icon" + "}"+"}");
            var jFiler_item_info = $('<div/>').addClass('jFiler-item-info float-left');
            var jFiler_item_title = $('<div/>').addClass('jFiler-item-title').text("{"+"{" + "fi-name | limitTo:35" + "}"+"}");
            var jFiler_item_others = $('<div/>').addClass('jFiler-item-others');
            var jFiler_item_status = $('<span/>').addClass('jFiler-item-status');
            var jFiler_item_assets = $('<div/>').addClass('jFiler-item-assets');
            var jFiler_ul = $('<ul/>').addClass('list-inline');
            var jFiler_li = $('<li/>');
            var jFiler_a = $('<a/>').addClass('icon-jfi-trash jFiler-item-trash-action');

            jFiler_li.append(jFiler_a);

            jFiler_ul.append(jFiler_li);

            jFiler_item_assets.append(jFiler_ul);

            jFiler_item_others.append(jFiler_item_status);

            jFiler_item_info.append(jFiler_item_title);
            jFiler_item_info.append(jFiler_item_others);
            jFiler_item_info.append(jFiler_item_assets);

            jFiler_item_inner.append(jFiler_item_icon);
            jFiler_item_inner.append(jFiler_item_info);

            jFiler_item_container.append(jFiler_item_inner);

            jFiler_item.append(jFiler_item_container);
            //

            var dropdown = $('<div/>').addClass('input-group-append dropup');
            var dropdown_btn = $('<button/>').attr({'type' : 'button',  'data-toggle' : 'dropdown', 'aria-haspopup' : true, 'aria-expanded' : false }).addClass('btn btn-outline-secondary rounded-right');
            var dropdown_btn_i = $('<i/>').addClass('fas fa-ellipsis-v');
            var dropdown_menu = $('<div/>').addClass('dropdown-menu').attr({'aria-labelledby' : 'dropdown-option'});
           
            dropdown_menu.append(btn_1);
            dropdown_menu.append(btn_2);
            dropdown_menu.append(btn_3);

            dropdown_btn.append(dropdown_btn_i);

            dropdown.append(dropdown_btn);
            dropdown.append(dropdown_menu);

            div_input_group.append(input);
            div_input_group.append(input_level);
            div_input_group.append(dropdown);
      
            tag_title.append(title);

            box.append(detail_f.append(tag_title));
            box.append(detail);
            box_textarea.append(detail_textarea);
            box_file.append(detail_file);

            detail.append(div_input_group);

            detail_textarea.append(textarea_input_group);

            detail_file.append(file);

            li.append(box);
            li.append(box_textarea);
            li.append(box_file);

            file.filer({
                limit: 5,
                maxSize: 10,
                extensions: ["jpg", "png", "gif"],
                showThumbs: true,
                templates : {
                    item: $('<div>').append(jFiler_item.clone()).html(),
                    itemAppend: $('<div>').append(jFiler_item.clone()).html()
                },
                files : files
            });    

            file.parent().find('.jFiler-input').addClass('d-none');

            $('.list-term').append(li);
            $('.list-term').append(li_break);
     
    }

</script>
@endsection