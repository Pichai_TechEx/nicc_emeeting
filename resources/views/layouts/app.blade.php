<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'ระบบห้องประชุมอิเล็กทรอนิกส์') }}</title>


    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link href="https://cdn.jsdelivr.net/npm/fullcalendar@3.10.2/dist/fullcalendar.min.css" rel="stylesheet">

   <!-- ICON -->

    <!-- emeeting-icon.ico, see above. -->
    <link rel="shortcut icon" href="{{asset('images/logos/emeeting-icon.ico') }}">

    <!-- A 192x192 PNG icon for Android Chrome -->
    <link rel="icon" type="image/png" href="{{asset('images/logos/emeeting-icon-192x192.png') }}" sizes="192x192">
    
    <!-- A 180x180 Apple Touch icon (for iPhone 6 Plus; other device will scale it down as needed). -->
    <link rel="apple-touch-icon" href="{{asset('images/logos/emeeting-icon-180x180.png') }}" sizes="180x180">

    @yield('script')
    <script> 
        $(document).ready( function () {
            
            var height = $(window).height();
            var h1 = typeof $('.navbar-expand-md').outerHeight(true) != 'undefined' ? $('.navbar-expand-md').outerHeight(true) : 0;
            var h2 = typeof $('.breadcrumb').outerHeight(true) != 'undefined' ? $('.breadcrumb').outerHeight(true) : 0;
            var f = $('footer').outerHeight(true);

            height = height - h1 - h2 - f;
            
            $('main').css('min-height', height);
        });
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark navbar-laravel">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'ระบบห้องประชุมอิเล็กทรอนิกส์') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="{{ route('home') }}">หน้าหลัก</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                บริหารการประชุม
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('pages.proposetopics.index') }}">เสนอหัวข้อการประชุม</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('pages.meetingpermissions.index') }}">เผยแพร่หัวข้อการประชุม</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                รายงาน
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('pages.meetingstatisticsreport.index') }}">รายงานสถิติการจัดประชุม</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                ตั้งค่าทั่วไป
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('pages.system.index') }}">ตั้งค่าระบบ</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('pages.organizations.index') }}">ตั้งค่าหน่วยงาน</a>
                                <a class="dropdown-item" href="{{ route('pages.usertype.index') }}">ตั้งค่าประเภทผู้ใช้งาน</a>
                                <a class="dropdown-item" href="{{ route('pages.user.index') }}">ตั้งค่าผู้ใช้งาน</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('pages.boards.index') }}">ตั้งค่าคณะกรรมการ</a>
                                <a class="dropdown-item" href="{{ route('pages.attendances.index') }}">ตั้งค่าผู้เข้าร่วมประชุม</a>
                                <a class="dropdown-item" href="{{ route('pages.template.index') }}">ตั้งค่าเทมเพลตวาระการประชุม</a>
                            </div>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">Login</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">Register</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->username }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        <nav aria-label="breadcrumb">
            @yield('breadcrumbs')
        </nav>
        <main class="py-4">
            @yield('content')
        </main>
        @include('layouts.footer')
    </div>
</body>
</html>

