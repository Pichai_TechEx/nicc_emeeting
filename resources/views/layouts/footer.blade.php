  <!-- Site footer -->
  <footer class="site-footer">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 text-center">
          <p class="copyright-text">Copyright &copy; 2017 All Rights Reserved by 
       <a href="#">TechEx</a>.
          </p>
        </div>
      </div>
    </div>
</footer>