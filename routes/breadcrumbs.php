<?php
// errors_404
Breadcrumbs::register('errors_404', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('404 Error Page');
});

// errors_500
Breadcrumbs::register('errors_500', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('500 Error Page');
});

// errors_520
Breadcrumbs::register('errors_520', function ($breadcrumbs) {
    $breadcrumbs->parent('home');
    $breadcrumbs->push('520 Error Page');
});

Breadcrumbs::register('home', function ($breadcrumbs) {
    $breadcrumbs->push('Home', route('home'));
});

// ข้อมูลการประชุม
Breadcrumbs::register('meeting_index', function ($breadcrumbs) {
    $breadcrumbs->push('ข้อมูลการประชุม', route('pages.meeting.index'));
});

// ข้อมูลการประชุม > สร้าง
Breadcrumbs::register('meeting_create', function ($breadcrumbs) {
    $breadcrumbs->parent('meeting_index');
    $breadcrumbs->push('สร้าง', route('pages.meeting.create'));
});

// ข้อมูลการประชุม > แก้ไข
Breadcrumbs::register('meeting_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('meeting_index');
    $breadcrumbs->push('แก้ไข', route('pages.meeting.edit',['id' => $id]));
});

// ข้อมูลการประชุม > ลงมติ
Breadcrumbs::register('meeting_voter', function ($breadcrumbs, $id, $title) {
    $breadcrumbs->parent('meeting_index');
    $breadcrumbs->push('ลงมติ', route('pages.meeting.voter',['id' => $id, 'title' => $title]));
});

// ข้อมูลการประชุม > ลงมติ
Breadcrumbs::register('meeting_voter_success', function ($breadcrumbs) {
    $breadcrumbs->parent('meeting_index');
    $breadcrumbs->push('ลงมติ');
    $breadcrumbs->push('สำเร็จ');
});

// ตั้งค่าทั่วไป > ตำแหน่งในที่ประชุม
Breadcrumbs::register('positioninmeeting_index', function ($breadcrumbs) {
    $breadcrumbs->push('ตั้งค่าทั่วไป');
    $breadcrumbs->push('ตำแหน่งในที่ประชุม', route('pages.positioninmeeting.index'));
});

// ตั้งค่าทั่วไป > ตำแหน่งในที่ประชุม > สร้าง
Breadcrumbs::register('positioninmeeting_create', function ($breadcrumbs) {
    $breadcrumbs->parent('positioninmeeting_index');
    $breadcrumbs->push('สร้าง', route('pages.positioninmeeting.create'));
});

// ตั้งค่าทั่วไป > ตำแหน่งในที่ประชุม > แก้ไข
Breadcrumbs::register('positioninmeeting_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('positioninmeeting_index');
    $breadcrumbs->push('แก้ไข', route('pages.positioninmeeting.edit',['id' => $id]));
});

// ตั้งค่าทั่วไป > ตั้งค่าหน่วยงาน
Breadcrumbs::register('organizations_index', function ($breadcrumbs) {
    $breadcrumbs->push('ตั้งค่าทั่วไป');
    $breadcrumbs->push('ตั้งค่าหน่วยงาน', route('pages.organizations.index'));
});

// ตั้งค่าทั่วไป > ตั้งค่าหน่วยงาน > เพิ่มหน่วยงาน
Breadcrumbs::register('organizations_create', function ($breadcrumbs) {
    $breadcrumbs->parent('organizations_index');
    $breadcrumbs->push('เพิ่มหน่วยงาน', route('pages.organizations.create'));
});

// ตั้งค่าทั่วไป > ตั้งค่าหน่วยงาน > แก้ไขหน่วยงาน
Breadcrumbs::register('organizations_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('organizations_index');
    $breadcrumbs->push('แก้ไขหน่วยงาน', route('pages.organizations.edit',['id' => $id]));
});

// ตั้งค่าทั่วไป > คำนำหน้าชื่อ
Breadcrumbs::register('titles_index', function ($breadcrumbs) {
    $breadcrumbs->push('ตั้งค่าทั่วไป');
    $breadcrumbs->push('คำนำหน้าชื่อ', route('pages.titles.index'));
});

// ตั้งค่าทั่วไป > คำนำหน้าชื่อ > สร้าง
Breadcrumbs::register('titles_create', function ($breadcrumbs) {
    $breadcrumbs->parent('titles_index');
    $breadcrumbs->push('สร้าง', route('pages.titles.create'));
});

// ตั้งค่าทั่วไป > คำนำหน้าชื่อ > แก้ไข
Breadcrumbs::register('titles_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('titles_index');
    $breadcrumbs->push('แก้ไข', route('pages.titles.edit',['id' => $id]));
});

// ตั้งค่าทั่วไป > เมนู
Breadcrumbs::register('menus_index', function ($breadcrumbs) {
    $breadcrumbs->push('ตั้งค่าทั่วไป');
    $breadcrumbs->push('เมนู', route('pages.menus.index'));
});

// ตั้งค่าทั่วไป > เมนู > สร้าง
Breadcrumbs::register('menus_create', function ($breadcrumbs) {
    $breadcrumbs->parent('menus_index');
    $breadcrumbs->push('สร้าง', route('pages.menus.create'));
});

// ตั้งค่าทั่วไป > เมนู > แก้ไข
Breadcrumbs::register('menus_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('menus_index');
    $breadcrumbs->push('แก้ไข', route('pages.menus.edit',['id' => $id]));
});

// ตั้งค่าทั่วไป > ตั้งค่าประเภทผู้ใช้งาน
Breadcrumbs::register('usertype_index', function ($breadcrumbs) {
    $breadcrumbs->push('ตั้งค่าทั่วไป');
    $breadcrumbs->push('ตั้งค่าประเภทผู้ใช้งาน', route('pages.usertype.index'));
});

// ตั้งค่าทั่วไป > ตั้งค่าประเภทผู้ใช้งาน > สร้าง
Breadcrumbs::register('usertype_create', function ($breadcrumbs) {
    $breadcrumbs->parent('usertype_index');
    $breadcrumbs->push('สร้าง', route('pages.usertype.create'));
});

// ตั้งค่าทั่วไป > ตั้งค่าประเภทผู้ใช้งาน > แก้ไข
Breadcrumbs::register('usertype_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('usertype_index');
    $breadcrumbs->push('แก้ไข', route('pages.usertype.edit',['id' => $id]));
});

// ตั้งค่าทั่วไป > ตั้งค่าผู้ใช้งาน
Breadcrumbs::register('user_index', function ($breadcrumbs) {
    $breadcrumbs->push('ตั้งค่าทั่วไป');
    $breadcrumbs->push('ตั้งค่าผู้ใช้งาน', route('pages.user.index'));
});

// ตั้งค่าทั่วไป > ตั้งค่าผู้ใช้งาน > สร้าง
Breadcrumbs::register('user_create', function ($breadcrumbs) {
    $breadcrumbs->parent('user_index');
    $breadcrumbs->push('สร้าง', route('pages.user.create'));
});

// ตั้งค่าทั่วไป > ตั้งค่าผู้ใช้งาน > แก้ไข
Breadcrumbs::register('user_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('user_index');
    $breadcrumbs->push('แก้ไข', route('pages.user.edit',['id' => $id]));
});

// ตั้งค่าทั่วไป > ตั้งค่าผู้เข้าร่วมประชุม
Breadcrumbs::register('attendances_index', function ($breadcrumbs) {
    $breadcrumbs->push('ตั้งค่าทั่วไป');
    $breadcrumbs->push('ตั้งค่าผู้เข้าร่วมประชุม', route('pages.attendances.index'));
});

// ตั้งค่าทั่วไป > ตั้งค่าผู้เข้าร่วมประชุม > สร้าง
Breadcrumbs::register('attendances_create', function ($breadcrumbs) {
    $breadcrumbs->parent('attendances_index');
    $breadcrumbs->push('สร้าง', route('pages.attendances.create'));
});

// ตั้งค่าทั่วไป > ตั้งค่าผู้เข้าร่วมประชุม > แก้ไข
Breadcrumbs::register('attendances_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('attendances_index');
    $breadcrumbs->push('แก้ไข', route('pages.attendances.edit',['id' => $id]));
});

// ตั้งค่าทั่วไป > ตั้งค่าคณะกรรมการ
Breadcrumbs::register('boards_index', function ($breadcrumbs) {
    $breadcrumbs->push('ตั้งค่าทั่วไป');
    $breadcrumbs->push('ตั้งค่าคณะกรรมการ', route('pages.boards.index'));
});

// ตั้งค่าทั่วไป > ตั้งค่าคณะกรรมการ > สร้างคณะกรรมการ
Breadcrumbs::register('boards_create', function ($breadcrumbs) {
    $breadcrumbs->parent('boards_index');
    $breadcrumbs->push('สร้างคณะกรรมการ', route('pages.boards.create'));
});

// ตั้งค่าทั่วไป > ตั้งค่าคณะกรรมการ > แก้ไขคณะกรรมการ
Breadcrumbs::register('boards_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('boards_index');
    $breadcrumbs->push('แก้ไขคณะกรรมการ', route('pages.boards.edit',['id' => $id]));
});

// ตั้งค่าทั่วไป > ตั้งค่าเทมเพลตวาระการประชุม
Breadcrumbs::register('template_index', function ($breadcrumbs) {
    $breadcrumbs->push('ตั้งค่าทั่วไป');
    $breadcrumbs->push('ตั้งค่าเทมเพลตวาระการประชุม', route('pages.template.index'));
});

// ตั้งค่าทั่วไป > ตั้งค่าเทมเพลตวาระการประชุม > เพิ่มเทมเพลต
Breadcrumbs::register('template_create', function ($breadcrumbs) {
    $breadcrumbs->parent('template_index');
    $breadcrumbs->push('เพิ่มเทมเพลต', route('pages.template.create'));
});

// ตั้งค่าทั่วไป > ตั้งค่าเทมเพลตวาระการประชุม > แก้ไขเทมเพลต
Breadcrumbs::register('template_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('template_index');
    $breadcrumbs->push('แก้ไขเทมเพลต', route('pages.template.edit',['id' => $id]));
});

// บริหารการประชุม > เผยแพร่หัวข้อการประชุม
Breadcrumbs::register('meetingpermissions_index', function ($breadcrumbs) {
    $breadcrumbs->push('บริหารการประชุม');
    $breadcrumbs->push('เผยแพร่หัวข้อการประชุม', route('pages.meetingpermissions.index'));
});

// บริหารการประชุม > เผยแพร่หัวข้อการประชุม > เพิ่มการเผยแพร่หัวข้อการประชุม
Breadcrumbs::register('meetingpermissions_create', function ($breadcrumbs) {
    $breadcrumbs->parent('meetingpermissions_index');
    $breadcrumbs->push('เพิ่มการเผยแพร่หัวข้อการประชุม', route('pages.meetingpermissions.create'));
});

// บริหารการประชุม > เผยแพร่หัวข้อการประชุม > แก้ไข
Breadcrumbs::register('meetingpermissions_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('meetingpermissions_index');
    $breadcrumbs->push('แก้ไขการเผยแพร่หัวข้อการประชุม', route('pages.meetingpermissions.edit',['id' => $id]));
});

// บริหารการประชุม > การเสนอหัวข้อการประชุม
Breadcrumbs::register('proposetopics_index', function ($breadcrumbs) {
    $breadcrumbs->push('บริหารการประชุม');
    $breadcrumbs->push('การเสนอหัวข้อการประชุม', route('pages.proposetopics.index'));
});

// บริหารการประชุม > การเสนอหัวข้อการประชุม > เพิ่มเสนอหัวข้อการประชุม
Breadcrumbs::register('proposetopics_create', function ($breadcrumbs) {
    $breadcrumbs->parent('proposetopics_index');
    $breadcrumbs->push('เพิ่มเสนอหัวข้อการประชุม', route('pages.proposetopics.create'));
});

// บริหารการประชุม > การเสนอหัวข้อการประชุม > เสนอหัวข้อ
Breadcrumbs::register('proposetopics_propose', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('proposetopics_index');
    $breadcrumbs->push('เสนอหัวข้อ', route('pages.proposetopics.propose', ['id' => $id]));
});

// บริหารการประชุม > การเสนอหัวข้อการประชุม > แก้ไขเสนอหัวข้อการประชุม
Breadcrumbs::register('proposetopics_edit', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('proposetopics_index');
    $breadcrumbs->push('แก้ไข', route('pages.proposetopics.edit',['id' => $id]));
});

// บริหารการประชุม > การเสนอหัวข้อการประชุม > หัวข้อที่ถูกเสนอ
Breadcrumbs::register('proposetopics_show', function ($breadcrumbs, $id) {
    $breadcrumbs->parent('proposetopics_index');
    $breadcrumbs->push('หัวข้อที่ถูกเสนอ', route('pages.proposetopics.show',['id' => $id]));
});

// รายงาาน > รายงานสถิติการจัดประชุม
Breadcrumbs::register('meetingstatisticsreport_index', function ($breadcrumbs) {
    $breadcrumbs->push('รายงาาน');
    $breadcrumbs->push('รายงานสถิติการจัดประชุม', route('pages.meetingstatisticsreport.index'));
});

// ตั้งค่าทั่วไป > ระบบ
Breadcrumbs::register('system_index', function ($breadcrumbs) {
    $breadcrumbs->push('ตั้งค่าทั่วไป');
    $breadcrumbs->push('ตั้งค่าระบบ', route('pages.system.index'));
});