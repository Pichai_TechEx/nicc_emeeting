<?php

use Illuminate\Http\Request;
use TechEx\{Menu, MenusPermissions, MeetingIssues};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    try {
        if(Menu::all()->count() > 0) {
            foreach (Menu::all() as $key => $menu) {
                $path_url = strtolower(str_replace('Controller', '', $menu->controller));
              
                Route::get("/{$path_url}", function(Request $request) use ($menu) {

                    $user       = $request->user();
                    $roles      = $user->roles()->first();

                    if($roles) {

                        $middleware = "role:".$roles->id;
                                
                        Route::middleware([$middleware])->group(function () use ($menu, $roles) {
                            $menus_permissions = $roles->roles_menus_permissions()->where('menu_id', $menu->id)->get();
                            $permission_txt = "permission:";
                            if(!empty($menus_permissions) &&  count($menus_permissions) > 0) {
                                foreach ($menus_permissions as $key => $permission) {
                                $slug = $permission->permission->slug;
                                   $permission_txt .= $slug.(((count($menus_permissions)-1) != $key) ? ',' : '');
                                }
                            }

                            Route::middleware([$permission_txt])->group(function() use ($menu) {});
                        });

                        if(!empty($roles->roles_menus_permissions()->where('menu_id', $menu->id)->first())) {
                                        
                            $route = "TechEx/\/Http/\/Controllers";
                            $route .= '/\/';
                            $route = str_replace('/', '', $route);
                            $route .= $menu->controller."@index";
                          
                            return \App::call($route);
                        } else {
                            abort(404);
                        }
                    }
                })->name("pages.{$path_url}.index");

                Route::get("/{$path_url}/create", function(Request $request) use ($menu) {

                    $user       = $request->user();
                    $roles      = $user->roles()->first();

                    if($roles) {

                        $middleware = "role:".$roles->id;
                                
                        Route::middleware([$middleware])->group(function () use ($menu, $roles) {

                            $menus_permissions = $roles->roles_menus_permissions()->where('menu_id', $menu->id)->get();
                            $permission_txt = "permission:";
                            if(!empty($menus_permissions) &&  count($menus_permissions) > 0) {
                                foreach ($menus_permissions as $key => $permission) {
                                    $slug = $permission->permission->slug;
                                   $permission_txt .= $slug.(((count($menus_permissions)-1) != $key) ? ',' : '');
                                }
                            }

                            Route::middleware([$permission_txt])->group(function() use ($menu) {});
                        });

                        if(!empty($roles->roles_menus_permissions()->where('menu_id', $menu->id)->first())) {
                                        
                            $route = "TechEx/\/Http/\/Controllers";
                            $route .= '/\/';
                            $route = str_replace('/', '', $route);
                            $route .= $menu->controller."@create";
                          
                            return \App::call($route);
                        } else {
                            abort(404);
                        }
                    }
                })->name("pages.{$path_url}.create");

                Route::post("/{$path_url}/store", function(Request $request) use ($menu) {

                    $user       = $request->user();
                    $roles      = $user->roles()->first();

                    if($roles) {

                        $middleware = "role:".$roles->id;
                                
                        Route::middleware([$middleware])->group(function () use ($menu, $roles) {

                            $menus_permissions = $roles->roles_menus_permissions()->where('menu_id', $menu->id)->get();
                            $permission_txt = "permission:";
                            if(!empty($menus_permissions) &&  count($menus_permissions) > 0) {
                                foreach ($menus_permissions as $key => $permission) {
                                    $slug = $permission->permission->slug;
                                   $permission_txt .= $slug.(((count($menus_permissions)-1) != $key) ? ',' : '');
                                }
                            }

                            Route::middleware([$permission_txt])->group(function() use ($menu) {});
                        });

                        if(!empty($roles->roles_menus_permissions()->where('menu_id', $menu->id)->first())) {
                                        
                            $route = "TechEx/\/Http/\/Controllers";
                            $route .= '/\/';
                            $route = str_replace('/', '', $route);
                            $route .= $menu->controller."@store";
                          
                            return \App::call($route);
                        } else {
                            abort(404);
                        }
                    }
                })->name("pages.{$path_url}.store");

                Route::get("/{$path_url}/{id}", function(Request $request) use ($menu) {

                    $user       = $request->user();
                    $roles      = $user->roles()->first();

                    if($roles) {

                        $middleware = "role:".$roles->id;
                                
                        Route::middleware([$middleware])->group(function () use ($menu, $roles) {

                            $menus_permissions = $roles->roles_menus_permissions()->where('menu_id', $menu->id)->get();
                            $permission_txt = "permission:";
                            if(!empty($menus_permissions) &&  count($menus_permissions) > 0) {
                                foreach ($menus_permissions as $key => $permission) {
                                    $slug = $permission->permission->slug;
                                   $permission_txt .= $slug.(((count($menus_permissions)-1) != $key) ? ',' : '');
                                }
                            }

                            Route::middleware([$permission_txt])->group(function() use ($menu) {});
                        });

                        if(!empty($roles->roles_menus_permissions()->where('menu_id', $menu->id)->first())) {
                                        
                            $route = "TechEx/\/Http/\/Controllers";
                            $route .= '/\/';
                            $route = str_replace('/', '', $route);
                            $route .= $menu->controller."@show";
                          
                            return \App::call($route);
                        } else {
                            abort(404);
                        }
                    }
                })->name("pages.{$path_url}.show");

                Route::get("/{$path_url}/{id}/edit", function(Request $request) use ($menu) {
                  
                    $user       = $request->user();
                    $roles      = $user->roles()->first();

                    if($roles) {

                        $middleware = "role:".$roles->id;
                                
                        Route::middleware([$middleware])->group(function () use ($menu, $roles) {

                            $menus_permissions = $roles->roles_menus_permissions()->where('menu_id', $menu->id)->get();
                            $permission_txt = "permission:";
                            if(!empty($menus_permissions) &&  count($menus_permissions) > 0) {
                                foreach ($menus_permissions as $key => $permission) {
                                    $slug = $permission->permission->slug;
                                   $permission_txt .= $slug.(((count($menus_permissions)-1) != $key) ? ',' : '');
                                }
                            }

                            Route::middleware([$permission_txt])->group(function() use ($menu) {});
                        });

                        if(!empty($roles->roles_menus_permissions()->where('menu_id', $menu->id)->first())) {
                                        
                            $route = "TechEx/\/Http/\/Controllers";
                            $route .= '/\/';
                            $route = str_replace('/', '', $route);
                            $route .= $menu->controller."@edit";
                          
                            return \App::call($route, ['id' => $request->id]);
                        } else {
                            abort(404);
                        }
                    }
                })->name("pages.{$path_url}.edit");

                Route::patch("/{$path_url}/{id}", function(Request $request) use ($menu) {

                    $user       = $request->user();
                    $roles      = $user->roles()->first();

                    if($roles) {

                        $middleware = "role:".$roles->id;
                                
                        Route::middleware([$middleware])->group(function () use ($menu, $roles) {

                            $menus_permissions = $roles->roles_menus_permissions()->where('menu_id', $menu->id)->get();
                            $permission_txt = "permission:";
                            if(!empty($menus_permissions) &&  count($menus_permissions) > 0) {
                                foreach ($menus_permissions as $key => $permission) {
                                    $slug = $permission->permission->slug;
                                   $permission_txt .= $slug.(((count($menus_permissions)-1) != $key) ? ',' : '');
                                }
                            }

                            Route::middleware([$permission_txt])->group(function() use ($menu) {});
                        });

                        if(!empty($roles->roles_menus_permissions()->where('menu_id', $menu->id)->first())) {
                                        
                            $route = "TechEx/\/Http/\/Controllers";
                            $route .= '/\/';
                            $route = str_replace('/', '', $route);
                            $route .= $menu->controller."@update";
                          
                            return \App::call($route, ['id' => $request->id]);
                        } else {
                            abort(404);
                        }
                    }
                })->name("pages.{$path_url}.update");

                Route::delete("/{$path_url}/{id}", function(Request $request) use ($menu) {

                    $user       = $request->user();
                    $roles      = $user->roles()->first();

                    if($roles) {

                        $middleware = "role:".$roles->id;
                                
                        Route::middleware([$middleware])->group(function () use ($menu, $roles) {

                            $menus_permissions = $roles->roles_menus_permissions()->where('menu_id', $menu->id)->get();
                            $permission_txt = "permission:";
                            if(!empty($menus_permissions) &&  count($menus_permissions) > 0) {
                                foreach ($menus_permissions as $key => $permission) {
                                    $slug = $permission->permission->slug;
                                   $permission_txt .= $slug.(((count($menus_permissions)-1) != $key) ? ',' : '');
                                }
                            }

                            Route::middleware([$permission_txt])->group(function() use ($menu) {});
                        });

                        if(!empty($roles->roles_menus_permissions()->where('menu_id', $menu->id)->first())) {
                                        
                            $route = "TechEx/\/Http/\/Controllers";
                            $route .= '/\/';
                            $route = str_replace('/', '', $route);
                            $route .= $menu->controller."@destroy";
                          
                            return \App::call($route, ['id' => $request->id]);
                        } else {
                            abort(404);
                        }
                    }
                })->name("pages.{$path_url}.destroy");

                Route::post("/{$path_url}/datatables", function(Request $request) use ($menu) {

                    $user       = $request->user();
                    $roles      = $user->roles()->first();

                    if($roles) {

                        $middleware = "role:".$roles->id;
                                
                        Route::middleware([$middleware])->group(function () use ($menu, $roles) {

                            $menus_permissions = $roles->roles_menus_permissions()->where('menu_id', $menu->id)->get();
                            $permission_txt = "permission:";
                            if(!empty($menus_permissions) &&  count($menus_permissions) > 0) {
                                foreach ($menus_permissions as $key => $permission) {
                                    $slug = $permission->permission->slug;
                                   $permission_txt .= $slug.(((count($menus_permissions)-1) != $key) ? ',' : '');
                                }
                            }

                            Route::middleware([$permission_txt])->group(function() use ($menu) {});
                        });

                        if(!empty($roles->roles_menus_permissions()->where('menu_id', $menu->id)->first())) {
                                        
                            $route = "TechEx/\/Http/\/Controllers";
                            $route .= '/\/';
                            $route = str_replace('/', '', $route);
                            $route .= $menu->controller."@datatables";
                          
                            return \App::call($route);
                        } else {
                            abort(404);
                        }
                    }
                })->name("pages.{$path_url}.datatables");
            }
        }
    } catch (\Exception $e) {
        return [];
    }
  
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
    
    // MeetingController
    Route::get('/meeting', 'HomeController@index')->name('pages.meeting.index');
    Route::get('/meeting/create', 'MeetingController@create')->name('pages.meeting.create');
    Route::post('/meeting', 'MeetingController@store')->name('pages.meeting.store');
    Route::get('/meeting/{id}', 'MeetingController@show')->name('pages.meeting.show');
    Route::get('/meeting/{id}/edit', 'MeetingController@edit')->name('pages.meeting.edit');
    Route::patch('/meeting/{id}', 'MeetingController@update')->name('pages.meeting.update');
    Route::delete('/meeting/{id}', 'MeetingController@destroy')->name('pages.meeting.destroy');
    Route::post('/meeting/datatables', 'MeetingController@datatables')->name('pages.meeting.datatables');
    Route::post('/meeting/sendmail', 'MeetingController@sendmail')->name('pages.meeting.sendmail');
    
    // MeetingPermissionsController
    Route::get('/meetingpermissions', 'MeetingPermissionsController@index')->name('pages.meetingpermissions.index');
    Route::get('/meetingpermissions/create', 'MeetingPermissionsController@create')->name('pages.meetingpermissions.create');
    Route::post('/meetingpermissions', 'MeetingPermissionsController@store')->name('pages.meetingpermissions.store');
    Route::get('/meetingpermissions/{id}', 'MeetingPermissionsController@show')->name('pages.meetingpermissions.show');
    Route::get('/meetingpermissions/{id}/edit', 'MeetingPermissionsController@edit')->name('pages.meetingpermissions.edit');
    Route::patch('/meetingpermissions/{id}', 'MeetingPermissionsController@update')->name('pages.meetingpermissions.update');
    Route::delete('/meetingpermissions/{id}', 'MeetingPermissionsController@destroy')->name('pages.meetingpermissions.destroy');
    Route::post('/meetingpermissions/datatables', 'MeetingPermissionsController@datatables')->name('pages.meetingpermissions.datatables');

    // // PositionInMeetingController
    // Route::get('/positioninmeeting', 'PositionInMeetingController@index')->name('pages.positioninmeeting.index');
    // Route::get('/positioninmeeting/create', 'PositionInMeetingController@create')->name('pages.positioninmeeting.create');
    // Route::post('/positioninmeeting', 'PositionInMeetingController@store')->name('pages.positioninmeeting.store');
    // Route::get('/positioninmeeting/{id}', 'PositionInMeetingController@show')->name('pages.positioninmeeting.show');
    // Route::get('/positioninmeeting/{id}/edit', 'PositionInMeetingController@edit')->name('pages.positioninmeeting.edit');
    // Route::patch('/positioninmeeting/{id}', 'PositionInMeetingController@update')->name('pages.positioninmeeting.update');
    // Route::delete('/positioninmeeting/{id}', 'PositionInMeetingController@destroy')->name('pages.positioninmeeting.destroy');
    // Route::post('/positioninmeeting/datatables', 'PositionInMeetingController@datatables')->name('pages.positioninmeeting.datatables');

    // SystemController
    Route::get('/system', 'SystemController@index')->name('pages.system.index');
    Route::patch('/system', 'SystemController@update')->name('pages.system.update');

    // // OrganizationsController
    // Route::get('/organizations', 'OrganizationsController@index')->name('pages.organizations.index');
    // Route::get('/organizations/create', 'OrganizationsController@create')->name('pages.organizations.create');
    // Route::post('/organizations', 'OrganizationsController@store')->name('pages.organizations.store');
    // Route::get('/organizations/{id}', 'OrganizationsController@show')->name('pages.organizations.show');
    // Route::get('/organizations/{id}/edit', 'OrganizationsController@edit')->name('pages.organizations.edit');
    // Route::patch('/organizations/{id}', 'OrganizationsController@update')->name('pages.organizations.update');
    // Route::delete('/organizations/{id}', 'OrganizationsController@destroy')->name('pages.organizations.destroy');
    // Route::post('/organizations/datatables', 'OrganizationsController@datatables')->name('pages.organizations.datatables');

    // TitlesController
    Route::get('/titles', 'TitlesController@index')->name('pages.titles.index');
    Route::get('/titles/create', 'TitlesController@create')->name('pages.titles.create');
    Route::post('/titles', 'TitlesController@store')->name('pages.titles.store');
    Route::get('/titles/{id}', 'TitlesController@show')->name('pages.titles.show');
    Route::get('/titles/{id}/edit', 'TitlesController@edit')->name('pages.titles.edit');
    Route::patch('/titles/{id}', 'TitlesController@update')->name('pages.titles.update');
    Route::delete('/titles/{id}', 'TitlesController@destroy')->name('pages.titles.destroy');
    Route::post('/titles/datatables', 'TitlesController@datatables')->name('pages.titles.datatables');

    // MenusController
    Route::get('/menus', 'MenusController@index')->name('pages.menus.index');
    Route::get('/menus/create', 'MenusController@create')->name('pages.menus.create');
    Route::post('/menus', 'MenusController@store')->name('pages.menus.store');
    Route::get('/menus/{id}', 'MenusController@show')->name('pages.menus.show');
    Route::get('/menus/{id}/edit', 'MenusController@edit')->name('pages.menus.edit');
    Route::patch('/menus/{id}', 'MenusController@update')->name('pages.menus.update');
    Route::delete('/menus/{id}', 'MenusController@destroy')->name('pages.menus.destroy');
    Route::post('/menus/datatables', 'MenusController@datatables')->name('pages.menus.datatables');

    // // UserTypeController
    // Route::get('/usertype', 'UserTypeController@index')->name('pages.usertype.index');
    // Route::get('/usertype/create', 'UserTypeController@create')->name('pages.usertype.create');
    // Route::post('/usertype', 'UserTypeController@store')->name('pages.usertype.store');
    // Route::get('/usertype/{id}', 'UserTypeController@show')->name('pages.usertype.show');
    // Route::get('/usertype/{id}/edit', 'UserTypeController@edit')->name('pages.usertype.edit');
    // Route::patch('/usertype/{id}', 'UserTypeController@update')->name('pages.usertype.update');
    // Route::delete('/usertype/{id}', 'UserTypeController@destroy')->name('pages.usertype.destroy');
    // Route::post('/usertype/datatables', 'UserTypeController@datatables')->name('pages.usertype.datatables');

    // UserController
    Route::get('/user', 'UserController@index')->name('pages.user.index');
    Route::get('/user/create', 'UserController@create')->name('pages.user.create');
    Route::post('/user', 'UserController@store')->name('pages.user.store');
    Route::get('/user/{id}', 'UserController@show')->name('pages.user.show');
    Route::get('/user/{id}/edit', 'UserController@edit')->name('pages.user.edit');
    Route::patch('/user/{id}', 'UserController@update')->name('pages.user.update');
    Route::delete('/user/{id}', 'UserController@destroy')->name('pages.user.destroy');
    Route::post('/user/datatables', 'UserController@datatables')->name('pages.user.datatables');

    // AttendancesController
    Route::get('/attendances', 'AttendancesController@index')->name('pages.attendances.index');
    Route::get('/attendances/create', 'AttendancesController@create')->name('pages.attendances.create');
    Route::post('/attendances', 'AttendancesController@store')->name('pages.attendances.store');
    Route::get('/attendances/{id}', 'AttendancesController@show')->name('pages.attendances.show');
    Route::get('/attendances/{id}/edit', 'AttendancesController@edit')->name('pages.attendances.edit');
    Route::patch('/attendances/{id}', 'AttendancesController@update')->name('pages.attendances.update');
    Route::delete('/attendances/{id}', 'AttendancesController@destroy')->name('pages.attendances.destroy');
    Route::post('/attendances/datatables', 'AttendancesController@datatables')->name('pages.attendances.datatables');

    // BoardsController
    Route::get('/boards', 'BoardsController@index')->name('pages.boards.index');
    Route::get('/boards/create', 'BoardsController@create')->name('pages.boards.create');
    Route::post('/boards', 'BoardsController@store')->name('pages.boards.store');
    Route::get('/boards/{id}', 'BoardsController@show')->name('pages.boards.show');
    Route::get('/boards/{id}/edit', 'BoardsController@edit')->name('pages.boards.edit');
    Route::patch('/boards/{id}', 'BoardsController@update')->name('pages.boards.update');
    Route::delete('/boards/{id}', 'BoardsController@destroy')->name('pages.boards.destroy');
    Route::post('/boards/datatables', 'BoardsController@datatables')->name('pages.boards.datatables');
    Route::post('/boards/datatables/select', 'BoardsController@datatablesselect')->name('pages.boards.datatablesselect');

    // TemplateController
    Route::get('/template', 'TemplateController@index')->name('pages.template.index');
    Route::get('/template/create', 'TemplateController@create')->name('pages.template.create');
    Route::post('/template', 'TemplateController@store')->name('pages.template.store');
    Route::get('/template/{id}', 'TemplateController@show')->name('pages.template.show');
    Route::get('/template/{id}/edit', 'TemplateController@edit')->name('pages.template.edit');
    Route::patch('/template/{id}', 'TemplateController@update')->name('pages.template.update');
    Route::delete('/template/{id}', 'TemplateController@destroy')->name('pages.template.destroy');
    Route::post('/template/datatables', 'TemplateController@datatables')->name('pages.template.datatables');
    Route::post('/template/datatables/select', 'TemplateController@datatablesselect')->name('pages.template.datatablesselect');

    // ProposeTopicsController
    Route::get('/proposetopics', 'ProposeTopicsController@index')->name('pages.proposetopics.index');
    Route::get('/proposetopics/create', 'ProposeTopicsController@create')->name('pages.proposetopics.create');
    Route::post('/proposetopics', 'ProposeTopicsController@store')->name('pages.proposetopics.store');
    Route::get('/proposetopics/{id}', 'ProposeTopicsController@show')->name('pages.proposetopics.show');
    Route::get('/proposetopics/{id}/edit', 'ProposeTopicsController@edit')->name('pages.proposetopics.edit');
    Route::get('/proposetopics/{id}/propose', 'ProposeTopicsController@propose')->name('pages.proposetopics.propose');
    Route::patch('/proposetopics/{id}', 'ProposeTopicsController@update')->name('pages.proposetopics.update');
    Route::patch('/proposetopics/{id}/send', 'ProposeTopicsController@send')->name('pages.proposetopics.send');
    Route::delete('/proposetopics/{id}', 'ProposeTopicsController@destroy')->name('pages.proposetopics.destroy');
    Route::post('/proposetopics/datatables', 'ProposeTopicsController@datatables')->name('pages.proposetopics.datatables');
    Route::post('/proposetopics/meetingdatatables', 'ProposeTopicsController@meetingdatatables')->name('pages.proposetopics.meetingdatatables');
    Route::post('/proposetopics/datatables/select', 'ProposeTopicsController@datatablesselect')->name('pages.proposetopics.datatablesselect');

    // MeetingStatisticsReportController
    Route::get('/meetingstatisticsreport', 'MeetingStatisticsReportController@index')->name('pages.meetingstatisticsreport.index');
    Route::get('/meetingstatisticsreport/create', 'MeetingStatisticsReportController@create')->name('pages.meetingstatisticsreport.create');
    Route::post('/meetingstatisticsreport', 'MeetingStatisticsReportController@store')->name('pages.meetingstatisticsreport.store');
    Route::get('/meetingstatisticsreport/{id}', 'MeetingStatisticsReportController@show')->name('pages.meetingstatisticsreport.show');
    Route::get('/meetingstatisticsreport/{id}/edit', 'MeetingStatisticsReportController@edit')->name('pages.meetingstatisticsreport.edit');
    Route::patch('/meetingstatisticsreport/{id}', 'MeetingStatisticsReportController@update')->name('pages.meetingstatisticsreport.update');
    Route::delete('/meetingstatisticsreport/{id}', 'MeetingStatisticsReportController@destroy')->name('pages.meetingstatisticsreport.destroy');
    Route::post('/meetingstatisticsreport/datatables', 'MeetingStatisticsReportController@datatables')->name('pages.meetingstatisticsreport.datatables');
    Route::get('/export', 'MeetingStatisticsReportController@export')->name('pages.export');

    // ALL API
    Route::post('/titles/selects', 'TitlesController@selects')->name('pages.titles.selects');
    Route::post('/statuses/selects', 'StatusController@selects')->name('pages.statuses.selects');
    Route::post('/meetingpermissions/selects', 'MeetingPermissionsController@selects')->name('pages.meetingpermissions.selects');
    Route::post('/organizations/selects', 'OrganizationsController@selects')->name('pages.organizations.selects');
    Route::post('/organizations/selectsapi', 'OrganizationsController@selectsapi')->name('pages.organizations.selectsapi');
    Route::post('/positioninmeeting/selects', 'PositionInMeetingController@selects')->name('pages.positioninmeeting.selects');
    Route::post('/user/selectsapi', 'UserController@selectsapi')->name('pages.user.selectsapi');
    Route::post('/attendances/selectsapi', 'AttendancesController@selectsapi')->name('pages.attendances.selectsapi');
    Route::post('/meeting/selectsapi', 'MeetingController@selectsapi')->name('pages.meeting.selectsapi');
});

Route::get('/meeting/{id}/title/{title}/voter', 'MeetingController@voter')->name('pages.meeting.voter');
Route::put('/meeting/{id}', 'MeetingController@sendvote')->name('pages.meeting.sendvote');

Route::get('/meeting/{id}/title/{title}/qrcode', function ($id, $title) 
{   
     $id = Crypt::decryptString($id);
    // dd(route('pages.meeting.voter', ['id' => Crypt::encryptString($id), 'title' => $title]));
     return  QRCode::url(route('pages.meeting.voter', ['id' => Crypt::encryptString($id), 'title' => $title]))
                 ->setSize(8)
                 ->setMargin(1)
                 ->png();
})->name('pages.meeting.qrcode'); 

Route::get('/meeting/voter/success', function () {
    return view('layouts.partials.meeting.card-voter-success');
})->name('pages.meeting.voter.success');

Route::post('/update-voter-real-time/{id}', function (Request $request, $id) {

    if(Crypt::decryptString($id)) {
        
        $meeting_issues = MeetingIssues::find(Crypt::decryptString($id));
        $meeting_issues->target = $request->input('target', '');
   
        event(new \TechEx\Events\VoterEvent($meeting_issues));

        return response()->json([
            'status' => true,
            'message' => 'Successful'
        ]);

    } else {
        return response()->json([
            'status' => false,
            'message' => 'Unsuccessful'
        ]);
    }

})->name('pages.meeting.voter.update');

Route::post('/clear-voter-real-time', 'MeetingController@clearvote')->name('pages.meeting.voter.clear');
Route::post('/meeting/beautifulformat', 'MeetingController@beautifulformat')->name('pages.meeting.beautifulformat');
Route::post('/template/call/json', 'TemplateController@calljson')->name('pages.template.calljson');






